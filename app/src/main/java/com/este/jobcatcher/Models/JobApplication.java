package com.este.jobcatcher.models;

import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Job Application Model Class
 */
public class JobApplication {
    private String jobTitle;
    private String companyName;
    private Boolean locationRemote;
    private String locationName;
    private Double locationLat;
    private Double locationLong;
    private Boolean locationSet;
    private String currentApplicationStage;
    private String currentApplicationStageStatus;
    private Long currentApplicationStageDateStarted;
    private Long currentApplicationStageDateCompleted;
    private String currentApplicationStageNotes;
    private Boolean currentApplicationStageCompleted;
    private Long deadlineDate;
    private Boolean reminderSet;
    private Integer reminderId;
    private Long reminderDateTime;
    private String wageCurrency;
    private Double wageAmount;
    private String wageFrequency;
    private String jobType;
    private Double jobHours;
    private String jobReference;
    private String jobDescription;
    private String jobAdvertURL;
    private String applicationNotes;
    private String contactTitle;
    private String contactFirstName;
    private String contactSurname;
    private String contactPosition;
    private String contactEmail;
    private String contactPhone;
    private Integer applicationPriority;
    private String associatedJobsiteAgencyID;

    private String applicationStatus;
    private Long dateApplied;
    private Long dateCompleted;

    private HashMap<String, Object> timestampCreated;
    private HashMap<String, Object> timestampLastChanged;
    private HashMap<String, ApplicationStage> applicationStageHistory;

    private Long emailFollowupTimeSent;
    private Long emailFollowupTimeCheck;
    private Boolean emailFollowupSent;

    /**
     * Required public constructor
     */
    public JobApplication() {
    }

    /**
     * Use this constructor to create new Job Applications.
     * Takes all Job Application details and sets the last
     * changed time to what is stored in ServerValue.TIMESTAMP
     *
     * @param   jobTitle - Job Title
     * @param   companyName - Company Name
     * @param   dateApplied - Date Applied - Set when user moves to CV/Application Form stage
     * @param   dateCompleted - Date Completed
     * @param   deadlineDate - Deadline Date
     * @param   currentApplicationStage - Current Application Stage
     * @param   currentApplicationStageStatus - Current Application Stage Status - In Progress / Successful / Unsuccessful / Withdrawn
     * @param   currentApplicationStageDateStarted - Current Application Stage date started
     * @param   currentApplicationStageDateCompleted - Current Application Stage date completed
     * @param   currentApplicationStageCompleted - If current application stage has been completed.
     *
     * @param   currentApplicationStageNotes - Current Application Stage Notes
     * @param   applicationStatus - Whether Application Stage Prospective, In Progress, Successful, Unsuccessful or Withdrawn
     *
     * @param   reminderSet - Check if Reminder Set (Boolean)
     * @param   reminderDateTime - Long Reminder time date (Long)
     *
     * @param   wageCurrency - Wage Currency
     * @param   wageAmount - Wage Amount
     * @param   wageFrequency - Wage Frequency
     *
     * @param   locationRemote - Whether location is remote or not
     * @param   locationName - Location Name
     * @param   locationLat - Location Latitude
     * @param   locationLong - Location Longitude
     * @param   locationSet - Whether or not location has been set (used in Map Activity to list locations)
     *
     * @param   jobReference - Job Reference Number or ID
     * @param   jobDescription - Job Description
     * @param   jobAdvertURL - Job Advert URL
     * @param   jobType - Job Type e.g. Part Time, Full Time etc.
     * @param   jobHours - Job Hours
     * @param   applicationNotes - Application Notes
     * @param   applicationPriority - Application Priority (Not Important > Important)

     * @param   associatedJobsiteAgencyID - ID of Associated Jsa (String)
     * @param   contactTitle - Contact Title (e.g. Mr, Mrs)
     * @param   contactFirstName Contact First Name
     * @param   contactSurname Contact Surname
     * @param   contactPosition Contact Position
     * @param   contactEmail Contact Email
     * @param   contactPhone Contact Phone
     *
     *
     * @param   emailFollowupTimeSent Time Follow up Email sent
     * @param   emailFollowupTimeCheck Time user has chosen reminder card to be reshown
     * @param   emailFollowupSent Whether Follow up email sent
     */
    public JobApplication(String jobTitle, String companyName, Long dateApplied, Long dateCompleted, Long deadlineDate, String currentApplicationStage, String currentApplicationStageStatus, Long currentApplicationStageDateStarted, Long currentApplicationStageDateCompleted, String currentApplicationStageNotes, Boolean currentApplicationStageCompleted, HashMap<String, ApplicationStage> applicationStageHistory, String applicationStatus, Boolean reminderSet, Integer reminderId, Long reminderDateTime, String wageCurrency, Double wageAmount, String wageFrequency, Boolean locationRemote, String locationName, Double locationLat, Double locationLong, Boolean locationSet, String jobReference, String jobDescription, String jobAdvertURL, String jobType, Double jobHours, String applicationNotes, Integer applicationPriority,
                          String associatedJobsiteAgencyID, String contactTitle, String contactFirstName, String contactSurname, String contactPosition, String contactEmail, String contactPhone, Long emailFollowupTimeSent, Long emailFollowupTimeCheck, Boolean emailFollowupSent, HashMap<String, Object> timestampCreated) {

        this.jobTitle = jobTitle;
        this.companyName = companyName;
        this.dateApplied = dateApplied;
        this.dateCompleted = dateCompleted;
        this.deadlineDate = deadlineDate;
        this.currentApplicationStage = currentApplicationStage;
        this.currentApplicationStageStatus = currentApplicationStageStatus;
        this.currentApplicationStageDateStarted = currentApplicationStageDateStarted;
        this.currentApplicationStageDateCompleted = currentApplicationStageDateCompleted;
        this.currentApplicationStageNotes = currentApplicationStageNotes;
        this.currentApplicationStageCompleted = currentApplicationStageCompleted;
        this.applicationStageHistory = applicationStageHistory;
        this.applicationStatus = applicationStatus;

        this.reminderSet = reminderSet;
        this.reminderId = reminderId;
        this.reminderDateTime = reminderDateTime;

        this.wageCurrency = wageCurrency;
        this.wageAmount = wageAmount;
        this.wageFrequency = wageFrequency;

        this.locationRemote = locationRemote;
        this.locationName = locationName;
        this.locationLat = locationLat;
        this.locationLong = locationLong;
        this.locationSet = locationSet;

        this.jobReference = jobReference;
        this.jobDescription = jobDescription;
        this.jobAdvertURL = jobAdvertURL;
        this.jobType = jobType;
        this.jobHours = jobHours;
        this.applicationNotes = applicationNotes;
        this.applicationPriority = applicationPriority;

        this.associatedJobsiteAgencyID = associatedJobsiteAgencyID;
        this.contactTitle = contactTitle;
        this.contactFirstName = contactFirstName;
        this.contactSurname = contactSurname;
        this.contactPosition = contactPosition;
        this.contactEmail = contactEmail;
        this.contactPhone = contactPhone;

        this.emailFollowupTimeSent = emailFollowupTimeSent;
        this.emailFollowupTimeCheck = emailFollowupTimeCheck;
        this.emailFollowupSent = emailFollowupSent;

        this.timestampCreated = timestampCreated;

        //Date last changed will always be set to ServerValue.TIMESTAMP
        HashMap<String, Object> dateLastChangedObj = new HashMap<>();
        dateLastChangedObj.put("date", ServerValue.TIMESTAMP);
        this.timestampLastChanged = dateLastChangedObj;

    }

    public String getJobTitle() {
        return jobTitle;
    }

    public HashMap<String, ApplicationStage> getApplicationStageHistory() {
        return applicationStageHistory;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Long getDateApplied() {
        return dateApplied;
    }

    public Long getDateCompleted() {
        return dateCompleted;
    }

    public Long getDeadlineDate() {
        return deadlineDate;
    }

    public String getCurrentApplicationStage() {
        return currentApplicationStage;
    }

    public String getCurrentApplicationStageStatus() {
        return currentApplicationStageStatus;
    }

    public Long getCurrentApplicationStageDateStarted() {
        return currentApplicationStageDateStarted;
    }

    public Long getCurrentApplicationStageDateCompleted() {
        return currentApplicationStageDateCompleted;
    }

    public String getCurrentApplicationStageNotes() {
        return currentApplicationStageNotes;
    }

    public Boolean getCurrentApplicationStageCompleted() { return currentApplicationStageCompleted; }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public Boolean getReminderSet() { return reminderSet; }

    public Integer getReminderId() { return reminderId; }

    public Long getReminderDateTime() {
        return reminderDateTime;
    }

    public String getWageCurrency() {
        return wageCurrency;
    }

    public Double getWageAmount() {
        return wageAmount;
    }

    public String getWageFrequency() {
        return wageFrequency;
    }

    public Boolean getLocationRemote() {
        return locationRemote;
    }

    public String getLocationName() {
        return locationName;
    }

    public Double getLocationLat() {
        return locationLat;
    }

    public Double getLocationLong() {
        return locationLong;
    }

    public Boolean getLocationSet() {
        return locationSet;
    }

    public String getJobReference() {
        return jobReference;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public String getJobAdvertURL() {
        return jobAdvertURL;
    }

    public String getJobType() {
        return jobType;
    }

    public Double getJobHours() {
        return jobHours;
    }

    public String getApplicationNotes() {
        return applicationNotes;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public String getAssociatedJobsiteAgencyID() {
        return associatedJobsiteAgencyID;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public String getContactSurname() {
        return contactSurname;
    }

    public String getContactPosition() {
        return contactPosition;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public Long getEmailFollowupTimeSent() {
        return emailFollowupTimeSent;
    }

    public Long getEmailFollowupTimeCheck() {
        return emailFollowupTimeCheck;
    }

    public Boolean getEmailFollowupSent() {
        return emailFollowupSent;
    }

    public HashMap<String, Object> getTimestampLastChanged() {
        return timestampLastChanged;
    }

    public HashMap<String, Object> getTimestampCreated() {
        //If there is a dateCreated object already, then return that
        if (timestampCreated != null) {
            return timestampCreated;
        } else {
            //Otherwise make a new object set to ServerValue.TIMESTAMP
            HashMap<String, Object> timestampCreatedObj = new HashMap<>();
            timestampCreatedObj.put("date", ServerValue.TIMESTAMP);
            return timestampCreatedObj;
        }

    }

    @Exclude
    public long getTimestampLastChangedLong() {
        timestampLastChanged = getTimestampLastChanged();
        return (long) timestampLastChanged.get(Constants.FIREBASE_PROPERTY_TIMESTAMP);
    }

    @Exclude
    public long getTimestampCreatedLong() {

        long createdTimestamp = 0L;

        if (timestampCreated != null) {
            createdTimestamp = (long) timestampCreated.get(Constants.FIREBASE_PROPERTY_TIMESTAMP);
        }

        return createdTimestamp;
    }

}
