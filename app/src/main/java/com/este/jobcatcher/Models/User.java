package com.este.jobcatcher.models;

import java.util.HashMap;

/**
 * This model is used to store all relevant user data.
 */
public class User {
    private String firstName;
    private String surname;
    private String email;
    private HashMap<String, Object> timestampJoined;
    private Double timestampFirstOnline;
    private Double timestampLastOnline;
    private HashMap<String, Object> sessionTimeOb;
    private Double totalSessionTime;
    private Double userLocationLat;
    private Double userLocationLong;
    private String userLocationName;
    private String userDistanceUnit;
    private String userCurrency;

    /**
     * Required Public Constructor
     */
    public User() {
    }

    /**
     * Use this constructor to create new User.
     * Takes user name, email and timestampJoined as params
     *
     * @param firstName - User First Name
     * @param surname - User Surname
     * @param email - User Email Address
     * @param timestampJoined - Timestamp user joined (Long)
     * @param timestampFirstOnline - Timestamp user first online for current session (Long)
     * @param timestampLastOnline - Timestamp user last online for current session (Long)
     * @param totalSessionTime  - Total time user has sent in app (Long)
     * @param sessionTimeOb - List of User Session Times
     * @param userLocationLat - User Location Latitude
     * @param userLocationLong - User Location Longitude
     * @param userLocationName - User Location Name
     * @param userDistanceUnit - User Distance Unit (e.g. Miles, Kilometers)
     * @param userCurrency - User Currency
     */

    public User(String firstName, String surname, String email, HashMap<String, Object> timestampJoined, Double timestampFirstOnline, Double timestampLastOnline, Double totalSessionTime, HashMap<String, Object> sessionTimeOb, Double userLocationLat, Double userLocationLong, String userLocationName, String userDistanceUnit, String userCurrency) {
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.timestampJoined = timestampJoined;
        this.timestampFirstOnline = timestampFirstOnline;
        this.timestampLastOnline = timestampLastOnline;
        this.totalSessionTime = totalSessionTime;
        this.sessionTimeOb = sessionTimeOb;
        this.userLocationLat = userLocationLat;
        this.userLocationLong = userLocationLong;
        this.userLocationName = userLocationName;
        this.userDistanceUnit = userDistanceUnit;
        this.userCurrency = userCurrency;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public HashMap<String, Object> getTimestampJoined() {
        return timestampJoined;
    }

    public Double getTimestampFirstOnline() {
        return timestampFirstOnline;
    }

    public Double getTimestampLastOnline() {
        return timestampLastOnline;
    }

    public Double getTotalSessionTime() {
        return totalSessionTime;
    }

    public HashMap<String, Object> getSessionTimeOb() {
        return sessionTimeOb;
    }

    public Double getUserLocationLat() {
        return userLocationLat;
    }

    public Double getUserLocationLong() {
        return userLocationLong;
    }

    public String getUserLocationName() {
        return userLocationName;
    }

    public String getUserDistanceUnit() {
        return userDistanceUnit;
    }

    public String getUserCurrency() {
        return userCurrency;
    }

}
