package com.este.jobcatcher.alarms;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.R;
import com.este.jobcatcher.models.JobApplication;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

/**
 * Receives Alarm and create Notification
 */
public class AlarmAlertBroadcastReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = AlarmAlertBroadcastReceiver.class.getSimpleName();
    private Context ct;
    private String mJobApplicationId;
    private String msg_text_job_title;
    private String msg_text_company;
    private Boolean priority;
    private DatabaseReference mFirebaseRef;
    private DatabaseReference usingRef;

    @Override
    public void onReceive(Context context, Intent intent) {

        ct = context;

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ct);
        String userUID = sp.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        Log.d(LOG_TAG, "userUID: " + userUID);

        mJobApplicationId = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        priority = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);

        mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);

        usingRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(mJobApplicationId);

        Log.d(LOG_TAG, "mJobApplicationId: " + mJobApplicationId);
        Log.d(LOG_TAG, "priority: " + priority);
        Log.d(LOG_TAG, "usingRef: " + usingRef);

        if (mJobApplicationId != null) {

            //From Alarm
            usingRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    JobApplication jobApplication = dataSnapshot.getValue(JobApplication.class);

                    if (jobApplication == null) {

                        usingRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(mJobApplicationId);

                        usingRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                JobApplication jobApplication = dataSnapshot.getValue(JobApplication.class);

                                msg_text_job_title = jobApplication.getJobTitle();

                                msg_text_company = jobApplication.getCompanyName();

                                if (!msg_text_company.equals("")) {
                                    msg_text_company = " at " + msg_text_company;
                                }

                                priority = true;

                                createNotification();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        return;
                    }

                    msg_text_job_title = jobApplication.getJobTitle();

                    msg_text_company = jobApplication.getCompanyName();

                    if (!msg_text_company.equals("")) {
                        msg_text_company = " at " + msg_text_company;
                    }

                    priority = false;

                    createNotification();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    private void createNotification() {

        // Prepare intent which is triggered if the notification is selected
        Intent intent = new Intent(ct, JobApplicationDetailActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mJobApplicationId);
        intent.putExtra(Constants.INTENT_EXTRA_FROM_REMINDER, true);
        intent.putExtra(Constants.INTENT_EXTRA_PINNED, priority);
        PendingIntent pIntent = PendingIntent.getActivity(ct, (int) System.currentTimeMillis(), intent, 0);

        Bitmap icon = BitmapFactory.decodeResource(ct.getResources(), R.mipmap.ic_launcher);

        // Build notification
        NotificationCompat.Builder mNotification = new NotificationCompat.Builder(ct)
                .setContentTitle("There's a Job to follow up!")
                .setContentText(msg_text_job_title + msg_text_company)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(icon)
                .setColor(ContextCompat.getColor(ct, R.color.colorPrimary))
                .extend(new NotificationCompat.WearableExtender().setBackground(icon))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pIntent);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        // Sets an ID for the notification (Setting this as a different number creates separate notifications) If you want to have one single notification change mNotificationID to the sam number e.g. 001
        int mNotificationId = (int) calendar.getTimeInMillis();

        NotificationManager mNotifyMgr = (NotificationManager) ct.getSystemService(Context.NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mNotification.build());

    }

}
