package com.este.jobcatcher.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.este.jobcatcher.utils.Constants;

import java.util.Calendar;

/**
 * Creates and deletes system Alarms
 */
public class AlarmService extends Service {

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Long longReminderDateTime;
        Integer reminderId;
        String msg_text_id;
        Boolean deleteReminder;
        Boolean priority;

        longReminderDateTime = intent.getLongExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, 0);
        reminderId = intent.getIntExtra(Constants.INTENT_EXTRA_REMINDER_ID, 0);
        msg_text_id = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        priority = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);
        deleteReminder = intent.getBooleanExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, false);

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(longReminderDateTime);

        if (deleteReminder) {

            Intent myIntent = new Intent(AlarmService.this, AlarmAlertBroadcastReceiver.class);
            myIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, msg_text_id);
            myIntent.putExtra(Constants.INTENT_EXTRA_PINNED, priority);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(AlarmService.this, reminderId, myIntent,PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager alarmManager = (AlarmManager) AlarmService.this.getSystemService(Context.ALARM_SERVICE);

            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();

        } else {
            Intent myIntent = new Intent(AlarmService.this, AlarmAlertBroadcastReceiver.class);
            myIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, msg_text_id);
            myIntent.putExtra(Constants.INTENT_EXTRA_PINNED, priority);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(AlarmService.this, reminderId, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager alarmManager = (AlarmManager)AlarmService.this.getSystemService(Context.ALARM_SERVICE);

            alarmManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
        }

        return START_NOT_STICKY;
    }

}
