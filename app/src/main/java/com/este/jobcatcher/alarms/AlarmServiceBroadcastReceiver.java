package com.este.jobcatcher.alarms;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.models.JobApplication;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

/**
 * Gets Alarm details and passes to Alarm Service
 */

public class AlarmServiceBroadcastReceiver extends BroadcastReceiver {

    private Context ct;
    private String msg_text_id;
    private Long longReminderDateTime;
    private Integer reminderId;

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {

        ct = context;

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ct);
        String userUID = sp.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        DatabaseReference mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);

        longReminderDateTime = intent.getLongExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, 0);
        reminderId = intent.getIntExtra(Constants.INTENT_EXTRA_REMINDER_ID, 0);
        msg_text_id = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        Boolean priority = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);
        Boolean deleteReminder = intent.getBooleanExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, false);

        if(msg_text_id==null) {
            //If receiver started after boot

            DatabaseReference activeListRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
            DatabaseReference listPriorityRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);

            activeListRef.orderByChild(Constants.FIREBASE_SORT_REMINDER_SET).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        JobApplication jobApplication = child.getValue(JobApplication.class);

                        //This code stops all old/past reminders being set on start up!
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(System.currentTimeMillis());
                        long currentTimeDate = calendar.getTimeInMillis();
                        long reminderTimeDate = jobApplication.getReminderDateTime();

                        if (reminderTimeDate - currentTimeDate > 0) {

                            if (jobApplication.getReminderSet()) {

                                msg_text_id = child.getKey();
                                reminderId = jobApplication.getReminderId();
                                longReminderDateTime = jobApplication.getReminderDateTime();

                                Intent serviceIntent = new Intent(ct, AlarmService.class);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, longReminderDateTime);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, msg_text_id);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_PINNED, false);
                                ct.startService(serviceIntent);

                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            listPriorityRef.orderByChild(Constants.FIREBASE_SORT_REMINDER_SET).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        JobApplication jobApplication = child.getValue(JobApplication.class);

                        //This code stops all old/past reminders being set on start up!
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(System.currentTimeMillis());
                        long currentTimeDate = calendar.getTimeInMillis();
                        long reminderTimeDate = jobApplication.getReminderDateTime();

                        if (reminderTimeDate - currentTimeDate > 0) {

                            if (jobApplication.getReminderSet()) {

                                msg_text_id = child.getKey();
                                reminderId = jobApplication.getReminderId();
                                longReminderDateTime = jobApplication.getReminderDateTime();

                                Intent serviceIntent = new Intent(ct, AlarmService.class);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, longReminderDateTime);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, msg_text_id);
                                serviceIntent.putExtra(Constants.INTENT_EXTRA_PINNED, true);
                                ct.startService(serviceIntent);

                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            Intent serviceIntent = new Intent(context, AlarmService.class);
            serviceIntent.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, longReminderDateTime);
            serviceIntent.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
            serviceIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, msg_text_id);
            serviceIntent.putExtra(Constants.INTENT_EXTRA_PINNED, priority);
            serviceIntent.putExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, deleteReminder);
            context.startService(serviceIntent);
        }
    }

}