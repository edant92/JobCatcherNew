package com.este.jobcatcher.compare;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jsas.recyclerviewholders.CompareListItemHolder;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.Utils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

/**
 * Activity to view and modify Job Application Details
 */
public class CompareFragment extends Fragment {

    private Utils utils;

    //General Variables
    private static final String LOG_TAG = CompareFragment.class.getSimpleName();

    private LatLng mUserLocationLatLng;
    private String mUserDistanceUnit;

    private DatabaseReference mMainListRef;
    private DatabaseReference mMainListPriorityRef;
    private DatabaseReference mCompareRef;

    private TextView mTextViewJobTitle1;
    private TextView mTextViewCompany1;
    private TextView mTextViewLocationName1;
    private TextView mTextViewLocationDistance1;
    private TextView mTextViewLocationClosest1;
    private TextView mTextViewJobType1;
    private TextView mTextViewJobTypeHours1;

    private TextView mTextViewJobTitle2;
    private TextView mTextViewCompany2;
    private TextView mTextViewLocationName2;
    private TextView mTextViewLocationDistance2;
    private TextView mTextViewLocationClosest2;
    private TextView mTextViewJobType2;
    private TextView mTextViewJobTypeHours2;

    private long jobApplicationsCount;

    private Dialog mDialogDeadlinesMissed;
    private FirebaseRecyclerAdapter<JobApplication, CompareListItemHolder> mRecyclerViewAdapterDeadlinesMissed;
    private FirebaseRecyclerAdapter<JobApplication, CompareListItemHolder> mRecyclerViewAdapterDeadlinesMissedPinned;

    private int compareItem;
    private Boolean compare1Added = false;
    private Boolean compare2Added = false;
    private LatLng compare1LocationLatLng;
    private LatLng compare2LocationLatLng;
    private String compare1Location;
    private String compare2Location;
    private Boolean compare1LocationRemote;
    private Boolean compare2LocationRemote;
    private String compare1WageCurrency;
    private String compare2WageCurrency;

    private TextView mTextViewWagesAmount1Hourly;
    private TextView mTextViewWagesAmount1Daily;
    private TextView mTextViewWagesAmount1Weekly;
    private TextView mTextViewWagesAmount1BiWeekly;
    private TextView mTextViewWagesAmount1Monthly;
    private TextView mTextViewWagesAmount1Annually;
    private TextView mTextViewWagesAmount1LumpSumOther;
    private TextView mTextViewWagesAmount2Hourly;
    private TextView mTextViewWagesAmount2Daily;
    private TextView mTextViewWagesAmount2Weekly;
    private TextView mTextViewWagesAmount2BiWeekly;
    private TextView mTextViewWagesAmount2Monthly;
    private TextView mTextViewWagesAmount2Annually;
    private TextView mTextViewWagesAmount2LumpSumOther;

    private LinearLayout mLinearLayoutWages;
    private LinearLayout mLinearLayoutLocation;
    private LinearLayout mLinearLayoutJobType;

    private LinearLayout mLinearLayoutWagesAmountHourly;
    private LinearLayout mLinearLayoutWagesAmountDaily;
    private LinearLayout mLinearLayoutWagesAmountWeekly;
    private LinearLayout mLinearLayoutWagesAmountBiWeekly;
    private LinearLayout mLinearLayoutWagesAmountMonthly;
    private LinearLayout mLinearLayoutWagesAmountAnnually;
    private LinearLayout mLinearLayoutWagesAmountLumpSumOther;

    private final Integer DEFAULT_DAY_WORKING_HOURS = 8;
    private final Integer DEFAULT_WEEK_WORKING_DAYS = 5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.fragment_compare, container, false);

        utils = new Utils();

        final SharedPreferences mSharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        //Set location bounds from sharedPrefs
        Double mUserLocationLat = Double.parseDouble(mSharedPref.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, "0.0"));
        Double mUserLocationLng = Double.parseDouble(mSharedPref.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, "0.0"));
        mUserLocationLatLng = new LatLng(mUserLocationLat, mUserLocationLng);
        mUserDistanceUnit = mSharedPref.getString(Constants.SHARED_PREF_USER_DEFAULT_DISTANCE_UNIT, getResources().getStringArray(R.array.array_distance_unit)[0]);

        DatabaseReference mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);
        mMainListRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mMainListPriorityRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);

        mLinearLayoutWagesAmountHourly = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_hourly);
        mLinearLayoutWagesAmountDaily = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_daily);
        mLinearLayoutWagesAmountWeekly = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_weekly);
        mLinearLayoutWagesAmountBiWeekly = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_biweekly);
        mLinearLayoutWagesAmountMonthly = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_monthly);
        mLinearLayoutWagesAmountAnnually = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_annually);
        mLinearLayoutWagesAmountLumpSumOther = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages_lump_sum_other);

        mTextViewJobTitle1 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_job_title);
        mTextViewCompany1 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_company);
        mTextViewLocationName1 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_location_name);
        mTextViewLocationDistance1 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_location_distance);
        mTextViewLocationClosest1 = (TextView) convertView.findViewById(R.id.fragment_compare_ll_1_location_closest);
        mTextViewJobType1 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_job_type);
        mTextViewJobTypeHours1 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_job_type_hours);
        mTextViewWagesAmount1Hourly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_hourly);
        mTextViewWagesAmount1Daily = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_daily);
        mTextViewWagesAmount1Weekly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_weekly);
        mTextViewWagesAmount1BiWeekly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_biweekly);
        mTextViewWagesAmount1Monthly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_monthly);
        mTextViewWagesAmount1Annually = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_annually);
        mTextViewWagesAmount1LumpSumOther = (TextView) convertView.findViewById(R.id.fragment_compare_tv_1_wages_lump_sum_other);

        mTextViewJobTitle2 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_job_title);
        mTextViewCompany2 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_company);
        mTextViewLocationName2 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_location_name);
        mTextViewLocationDistance2 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_location_distance);
        mTextViewLocationClosest2 = (TextView) convertView.findViewById(R.id.fragment_compare_ll_2_location_closest);
        mTextViewJobType2 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_job_type);
        mTextViewJobTypeHours2 = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_job_type_hours);
        mTextViewWagesAmount2Hourly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_hourly);
        mTextViewWagesAmount2Daily = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_daily);
        mTextViewWagesAmount2Weekly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_weekly);
        mTextViewWagesAmount2BiWeekly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_biweekly);
        mTextViewWagesAmount2Monthly = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_monthly);
        mTextViewWagesAmount2Annually = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_annually);
        mTextViewWagesAmount2LumpSumOther = (TextView) convertView.findViewById(R.id.fragment_compare_tv_2_wages_lump_sum_other);

        mLinearLayoutWages = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_wages);
        mLinearLayoutLocation = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_location);
        mLinearLayoutJobType = (LinearLayout) convertView.findViewById(R.id.fragment_compare_ll_job_type);

        createCompareBroadcastReceiver();

        Button mButtonCompare1 = (Button) convertView.findViewById(R.id.fragment_compare_bt_1_add);
        Button mButtonCompare2 = (Button) convertView.findViewById(R.id.fragment_compare_bt_2_add);

        mButtonCompare1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compareItem = 1;
                createJobApplicationListDialog();
            }
        });

        mButtonCompare2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compareItem = 2;
                createJobApplicationListDialog();
            }
        });

        String filterWageFrequency = mSharedPref.getString(Constants.SHARED_PREF_COMPARE_FILTER_WAGE_FREQUENCY, getResources().getStringArray(R.array.array_wage_frequency_filter)[0]);

        final ArrayAdapter<CharSequence> adapterWageFrequency = ArrayAdapter.createFromResource(getContext(), R.array.array_wage_frequency_filter, R.layout.list_item_wage_frequency);

        final Spinner mSpinnerWageFrequency = (Spinner) convertView.findViewById(R.id.fragment_compare_sp_wage_frequency);
        mSpinnerWageFrequency.setAdapter(adapterWageFrequency);

        Utils mUtils = new Utils();
        mUtils.selectSpinnerValue(mSpinnerWageFrequency, filterWageFrequency);

        mSpinnerWageFrequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String wageFrequency = mSpinnerWageFrequency.getItemAtPosition(i).toString();
                setLayoutVisibilities(wageFrequency);
                Log.d(LOG_TAG, wageFrequency);
                mSharedPref.edit().putString(Constants.SHARED_PREF_COMPARE_FILTER_WAGE_FREQUENCY, wageFrequency).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return convertView;

    }

    private void setLayoutVisibilities(String selectedFrequencyFilter) {

        mLinearLayoutWagesAmountHourly.setVisibility(View.GONE);
        mLinearLayoutWagesAmountDaily.setVisibility(View.GONE);
        mLinearLayoutWagesAmountWeekly.setVisibility(View.GONE);
        mLinearLayoutWagesAmountBiWeekly.setVisibility(View.GONE);
        mLinearLayoutWagesAmountMonthly.setVisibility(View.GONE);
        mLinearLayoutWagesAmountAnnually.setVisibility(View.GONE);
        mLinearLayoutWagesAmountLumpSumOther.setVisibility(View.GONE);

        switch (selectedFrequencyFilter) {
            case Constants.ALL: //Show all calculated wages
                mLinearLayoutWagesAmountHourly.setVisibility(View.VISIBLE);
                mLinearLayoutWagesAmountDaily.setVisibility(View.VISIBLE);
                mLinearLayoutWagesAmountWeekly.setVisibility(View.VISIBLE);
                mLinearLayoutWagesAmountBiWeekly.setVisibility(View.VISIBLE);
                mLinearLayoutWagesAmountMonthly.setVisibility(View.VISIBLE);
                mLinearLayoutWagesAmountAnnually.setVisibility(View.VISIBLE);
                mLinearLayoutWagesAmountLumpSumOther.setVisibility(View.VISIBLE);
                break;
            case Constants.HOURLY: //Show only hourly calculated wages
                mLinearLayoutWagesAmountHourly.setVisibility(View.VISIBLE);
                break;
            case Constants.DAILY: //Show only daily calculated wages
                mLinearLayoutWagesAmountDaily.setVisibility(View.VISIBLE);
                break;
            case Constants.WEEKLY: //Show only weekly calculated wages
                mLinearLayoutWagesAmountWeekly.setVisibility(View.VISIBLE);
                break;
            case Constants.BIWEEKLY: //Show only bi-weekly calculated wages
                mLinearLayoutWagesAmountBiWeekly.setVisibility(View.VISIBLE);
                break;
            case Constants.MONTHLY: //Show only monthly calculated wages
                mLinearLayoutWagesAmountMonthly.setVisibility(View.VISIBLE);
                break;
            case Constants.ANNUALLY: //Show only annually calculated wages
                mLinearLayoutWagesAmountAnnually.setVisibility(View.VISIBLE);
                break;
            case Constants.LUMPSUMOTHER:
                mLinearLayoutWagesAmountLumpSumOther.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void createJobApplicationListDialog() {
        final AlertDialog.Builder listDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_recycler_view, null);
        listDialog.setView(convertView);
        listDialog.setTitle("Compare One");
        final ProgressBar mProgressBarAssociatedJobsiteAgency = (ProgressBar) convertView.findViewById(R.id.dialog_recycler_view_pb);
        final TextView mTextViewAssociatedJsa = (TextView) convertView.findViewById(R.id.dialog_recycler_view_tv);
        final RecyclerView mRecyclerViewAssociatedJobsiteAgencyID = (RecyclerView) convertView.findViewById(R.id.dialog_recycler_view_rv);
        final RecyclerView mRecyclerViewAssociatedJobsiteAgencyIDTwo = (RecyclerView) convertView.findViewById(R.id.dialog_recycler_view_rv_two);

        mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_JOB_TITLE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                jobApplicationsCount += dataSnapshot.getChildrenCount();

                if(jobApplicationsCount == 0) {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.VISIBLE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.GONE);
                } else {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.GONE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getContext());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewAssociatedJobsiteAgencyID.setHasFixedSize(false);
                    mRecyclerViewAssociatedJobsiteAgencyID.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterDeadlinesMissed = new FirebaseRecyclerAdapter<JobApplication, CompareListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, CompareListItemHolder.class, mMainListRef) {
                        @Override
                        public void populateViewHolder(CompareListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterDeadlinesMissed.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(false);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());

                        }
                    };

                    mRecyclerViewAssociatedJobsiteAgencyID.setAdapter(mRecyclerViewAdapterDeadlinesMissed);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_JOB_TITLE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                jobApplicationsCount += dataSnapshot.getChildrenCount();

                if(jobApplicationsCount == 0) {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.VISIBLE);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setVisibility(View.GONE);
                } else {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.GONE);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setVisibility(View.VISIBLE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getContext());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setHasFixedSize(false);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterDeadlinesMissedPinned = new FirebaseRecyclerAdapter<JobApplication, CompareListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, CompareListItemHolder.class, mMainListPriorityRef) {
                        @Override
                        public void populateViewHolder(CompareListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterDeadlinesMissedPinned.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(true);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());

                        }
                    };

                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setAdapter(mRecyclerViewAdapterDeadlinesMissedPinned);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDialogDeadlinesMissed = listDialog.show();

    }

    private void createCompareBroadcastReceiver() {
        BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String mJobApplicationId = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
                Boolean fromPriority = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);

                if (fromPriority) {
                    mCompareRef = mMainListPriorityRef.child(mJobApplicationId);
                } else {
                    mCompareRef = mMainListRef.child(mJobApplicationId);
                }

                addDataToCompare();

            }
        };

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter(Constants.INTENT_EXTRA_ASSOCIATED_JSA));

    }

    private void addDataToCompare() {

        mCompareRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Activity activity = getActivity();
                //Check to resolve Fragment not attached to Activity error
                if(activity != null && isAdded()) {
                    JobApplication jobApplication = dataSnapshot.getValue(JobApplication.class);

                    Log.d(LOG_TAG, "jobApplicationLatLng: " + jobApplication.getLocationLat() + ", " + jobApplication.getLocationLong());

                    if (compareItem == 1) {
                        compare1Added = true;
                        Double compare1LocationLat = jobApplication.getLocationLat();
                        Double compare1LocationLng = jobApplication.getLocationLong();
                        compare1LocationLatLng = new LatLng(compare1LocationLat, compare1LocationLng);
                        compare1Location = jobApplication.getLocationName();
                        compare1LocationRemote = jobApplication.getLocationRemote();
                        compare1WageCurrency = jobApplication.getWageCurrency();
                        mTextViewJobTitle1.setText(jobApplication.getJobTitle());
                        mTextViewCompany1.setText(jobApplication.getCompanyName());
                        if (compare1Location.equals("")) {
                            mTextViewLocationName1.setText(R.string.fragment_compare_tv_1_location_none);
                            mTextViewLocationDistance1.setText(getResources().getString(R.string.fragment_compare_tv_1_location_empty));
                        } else if(compare1LocationRemote) {
                            mTextViewLocationName1.setText(R.string.fragment_compare_tv_1_location_none);
                            mTextViewLocationDistance1.setText(getResources().getString(R.string.fragment_compare_tv_1_location_empty));
                        } else {
                            mTextViewLocationName1.setText(compare1Location);
                            mTextViewLocationDistance1.setText(getDistanceString(mUserLocationLatLng, new LatLng(compare1LocationLat, compare1LocationLng)));
                        }
                        mTextViewJobType1.setText(jobApplication.getJobType());
                        mTextViewJobTypeHours1.setText(getResources().getString(R.string.fragment_compare_tv_2_job_type_hours_hours, jobApplication.getJobHours().toString()));
                        wagesCalculations(jobApplication.getWageAmount(), jobApplication.getWageFrequency());
                    } else {
                        compare2Added = true;
                        Double compare2LocationLat = jobApplication.getLocationLat();
                        Double compare2LocationLng = jobApplication.getLocationLong();
                        compare2LocationLatLng = new LatLng(compare2LocationLat, compare2LocationLng);
                        compare2Location = jobApplication.getLocationName();
                        compare2LocationRemote = jobApplication.getLocationRemote();
                        compare2WageCurrency = jobApplication.getWageCurrency();
                        mTextViewJobTitle2.setText(jobApplication.getJobTitle());
                        mTextViewCompany2.setText(jobApplication.getCompanyName());
                        if (compare2Location.equals("")) {
                            mTextViewLocationName2.setText(R.string.fragment_compare_tv_2_location_none);
                            mTextViewLocationDistance2.setText(getResources().getString(R.string.fragment_compare_tv_2_location_empty));
                        } else if(compare2LocationRemote) {
                            mTextViewLocationName2.setText(R.string.fragment_compare_tv_2_location_none);
                            mTextViewLocationDistance2.setText(getResources().getString(R.string.fragment_compare_tv_2_location_empty));
                        } else {
                            mTextViewLocationName2.setText(compare2Location);
                            mTextViewLocationDistance2.setText(getDistanceString(mUserLocationLatLng, new LatLng(compare2LocationLat, compare2LocationLng)));
                        }
                        mTextViewJobType2.setText(jobApplication.getJobType());
                        mTextViewJobTypeHours2.setText(getResources().getString(R.string.fragment_compare_tv_2_job_type_hours_hours, jobApplication.getJobHours().toString()));
                        wagesCalculations(jobApplication.getWageAmount(), jobApplication.getWageFrequency());
                    }

                    compareActions();

                    if (mDialogDeadlinesMissed != null) {
                        mDialogDeadlinesMissed.dismiss();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void compareActions() {
        if (compare1Added && compare2Added) {

            mLinearLayoutLocation.setVisibility(View.VISIBLE);
            mLinearLayoutWages.setVisibility(View.VISIBLE);
            mLinearLayoutJobType.setVisibility(View.VISIBLE);

            float compare1distance = utils.getDistanceBetweenTwoPoints(mUserLocationLatLng, compare1LocationLatLng, mUserDistanceUnit);
            float compare2distance = utils.getDistanceBetweenTwoPoints(mUserLocationLatLng, compare2LocationLatLng, mUserDistanceUnit);

            if (compare1Location.equals("") || compare2Location.equals("") || compare1LocationRemote || compare2LocationRemote) {
                //If either application is remote or has no location set
                mTextViewLocationClosest1.setVisibility(View.GONE);
                mTextViewLocationClosest2.setVisibility(View.GONE);
            } else if (compare1distance < compare2distance) {
                mTextViewLocationClosest1.setVisibility(View.VISIBLE);
                mTextViewLocationClosest2.setVisibility(View.GONE);
                if (mUserDistanceUnit.equals(getResources().getStringArray(R.array.array_distance_unit)[0])) {
                    mTextViewLocationClosest1.setText(getResources().getString(R.string.fragment_compare_tv_distance_miles_closer, String.format(Locale.UK, "%.0f", compare2distance - compare1distance)));
                } else {
                    mTextViewLocationClosest1.setText(getResources().getString(R.string.fragment_compare_tv_distance_kilometres_closer, String.format(Locale.UK, "%.0f", compare2distance - compare1distance)));
                }
                Log.d(LOG_TAG, String.valueOf(compare2distance - compare1distance));
            } else if (compare2distance < compare1distance) {
                mTextViewLocationClosest1.setVisibility(View.GONE);
                mTextViewLocationClosest2.setVisibility(View.VISIBLE);
                if (mUserDistanceUnit.equals(getResources().getStringArray(R.array.array_distance_unit)[0])) {
                    mTextViewLocationClosest2.setText(getResources().getString(R.string.fragment_compare_tv_distance_miles_closer, String.format(Locale.UK, "%.0f", compare1distance - compare2distance)));
                } else {
                    mTextViewLocationClosest2.setText(getResources().getString(R.string.fragment_compare_tv_distance_kilometres_closer, String.format(Locale.UK, "%.0f", compare1distance - compare2distance)));
                }
            } else if (compare1distance == compare2distance) {
                mTextViewLocationClosest1.setVisibility(View.VISIBLE);
                mTextViewLocationClosest2.setVisibility(View.VISIBLE);
                mTextViewLocationClosest1.setText(getResources().getString(R.string.fragment_compare_tv_location_closest_same));
                mTextViewLocationClosest2.setText(getResources().getString(R.string.fragment_compare_tv_location_closest_same));
            }
        }
    }

    private String getDistanceString(LatLng latLongStart, LatLng latLongEnd) {
        String distanceString = "";
        float distance = utils.getDistanceBetweenTwoPoints(latLongStart, latLongEnd, mUserDistanceUnit);

        if (mUserDistanceUnit.equals(Constants.MILES)) {
            if (distance>1.609f) {
                distanceString = distanceString.concat(getResources().getString(R.string.fragment_compare_tv_distance_miles, String.format(Locale.UK, "%.0f", distance)));
            } else {
                distanceString = distanceString.concat(getResources().getString(R.string.fragment_compare_tv_distance_miles_under));
            }
        } else if (mUserDistanceUnit.equals(Constants.KILOMETRES)) {
            if(distance>1.0f) {
                distanceString = distanceString.concat(getResources().getString(R.string.fragment_compare_tv_distance_kilometres, String.format(Locale.UK, "%.0f", distance)));
            } else {
                distanceString = distanceString.concat(getResources().getString(R.string.fragment_compare_tv_distance_kilometres_under));
            }
        }

        return distanceString;
    }

    private void wagesCalculations(Double wageAmount, String wageFrequency){
        String wageAmountHourly = getWageAmountHourly(wageAmount, wageFrequency);
        String wageAmountDaily = getWageAmountDaily(wageAmount, wageFrequency);
        String wageAmountWeekly = getWageAmountWeekly(wageAmount, wageFrequency);
        String wageAmountBiWeekly = getWageAmountBiWeekly(wageAmount, wageFrequency);
        String wageAmountMonthly = getWageAmountMonthly(wageAmount, wageFrequency);
        String wageAmountAnnually = getWageAmountAnnually(wageAmount, wageFrequency);
        String wageAmountLumpSumOther = getWageAmountLumpSumOther(wageAmount, wageFrequency);

        if (compareItem == 1) {
            mTextViewWagesAmount1Hourly.setText(wageAmountHourly);
            mTextViewWagesAmount1Daily.setText(wageAmountDaily);
            mTextViewWagesAmount1Weekly.setText(wageAmountWeekly);
            mTextViewWagesAmount1BiWeekly.setText(wageAmountBiWeekly);
            mTextViewWagesAmount1Monthly.setText(wageAmountMonthly);
            mTextViewWagesAmount1Annually.setText(wageAmountAnnually);
            mTextViewWagesAmount1LumpSumOther.setText(wageAmountLumpSumOther);
            setTextViewBoldItalics(wageFrequency);
        } else {
            mTextViewWagesAmount2Hourly.setText(wageAmountHourly);
            mTextViewWagesAmount2Daily.setText(wageAmountDaily);
            mTextViewWagesAmount2Weekly.setText(wageAmountWeekly);
            mTextViewWagesAmount2BiWeekly.setText(wageAmountBiWeekly);
            mTextViewWagesAmount2Monthly.setText(wageAmountMonthly);
            mTextViewWagesAmount2Annually.setText(wageAmountAnnually);
            mTextViewWagesAmount2LumpSumOther.setText(wageAmountLumpSumOther);
            setTextViewBoldItalics(wageFrequency);
        }

    }

    private void setTextViewBoldItalics(String wageFrequency) {

        if (compareItem == 1) {
            mTextViewWagesAmount1Hourly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount1Daily.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount1Weekly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount1BiWeekly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount1Monthly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount1Annually.setTypeface(null, Typeface.NORMAL);
        } else {
            mTextViewWagesAmount2Hourly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount2Daily.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount2Weekly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount2BiWeekly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount2Monthly.setTypeface(null, Typeface.NORMAL);
            mTextViewWagesAmount2Annually.setTypeface(null, Typeface.NORMAL);
        }

        switch(wageFrequency) {
            case Constants.HOURLY:
                if (compareItem == 1) {
                    mTextViewWagesAmount1Hourly.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2Hourly.setTypeface(null, Typeface.BOLD);
                }
                break;

            case Constants.DAILY:
                if (compareItem == 1) {
                    mTextViewWagesAmount1Daily.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2Daily.setTypeface(null, Typeface.BOLD);
                }
                break;

            case Constants.WEEKLY:
                if (compareItem == 1) {
                    mTextViewWagesAmount1Weekly.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2Weekly.setTypeface(null, Typeface.BOLD);
                }
                break;

            case Constants.BIWEEKLY:
                if (compareItem == 1) {
                    mTextViewWagesAmount1BiWeekly.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2BiWeekly.setTypeface(null, Typeface.BOLD);
                }
                break;

            case Constants.MONTHLY:
                if (compareItem == 1) {
                    mTextViewWagesAmount1Monthly.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2Monthly.setTypeface(null, Typeface.BOLD);
                }
                break;

            case Constants.ANNUALLY:
                if (compareItem == 1) {
                    mTextViewWagesAmount1Annually.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2Annually.setTypeface(null, Typeface.BOLD);
                }
                break;

            case Constants.LUMPSUM:
            case Constants.OTHER:
                if (compareItem == 1) {
                    mTextViewWagesAmount1LumpSumOther.setTypeface(null, Typeface.BOLD);
                } else {
                    mTextViewWagesAmount2LumpSumOther.setTypeface(null, Typeface.BOLD);
                }
                break;

        }

    }

    private String getWageAmountHourly(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        //Add currency e.g. £ to string
        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        //Add relevant wageAmount to string e.g. 6.50
        switch (wageFrequency) {
            case Constants.HOURLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            case Constants.DAILY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_DAY_WORKING_HOURS));
                break;
            case Constants.WEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_DAY_WORKING_HOURS/DEFAULT_WEEK_WORKING_DAYS));
                break;
            case Constants.BIWEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_DAY_WORKING_HOURS/DEFAULT_WEEK_WORKING_DAYS/2));
                break;
            case Constants.MONTHLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_DAY_WORKING_HOURS/DEFAULT_WEEK_WORKING_DAYS/4));
                break;
            case Constants.ANNUALLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_DAY_WORKING_HOURS/DEFAULT_WEEK_WORKING_DAYS/4/12));
                break;
            default:
                sb.append("n/a");
        }

        //Add * if wageAmount has been calculated
        if (!wageFrequency.equals(Constants.HOURLY) && !wageFrequency.equals(Constants.LUMPSUM) && !wageFrequency.equals(Constants.OTHER)) {
            sb.append("*");
        }

        return sb.toString();
    }

    private String getWageAmountDaily(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        switch (wageFrequency) {
            case Constants.HOURLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_DAY_WORKING_HOURS));
                break;
            case Constants.DAILY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            case Constants.WEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_WEEK_WORKING_DAYS));
                break;
            case Constants.BIWEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_WEEK_WORKING_DAYS/2));
                break;
            case Constants.MONTHLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_WEEK_WORKING_DAYS/4));
                break;
            case Constants.ANNUALLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/DEFAULT_WEEK_WORKING_DAYS/4/12));
                break;
            default:
                sb.append("n/a");
        }

        //Add * if wageAmount has been calculated
        if (!wageFrequency.equals(Constants.DAILY) && !wageFrequency.equals(Constants.LUMPSUM) && !wageFrequency.equals(Constants.OTHER)) {
            sb.append("*");
        }

        return sb.toString();
    }

    private String getWageAmountWeekly(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        switch (wageFrequency) {
            case Constants.HOURLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_DAY_WORKING_HOURS*DEFAULT_WEEK_WORKING_DAYS));
                break;
            case Constants.DAILY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_DAY_WORKING_HOURS*DEFAULT_WEEK_WORKING_DAYS));
                break;
            case Constants.WEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            case Constants.BIWEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/2));
                break;
            case Constants.MONTHLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/4));
                break;
            case Constants.ANNUALLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/12/4));
                break;
            default:
                sb.append("n/a");
        }

        //Add * if wageAmount has been calculated
        if (!wageFrequency.equals(Constants.WEEKLY) && !wageFrequency.equals(Constants.LUMPSUM) && !wageFrequency.equals(Constants.OTHER)) {
            sb.append("*");
        }

        return sb.toString();
    }

    private String getWageAmountBiWeekly(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        switch (wageFrequency) {
            case Constants.HOURLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_DAY_WORKING_HOURS*DEFAULT_WEEK_WORKING_DAYS*2));
                break;
            case Constants.DAILY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_WEEK_WORKING_DAYS*2));
                break;
            case Constants.WEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*2));
                break;
            case Constants.BIWEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            case Constants.MONTHLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/2));
                break;
            case Constants.ANNUALLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/12/2));
                break;
            default:
                sb.append("n/a");
        }

        //Add * if wageAmount has been calculated
        if (!wageFrequency.equals(Constants.BIWEEKLY) && !wageFrequency.equals(Constants.LUMPSUM) && !wageFrequency.equals(Constants.OTHER)) {
            sb.append("*");
        }

        return sb.toString();
    }

    private String getWageAmountMonthly(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        switch (wageFrequency) {
            case Constants.HOURLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_DAY_WORKING_HOURS*DEFAULT_WEEK_WORKING_DAYS*4));
                break;
            case Constants.DAILY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_WEEK_WORKING_DAYS*4));
                break;
            case Constants.WEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*4));
                break;
            case Constants.BIWEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*2));
                break;
            case Constants.MONTHLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            case Constants.ANNUALLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount/12));
                break;
            default:
                sb.append("n/a");
        }

        //Add * if wageAmount has been calculated
        if (!wageFrequency.equals(Constants.MONTHLY) && !wageFrequency.equals(Constants.LUMPSUM) && !wageFrequency.equals(Constants.OTHER)) {
            sb.append("*");
        }

        return sb.toString();
    }

    private String getWageAmountAnnually(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        switch (wageFrequency) {
            case Constants.HOURLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_DAY_WORKING_HOURS*DEFAULT_WEEK_WORKING_DAYS*4*12));
                break;
            case Constants.DAILY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*DEFAULT_WEEK_WORKING_DAYS*4*12));
                break;
            case Constants.WEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*4*12));
                break;
            case Constants.BIWEEKLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*2*12));
                break;
            case Constants.MONTHLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount*12));
                break;
            case Constants.ANNUALLY:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            default:
                sb.append("n/a");
        }

        //Add * if wageAmount has been calculated
        if (!wageFrequency.equals(Constants.ANNUALLY) && !wageFrequency.equals(Constants.LUMPSUM) && !wageFrequency.equals(Constants.OTHER)) {
            sb.append("*");
        }

        return sb.toString();
    }

    private String getWageAmountLumpSumOther(Double wageAmount, String wageFrequency) {

        StringBuilder sb = new StringBuilder();

        if (compareItem == 1) {
            sb.append(compare1WageCurrency);
        } else {
            sb.append(compare2WageCurrency);
        }

        switch (wageFrequency) {
            case Constants.LUMPSUM:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            case Constants.OTHER:
                sb.append(String.format(Locale.UK, "%.2f", wageAmount));
                break;
            default:
                sb.append("n/a");
        }

        return sb.toString();
    }

}
