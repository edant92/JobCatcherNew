package com.este.jobcatcher.emailfollowup;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.general.UpgradeActivity;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.models.User;
import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Allows user to send a follow up Email
 */

public class FollowupEmailActivity extends AppCompatActivity {
    private static final String LOG_TAG = FollowupEmailActivity.class.getSimpleName();

    private Boolean isUserPremium;

    private DatabaseReference mJobApplicationRef;

    private String contactEmail;
    private String subject;
    private String greeting;
    private String content;
    private String closing;
    private String userName;

    private TextView mTextViewFollowupEmailContactEmail;
    private TextView mTextViewFollowupEmailSubject;
    private TextView mTextViewFollowupEmailGreeting;
    private TextView mTextViewFollowupEmailContent;
    private TextView mTextViewFollowupEmailClosing;
    private TextView mTextViewFollowupEmailUserName;

    private Dialog mDialogEditContactEmail;
    private Dialog mDialogEditSubject;
    private Dialog mDialogEditGreeting;
    private Dialog mDialogEditContent;
    private Dialog mDialogEditClosing;
    private Dialog mDialogEditUserName;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followup_email);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_followup_email_app_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = this.getIntent();
        String jobApplicationId = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        Boolean applicationPinned = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String userUID = sharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");
        isUserPremium = sharedPrefs.getBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, false);

        DatabaseReference mFirebaseRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mUserDetailsRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_USERS).child(userUID);

        //Getting null error on childPath for jobApplicationId
        //https://console.firebase.google.com/u/0/project/project-4896978972225863600/monitoring/app/android:com.este.jobcatcher/cluster/aa13a862?reportTypes=REPORT_TYPE_UNSPECIFIED&duration=2592000000
        //Quick fix is to just exit activity if either is null
        if (jobApplicationId == null) {
            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (applicationPinned) {
            mJobApplicationRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(jobApplicationId);
        } else {
            mJobApplicationRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(jobApplicationId);
        }

        mUserDetailsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    userName = user.getFirstName() + getResources().getString(R.string.generic_string_empty_space) + user.getSurname();
                    mTextViewFollowupEmailUserName.setText(userName);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

        initialiseUI();

        getDetailsFromJobApplication();

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present. */
        getMenuInflater().inflate(R.menu.menu_followup_email, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_send_email) {
            sendFollowupEmail();
        }

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initialiseUI() {
        LinearLayout mLinearLayoutFollowupEmailContactEmail = (LinearLayout) findViewById(R.id.activity_followup_email_ll_contact_email);
        mTextViewFollowupEmailContactEmail = (TextView) findViewById(R.id.activity_followup_email_tv_contact_email);
        LinearLayout mLinearLayoutFollowupEmailSubject = (LinearLayout) findViewById(R.id.activity_followup_email_ll_subject);
        mTextViewFollowupEmailSubject = (TextView) findViewById(R.id.activity_followup_email_tv_subject);
        mTextViewFollowupEmailGreeting = (TextView) findViewById(R.id.activity_followup_email_tv_greeting);
        mTextViewFollowupEmailContent = (TextView) findViewById(R.id.activity_followup_email_tv_content);
        mTextViewFollowupEmailClosing = (TextView) findViewById(R.id.activity_followup_email_tv_closing);
        mTextViewFollowupEmailUserName = (TextView) findViewById(R.id.activity_followup_email_tv_user_name);

        mLinearLayoutFollowupEmailContactEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createContactEmailDialog();
            }
        });

        mLinearLayoutFollowupEmailSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSubjectDialog();
            }
        });

        mTextViewFollowupEmailGreeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createGreetingDialog();
            }
        });

        mTextViewFollowupEmailContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createContentDialog();
            }
        });

        mTextViewFollowupEmailClosing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createClosingDialog();
            }
        });

        mTextViewFollowupEmailUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createUserNameDialog();
            }
        });

        //If User is not premium then show freeVersionFooter
        if (!isUserPremium) {
            LinearLayout mLinearLayoutFreeVersionWarning = (LinearLayout) findViewById(R.id.activity_followup_email_ll_free_version_warning);
            mLinearLayoutFreeVersionWarning.setVisibility(View.VISIBLE);


            mLinearLayoutFreeVersionWarning.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createFreeVersionWarning();
                }
            });
        }
    }

    private void sendFollowupEmail() {

        String[] emailTo = {contactEmail};
        String freeVersionFooter = getResources().getString(R.string.followup_email_send_free_version_footer);

        StringBuilder sb = new StringBuilder();
        sb.append(greeting);
        sb.append("\n\n");
        sb.append(content);
        sb.append("\n\n");
        sb.append(closing);
        sb.append("\n\n");
        sb.append(userName);

        //If User is not premium then add freeVersionFooter
        if (!isUserPremium) {
            sb.append("\n\n\n");
            sb.append(freeVersionFooter);
        }

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailTo);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());

        // need this to prompts email client only
        emailIntent.setType("message/rfc822");

        try {
            startActivity(Intent.createChooser(emailIntent, "Choose an email client"));

            HashMap<String, Object> emailFollowupProperties = new HashMap<>();
            emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_SENT, Calendar.getInstance().getTimeInMillis());
            emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_SENT, true);
            mJobApplicationRef.updateChildren(emailFollowupProperties);

            finish();
            Log.d(LOG_TAG, "Email sent.");
        } catch (android.content.ActivityNotFoundException ex) {
            Log.d(LOG_TAG, "Error sending email: " + ex.getLocalizedMessage());
            Toast.makeText(this, "Unable to send follow up. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getDetailsFromJobApplication() {
        mJobApplicationRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                JobApplication jobApplication = dataSnapshot.getValue(JobApplication.class);

                if (jobApplication == null) {
                    finish();
                    return;
                }

                contactEmail = jobApplication.getContactEmail();
                if (contactEmail.equals("")) {
                    mTextViewFollowupEmailContactEmail.setText(getResources().getString(R.string.activity_followup_email_et_contact_email));
                } else {
                    mTextViewFollowupEmailContactEmail.setText(contactEmail);
                }

                subject = subjectStringBuilder(jobApplication.getJobTitle(), jobApplication.getCompanyName(), jobApplication.getJobReference());
                mTextViewFollowupEmailSubject.setText(subject);

                greeting = greetingStringBuilder(jobApplication.getContactTitle(), jobApplication.getContactFirstName(), jobApplication.getContactSurname());
                mTextViewFollowupEmailGreeting.setText(greeting);

                content = contentStringBuilder(jobApplication.getJobTitle(), jobApplication.getCompanyName(), jobApplication.getJobReference());
                mTextViewFollowupEmailContent.setText(content);

                closing = closingStringBuilder();
                mTextViewFollowupEmailClosing.setText(closingStringBuilder());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private String contactEmailStringBuilder(String contactEmail) {
        if (contactEmail.equals("")) {
            return "";
        } else {
            return contactEmail;
        }
    }

    private String subjectStringBuilder(String jobTitle, String companyName, String jobReference) {

        StringBuilder sb = new StringBuilder();
        sb.append("Follow up for ");

        if (!jobTitle.equals("")) {
            sb.append("position of ");
            sb.append(jobTitle);
        }

        if (!companyName.equals("")) {

            if (!jobTitle.equals("")) {
                sb.append(" at ");
            } else {
                sb.append("position at ");
            }
            sb.append(companyName);
        }

        if (!jobTitle.equals("") && !companyName.equals("")) {
            if (!jobReference.equals("")) {
                sb.append("- Reference: ");
                sb.append(jobReference);
            }
        } else {
            if (!jobReference.equals("")) {
                sb.append("Job Reference: ");
                sb.append(jobReference);
            }
        }

        if (jobTitle.equals("") && companyName.equals("") && jobReference.equals("")) {
            sb.append("my recent job application");
        }


        if (sb.toString().equals("")) {
            return getResources().getString(R.string.activity_followup_email_tv_subject);
        } else {
            return sb.toString();
        }

    }

    private String greetingStringBuilder(String contactTitle, String contactFirstName, String contactSurname) {

        String emailSalutation = getResources().getString(R.string.activity_followup_email_tv_salutation_default);

        StringBuilder sb = new StringBuilder();

        if (!contactFirstName.equals("") || !contactSurname.equals("")) {
            sb.append(emailSalutation);
            sb.append(" ");

            sb.append(contactTitle);
            sb.append(" ");

            sb.append(contactFirstName);
            sb.append(" ");
            sb.append(contactSurname);
            sb.append(",");
        } else {
            sb.append(getResources().getString(R.string.activity_followup_email_tv_greeting));
        }

        return sb.toString();
    }

    private String contentStringBuilder(String jobTitle, String companyName, String jobReference) {

        if (jobTitle.equals("") && companyName.equals("") && jobReference.equals("")) {
            content = getResources().getString(R.string.activity_followup_email_et_content_no_job_title_no_company);
        } else {

            StringBuilder sb = new StringBuilder();

            sb.append(getResources().getString(R.string.activity_followup_email_tv_content_start));

            if (!jobTitle.equals("") && !companyName.equals("")) {
                sb.append(String.format(getResources().getString(R.string.activity_followup_email_tv_content_jt_cn), jobTitle, companyName));
            } else if (!jobTitle.equals("") && companyName.equals("")){
                sb.append(String.format(getResources().getString(R.string.activity_followup_email_tv_content_jt), jobTitle));
            } else if (jobTitle.equals("") && !companyName.equals("")){
                sb.append(String.format(getResources().getString(R.string.activity_followup_email_tv_content_cn), companyName));
            } else {
                sb.append(String.format(getResources().getString(R.string.activity_followup_email_tv_content_jr), jobReference));
            }

            sb.append(getResources().getString(R.string.activity_followup_email_et_content));

            content = sb.toString();

        }

        return content;
    }

    private String closingStringBuilder() {
        return getResources().getString(R.string.activity_followup_email_tv_closing_default);
    }

    private void createContactEmailDialog(){

        final AlertDialog.Builder editContactEmailDialog = new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_follow_up_contact_email, null);
        editContactEmailDialog.setView(editView);
        final EditText mEditTextContactEmail = (EditText) editView.findViewById(R.id.dialog_edit_et_followup_contact_email);

        mEditTextContactEmail.setText(contactEmail);

        mEditTextContactEmail.setSelection(mEditTextContactEmail.getText().length());

        editContactEmailDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                contactEmail = mEditTextContactEmail.getText().toString().trim();
                mTextViewFollowupEmailContactEmail.setText(contactEmail);
                mDialogEditContactEmail.dismiss();

            }
        });
        editContactEmailDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditContactEmail.dismiss();
            }
        });

        editContactEmailDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                contactEmail = mEditTextContactEmail.getText().toString().trim();
                mTextViewFollowupEmailContactEmail.setHint(getResources().getString(R.string.activity_followup_email_et_contact_email));
                mDialogEditContactEmail.dismiss();
            }
        });

        mDialogEditContactEmail = editContactEmailDialog.show();

        if (mDialogEditContactEmail.getWindow() != null) {
            mDialogEditContactEmail.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createSubjectDialog(){

        final AlertDialog.Builder editSubjectDialog = new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_follow_up_subject, null);
        editSubjectDialog.setView(editView);
        final EditText mEditTextSubject = (EditText) editView.findViewById(R.id.dialog_edit_et_followup_subject);

        mEditTextSubject.setText(subject);

        mEditTextSubject.setSelection(mEditTextSubject.getText().length());

        editSubjectDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                subject = mEditTextSubject.getText().toString().trim();
                mTextViewFollowupEmailSubject.setText(subject);
                mDialogEditSubject.dismiss();

            }
        });
        editSubjectDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditSubject.dismiss();
            }
        });

        editSubjectDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                subject = mEditTextSubject.getText().toString().trim();
                mTextViewFollowupEmailSubject.setHint(getResources().getString(R.string.activity_followup_email_tv_subject));
                mDialogEditSubject.dismiss();
            }
        });

        mDialogEditSubject = editSubjectDialog.show();

        if (mDialogEditSubject.getWindow() != null) {
            mDialogEditSubject.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createGreetingDialog(){

        final AlertDialog.Builder editGreetingDialog = new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_follow_up_greeting, null);
        editGreetingDialog.setView(editView);
        final EditText mEditTextGreeting = (EditText) editView.findViewById(R.id.dialog_edit_et_followup_greeting);

        mEditTextGreeting.setText(greeting);

        mEditTextGreeting.setSelection(mEditTextGreeting.getText().length());

        editGreetingDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                greeting = mEditTextGreeting.getText().toString().trim();
                mTextViewFollowupEmailGreeting.setText(greeting);
                mDialogEditGreeting.dismiss();

            }
        });
        editGreetingDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditGreeting.dismiss();
            }
        });

        editGreetingDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                greeting = getResources().getString(R.string.activity_followup_email_tv_greeting);
                mTextViewFollowupEmailGreeting.setText(greeting);
                mDialogEditGreeting.dismiss();
            }
        });

        mDialogEditGreeting = editGreetingDialog.show();

        if (mDialogEditGreeting.getWindow() != null) {
            mDialogEditGreeting.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createContentDialog(){

        final AlertDialog.Builder editContentDialog = new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_follow_up_content, null);
        editContentDialog.setView(editView);
        final EditText mEditTextContent = (EditText) editView.findViewById(R.id.dialog_edit_et_followup_content);

        mEditTextContent.setText(content);

        mEditTextContent.setSelection(mEditTextContent.getText().length());

        editContentDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                content = mEditTextContent.getText().toString().trim();
                mTextViewFollowupEmailContent.setText(content);
                mDialogEditContent.dismiss();

            }
        });
        editContentDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditContent.dismiss();
            }
        });

        editContentDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                content = mEditTextContent.getText().toString().trim();
                mTextViewFollowupEmailContent.setText(getResources().getString(R.string.activity_followup_email_et_content_no_job_title_no_company));
                mDialogEditContent.dismiss();
            }
        });

        mDialogEditContent = editContentDialog.show();

        if (mDialogEditContent.getWindow() != null) {
            mDialogEditContent.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createClosingDialog(){

        final AlertDialog.Builder editClosingDialog = new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_follow_up_closing, null);
        editClosingDialog.setView(editView);
        final EditText mEditTextClosing = (EditText) editView.findViewById(R.id.dialog_edit_et_followup_closing);

        mEditTextClosing.setText(closing);

        mEditTextClosing.setSelection(mEditTextClosing.getText().length());

        editClosingDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                closing = mEditTextClosing.getText().toString().trim();
                mTextViewFollowupEmailClosing.setText(closing);
                mDialogEditClosing.dismiss();

            }
        });
        editClosingDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditClosing.dismiss();
            }
        });

        editClosingDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                closing = getResources().getString(R.string.activity_followup_email_tv_closing_default);
                mTextViewFollowupEmailClosing.setHint(closing);
                mDialogEditClosing.dismiss();
            }
        });

        mDialogEditClosing = editClosingDialog.show();

        if (mDialogEditClosing.getWindow() != null) {
            mDialogEditClosing.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createUserNameDialog(){

        final AlertDialog.Builder editUserNameDialog = new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_follow_up_user_name, null);
        editUserNameDialog.setView(editView);
        final EditText mEditTextUserName = (EditText) editView.findViewById(R.id.dialog_edit_et_followup_user_name);

        mEditTextUserName.setText(userName);

        mEditTextUserName.setSelection(mEditTextUserName.getText().length());

        editUserNameDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                userName = mEditTextUserName.getText().toString().trim();
                mTextViewFollowupEmailUserName.setText(userName);
                mDialogEditUserName.dismiss();

            }
        });
        editUserNameDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditUserName.dismiss();
            }
        });

        editUserNameDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                userName = "";
                mTextViewFollowupEmailUserName.setText(userName);
                mDialogEditUserName.dismiss();
            }
        });

        mDialogEditUserName = editUserNameDialog.show();

        if (mDialogEditUserName.getWindow() != null) {
            mDialogEditUserName.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createFreeVersionWarning() {
        new AlertDialog.Builder(FollowupEmailActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle(R.string.dialog_followup_email_free_version_warning_title)
                .setMessage(getResources().getString(R.string.followup_email_send_free_version_footer_warning))
                .setPositiveButton(getResources().getString(R.string.dialog_followup_email_free_version_warning_button_positive), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(FollowupEmailActivity.this, UpgradeActivity.class));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.dialog_followup_email_free_version_warning_button_negative), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(FollowupEmailActivity.this, "Why no upgrade? :(", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

}