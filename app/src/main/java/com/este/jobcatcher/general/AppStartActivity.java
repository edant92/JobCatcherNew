package com.este.jobcatcher.general;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.MainActivity;
import com.este.jobcatcher.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Activity for app 'quick start' so user doesn't see login screen if they are already authenticated.
 */

public class AppStartActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private SharedPreferences mSharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_start);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //If User is authenticated

                    mSharedPref = PreferenceManager.getDefaultSharedPreferences(AppStartActivity.this);
                    mSharedPref.edit().putString(Constants.SHARED_PREF_KEY_USER_UID, user.getUid()).apply();

                    startActivity(new Intent(AppStartActivity.this, MainActivity.class));
                    finish();

                } else {
                    //If user is not authenticated
                    startActivity(new Intent(AppStartActivity.this, IntroActivity.class));
                    finish();
                }

            }
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        super.onStop();
    }

}
