package com.este.jobcatcher.general;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.MainActivity;
import com.este.jobcatcher.models.User;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * This Activity deals with Creating an Account using an email address
 */
public class CreateAccountActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String LOG_TAG = CreateAccountActivity.class.getSimpleName();

    private String userUID;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;
    private AlertDialog locationAddDialog;
    private AutoCompleteTextView mAutocompleteTextViewLocation;
    private AppCompatImageView mImageViewLocation;
    private ProgressBar mAutocompleteProgressBar;
    private GoogleApiClient mGoogleApiClientSignIn;
    private GoogleApiClient mGoogleApiClientGeo;

    private PlaceArrayAdapter mPlaceArrayAdapter;

    private EditText mEditTextUserFirstName;
    private EditText mEditTextUserSurname;
    private EditText mEditTextUserEmail;
    private EditText mEditTextUserPassword;
    private TextView mTextViewLocation;
    private TextView mTextViewDistanceUnit;
    private TextView mTextViewCurrency;

    private AlertDialog mCurrencyListDialog;

    private SharedPreferences mSharedPref;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private String userFirstName;
    private String userSurname;
    private String userEmail;
    private Double userLocationLat;
    private Double userLocationLng;
    private String userLocationName;
    private String userDistanceUnit;
    private String userCurrency;

    /* A dialog that is presented until the Firebase authentication finished. */
    private ProgressDialog mCreateAccountProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_activity_create_account);
        /* Common toolbar setup */
        setSupportActionBar(toolbar);
        /* Add back button to the action bar */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        //Set Default values
        userLocationLat = 0.0;
        userLocationLng = 0.0;
        userLocationName = "";
        userCurrency = getResources().getStringArray(R.array.array_currency)[0];
        userDistanceUnit = getResources().getStringArray(R.array.array_distance_unit)[0];

        mAuth = FirebaseAuth.getInstance();

        //This sets starting Fragment in MainListActivity to OverviewFragment
        mSharedPref.edit().putString(Constants.SHARED_PREF_CURRENT_FRAGMENT_TAG, Constants.FRAGMENT_TAG_OVERVIEW).apply();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null) {
                    // User is signed in

                    userUID = user.getUid();

                    mSharedPref.edit().putString(Constants.SHARED_PREF_KEY_USER_UID, userUID).apply();

                    /* If no user db object exists (e.g. account being created), make a user */
                    final DatabaseReference userLocation = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_USERS).child(userUID);

                    userLocation.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            /* If there is no user db object, make one */
                            if (dataSnapshot.getValue() == null) {

                                    /* Set raw version of date to the ServerValue.TIMESTAMP value and save into dateCreatedMap */
                                    HashMap<String, Object> timestampJoined = new HashMap<>();
                                    timestampJoined.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

                                    User newUser = new User(userFirstName, userSurname, userEmail, timestampJoined, 0.0, 0.0, 0.0, null, userLocationLat, userLocationLng, userLocationName, userDistanceUnit, userCurrency);
                                    userLocation.setValue(newUser);

                                    mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_WAGE_CURRENCY, userCurrency).apply();
                                    mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_WAGE_FREQUENCY, getResources().getStringArray(R.array.array_wage_frequency)[0]).apply();
                                    mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_JOB_TYPE, getResources().getStringArray(R.array.array_job_type)[0]).apply();
                                    mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_DISTANCE_UNIT, userDistanceUnit).apply();

                                    /* Go to main activity */
                                    Intent intent = new Intent(CreateAccountActivity.this, MainActivity.class);
                                    startActivity(intent);

                            } else {

                                /* Go to main activity */
                                Intent intent = new Intent(CreateAccountActivity.this, MainActivity.class);
                                startActivity(intent);

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    Log.d(LOG_TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(LOG_TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClientSignIn = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleApiClientGeo = new GoogleApiClient.Builder(CreateAccountActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();

        initialiseUI();

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        googleApiSignInStart();
    }

    @Override
    public void onStop() {
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        googleApiSignInStop();
        super.onStop();
    }

    private final AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            if (item != null) {
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Selected: " + item.description);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClientGeo, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            }
        }
    };

    private final ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                mAutocompleteProgressBar.setVisibility(View.GONE);
                mImageViewLocation.setVisibility(View.VISIBLE);
                return;
            }

            // Selecting the first object buffer.
            final Place place = places.get(0);
            //Removes progress bar
            mAutocompleteProgressBar.setVisibility(View.GONE);
            mImageViewLocation.setVisibility(View.VISIBLE);

            //Moves text cursor to start of EditText
            mAutocompleteTextViewLocation.setSelection(0);

            String str = place.getLatLng().toString();
            String latlongRemoveStart = str.replace("lat/lng:", "");
            String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
            String[] parts = latlongRemoveEnd.split(",");
            userLocationLat = Double.parseDouble(parts[0]);
            userLocationLng = Double.parseDouble(parts[1]);
            userLocationName = place.getName().toString();

            mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, userLocationLat.toString()).apply();
            mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, userLocationLng.toString()).apply();
            mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, userLocationName).apply();

        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClientGeo);
        Log.i(LOG_TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    private void googleApiGeoStart() {
        if (mGoogleApiClientGeo != null) {
            mGoogleApiClientGeo.connect();
        }
    }

    private void googleApiSignInStart() {
        if (mGoogleApiClientSignIn != null) {
            mGoogleApiClientSignIn.connect();
        }
    }

    private void googleApiGeoStop() {
        if (mGoogleApiClientGeo != null) {
            mGoogleApiClientGeo.disconnect();
        }
    }

    private void googleApiSignInStop() {
        if (mGoogleApiClientSignIn != null) {
            mGoogleApiClientSignIn.disconnect();
        }
    }

    private void initialiseUI() {

        mEditTextUserEmail = (EditText) findViewById(R.id.activity_create_account_et_create_account_user_email);
        mEditTextUserPassword = (EditText) findViewById(R.id.activity_create_account_et_create_account_user_password);
        mEditTextUserFirstName = (EditText) findViewById(R.id.activity_create_account_et_create_account_user_first_name);
        mEditTextUserSurname = (EditText) findViewById(R.id.activity_create_account_et_create_account_user_surname);
        LinearLayout mLinearLayoutLocation = (LinearLayout) findViewById(R.id.activity_create_account_ll_create_account_location);
        mTextViewLocation = (TextView) findViewById(R.id.activity_create_account_tv_create_account_location_name);
        LinearLayout mLinearLayoutDistanceUnit = (LinearLayout) findViewById(R.id.activity_create_account_ll_create_account_distance_unit);
        mTextViewDistanceUnit = (TextView) findViewById(R.id.activity_create_account_tv_create_account_distance_unit);
        LinearLayout mLinearLayoutCurrency = (LinearLayout) findViewById(R.id.activity_create_account_ll_create_account_currency);
        mTextViewCurrency = (TextView) findViewById(R.id.activity_create_account_tv_create_account_currency);

        mEditTextUserEmail.requestFocus();

        mTextViewDistanceUnit.setText(userDistanceUnit);
        mTextViewCurrency.setText(userCurrency);

        mLinearLayoutLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createLocationAddDialogNew();
            }
        });

        mLinearLayoutDistanceUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDistanceUnitListDialog();
            }
        });

        mLinearLayoutCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCurrencyListDialog();
            }
        });

        Button mButtonCreateAccountSubmit = (Button) findViewById(R.id.activity_create_account_bt_create_account_submit);
        mButtonCreateAccountSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccountSubmit();
            }
        });

        mCreateAccountProgressDialog = new ProgressDialog(this);
        mCreateAccountProgressDialog.setMessage(getResources().getString(R.string.activity_create_account_pd_loading_creating_account));
        mCreateAccountProgressDialog.setCancelable(false);

    }

    private void createAccountSubmit() {
        userFirstName = mEditTextUserFirstName.getText().toString().trim();
        userSurname = mEditTextUserSurname.getText().toString().trim();
        userEmail = mEditTextUserEmail.getText().toString().trim();
        final String userPassword = mEditTextUserPassword.getText().toString().trim();

        if (!isValidEmail(userEmail)) {
            mEditTextUserEmail.setError(getResources().getString(R.string.activity_create_account_error_message_email_empty));
        }
        if (userPassword.equals("")) {
            mEditTextUserPassword.setError(getResources().getString(R.string.activity_create_account_error_message_password_empty));
        }
        if (userPassword.length() < 6) {
            mEditTextUserPassword.setError(getResources().getString(R.string.activity_create_account_error_message_password_too_short));
        }

        if (isValidEmail(userEmail) && userPassword.length() >=6) {

            mCreateAccountProgressDialog.show();

            mAuth.createUserWithEmailAndPassword(userEmail, userPassword)
                    .addOnCompleteListener(CreateAccountActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(LOG_TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {

                                try {
                                    throw task.getException();
                                } catch (Exception e) {
                                    Log.d(LOG_TAG, e.getLocalizedMessage());
                                    Toast.makeText(CreateAccountActivity.this, getResources().getString(R.string.activity_create_account_error_message_unable_to_create_account) + " " + e.getLocalizedMessage(),
                                            Toast.LENGTH_LONG).show();
                                    mCreateAccountProgressDialog.dismiss();
                                }

                            }

                        }
                    });
        }
    }

    private void createLocationAddDialogNew() {

        final AlertDialog.Builder alertDialogBuilderLocationAdd = new AlertDialog.Builder(CreateAccountActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_location_user_add, null);
        alertDialogBuilderLocationAdd.setView(convertView);

        LinearLayout mLinearLayoutGetUserLocation = (LinearLayout) convertView.findViewById(R.id.dialog_location_user_add_ll_location_user);
        mAutocompleteTextViewLocation = (AutoCompleteTextView) convertView.findViewById(R.id.dialog_location_user_add_actv);
        mImageViewLocation = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_user_add_iv_location_icon);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_user_add_progress_bar);

        if (userLocationName != null) {
            mAutocompleteTextViewLocation.setText(userLocationName);
        }

        alertDialogBuilderLocationAdd.setPositiveButton(getResources().getString(R.string.dialog_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (userLocationName != null) {
                        mTextViewLocation.setText(userLocationName);
                } else {
                        mTextViewLocation.setText(getResources().getString(R.string.activity_create_account_tv_create_account_location_name_empty));
                }
                googleApiGeoStop();
                locationAddDialog.dismiss();
            }
        });

        alertDialogBuilderLocationAdd.setNegativeButton(getResources().getString(R.string.dialog_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiGeoStop();
                locationAddDialog.dismiss();
            }
        });

        alertDialogBuilderLocationAdd.setNeutralButton(getResources().getString(R.string.dialog_location_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiGeoStop();
                userLocationName = "";
                mTextViewLocation.setText(getResources().getString(R.string.activity_create_account_tv_create_account_location_name_empty));
                locationAddDialog.dismiss();
            }
        });

        locationAddDialog = alertDialogBuilderLocationAdd.show();
        locationAddDialog.setCancelable(false);
        locationAddDialog.setCanceledOnTouchOutside(false);

        //LOCATION API CODE
        googleApiGeoStart();
        mAutocompleteTextViewLocation = (AutoCompleteTextView) convertView.findViewById(R.id.dialog_location_user_add_actv);
        mAutocompleteTextViewLocation.setThreshold(3);
        mAutocompleteTextViewLocation.setOnItemClickListener(mAutocompleteClickListener);

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, R.layout.list_item_location_autocomplete, null, null);
        mAutocompleteTextViewLocation.setAdapter(mPlaceArrayAdapter);

        mLinearLayoutGetUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(CreateAccountActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(CreateAccountActivity.this, new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
                } else {
                    Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClientSignIn);
                    getUserLocation(lastLocation);
                }

            }
        });

        mAutocompleteTextViewLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAutocompleteProgressBar.setVisibility(View.VISIBLE);
                mImageViewLocation.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mAutocompleteTextViewLocation.getText().toString().equals("")) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                }
            }
        });

        mAutocompleteTextViewLocation.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (mAutocompleteTextViewLocation.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                        int leftEdgeOfRightDrawable = mAutocompleteTextViewLocation.getRight()
                                - mAutocompleteTextViewLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                        if (event.getRawX() >= leftEdgeOfRightDrawable) {
                            // clicked on clear icon
                            mAutocompleteTextViewLocation.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

    }

    private void getUserLocation(Location lastLocation) {

        System.out.println("locationLat: " + lastLocation);

        if (lastLocation != null) {
            userLocationLat = lastLocation.getLatitude();
            userLocationLng = lastLocation.getLongitude();

            mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, userLocationLat.toString()).apply();
            mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, userLocationLng.toString()).apply();

            try {
                Geocoder geo = new Geocoder(CreateAccountActivity.this, Locale.getDefault());
                List<Address> addresses = geo.getFromLocation(userLocationLat, userLocationLng, 1);

                if (addresses.isEmpty()) {
                    Toast.makeText(CreateAccountActivity.this, "Waiting for location", Toast.LENGTH_SHORT).show();
                } else {

                    if (addresses.size() > 0) {

                        if (addresses.get(0).getLocality() != null) {
                            userLocationName = addresses.get(0).getLocality();
                        } else {
                            userLocationName = addresses.get(0).getFeatureName();
                        }

                        mTextViewLocation.setText(userLocationName);

                        mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, userLocationName).apply();

                        googleApiGeoStop();

                        if (locationAddDialog != null) {
                            locationAddDialog.dismiss();
                        }

                    }

                }
            } catch (Exception e) {
                e.printStackTrace(); // getFromLocation() may sometimes fail
            }
        } else {
            Toast.makeText(CreateAccountActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
        }
    }

    private void createDistanceUnitListDialog() {
        final AlertDialog.Builder dialogListDistanceUnit = new AlertDialog.Builder(CreateAccountActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_list_distance_unit, null);
        dialogListDistanceUnit.setView(convertView);

        final ListView mListViewDistanceUnit = (ListView) convertView.findViewById(R.id.dialog_list_distance_unit_lv);

        ArrayAdapter<CharSequence> adapterDefaultDistanceUnit = ArrayAdapter.createFromResource(this, R.array.array_distance_unit, R.layout.list_item_distance_unit);

        mListViewDistanceUnit.setAdapter(adapterDefaultDistanceUnit);

        mListViewDistanceUnit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userDistanceUnit = mListViewDistanceUnit.getItemAtPosition(position).toString();

                mTextViewDistanceUnit.setText(userDistanceUnit);

                mCurrencyListDialog.dismiss();
            }
        });

        mCurrencyListDialog = dialogListDistanceUnit.show();

    }

    private void createCurrencyListDialog() {
        final AlertDialog.Builder dialogListCurrency = new AlertDialog.Builder(CreateAccountActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_list_currency, null);
        dialogListCurrency.setView(convertView);

        final ListView mListViewCurrency = (ListView) convertView.findViewById(R.id.dialog_list_currency_lv);

        ArrayAdapter<CharSequence> adapterCurrency = ArrayAdapter.createFromResource(this, R.array.array_currency, R.layout.list_item_currency);

        mListViewCurrency.setAdapter(adapterCurrency);

        mListViewCurrency.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userCurrency = mListViewCurrency.getItemAtPosition(position).toString();

                mTextViewCurrency.setText(userCurrency);

                mCurrencyListDialog.dismiss();
            }
        });

        mCurrencyListDialog = dialogListCurrency.show();

    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}
