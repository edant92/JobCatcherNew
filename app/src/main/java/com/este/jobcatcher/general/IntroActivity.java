package com.este.jobcatcher.general;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.este.jobcatcher.R;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * Displays key features of application to users
 */

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Integer colorWhite = Color.parseColor("#ffffff");
        Integer colorGreen = Color.parseColor("#4CAF50");
        
        addSlide(AppIntroFragment.newInstance("The Perfect Record", "Track your job applications all in one place (All synced locally and to the cloud!)", R.drawable.screenshot_joblist, colorWhite, colorGreen, colorGreen));
        addSlide(AppIntroFragment.newInstance("Set Deadlines And Reminders", "Never miss a chance to get the job again", R.drawable.screenshot_overview, colorWhite, colorGreen, colorGreen));
        addSlide(AppIntroFragment.newInstance("JobMap", "View the location of all your job applications", R.drawable.screenshot_jobmap, colorWhite, colorGreen, colorGreen));
        addSlide(AppIntroFragment.newInstance("Compare Jobs", "Compare Job locations, wages and more", R.drawable.screenshot_compare, colorWhite, colorGreen, colorGreen));
        addSlide(AppIntroFragment.newInstance("Add from anywhere", "Directly save job adverts from Indeed, Reed, Adzuna and Authentic Jobs", R.drawable.screenshot_add_from_url, colorWhite, colorGreen, colorGreen));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(colorGreen);
        setSeparatorColor(colorGreen);

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

}
