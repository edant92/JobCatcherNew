package com.este.jobcatcher.general;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Enable Firebase Offline Functionality
 */
public class JobCatcherApplication extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        /* Enable Firebase Offline Functionality*/
        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getToken();
    }

}
