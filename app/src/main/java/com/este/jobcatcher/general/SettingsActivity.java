package com.este.jobcatcher.general;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.este.jobcatcher.R;
import com.este.jobcatcher.models.User;
import com.este.jobcatcher.utils.Constants;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * This Activity deals with JobCatcher settings/defaults and logging out/deleting account.
 */
public class SettingsActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback {

    private static final String LOG_TAG = SettingsActivity.class.getSimpleName();

    private String userUID;

    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseRef;
    private DatabaseReference mUserRef;
    private DatabaseReference mJobApplicationsAllRef;
    private DatabaseReference mJobApplicationsRef;
    private DatabaseReference mJobApplicationsPriorityRef;
    private DatabaseReference mJobApplicationsArchiveRef;
    private DatabaseReference mJsasRef;

    private User mUser;

    private String strProvider;

    private String userFirstName;
    private String userSurname;
    private String userEmail;
    private String userPassword;
    private Double userLocationLat;
    private Double userLocationLng;
    private String userLocationName;
    private String userDistanceUnit;
    private String userCurrency;

    private CoordinatorLayout coordinatorlayout;

    private ProgressDialog mProgressDialog;

    private Button mButtonContact;

    private LinearLayout mLinearLayoutUserName;
    private TextView mTextViewFirstName;
    private TextView mTextViewSurname;

    private LinearLayout mLinearLayoutEmail;
    private TextView mTextViewEmail;

    private LinearLayout mLinearLayoutPasswordReset;

    private LinearLayout mLinearLayoutDefaultLocation;
    private TextView mTextViewDefaultLocation;
    private LinearLayout mLinearLayoutDefaultDistanceUnit;
    private TextView mTextViewDefaultDistanceUnit;
    private LinearLayout mLinearLayoutDefaultCurrency;
    private TextView mTextViewDefaultCurrency;

    private LinearLayout mLinearLayoutDeleteJobApplications;
    private LinearLayout mLinearLayoutDeleteJsas;
    private LinearLayout mLinearLayoutDeleteAccount;

    private Dialog mEditUserNameDialog;
    private Dialog mEditContactEmailDialog;
    private AlertDialog mUserReAuthDialog;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;
    private AlertDialog locationAddEditDialog;
    private LinearLayout mLinearLayoutGetUserLocation;
    private AutoCompleteTextView mAutocompleteTextViewLocation;
    private ProgressBar mAutocompleteProgressBar;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLngBounds BOUNDS_USER;

    private Dialog mDialogLocationMap;
    private MapFragment mMapDialogFragmentLocation;

    private AlertDialog mCurrencyListDialog;

    private LinearLayout mLinearLayoutLogout;
    private LinearLayout mLinearLayoutSettingsDelete;

    private SharedPreferences mSharedPref;

    private static final int RC_SIGN_IN = 9001;
    private FirebaseAuth mAuth;

    private String settingsAuthOption;

    private Dialog mDialogSettingsDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_activity_settings);
        /* Common toolbar setup */
        setSupportActionBar(toolbar);
        /* Add back button to the action bar */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mFirebaseRef = FirebaseDatabase.getInstance().getReference();
        mUserRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_USERS).child(userUID);
        mJobApplicationsAllRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);
        mJsasRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);
        mJobApplicationsRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mJobApplicationsPriorityRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);
        mJobApplicationsArchiveRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE);

        Log.d(LOG_TAG, mFirebaseRef.toString());
        Log.d(LOG_TAG, mUserRef.toString());
        Log.d(LOG_TAG, mJobApplicationsAllRef.toString());
        Log.d(LOG_TAG, mJsasRef.toString());

        coordinatorlayout = (CoordinatorLayout) findViewById(R.id.activity_settings_coordinatorlayout);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getResources().getString(R.string.activity_settings_pd_password_reset));
        mProgressDialog.setCancelable(false);

        mButtonContact = (Button) findViewById(R.id.activity_settings_bt_contact);

        mLinearLayoutUserName = (LinearLayout) findViewById(R.id.activity_settings_ll_user_name);
        mTextViewFirstName = (TextView) findViewById(R.id.activity_settings_tv_user_first_name);
        mTextViewSurname = (TextView) findViewById(R.id.activity_settings_tv_user_surname);

        mLinearLayoutEmail = (LinearLayout) findViewById(R.id.activity_settings_ll_user_email);
        mTextViewEmail = (TextView) findViewById(R.id.activity_settings_tv_user_email);

        mLinearLayoutPasswordReset = (LinearLayout) findViewById(R.id.activity_settings_ll_password_reset);

        if (mFirebaseUser.getProviders() != null) {

            strProvider = mFirebaseUser.getProviders().toString().replaceAll("\\[", "").replaceAll("\\]","");

            if (strProvider.equals(Constants.FIREBASE_PROVIDER_GOOGLE)) {
                mLinearLayoutEmail.setVisibility(View.GONE);
                mLinearLayoutPasswordReset.setVisibility(View.GONE);
            }

        }

        mLinearLayoutDefaultLocation = (LinearLayout) findViewById(R.id.activity_settings_ll_location);
        mTextViewDefaultLocation = (TextView) findViewById(R.id.activity_settings_tv_location_name);
        mLinearLayoutDefaultDistanceUnit = (LinearLayout) findViewById(R.id.activity_settings_ll_distance_unit);
        mTextViewDefaultDistanceUnit = (TextView) findViewById(R.id.activity_settings_tv_distance_unit);
        mLinearLayoutDefaultCurrency = (LinearLayout) findViewById(R.id.activity_settings_ll_currency);
        mTextViewDefaultCurrency = (TextView) findViewById(R.id.activity_settings_tv_currency);

        mLinearLayoutLogout = (LinearLayout) findViewById(R.id.activity_settings_ll_logout);

        mLinearLayoutSettingsDelete = (LinearLayout) findViewById(R.id.activity_settings_ll_delete_job_applications);

        //Set saved userLocationLat value if Firebase value does not load in time
        userLocationLat = Double.parseDouble(mSharedPref.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, "0.0"));
        userLocationLng = Double.parseDouble(mSharedPref.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, "0.0"));
        userLocationName = mSharedPref.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, "");

        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mUser = dataSnapshot.getValue(User.class);
                if (mUser == null) {
                    mUser = new User();
                }

                userFirstName = mUser.getFirstName();
                if (userFirstName == null) {
                    userFirstName = "";
                }

                userSurname = mUser.getSurname();
                if (userSurname == null) {
                    userSurname = "";
                }

                userEmail = mUser.getEmail();
                if (userEmail == null) {
                    userEmail = "";
                }

                userPassword = ""; //UserPassword is not stored in firebase database. This is only a holding property.

                if (userLocationLat.equals(0.0)) {
                    if (mUser.getUserLocationLat() != null || !mUser.getUserLocationLat().equals(0.0)) {
                        userLocationLat = mUser.getUserLocationLong();
                    } else {
                        userLocationLat = 0.0;
                    }
                }
                if (userLocationLng.equals(0.0)) {
                    if (mUser.getUserLocationLong() != null || !mUser.getUserLocationLong().equals(0.0)) {
                        userLocationLng = mUser.getUserLocationLong();
                    } else {
                        userLocationLng = 0.0;
                    }
                }

                if (userLocationName.equals("")) {
                    if (mUser.getUserLocationName() != null || !mUser.getUserLocationName().equals("")) {
                        userLocationName = mUser.getUserLocationName();
                    } else {
                        userLocationName = "";
                    }
                }

                BOUNDS_USER = new LatLngBounds(
                        new LatLng(userLocationLat, userLocationLng), new LatLng(userLocationLat, userLocationLng));

                userDistanceUnit = mUser.getUserDistanceUnit();
                userCurrency = mUser.getUserCurrency();

                mTextViewFirstName.setText(userFirstName);
                mTextViewSurname.setText(userSurname);
                mTextViewEmail.setText(userEmail);
                mTextViewDefaultLocation.setText(userLocationName);
                mTextViewDefaultDistanceUnit.setText(userDistanceUnit);
                mTextViewDefaultCurrency.setText(userCurrency);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mButtonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendContactEmail();
            }
        });

        mLinearLayoutUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditUserNameDialog();
            }
        });

        mLinearLayoutEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsAuthOption = Constants.SETTINGS_AUTH_OPTION_CHANGE_EMAIL;
                reAuthUser();
            }
        });

        mLinearLayoutPasswordReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.show();
                sendResetPasswordEmail();
            }
        });


        mLinearLayoutDefaultLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userLocationName.equals("")) {
                    createLocationAddEditDialog();
                } else {
                    createLocationMapDialog();
                }
            }
        });

        mLinearLayoutDefaultDistanceUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDefaultDistanceUnitDialog();
            }
        });

        mLinearLayoutDefaultCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDefaultCurrencyDialog();
            }
        });

        mLinearLayoutLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        mLinearLayoutSettingsDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSettingsDeleteDialog();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendContactEmail() {

        String[] emailTo = {"hi@getjobcatcher.com"};

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailTo);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "JobCatcher Feedback");

        // need this to prompts email client only
        emailIntent.setType("message/rfc822");

        try {
            startActivity(Intent.createChooser(emailIntent, "Choose an email client"));
            Log.d(LOG_TAG, "Email sent.");
        } catch (android.content.ActivityNotFoundException ex) {
            Log.d(LOG_TAG, "Error sending email: " + ex.getLocalizedMessage());
            Toast.makeText(this, "Error sending email: " + ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void createEditUserNameDialog(){

        final AlertDialog.Builder editJobTitleCompanyDialog = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_user_name, null);
        editJobTitleCompanyDialog.setView(editView);
        final EditText mEditTextFirstName = (EditText) editView.findViewById(R.id.dialog_edit_user_name_et_first_name);
        final EditText mEditTextSurname = (EditText) editView.findViewById(R.id.dialog_edit_user_name_et_surname);

        mEditTextFirstName.setText(userFirstName);
        mEditTextSurname.setText(userSurname);

        //Sets cursor to end of EditText
        mEditTextFirstName.setSelection(mEditTextFirstName.getText().length());
        mEditTextSurname.setSelection(mEditTextSurname.getText().length());

        editJobTitleCompanyDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                userFirstName = mEditTextFirstName.getText().toString().trim();
                userSurname = mEditTextSurname.getText().toString().trim();

                    /* Add properties changed to a HashMap */
                HashMap<String, Object> updatedProperties = new HashMap<>();

                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_FIRST_NAME, userFirstName);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_SURNAME, userSurname);

                mUserRef.updateChildren(updatedProperties);

                mTextViewFirstName.setText(userFirstName);
                mTextViewFirstName.setTypeface(null, Typeface.NORMAL);
                mTextViewFirstName.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.list_item_secondary_text));
                mTextViewSurname.setText(userSurname);
                mTextViewSurname.setTypeface(null, Typeface.NORMAL);
                mTextViewSurname.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.list_item_secondary_text));

                mEditUserNameDialog.dismiss();

            }
        });
        editJobTitleCompanyDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditUserNameDialog.dismiss();
            }
        });

        mEditUserNameDialog = editJobTitleCompanyDialog.show();

        if (mEditUserNameDialog.getWindow() != null) {
            mEditUserNameDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createEditUserEmailDialog(){

        final AlertDialog.Builder editUserEmailDialog = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_user_email, null);
        editUserEmailDialog.setView(editView);
        final EditText mEditTextContactEmail = (EditText) editView.findViewById(R.id.dialog_edit_user_email_et);

        mEditTextContactEmail.setText(userEmail);

        mEditTextContactEmail.setSelection(mEditTextContactEmail.getText().length());

        editUserEmailDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                userEmail = mEditTextContactEmail.getText().toString().trim();

                    mFirebaseUser.updateEmail(userEmail)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(LOG_TAG, "User email address updated.");
                                    }
                                }
                            });

                /* Add properties changed to a HashMap */
                HashMap<String, Object> updatedProperties = new HashMap<>();

                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_EMAIL, userEmail);

                mUserRef.updateChildren(updatedProperties);

                mTextViewEmail.setText(userEmail);
                mTextViewEmail.setTypeface(null, Typeface.NORMAL);
                mTextViewEmail.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.list_item_secondary_text));

                mEditContactEmailDialog.dismiss();

            }
        });
        editUserEmailDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditContactEmailDialog.dismiss();
            }
        });

        mEditContactEmailDialog = editUserEmailDialog.show();

        if (mEditContactEmailDialog.getWindow() != null) {
            mEditContactEmailDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createLocationAddEditDialog() {

        final AlertDialog.Builder locationAddAlertDialog = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_location_user_edit, null);
        locationAddAlertDialog.setView(convertView);

        mLinearLayoutGetUserLocation = (LinearLayout) convertView.findViewById(R.id.dialog_location_edit_ll_location_user);
        mAutocompleteTextViewLocation = (AutoCompleteTextView) convertView.findViewById(R.id.dialog_location_edit_autocomplete_create_account);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_edit_progress_bar);

        mAutocompleteTextViewLocation.setText(userLocationName);

        locationAddAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (userLocationName != null) {
                    mTextViewDefaultLocation.setText(userLocationName);

                    setUserLocationData();

                } else {
                    mTextViewDefaultLocation.setText(getResources().getString(R.string.activity_settings_tv_location_name_empty));
                }
                googleApiStop();
                locationAddEditDialog.dismiss();
            }
        });
        locationAddAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiStop();
                locationAddEditDialog.dismiss();
            }
        });

        locationAddAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_location_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiStop();

                userLocationLat = 0.0;
                userLocationLng = 0.0;
                userLocationName = "";

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_LOCATION_LAT, userLocationLat);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_LOCATION_LONG, userLocationLng);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_LOCATION_NAME, "");
                mUserRef.updateChildren(updatedProperties);

                mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, userLocationLat.toString()).apply();
                mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, userLocationLng.toString()).apply();
                mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, userLocationName).apply();

                mTextViewDefaultLocation.setText(getResources().getString(R.string.activity_job_application_new_tv_location));
                mTextViewDefaultLocation.setTypeface(null, Typeface.ITALIC);
                locationAddEditDialog.dismiss();
            }
        });

        locationAddEditDialog = locationAddAlertDialog.show();
        locationAddEditDialog.setCancelable(false);
        locationAddEditDialog.setCanceledOnTouchOutside(false);

        //LOCATION API CODE
        mGoogleApiClient = new GoogleApiClient.Builder(SettingsActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiStart();
        mAutocompleteTextViewLocation = (AutoCompleteTextView) convertView.findViewById(R.id
                .dialog_location_edit_autocomplete_create_account);
        mAutocompleteTextViewLocation.setThreshold(3);
        mAutocompleteTextViewLocation.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, R.layout.list_item_location_autocomplete,
                BOUNDS_USER, null);
        mAutocompleteTextViewLocation.setAdapter(mPlaceArrayAdapter);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_edit_progress_bar);

        mLinearLayoutGetUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(SettingsActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION },
                            PERMISSION_ACCESS_COARSE_LOCATION);
                }

                if (ContextCompat.checkSelfPermission(SettingsActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    System.out.println("locationLat: " + lastLocation);

                    if (lastLocation != null) {
                        userLocationLat = lastLocation.getLatitude();
                        userLocationLng = lastLocation.getLongitude();

                        try {

                            Geocoder geo = new Geocoder(SettingsActivity.this, Locale.getDefault());
                            List<Address> addresses = geo.getFromLocation(userLocationLat, userLocationLng, 1);
                            if (addresses.isEmpty()) {
                                Toast.makeText(SettingsActivity.this, "Waiting for location", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                if (addresses.size() > 0) {

                                    if (addresses.get(0).getLocality() != null) {
                                        userLocationName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
                                    } else {
                                        userLocationName = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getCountryName();
                                    }

                                    Log.d(LOG_TAG, addresses.get(0).toString());

                                    mAutocompleteTextViewLocation.setText(userLocationName);

                                    if (userLocationName != null) {
                                        mTextViewDefaultLocation.setText(userLocationName);
                                    }

                                    googleApiStop();
                                    if (locationAddEditDialog != null) {
                                        locationAddEditDialog.dismiss();
                                    }

                                    setUserLocationData();

                                }

                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace(); // getFromLocation() may sometimes fail
                        }
                    } else {
                        Toast.makeText(SettingsActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        mAutocompleteTextViewLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAutocompleteProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mAutocompleteTextViewLocation.getText().toString().equals("")) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                }
            }
        });

        mAutocompleteTextViewLocation.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = mAutocompleteTextViewLocation.getRight()
                            - mAutocompleteTextViewLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        mAutocompleteTextViewLocation.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

    }

    private void setUserLocationData() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_LOCATION_LAT, userLocationLat);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_LOCATION_LONG, userLocationLng);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_LOCATION_NAME, userLocationName);
        mUserRef.updateChildren(updatedProperties);

        mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, userLocationLat.toString()).apply();
        mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, userLocationLng.toString()).apply();
        mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, userLocationName).apply();
    }

    private void createLocationMapDialog() {

        final AlertDialog.Builder locationMapAlertDialog = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View mapView = View.inflate(this, R.layout.dialog_location_map, null);
        locationMapAlertDialog.setView(mapView);

        locationMapAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_map_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getFragmentManager().beginTransaction().remove(mMapDialogFragmentLocation).commit();
                createLocationAddEditDialog();
                mDialogLocationMap.dismiss();
            }
        });
        locationMapAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_map_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getFragmentManager().beginTransaction().remove(mMapDialogFragmentLocation).commit();
                mDialogLocationMap.dismiss();
            }
        });

        mDialogLocationMap = locationMapAlertDialog.show();
        mDialogLocationMap.setCancelable(false);
        mDialogLocationMap.setCanceledOnTouchOutside(false);

        mMapDialogFragmentLocation = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_detail_activity));

        mMapDialogFragmentLocation.getMapAsync(this);

    }

    private void createDefaultDistanceUnitDialog() {
        final AlertDialog.Builder dialogListDefaultDistance = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_list_distance_unit, null);
        dialogListDefaultDistance.setView(convertView);

        final ListView mListViewDefaultDistanceUnit = (ListView) convertView.findViewById(R.id.dialog_list_distance_unit_lv);

        ArrayAdapter<CharSequence> adapterDefaultDistanceUnit = ArrayAdapter.createFromResource(this, R.array.array_distance_unit, R.layout.list_item_distance_unit);

        mListViewDefaultDistanceUnit.setAdapter(adapterDefaultDistanceUnit);

        mListViewDefaultDistanceUnit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userDistanceUnit = mListViewDefaultDistanceUnit.getItemAtPosition(position).toString();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_DISTANCE_UNIT, userDistanceUnit);
                mUserRef.updateChildren(updatedProperties);


                mSharedPref.edit().putString(Constants.SHARED_PREF_USER_DEFAULT_DISTANCE_UNIT, userDistanceUnit).apply();

                mTextViewDefaultDistanceUnit.setText(userDistanceUnit);
                mCurrencyListDialog.dismiss();
            }
        });

        mCurrencyListDialog = dialogListDefaultDistance.show();

    }

    private void createDefaultCurrencyDialog() {
        final AlertDialog.Builder dialogListCurrency = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_list_currency, null);
        dialogListCurrency.setView(convertView);

        final ListView mListViewCurrency = (ListView) convertView.findViewById(R.id.dialog_list_currency_lv);

        ArrayAdapter<CharSequence> adapterCurrency = ArrayAdapter.createFromResource(this, R.array.array_currency, R.layout.list_item_currency);

        mListViewCurrency.setAdapter(adapterCurrency);

        mListViewCurrency.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userCurrency = mListViewCurrency.getItemAtPosition(position).toString();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_USER_CURRENCY, userCurrency);
                mUserRef.updateChildren(updatedProperties);

                mTextViewDefaultCurrency.setText(userCurrency);

                mCurrencyListDialog.dismiss();
            }
        });

        mCurrencyListDialog = dialogListCurrency.show();

    }

    private final AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            if (item != null) {
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Selected: " + item.description);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            }
        }
    };

    private final ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                mAutocompleteProgressBar.setVisibility(View.GONE);
                return;
            }

            // Selecting the first object buffer.
            final Place place = places.get(0);
            //Removes progress bar
            mAutocompleteProgressBar.setVisibility(View.GONE);

            //Moves text cursor to start of EditText
            mAutocompleteTextViewLocation.setSelection(0);

            String str = place.getLatLng().toString();
            String latlongRemoveStart = str.replace("lat/lng:", "");
            String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
            String[] parts = latlongRemoveEnd.split(",");
            userLocationLat = Double.parseDouble(parts[0]);
            userLocationLng = Double.parseDouble(parts[1]);

            userLocationName = place.getName().toString();
        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    private void googleApiStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    private void googleApiStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiStop();
    }

    @Override
    public void onStop() {
        googleApiStop();
        super.onStop();
    }

    //Google AuthUser: reAuthUser > signIn() > onActivityResult() > firebaseAuthWithGoogle
    private void reAuthUser() {

        if (strProvider.equals(Constants.FIREBASE_PROVIDER_PASSWORD)) {

            createReAuthUserDialog();

        } else if (strProvider.equals(Constants.FIREBASE_PROVIDER_GOOGLE)) {

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
            }

            signIn();

            switch(settingsAuthOption) {
                case Constants.SETTINGS_AUTH_OPTION_CHANGE_EMAIL:
                    createEditUserEmailDialog();
                    break;
                case Constants.SETTINGS_AUTH_OPTION_DELETE_JOB_APPLICATIONS:
                    createDeleteJobApplicationsConfirmDialog();
                    break;
                case Constants.SETTINGS_AUTH_OPTION_DELETE_JOBSITE_AGENCIES:
                    createDeleteJsasConfirmDialog();
                    break;
                case Constants.SETTINGS_AUTH_OPTION_DELETE_ACCOUNT:
                    createDeleteAccountConfirmDialog();
                    break;

            }

        }

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                Toast.makeText(SettingsActivity.this, "Authentication failed. Please try again later.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(LOG_TAG, "firebaseAuthWithGoogle:" + acct.getId());

        // Prompt the user to re-provide their sign-in credentials
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        if (user != null) {
            user.reauthenticate(credential)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.d(LOG_TAG, "User re-authenticated.");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(LOG_TAG, "signInWithCredential", e);
                            Toast.makeText(SettingsActivity.this, "Authentication failed. Please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    });
        }

    }

    private void createReAuthUserDialog(){

        final AlertDialog.Builder userReAuthDialog = new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_user_re_auth, null);
        userReAuthDialog.setView(editView);
        userReAuthDialog.setTitle(getResources().getString(R.string.activity_settings_dialog_user_re_auth_title));
        final EditText mEditTextUserPassword = (EditText) editView.findViewById(R.id.dialog_edit_user_re_auth_et);

        userReAuthDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), null);

        userReAuthDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mUserReAuthDialog.dismiss();
            }
        });

        if (strProvider.equals(Constants.FIREBASE_PROVIDER_PASSWORD)) {
            userReAuthDialog.setNeutralButton(getResources().getString(R.string.dialog_user_re_auth_neutral_button), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mProgressDialog.show();
                    sendResetPasswordEmail();
                }
            });
        }

        mUserReAuthDialog = userReAuthDialog.create();
        mUserReAuthDialog.show();

        if (mUserReAuthDialog.getWindow() != null) {
            mUserReAuthDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        Button mButtonUserReAuth = mUserReAuthDialog.getButton(AlertDialog.BUTTON_POSITIVE);

        mButtonUserReAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPassword = mEditTextUserPassword.getText().toString().trim();

                if (!userPassword.equals("")) {
                    final AuthCredential credential = EmailAuthProvider
                            .getCredential(userEmail, userPassword);

                    // Prompt the user to re-provide their sign-in credentials
                    mFirebaseUser.reauthenticate(credential)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isSuccessful()) {
                                        Log.d(LOG_TAG, "User re-authenticated.");

                                        switch(settingsAuthOption) {
                                            case Constants.SETTINGS_AUTH_OPTION_CHANGE_EMAIL:
                                                createEditUserEmailDialog();
                                                break;
                                            case Constants.SETTINGS_AUTH_OPTION_DELETE_JOB_APPLICATIONS:
                                                createDeleteJobApplicationsConfirmDialog();
                                                break;
                                            case Constants.SETTINGS_AUTH_OPTION_DELETE_JOBSITE_AGENCIES:
                                                createDeleteJsasConfirmDialog();
                                                break;
                                            case Constants.SETTINGS_AUTH_OPTION_DELETE_ACCOUNT:
                                                createDeleteAccountConfirmDialog();
                                                break;

                                        }
                                        mUserReAuthDialog.dismiss();
                                    } else {
                                        Log.d(LOG_TAG, "User re-authentication failed.");
                                        mEditTextUserPassword.setError(getResources().getString(R.string.activity_settings_dialog_user_re_auth_error));
                                    }

                                }
                            });
                } else {
                    Log.d(LOG_TAG, "User re-authentication failed.");
                    mEditTextUserPassword.setError(getResources().getString(R.string.activity_settings_dialog_user_re_auth_empty));
                }

            }
        });

    }

    private void sendResetPasswordEmail() {
        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth.sendPasswordResetEmail(userEmail)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mProgressDialog.dismiss();
                        if (task.isSuccessful()) {
                            Log.d(LOG_TAG, getResources().getString(R.string.dialog_user_re_auth_password_email_sent) + userEmail);
                            Snackbar.make(coordinatorlayout, getResources().getString(R.string.dialog_user_re_auth_password_email_sent) + userEmail, Snackbar.LENGTH_LONG)
                                    .show();

                        }
                    }
                });
    }

    private void createDeleteAccountConfirmDialog() {
        new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle("Delete JobCatcher Account")
                .setMessage("Are you sure you want to delete your JobCatcher account and applications? \n\nThis action cannot be undone.")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAccount();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    private void deleteAccount() {
        deleteJobApplications();
        deleteJsas();
        deleteUser();
        mFirebaseUser.delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(LOG_TAG, "User account deleted.");
                            startActivity(new Intent(SettingsActivity.this,LoginActivity.class));
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(LOG_TAG,"Unable to delete account because: ", e);
            }
        });
    }

    private void createDeleteJobApplicationsConfirmDialog() {
        new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle("Delete JobCatcher Applications")
                .setMessage("Are you sure you want to delete all your JobCatcher applications? \n\nThis action cannot be undone.")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteJobApplications();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    private void createDeleteJsasConfirmDialog() {
        new AlertDialog.Builder(SettingsActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle("Delete all Jobsite/Agency Details")
                .setMessage("Are you sure you want to delete all your Jobsite/Agency Details? \n\nThis action cannot be undone.")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteJsas();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    private void deleteJobApplications() {

        mJobApplicationsAllRef.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(LOG_TAG, getString(R.string.log_error_updating_data) + databaseError.getMessage());
                }
                Snackbar.make(coordinatorlayout, "All Job Applications deleted", Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }

    private void deleteJsas() {
        mJsasRef.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(LOG_TAG, getString(R.string.log_error_updating_data) + databaseError.getMessage());
                }
                Snackbar.make(coordinatorlayout, "All Jobsite/Agency Data deleted", Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }

    private void deleteUser() {
        mUserRef.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(LOG_TAG, getString(R.string.log_error_updating_data) + databaseError.getMessage());
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {

        if (mMapDialogFragmentLocation == null) {
            Toast.makeText(this, getResources().getString(R.string.dialog_location_map_error), Toast.LENGTH_SHORT).show();
        } else {
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            UiSettings mapSettings = map.getUiSettings();
            mapSettings.setZoomControlsEnabled(true);
        }

        LatLng jobApplicationLatLng = new LatLng(userLocationLat, userLocationLng);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(jobApplicationLatLng, 10));
        map.addMarker(new MarkerOptions()
                .position(jobApplicationLatLng)
                .title(userLocationName));

    }

    private void createSettingsDeleteDialog() {
        final AlertDialog.Builder dialogSettingsDelete = new AlertDialog.Builder(this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(this, R.layout.dialog_settings_delete, null);
        dialogSettingsDelete.setView(dialogView);

        TextView mLinearLayoutDeleteJobApplications = (TextView) dialogView.findViewById(R.id.dialog_settings_delete_tv_delete_job_applications);
        TextView mLinearLayoutDeleteJsas = (TextView) dialogView.findViewById(R.id.dialog_settings_delete_tv_delete_jsas);
        TextView mLinearLayoutDeleteAccount = (TextView) dialogView.findViewById(R.id.dialog_settings_delete_tv_delete_account);

        mLinearLayoutDeleteJobApplications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsAuthOption = Constants.SETTINGS_AUTH_OPTION_DELETE_JOB_APPLICATIONS;
                reAuthUser();
            }
        });

        mLinearLayoutDeleteJsas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsAuthOption = Constants.SETTINGS_AUTH_OPTION_DELETE_JOBSITE_AGENCIES;
                reAuthUser();
            }
        });

        mLinearLayoutDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsAuthOption = Constants.SETTINGS_AUTH_OPTION_DELETE_ACCOUNT;
                reAuthUser();
            }
        });

        dialogSettingsDelete.setNegativeButton(getResources().getString(R.string.dialog_negative_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogSettingsDelete.dismiss();
            }

        });

        mDialogSettingsDelete = dialogSettingsDelete.show();
        mDialogSettingsDelete.setCanceledOnTouchOutside(false);
    }

}
