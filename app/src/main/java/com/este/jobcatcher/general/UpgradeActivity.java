package com.este.jobcatcher.general;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.este.jobcatcher.R;
import com.este.jobcatcher.utils.Constants;

/**
 * This Activity deals with the Upgrade process
 */
public class UpgradeActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {

    private static final String LOG_TAG = UpgradeActivity.class.getSimpleName();

    private SharedPreferences sharedPrefs;

    //In-app Purchase/Billing
    private static final String SKU_PREMIUM = "sku_premium";

    private Button mButtonUpgrade;

    BillingProcessor bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_upgrade_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        mButtonUpgrade = (Button) findViewById(R.id.activity_upgrade_bt_upgrade);

        bp = new BillingProcessor(this, getResources().getString(R.string.google_play_iap_base_64) ,this);

        if (bp.loadOwnedPurchasesFromGoogle() && bp.isPurchased(SKU_PREMIUM)) {
            mButtonUpgrade.setVisibility(View.GONE);
            sharedPrefs.edit().putBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, true).apply();
            Log.d(LOG_TAG, " Purchased");
        } else {
            mButtonUpgrade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bp.purchase(UpgradeActivity.this, SKU_PREMIUM);
                }
            });
            Log.d(LOG_TAG, " Not Purchased");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(LOG_TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    @Override
    public void onDestroy() {

        if (bp != null) {
            bp.release();
        }

        super.onDestroy();
    }

    @Override
    public void onBillingInitialized() {
        /*
         * Called when BillingProcessor was initialized and it's ready to purchase
         */
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        Log.d(LOG_TAG, "Purchase is premium upgrade. Congratulating user.");
        alert("Thank you for upgrading to JobCatcher premium!");
        sharedPrefs.edit().putBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, true).apply();
        mButtonUpgrade.setVisibility(View.GONE);
        Log.d(LOG_TAG, SKU_PREMIUM + " purchased");
        onBackPressed();

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        /*
         * Called when some error occurred. See Constants class for more details
         *
         * Note - this includes handling the case where the user canceled the buy dialog:
         * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
         */

        sharedPrefs.edit().putBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, false).apply();
        Log.d(LOG_TAG, SKU_PREMIUM + "not purchased");

    }

    @Override
    public void onPurchaseHistoryRestored() {
        /*
         * Called when purchase history was restored and the list of all owned PRODUCT ID's
         * was loaded from Google Play
         */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

}
