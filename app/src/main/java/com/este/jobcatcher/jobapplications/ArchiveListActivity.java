package com.este.jobcatcher.jobapplications;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.recyclerviewholders.ArchiveListItemHolder;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Calendar;

/**
 * Displays a List of Archived JobApplications
 */
public class ArchiveListActivity extends AppCompatActivity {
    private DatabaseReference mJobApplicationArchiveRef;

    private RecyclerView mRecyclerViewJobApplicationArchives;
    private FirebaseRecyclerAdapter<JobApplication, ArchiveListItemHolder> mRecyclerViewAdapterArchives;
    private Query orderedListQuery;

    private SharedPreferences mSharedPref;
    private String userUID;

    private Calendar currentDateCalendar;
    private Long currentDateCalendarMillis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_application_archive_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_job_application_archive);
        /* Common toolbar setup */
        setSupportActionBar(toolbar);
        /* Add back button to the action bar */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        ///Create calendar for today's date
        currentDateCalendar = Calendar.getInstance();
        currentDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        currentDateCalendar.set(Calendar.MINUTE, 0);
        currentDateCalendar.set(Calendar.SECOND, 0);
        currentDateCalendar.set(Calendar.MILLISECOND, 0);
        currentDateCalendarMillis = currentDateCalendar.getTimeInMillis();

        mJobApplicationArchiveRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE);
        orderedListQuery = mJobApplicationArchiveRef.orderByKey();

        mRecyclerViewJobApplicationArchives = (RecyclerView) findViewById(R.id.activity_job_application_archive_list_tv);

        //Create the adapter
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(false);
        manager.setStackFromEnd(false);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewJobApplicationArchives.setHasFixedSize(false);
        mRecyclerViewJobApplicationArchives.setLayoutManager(manager);

        mRecyclerViewAdapterArchives = new FirebaseRecyclerAdapter<JobApplication, ArchiveListItemHolder>(JobApplication.class, R.layout.list_item_job_application, ArchiveListItemHolder.class, orderedListQuery) {
            @Override
            public void populateViewHolder(ArchiveListItemHolder jobApplicationView, JobApplication jobApplication, int position) {

                jobApplicationView.getArchiveListItemKey(mRecyclerViewAdapterArchives.getRef(position).getKey());
                jobApplicationView.setArchiveListItemJobTitle(jobApplication.getJobTitle());
                jobApplicationView.setArchiveListItemCompanyName(jobApplication.getCompanyName());
                jobApplicationView.setArchiveListItemCurrentApplicationStage(jobApplication.getCurrentApplicationStage());
                jobApplicationView.setArchiveListItemReminderDateTime(jobApplication.getDeadlineDate(), currentDateCalendarMillis);

            }
        };

        mRecyclerViewJobApplicationArchives.setAdapter(mRecyclerViewAdapterArchives);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
