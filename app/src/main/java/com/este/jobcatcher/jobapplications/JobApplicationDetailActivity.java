package com.este.jobcatcher.jobapplications;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.este.jobcatcher.R;
import com.este.jobcatcher.alarms.AlarmServiceBroadcastReceiver;
import com.este.jobcatcher.emailfollowup.FollowupEmailActivity;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Activity to view and modify Job Application Details
 */
public class JobApplicationDetailActivity extends AppCompatActivity {

    //General Variables
    private static final String LOG_TAG = JobApplicationDetailActivity.class.getSimpleName();

    private final Utils utils = new Utils();

    private DatabaseReference mArchiveListRef;
    private DatabaseReference mJobApplicationRef;

    private ValueEventListener mActiveListListener;
    private String jobApplicationId;
    private Boolean applicationPinned;
    private Boolean fromNotification;
    private JobApplication jobApplication;
    private String userUID;
    private SimpleDateFormat sdf;
    private SimpleDateFormat stf;

    //Firebase Object Variables
    private String jobTitle;
    private String companyName;
    private Long reminderDateTime;
    private Integer reminderId;

    //UI - Main
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private TextView mTextViewCompanyName;
    JobApplicationDetailActivityFragmentProgress mFragmentProgress = new JobApplicationDetailActivityFragmentProgress();
    JobApplicationDetailActivityFragmentDetails mFragmentDetails = new JobApplicationDetailActivityFragmentDetails();

    //UI - Dialogs
    private Dialog mDialogJobTitleCompany;
    private Dialog mDialogFromNotification;
    private AlertDialog mDialogReminder;
    private LinearLayout mLinearLayoutReminderEditTime;
    private TextView mTextViewReminderEditTime;
    private LinearLayout mLinearLayoutReminderEditDate;
    private TextView mTextViewReminderEditDate;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_application_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_job_application_details_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent intent = this.getIntent();
        jobApplicationId = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        fromNotification = intent.getBooleanExtra(Constants.INTENT_EXTRA_FROM_REMINDER, false);
        applicationPinned = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);

        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        String dateFormat = "dd MMM yyyy";
        sdf = new SimpleDateFormat(dateFormat, Locale.US);
        String timeFormat = "HH:mm";
        stf = new SimpleDateFormat(timeFormat, Locale.US);

        createFirebaseReferences();

        initialiseLayout();

        addFirebaseListener();

    }

    private void createFirebaseReferences() {

        DatabaseReference mBaseRef = FirebaseDatabase.getInstance().getReference();

        if (applicationPinned) {
            mJobApplicationRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(jobApplicationId);
        } else {
            mJobApplicationRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(jobApplicationId);
        }

        mArchiveListRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE).child(jobApplicationId);
    }

    private void initialiseLayout() {

        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.activity_job_application_details_collapsing_toolbar);
        mTextViewCompanyName = (TextView) findViewById(R.id.activity_job_application_details_tv_company_name);

        mCollapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        mCollapsingToolbarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJobTitleCompanyNameDialog();
            }
        });

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void addFirebaseListener() {

        mActiveListListener = mJobApplicationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                jobApplication = dataSnapshot.getValue(JobApplication.class);

                if (jobApplication == null) {
                    finish();
                    //If Job Application is empty, return to MainActivity
                    return;
                }

                getJobApplicationValues(jobApplication);

                variableNullChecks();

                //If DetailActivity opened from Reminder Notification - open mDialogFromNotification
                if (fromNotification) {
                    createFromNotificationDialog();
                }

                setValuesToUI();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

    }

    private void createJobTitleCompanyNameDialog(){

        final AlertDialog.Builder editJobTitleCompanyDialog = new AlertDialog.Builder(JobApplicationDetailActivity.this, R.style.CustomStyleAlertDialog);
        View editView = View.inflate(this, R.layout.dialog_edit_job_title_company, null);
        editJobTitleCompanyDialog.setView(editView);
        final EditText mEditTextJobTitle = (EditText) editView.findViewById(R.id.dialog_edit_job_title_company_et_job_title);
        final EditText mEditTextCompany = (EditText) editView.findViewById(R.id.dialog_edit_job_title_company_et_company);

        mEditTextJobTitle.setText(jobTitle);
        mEditTextCompany.setText(companyName);

        //Sets cursor to end of EditText
        mEditTextJobTitle.setSelection(mEditTextJobTitle.getText().length());
        mEditTextCompany.setSelection(mEditTextCompany.getText().length());

        editJobTitleCompanyDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_TITLE, mEditTextJobTitle.getText().toString().trim());
                updatedProperties.put(Constants.FIREBASE_PROPERTY_COMPANY_NAME, mEditTextCompany.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobTitleCompany.dismiss();
            }
        });
        editJobTitleCompanyDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobTitleCompany.dismiss();
            }
        });

        mDialogJobTitleCompany = editJobTitleCompanyDialog.show();

        if (mDialogJobTitleCompany.getWindow() != null) {
            mDialogJobTitleCompany.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createDeleteJobApplicationDialog() {
        new AlertDialog.Builder(JobApplicationDetailActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle(this.getString(R.string.dialog_activity_job_application_details_delete_application_title))
                .setMessage(this.getString(R.string.dialog_activity_job_application_details_delete_application_message_p1)
                        + this.getString(R.string.dialog_activity_job_application_details_delete_application_message_p2))
                .setPositiveButton(this.getString(R.string.dialog_activity_job_application_details_delete_application_positive_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteJobApplication();
                        Intent goToMainActivity = new Intent(JobApplicationDetailActivity.this, MainActivity.class);
                        startActivity(goToMainActivity);
                    }
                })
                .setNegativeButton(R.string.dialog_activity_job_application_details_delete_application_negative_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNeutralButton(R.string.dialog_activity_job_application_details_delete_application_neutral_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //deletes any reminders associated with archived job application
                        alarmScheduleServiceDelete();
                        deleteReminder();

                        //copies Job Application to archive list
                        moveFirebaseRecordToArchive();

                        //deletes Job Application at current location
                        deleteJobApplication();

                        Intent goToMainActivity = new Intent(JobApplicationDetailActivity.this, MainActivity.class);
                        goToMainActivity.putExtra("applicationArchived", true);
                        startActivity(goToMainActivity);
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    private void moveFirebaseRecordToArchive() {
        mJobApplicationRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mArchiveListRef.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            System.out.println("moveFirebaseRecordToArchive() failed. firebaseError = %s");
                        }
                        else {
                            System.out.println("moveFirebaseRecordToArchive() Great success!. firebaseError = %s");
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("moveFirebaseRecordToArchive() failed. firebaseError = %s");
            }
        });
    }

    private void deleteJobApplication() {
        mJobApplicationRef.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(LOG_TAG, getString(R.string.log_error_updating_data) + databaseError.getMessage());
                }
            }
        });
    }

    private void createFromNotificationDialog() {
        //This dialog is created when detail activity is accessed from a reminder notification.
        final AlertDialog.Builder fromNotificationAlertDialog = new AlertDialog.Builder(JobApplicationDetailActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(this, R.layout.dialog_from_notification, null);
        fromNotificationAlertDialog.setView(dialogView);
        fromNotificationAlertDialog.setTitle(getResources().getString(R.string.dialog_from_reminder_notification_title));

        TextView mTextViewContent = (TextView) dialogView.findViewById(R.id.dialog_from_notification_content_tv);
        LinearLayout mLinearLayoutSendFollowupEmail = (LinearLayout) dialogView.findViewById(R.id.dialog_from_notification_ll_send_followup_email);
        LinearLayout mLinearLayoutSetNewReminder = (LinearLayout) dialogView.findViewById(R.id.dialog_from_notification_ll_set_new_reminder);
        LinearLayout mLinearLayoutViewJobApplication = (LinearLayout) dialogView.findViewById(R.id.dialog_from_notification_ll_view_job_application);

        if (jobTitle.equals("") && companyName.equals("")) {
            mTextViewContent.setVisibility(View.GONE);
        } else if (jobTitle.equals("") && !companyName.equals("")) {
            mTextViewContent.setText(getResources().getString(R.string.dialog_from_reminder_notification_tv_content1)
                    + companyName
                    + getResources().getString(R.string.dialog_from_reminder_notification_tv_content2));
        } else if (!jobTitle.equals("") && companyName.equals("")) {
            mTextViewContent.setText(getResources().getString(R.string.dialog_from_reminder_notification_tv_content1)
                    + jobTitle
                    + getResources().getString(R.string.dialog_from_reminder_notification_tv_content2));
        } else {
            mTextViewContent.setText(getResources().getString(R.string.dialog_from_reminder_notification_tv_content1)
                    + jobTitle
                    + getResources().getString(R.string.dialog_from_reminder_notification_tv_at)
                    + companyName
                    + getResources().getString(R.string.dialog_from_reminder_notification_tv_content2));
        }

        mLinearLayoutSendFollowupEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEmailFollowUpActivity();
                fromNotification = false;
                mDialogFromNotification.dismiss();
            }
        });

        mLinearLayoutSetNewReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromNotification = false;
                createReminderEditDialog();

                mDialogFromNotification.dismiss();
            }
        });

        mLinearLayoutViewJobApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromNotification = false;
                mDialogFromNotification.dismiss();
            }
        });

        mDialogFromNotification = fromNotificationAlertDialog.show();
        mDialogFromNotification.setCancelable(false);
        mDialogFromNotification.setCanceledOnTouchOutside(false);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present. */
        getMenuInflater().inflate(R.menu.menu_activity_job_application_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_archive_job_application) {
            //deletes any reminders associated with archived job application
            alarmScheduleServiceDelete();
            deleteReminder();

            //copies Job Application to archive list
            moveFirebaseRecordToArchive();

            //deletes Job Application at current location
            deleteJobApplication();

            Intent goToMainActivity = new Intent(this, MainActivity.class);
            goToMainActivity.putExtra(Constants.INTENT_EXTRA_ARCHIVED, true);
            this.startActivity(goToMainActivity);

            return true;
        }

        if (id == R.id.action_delete_job_application) {
            createDeleteJobApplicationDialog();
            return true;
        }

        if (id == android.R.id.home) {

            onBackPressed();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getJobApplicationValues(JobApplication jobApplication) {
        //Get Job Application Property Values
        jobTitle = jobApplication.getJobTitle();
        companyName = jobApplication.getCompanyName();
        reminderDateTime = jobApplication.getReminderDateTime();
        reminderId = jobApplication.getReminderId();
    }

    private void variableNullChecks() {

        if (jobTitle == null) {
            jobTitle = "";
        }

        if (companyName == null) {
            companyName = "";
        }

        if (reminderDateTime == null) {
            reminderDateTime = 0L;
        }

        if (reminderId == null) {
            reminderId = 0;
        }

    }

    private void setValuesToUI() {
        // Set TextViews to Property Values
        if (jobTitle.equals("")) {
            mCollapsingToolbarLayout.setTitle(getResources().getString(R.string.activity_job_application_details_tv_job_title_empty));
        } else {
            mCollapsingToolbarLayout.setTitle(jobTitle);
        }

        if (companyName.equals("")) {
            mTextViewCompanyName.setText(R.string.activity_job_application_details_tv_company_name_empty);
        } else {
            mTextViewCompanyName.setText(companyName);
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        JobApplicationDetailActivity.ViewPagerAdapter adapter = new JobApplicationDetailActivity.ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(mFragmentProgress, "Progress");
        adapter.addFragment(mFragmentDetails, "Details");
        viewPager.setAdapter(adapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void alarmScheduleServiceDelete() {
        Intent alarmServiceIntentDelete = new Intent(JobApplicationDetailActivity.this, AlarmServiceBroadcastReceiver.class);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, reminderDateTime);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, true);
        if (applicationPinned) {
            alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_PINNED, true);
        } else {
            alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        }
        sendBroadcast(alarmServiceIntentDelete, null);
    }

    private void deleteReminder() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_SET, false);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_ID, reminderId);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE, 0L);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
    }

    @Override
    public void onStop() {
        super.onStop();
        // Cleanup when the activity is stopped.
        if (mJobApplicationRef != null) {
            mJobApplicationRef.removeEventListener(mActiveListListener);
        }
    }

    private void goToEmailFollowUpActivity(){
        Intent goToFollowupEmail = new Intent(this, FollowupEmailActivity.class);
        goToFollowupEmail.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
        goToFollowupEmail.putExtra(Constants.INTENT_EXTRA_PINNED, applicationPinned);
        startActivity(goToFollowupEmail);
    }

    public void createReminderEditDialog() {

        final AlertDialog.Builder reminderEditAlertDialog = new AlertDialog.Builder(this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_reminder_edit, null);
        reminderEditAlertDialog.setView(convertView);
        mLinearLayoutReminderEditTime = (LinearLayout) convertView.findViewById(R.id.dialog_reminder_edit_ll_time);
        mLinearLayoutReminderEditDate = (LinearLayout) convertView.findViewById(R.id.dialog_reminder_edit_ll_date);
        mTextViewReminderEditTime = (TextView) convertView.findViewById(R.id.dialog_reminder_edit_tv_time);
        mTextViewReminderEditDate = (TextView) convertView.findViewById(R.id.dialog_reminder_edit_tv_date);

        reminderEditTimeDatePickers();

        String reminderNegativeButtonText;

            mTextViewReminderEditTime.setText(stf.format(reminderDateTime));
            mTextViewReminderEditDate.setText(sdf.format(reminderDateTime));
            reminderNegativeButtonText = getString(R.string.dialog_reminder_edit_negative_button_close);

        reminderEditAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_reminder_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submitReminderEditData();
                mDialogFromNotification.dismiss();
                mDialogReminder.dismiss();
            }
        });
        reminderEditAlertDialog.setNegativeButton(reminderNegativeButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogReminder.dismiss();
            }
        });
        reminderEditAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_reminder_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteReminder();
                alarmScheduleServiceDelete();
                mDialogFromNotification.dismiss();
                mDialogReminder.dismiss();
            }
        });

        mDialogReminder = reminderEditAlertDialog.create();
        mDialogReminder.show();

        mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

    }

    private void submitReminderEditData() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_SET, true);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE, reminderDateTime);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
        alarmScheduleServiceEdit();
    }

    private void alarmScheduleServiceEdit() {
        Intent alarmServiceIntentEdit = new Intent(this, AlarmServiceBroadcastReceiver.class);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, reminderDateTime);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, false);
        if (applicationPinned) {
            alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_PINNED, true);
        } else {
            alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        }
        sendBroadcast(alarmServiceIntentEdit, null);
    }

    private void reminderEditTimeDatePickers() {

        final Calendar c = Calendar.getInstance();

        final TimePickerDialog.OnTimeSetListener reminder_edit_time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);
                mTextViewReminderEditTime.setText(stf.format(c.getTime()));
                reminderDateTime = c.getTimeInMillis();
                if (!mTextViewReminderEditDate.getText().toString().equals("")) {
                    mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        final DatePickerDialog.OnDateSetListener reminder_edit_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mTextViewReminderEditDate.setText(sdf.format(c.getTime()));
                reminderDateTime = c.getTimeInMillis();
                if (!mTextViewReminderEditTime.getText().toString().equals("")) {
                    mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        mLinearLayoutReminderEditTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog reminderTimePickerDialog = new TimePickerDialog(JobApplicationDetailActivity.this,
                        R.style.CustomStyleTimeDatePickerDialog, reminder_edit_time, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
                reminderTimePickerDialog.show();
            }
        });
        mLinearLayoutReminderEditDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog reminderDatePickerDialog = new DatePickerDialog(JobApplicationDetailActivity.this,
                        R.style.CustomStyleTimeDatePickerDialog, reminder_edit_date, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                reminderDatePickerDialog.show();
            }
        });
    }

}
