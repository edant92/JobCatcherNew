package com.este.jobcatcher.jobapplications;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.recyclerviewholders.AssociatedJsaItemHolder;
import com.este.jobcatcher.jsas.JsaDetailActivity;
import com.este.jobcatcher.jsas.JsaNewActivity;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.este.jobcatcher.utils.Utils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * Displays details of a Jobsite/Agency
 */
public class JobApplicationDetailActivityFragmentDetails extends Fragment  implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback {

    private static final String LOG_TAG = JobApplicationDetailActivityFragmentDetails.class.getSimpleName();
    private final Utils utils = new Utils();
    
    private DatabaseReference mBaseRef;
    private DatabaseReference mAssociatedJsaItemRef;
    private DatabaseReference mAssociatedJsaListRef;
    private DatabaseReference mJobApplicationRef;

    private ValueEventListener mActiveListListener;
    private BroadcastReceiver mMessageReceiver;
    private String jobApplicationId;
    private Boolean applicationPinned;
    private String jsaId;
    private JobApplication jobApplication;
    private SimpleDateFormat sdtf;
    private String userUID;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLngBounds BOUNDS_USER;
    private Double mUserDefaultBoundsLat;
    private Double mUserDefaultBoundsLng;
    private String mUserDistanceUnit;
    private String associatedJSAIdName;
    private FirebaseRecyclerAdapter<JobsiteAgency, AssociatedJsaItemHolder> mRecyclerViewAdapterAssociatedJsaList;
    private String progressText;

    //UI - Main
    private LinearLayout mLinearLayoutLocation;
    private TextView mTextViewLocationName;
    private TextView mTextViewLocationDistanceFromUser;
    private LinearLayout mLinearLayoutJobType;
    private TextView mTextViewJobType;
    private TextView mTextViewJobHours;
    private LinearLayout mLinearLayoutWages;
    private TextView mTextViewWageCurrency;
    private TextView mTextViewWageAmount;
    private TextView mTextViewWageFrequency;
    private LinearLayout mLinearLayoutJobReference;
    private TextView mTextViewJobReference;
    private LinearLayout mLinearLayoutJobDescription;
    private TextView mTextViewJobDescription;
    private LinearLayout mLinearLayoutJobAdvertURL;
    private TextView mTextViewJobAdvertURL;
    private LinearLayout mLinearLayoutApplicationNotes;
    private TextView mTextViewApplicationNotes;
    private TextView mTextViewContactTitle;
    private TextView mTextViewContactFirstName;
    private TextView mTextViewContactSurname;
    private TextView mTextViewContactPosition;
    private TextView mTextViewContactEmail;
    private TextView mTextViewContactPhone;
    private LinearLayout mLinearLayoutContactDetailsEdit;
    private FrameLayout mFrameLayoutAssociatedJobsiteAgency;
    private AppCompatImageView mImageViewAssociatedJobsiteAgencyLink;
    private TextView mTextViewAssociatedJSA;
    private AppCompatImageView mImageViewAssociatedJobsiteAgencyClear;
    private AppCompatSeekBar mSeekBarPriority;
    private TextView mTextViewEditPriority;
    private AppCompatImageButton mImageButtonAdditionalInformation;

    //UI - Main
    private Dialog mDialogLocationMap;
    private MapFragment mMapDialogFragmentLocation;
    private Dialog locationEditDialog;
    private AutoCompleteTextView mAutocompleteTextView;
    private ListView mListViewAutoComplete;
    private AppCompatImageView mImageViewLocation;
    private ProgressBar mAutocompleteProgressBar;
    private Dialog mDialogJobTypeDialog;
    private TextView mEditJobTypeDialogTextView;
    private EditText mEditJobTypeDialogEditTextHours;
    private Dialog mDialogJobTypeList;
    private Dialog mDialogWage;
    private TextView mEditWageDialogTextViewWageCurrency;
    private EditText mEditWageDialogEditTextWageAmount;
    private TextView mEditWageDialogTextViewWageFrequency;
    private Dialog mDialogWageCurrencyList;
    private Dialog mDialogWageFrequencyList;
    private Dialog mDialogJobReference;
    private Dialog mDialogJobAdvertUrl;
    private Dialog mDialogJobAdvertUrlAction;
    private Dialog mDialogJobDescription;
    private Dialog mDialogEditNotes;
    private Dialog mDialogContactDetails;
    private Dialog mDialogAssociatedJSAList;
    private Dialog mDialogAdditionalInformation;
    
    //Firebase Object Variables
    private String jobTitle;
    private String companyName;
    private String wageCurrency;
    private Double wageAmount;
    private String wageFrequency;
    private Boolean locationRemote;
    private String locationName;
    private Double locationLat;
    private Double locationLng;
    private Boolean locationSet;
    private String jobReference;
    private String jobDescription;
    private String jobType;
    private Double jobHours;
    private String applicationNotes;
    private String contactTitle;
    private String contactFirstName;
    private String contactSurname;
    private String contactPosition;
    private String contactEmail;
    private String contactPhone;
    private String jobAdvertURL;
    private Integer applicationPriority;
    private Long timestampLastChanged;
    private Long timestampCreated;
    
    //Other Variables
    private LatLng jobApplicationLatLong;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pager_job_application_details, container, false);

        JobApplicationDetailActivity activity = (JobApplicationDetailActivity) getActivity();
        Intent intent = activity.getIntent();
        jobApplicationId = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        applicationPinned = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);
        jsaId = intent.getStringExtra(Constants.INTENT_EXTRA_JSA_ID);

        String dateTimeFormat = "dd MMM, HH:mm";
        sdtf = new SimpleDateFormat(dateTimeFormat, Locale.US);

        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        createFirebaseReferences();

        //Set location bounds from sharedPrefs
        mUserDefaultBoundsLat = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, "0.0"));
        mUserDefaultBoundsLng = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, "0.0"));
        Log.d(LOG_TAG, "Current LatLng: " + mUserDefaultBoundsLat + ", " + mUserDefaultBoundsLng);
        BOUNDS_USER = new LatLngBounds(
                new LatLng(mUserDefaultBoundsLat, mUserDefaultBoundsLng), new LatLng(mUserDefaultBoundsLat, mUserDefaultBoundsLng));
        mUserDistanceUnit = mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_DISTANCE_UNIT, getResources().getStringArray(R.array.array_distance_unit)[0]);

        initialiseLayout(rootView);

        addFirebaseListener();

        if (jsaId != null && !jsaId.equals("")) {
            mAssociatedJsaItemRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(jsaId);
            associateJsaDataToJobApplication();
        }

        createAssociatedJsaBroadcastReceiver();
        
        return rootView;

    }

    private void createFirebaseReferences() {

        mBaseRef = FirebaseDatabase.getInstance().getReference();

        if (applicationPinned) {
            mJobApplicationRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(jobApplicationId);
        } else {
            mJobApplicationRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(jobApplicationId);
        }

    }

    private void initialiseLayout(View rootView) {

        mLinearLayoutLocation = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_location);
        mTextViewLocationName = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_location_name);
        mTextViewLocationDistanceFromUser = (TextView) rootView.findViewById(R.id.tv_location_distance_from_default);
        mLinearLayoutJobType = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_job_type);
        mTextViewJobType = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_job_type);
        mTextViewJobHours = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_job_hours);
        mLinearLayoutWages = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_wages);
        mTextViewWageCurrency = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_wage_currency);
        mTextViewWageAmount = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_wage_amount);
        mTextViewWageFrequency = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_wage_frequency);
        mLinearLayoutJobReference = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_job_reference);
        mTextViewJobReference = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_job_reference);
        mLinearLayoutJobDescription = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_job_description);
        mTextViewJobDescription = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_job_description);
        mLinearLayoutJobAdvertURL = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_job_advert_url);
        mTextViewJobAdvertURL = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_job_advert_url);
        mLinearLayoutApplicationNotes = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_ll_application_notes);
        mTextViewApplicationNotes = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_application_notes);
        mTextViewContactTitle = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_contact_title);
        mTextViewContactFirstName = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_contact_first_name);
        mTextViewContactSurname = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_contact_surname);
        mTextViewContactPosition = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_contact_position);
        mTextViewContactEmail = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_contact_email);
        mTextViewContactPhone = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_contact_phone);
        mLinearLayoutContactDetailsEdit = (LinearLayout) rootView.findViewById(R.id.pager_job_application_details_cv_contact_details);
        mFrameLayoutAssociatedJobsiteAgency = (FrameLayout) rootView.findViewById(R.id.pager_job_application_details_fl_associated_jsa);
        mImageViewAssociatedJobsiteAgencyLink = (AppCompatImageView) rootView.findViewById(R.id.pager_job_application_details_iv_associated_jsa_link);
        mTextViewAssociatedJSA = (TextView) rootView.findViewById(R.id.pager_job_application_details_tv_associated_jsa);
        mImageViewAssociatedJobsiteAgencyClear = (AppCompatImageView) rootView.findViewById(R.id.pager_job_application_details_ib_associated_jsa_clear);
        mSeekBarPriority = (AppCompatSeekBar) rootView.findViewById(R.id.activity_ja_details_sb_priority);
        mTextViewEditPriority = (TextView) rootView.findViewById(R.id.activity_ja_details_tv_priority);
        mImageButtonAdditionalInformation = (AppCompatImageButton) rootView.findViewById(R.id.pager_job_application_details_ib_dates);

        mLinearLayoutJobType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJobTypeDialog();
            }
        });

        mLinearLayoutWages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWagesDialog();
            }
        });

        mLinearLayoutJobReference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJobReferenceDialog();
            }
        });

        mLinearLayoutJobDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJobDescriptionDialog();
            }
        });

        mLinearLayoutJobAdvertURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (jobAdvertURL.equals("")) {
                    createJobAdvertUrlDialog();
                } else {
                    createJobAdvertUrlActionSelect();
                }
            }
        });

        mLinearLayoutApplicationNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNotesDialog();
            }
        });

        mLinearLayoutContactDetailsEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createContactDetailsDialog();
            }
        });

        mImageViewAssociatedJobsiteAgencyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAssociatedJSAListDialog();
            }
        });

        mFrameLayoutAssociatedJobsiteAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageViewAssociatedJobsiteAgencyClear.getVisibility() == View.GONE) {
                    createAssociatedJSAListDialog();
                } else {
                    Intent goToJSADetailActivity = new Intent(getContext(), JsaDetailActivity.class);
                    goToJSADetailActivity.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
                    goToJSADetailActivity.putExtra(Constants.INTENT_EXTRA_JSA_ID, jsaId);
                    goToJSADetailActivity.putExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY_JOB_APPLICATION_DETAIL, true);
                    getContext().startActivity(goToJSADetailActivity);
                }
            }
        });

        mImageViewAssociatedJobsiteAgencyClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                associatedJobsiteAgencyClear();
            }
        });

    }

    private void addFirebaseListener() {

        mActiveListListener = mJobApplicationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                jobApplication = dataSnapshot.getValue(JobApplication.class);

                if (jobApplication == null) {
                    getActivity().finish();
                    //If Job Application is empty, return to MainActivity
                    return;
                }

                getJobApplicationValues(jobApplication);

                variableNullChecks();
                
                jobApplicationLatLong = new LatLng(locationLat, locationLng);

                setValuesToUI();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

    }

    private void createAssociatedJsaBroadcastReceiver() {
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Called when Associated Jsa RecyclerView Item is clicked
                mAssociatedJsaItemRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(jsaId);
                associateJsaDataToJobApplication();
                mDialogAssociatedJSAList.dismiss();
            }
        };

    }

    private void associateJsaDataToJobApplication() {

        mAssociatedJsaItemRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                jsaId = dataSnapshot.getKey();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID, jsaId);
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);

                JobsiteAgency associatedJsaItemData = dataSnapshot.getValue(JobsiteAgency.class);

                String associatedJsaName = associatedJsaItemData.getJsaName();

                if (!associatedJsaName.equals("")) {
                    mTextViewAssociatedJSA.setText(associatedJsaName);
                    mTextViewAssociatedJSA.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
                } else {
                    mTextViewAssociatedJSA.setText(getResources().getString(R.string.pager_job_application_details_tv_associated_jsa_link_empty));
                    mTextViewAssociatedJSA.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                }

                mImageViewAssociatedJobsiteAgencyClear.setVisibility(View.VISIBLE);
                mImageViewAssociatedJobsiteAgencyLink.setVisibility(View.GONE);
                mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));

                if (mDialogAssociatedJSAList != null) {
                    mDialogAssociatedJSAList.dismiss();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void getJobApplicationValues(JobApplication jobApplication) {
        //Get Job Application Property Values
        jobTitle = jobApplication.getJobTitle();
        companyName = jobApplication.getCompanyName();
        wageCurrency = jobApplication.getWageCurrency();
        wageAmount = jobApplication.getWageAmount();
        wageFrequency = jobApplication.getWageFrequency();
        locationRemote = jobApplication.getLocationRemote();
        locationName = jobApplication.getLocationName();
        locationLat = jobApplication.getLocationLat();
        locationLng = jobApplication.getLocationLong();
        locationSet = jobApplication.getLocationSet();
        jobReference = jobApplication.getJobReference();
        jobDescription = jobApplication.getJobDescription();
        jobType = jobApplication.getJobType();
        jobHours = jobApplication.getJobHours();
        applicationNotes = jobApplication.getApplicationNotes();
        jobAdvertURL = jobApplication.getJobAdvertURL();
        applicationPriority = jobApplication.getApplicationPriority();
        contactTitle = jobApplication.getContactTitle();
        contactFirstName = jobApplication.getContactFirstName();
        contactSurname = jobApplication.getContactSurname();
        contactPosition = jobApplication.getContactPosition();
        contactEmail = jobApplication.getContactEmail();
        contactPhone = jobApplication.getContactPhone();
        jsaId = jobApplication.getAssociatedJobsiteAgencyID();
        timestampCreated = jobApplication.getTimestampCreatedLong();
        timestampLastChanged = jobApplication.getTimestampLastChangedLong();
    }

    private void variableNullChecks() {

        if (jobTitle == null) {
            jobTitle = "";
        }

        if (companyName == null) {
            companyName = "";
        }
        
        if (wageCurrency == null) {
            wageCurrency = getResources().getStringArray(R.array.array_currency)[0];
        }

        if (wageAmount == null) {
            wageAmount = 0.00;
        }

        if (wageFrequency == null) {
            wageFrequency = getResources().getStringArray(R.array.array_wage_frequency)[0];
        }

        if (locationRemote == null) {
            locationRemote = false;
        }

        if (locationName == null) {
            locationName = "";
        }

        if (locationLat == null) {
            locationLat = 0.0;
        }

        if (locationLng == null) {
            locationLng = 0.0;
        }

        if (locationSet == null) {
            locationSet = false;
        }

        if (jobReference == null) {
            jobReference = "";
        }

        if (jobDescription == null) {
            jobDescription = "";
        }

        if (jobType == null) {
            jobType = getResources().getStringArray(R.array.array_job_type)[0];
        }

        if (jobHours == null) {
            jobHours = 0.0;
        }

        if (applicationNotes == null) {
            applicationNotes = "";
        }

        if (jobAdvertURL == null) {
            jobAdvertURL = "";
        }

        if (applicationPriority == null) {
            applicationPriority = 2;
        }

        if (contactTitle == null) {
            contactTitle = "";
        }

        if (contactFirstName == null) {
            contactFirstName = "";
        }

        if (contactSurname == null) {
            contactSurname = "";
        }

        if (contactPosition == null) {
            contactPosition = "";
        }

        if (contactEmail == null) {
            contactEmail = "";
        }

        if (contactPhone == null) {
            contactPhone = "";
        }

        if (jsaId == null) {
            jsaId = "";
        }

        if (timestampLastChanged == null) {
            timestampLastChanged = 0L;
        }

        if (timestampCreated == null) {
            timestampCreated = 0L;
        }

    }

    private void setValuesToUI() {
        // Set TextViews to Property Values

        if (locationName.equals("")) {
            mTextViewLocationName.setText(R.string.pager_job_application_details_tv_location_name);
            mTextViewLocationDistanceFromUser.setText(R.string.pager_job_application_details_tv_location_empty);
            mLinearLayoutLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createLocationEditDialog();
                }
            });
        } else if (locationRemote) {
            mTextViewLocationName.setText(locationName);
            mTextViewLocationDistanceFromUser.setText(R.string.pager_job_application_details_tv_location_empty);
            mLinearLayoutLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createLocationEditDialog();
                }
            });
        } else {
            mTextViewLocationName.setText(locationName);
            mTextViewLocationDistanceFromUser.setText(getDistance(jobApplicationLatLong, new LatLng(mUserDefaultBoundsLat, mUserDefaultBoundsLng)));
            mLinearLayoutLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createLocationMapDialog();
                }
            });
        }

        if ((jobType.equals("")) && (jobHours == 0.00)) {
            mTextViewJobType.setText(getResources().getString(R.string.pager_job_application_details_tv_job_type_empty));
            mTextViewJobHours.setText("");
        } else {
            mTextViewJobType.setText(jobType);
            mTextViewJobHours.setText(getResources().getString(R.string.pager_job_application_details_tv_job_type_hours, String.format(Locale.UK, "%.1f", jobHours)));
        }

        mTextViewWageCurrency.setText(wageCurrency);
        mTextViewWageAmount.setText(String.format(Locale.UK, "%.2f", wageAmount));
        mTextViewWageFrequency.setText(wageFrequency);

        if (jobReference.equals("")) {
            mTextViewJobReference.setText(R.string.pager_job_application_details_tv_job_reference_empty);
        } else {
            mTextViewJobReference.setText(jobReference);
        }

        if (jobDescription.equals("")) {
            mTextViewJobDescription.setText(R.string.pager_job_application_details_tv_job_description_empty);
        } else {
            mTextViewJobDescription.setText(jobDescription);
        }

        if (applicationNotes.equals("")) {
            mTextViewApplicationNotes.setText(R.string.pager_job_application_details_tv_application_notes_empty);
        } else {
            mTextViewApplicationNotes.setText(applicationNotes);
        }

        if (jobAdvertURL.equals("")) {
            mTextViewJobAdvertURL.setText(R.string.pager_job_application_details_tv_job_advert_url_empty);
        } else {
            mTextViewJobAdvertURL.setText(jobAdvertURL);
        }

        mSeekBarPriority.setProgress(applicationPriority);
        progressText = getResources().getStringArray(R.array.array_job_priority)[applicationPriority];
        mTextViewEditPriority.setText(progressText);
        mSeekBarPriority.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                applicationPriority = progressValue;
                progressText = getResources().getStringArray(R.array.array_job_priority)[applicationPriority];
                mTextViewEditPriority.setText(progressText);
                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_APPLICATION_PRIORITY, applicationPriority);
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        mImageButtonAdditionalInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAdditionalInformationDialog();
            }
        });

        mTextViewContactTitle.setText(contactTitle);
        mTextViewContactFirstName.setText(contactFirstName);
        mTextViewContactSurname.setText(contactSurname);
        mTextViewContactPosition.setText(contactPosition);

        if (contactEmail.equals("")) {
            mTextViewContactEmail.setText(getResources().getString(R.string.pager_job_application_details_tv_contact_email_empty));
        } else {
            mTextViewContactEmail.setText(contactEmail);
        }

        if (contactPhone.equals("")) {
            mTextViewContactPhone.setText(getResources().getString(R.string.pager_job_application_details_tv_contact_phone_empty));
        } else {
            mTextViewContactPhone.setText(contactPhone);
        }

        if (!jsaId.equals("")) {
            //If there is an AssociatedJsaId in the Job Application Object
            DatabaseReference jsaIdRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(jsaId);

            jsaIdRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    JobsiteAgency jsa = dataSnapshot.getValue(JobsiteAgency.class);

                    if (jsa != null) {
                        //If the AssociatedJsa Object IS NOT empty
                        associatedJSAIdName = jsa.getJsaName();
                        mTextViewAssociatedJSA.setText(associatedJSAIdName);
                        mTextViewAssociatedJSA.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
                        mImageViewAssociatedJobsiteAgencyClear.setVisibility(View.VISIBLE);
                        mImageViewAssociatedJobsiteAgencyLink.setVisibility(View.GONE);
                        mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                    } else {
                        //If the AssociatedJsa Object IS empty e.g. JSA has been deleted.
                        Toast.makeText(getContext(), "Associated JSA no longer exists. \nPlease set another.", Toast.LENGTH_SHORT).show();

                        jsaId = "";

                        HashMap<String, Object> updateAssociatedJSA = new HashMap<>();
                        updateAssociatedJSA.put(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID, jsaId);
                        mJobApplicationRef.updateChildren(updateAssociatedJSA);
                        utils.updateTimestampLastChanged(mJobApplicationRef);

                        mTextViewAssociatedJSA.setText(getResources().getString(R.string.pager_job_application_details_tv_associated_jsa_link_empty));
                        mTextViewAssociatedJSA.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println(LOG_TAG + getString(R.string.log_error_the_read_failed) + databaseError.getMessage());
                }
            });

        } else {
            //If there is no AssociatedJsaId in the Job Application Object
            mTextViewAssociatedJSA.setText(getResources().getString(R.string.pager_job_application_details_tv_associated_jsa_link_empty));
            mTextViewAssociatedJSA.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
        }
        
    }

    private void createLocationMapDialog() {

        final AlertDialog.Builder locationMapAlertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View mapView = View.inflate(getContext(), R.layout.dialog_location_map, null);
        locationMapAlertDialog.setView(mapView);

        locationMapAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_map_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().getFragmentManager().beginTransaction().remove(mMapDialogFragmentLocation).commit();
                createLocationEditDialog();
                mDialogLocationMap.dismiss();
            }
        });
        locationMapAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_map_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().getFragmentManager().beginTransaction().remove(mMapDialogFragmentLocation).commit();
                mDialogLocationMap.dismiss();
            }
        });

        mDialogLocationMap = locationMapAlertDialog.show();
        mDialogLocationMap.setCancelable(false);
        mDialogLocationMap.setCanceledOnTouchOutside(false);

        mMapDialogFragmentLocation = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map_detail_activity));

        mMapDialogFragmentLocation.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {

                if (map != null) {
                    if (mMapDialogFragmentLocation == null) {
                        Toast.makeText(getContext(), getResources().getString(R.string.dialog_location_map_error), Toast.LENGTH_SHORT).show();
                    } else {
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        UiSettings mapSettings = map.getUiSettings();
                        mapSettings.setZoomControlsEnabled(true);
                    }

                    LatLng jobApplicationLatLong = new LatLng(locationLat, locationLng);

                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(jobApplicationLatLong, 10));
                    map.addMarker(new MarkerOptions()
                            .position(jobApplicationLatLong)
                            .title(jobTitle)
                            .snippet(companyName));
                }

            }
        });

    }

    private void createLocationEditDialog() {

        final AlertDialog.Builder locationAddAlertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_location_edit, null);
        locationAddAlertDialog.setView(convertView);

        mImageViewLocation = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_edit_iv_location_icon);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_edit_progress_bar);
        mAutocompleteTextView = (AutoCompleteTextView) convertView.findViewById(R.id.dialog_location_edit_autocomplete_create_account);
        mListViewAutoComplete = (ListView) convertView.findViewById(R.id.activity_map_edit_lv_search_result);
        final CheckBox mCheckBoxNoLocation = (CheckBox) convertView.findViewById(R.id.dialog_location_edit_cb_no_location);
        final LinearLayout mLinearLayoutLocationUser = (LinearLayout) convertView.findViewById(R.id.dialog_location_edit_ll_location_user);
        final AppCompatImageView mImageViewLocationUser = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_edit_iv_location_user_icon);
        final TextView mTextViewLocationUser = (TextView) convertView.findViewById(R.id.dialog_location_edit_tv_location_user);

        //LOCATION API CODE
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiStart();
        if (locationName != null) {
            mAutocompleteTextView.setText(locationName);
            mAutocompleteTextView.setSelection(mAutocompleteTextView.getText().length());
        }
        setAutoCompleteOnClickListener();
        mAutocompleteTextView.setThreshold(2);
        mAutocompleteTextView.setDropDownHeight(0);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), R.layout.list_item_location_autocomplete,BOUNDS_USER, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);
        mListViewAutoComplete.setAdapter(mPlaceArrayAdapter);

        mCheckBoxNoLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mAutocompleteTextView.setEnabled(false);
                    mAutocompleteTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mLinearLayoutLocationUser.setEnabled(false);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mTextViewLocationUser.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused), ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                } else {
                    mAutocompleteTextView.setEnabled(true);
                    mAutocompleteTextView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
                    mLinearLayoutLocationUser.setEnabled(true);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black));
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(android.R.color.black, android.R.color.black);
                    mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                }
            }
        });

        if (locationRemote) {
            mCheckBoxNoLocation.setChecked(true);
        } else {
            mCheckBoxNoLocation.setChecked(false);
        }

        mLinearLayoutLocationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserLocation();
            }
        });

        mAutocompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mListViewAutoComplete.getCount() > 0) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                    if (mListViewAutoComplete.getAdapter() == null){
                        mListViewAutoComplete.setAdapter(mPlaceArrayAdapter);
                    }
                } else if (mAutocompleteTextView.getText().toString().length() > 2) {
                    mAutocompleteProgressBar.setVisibility(View.VISIBLE);
                    mImageViewLocation.setVisibility(View.GONE);
                    if (mListViewAutoComplete.getAdapter() == null){
                        mListViewAutoComplete.setAdapter(mPlaceArrayAdapter);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mListViewAutoComplete.getCount() > 0) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                }
            }
        });

        mAutocompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = mAutocompleteTextView.getRight()
                            - mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        mAutocompleteTextView.setText("");
                        mListViewAutoComplete.setAdapter(null);
                        return true;
                    }
                }
                return false;
            }
        });

        locationAddAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (mCheckBoxNoLocation.isChecked()) {
                    locationRemote = true;
                    locationName = "Remote";
                    locationLat = 0.0;
                    locationLng = 0.0;
                    locationSet = false;
                    setLocationEditData();
                } else {
                    if (locationName != null) {
                        setLocationEditData();
                    }
                }

                googleApiStop();
                locationEditDialog.dismiss();
            }
        });
        locationAddAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_location_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                locationRemote = false;
                locationName = "";
                locationLat = 0.0;
                locationLng = 0.0;
                locationSet = false;
                setLocationEditData();
                googleApiStop();
                locationEditDialog.dismiss();
            }
        });
        locationAddAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiStop();
                locationEditDialog.dismiss();
            }
        });

        locationEditDialog = locationAddAlertDialog.show();
        locationEditDialog.setCancelable(false);
        locationEditDialog.setCanceledOnTouchOutside(false);

    }

    private void getUserLocation() {
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            System.out.println("locationLat: " + lastLocation);

            if (lastLocation != null) {

                locationLat = lastLocation.getLatitude();
                locationLng = lastLocation.getLongitude();
                locationSet = true;
                locationRemote = false;

                try {

                    Geocoder geo = new Geocoder(getContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(locationLat, locationLng, 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(getContext(), "Waiting for location", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (addresses.size() > 0) {
                            if (addresses.get(0).getLocality() != null) {
                                locationName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
                            } else {
                                locationName = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getCountryName();
                            }

                            Log.d(LOG_TAG, addresses.get(0).toString());

                            if (locationName != null) {
                                mTextViewLocationName.setText(locationName);
                                mTextViewLocationDistanceFromUser.setText(getDistance(new LatLng(locationLat, locationLng), new LatLng(mUserDefaultBoundsLat, mUserDefaultBoundsLng)));
                            }

                            googleApiStop();
                            if (locationEditDialog != null) {
                                locationEditDialog.dismiss();
                            }

                            setLocationEditData();

                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.dialog_location_edit_tv_user_location_error), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private String getDistance(LatLng start_latlong, LatLng end_latlong){
        Location l1=new Location("One");
        l1.setLatitude(start_latlong.latitude);
        l1.setLongitude(start_latlong.longitude);

        Location l2=new Location("Two");
        l2.setLatitude(end_latlong.latitude);
        l2.setLongitude(end_latlong.longitude);

        if (end_latlong.latitude == 0.0 && end_latlong.longitude == 0) {
            return getResources().getString(R.string.pager_job_application_details_tv_distance_no_user_location);
        }

        float distance=l1.distanceTo(l2);

        String distanceString = getResources().getString(R.string.pager_job_application_details_tv_distance_from_user_1);
        if (mUserDistanceUnit.equals(getResources().getStringArray(R.array.array_distance_unit)[0])) {
            distance = (distance / 1000.0f) * 0.621371f;
            if (distance>1.609f) {
                distanceString = distanceString.concat(getResources().getString(R.string.pager_job_application_details_tv_distance_unit_miles, String.format(Locale.UK, "%.0f", distance)));
            } else {
                distanceString = distanceString.concat(getResources().getString(R.string.pager_job_application_details_tv_distance_unit_miles_under));
            }
        } else if (mUserDistanceUnit.equals(getResources().getStringArray(R.array.array_distance_unit)[1])) {
            distance = distance / 1000.0f;
            if(distance>1.0f) {
                distanceString = distanceString.concat(getResources().getString(R.string.pager_job_application_details_tv_distance_unit_kilometres, String.format(Locale.UK, "%.0f", distance)));
            } else {
                distanceString = distanceString.concat(getResources().getString(R.string.pager_job_application_details_tv_distance_unit_kilometres_under));
            }
        }

        return distanceString;
    }

    private void setAutoCompleteOnClickListener() {
        AdapterView.OnItemClickListener mAutocompleteClickListener
                = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
                if (item != null) {
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Selected: " + item.description);
                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
                    setPlaceDetailsCallback(placeResult);
                    Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
                }
            }
        };
        mListViewAutoComplete.setOnItemClickListener(mAutocompleteClickListener);
    }

    private void setPlaceDetailsCallback(PendingResult<PlaceBuffer> placeResult) {
        ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(@NonNull PlaceBuffer places) {
                if (!places.getStatus().isSuccess()) {
                    Log.e(LOG_TAG, "Place query did not complete. Error: " +
                            places.getStatus().toString());
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                    return;
                }

                // Selecting the first object buffer.
                final Place place = places.get(0);

                //Removes progress bar
                mAutocompleteProgressBar.setVisibility(View.GONE);
                mImageViewLocation.setVisibility(View.VISIBLE);

                String str = place.getLatLng().toString();
                String latlongRemoveStart = str.replace("lat/lng:", "");
                String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
                String[] parts = latlongRemoveEnd.split(",");
                locationLat = Double.parseDouble(parts[0]);
                locationLng = Double.parseDouble(parts[1]);

                locationName = place.getName().toString();

                locationRemote = false;
                locationSet = true;

                //Moves text cursor to end of EditText
                mAutocompleteTextView.setText(locationName);
                mAutocompleteTextView.setSelection(mAutocompleteTextView.getText().length());

                mListViewAutoComplete.setAdapter(null);

            }
        };

        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
    }

    private void setLocationEditData() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, locationRemote);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, locationName);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, locationLat);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, locationLng);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, locationSet);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
    }

    private void googleApiStart() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    private void googleApiStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    private void createJobTypeDialog(){

        final AlertDialog.Builder editJobTypeDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_job_type, null);
        editJobTypeDialog.setView(editView);
        mEditJobTypeDialogTextView = (TextView) editView.findViewById(R.id.activity_ja_new_tv_job_type);
        mEditJobTypeDialogEditTextHours = (EditText) editView.findViewById(R.id.dialog_edit_job_type_et_hours);

        if (jobType.equals("")) {
            mEditJobTypeDialogTextView.setText(getResources().getString(R.string.pager_job_application_details_tv_job_type_empty));
        } else {
            mEditJobTypeDialogTextView.setText(jobType);
        }

        mEditJobTypeDialogEditTextHours.setText(String.valueOf(jobHours));
        mEditJobTypeDialogEditTextHours.setSelection(mEditJobTypeDialogEditTextHours.getText().length());

        mEditJobTypeDialogTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJobTypeListDialog();
            }
        });

        editJobTypeDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                /* Add properties changed to a HashMap */
                HashMap<String, Object> updatedProperties = new HashMap<>();
                try {
                    jobHours = Double.parseDouble(mEditJobTypeDialogEditTextHours.getText().toString().trim());
                } catch (NumberFormatException e) {
                    jobHours = 0.00;
                }
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, mEditJobTypeDialogTextView.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);

                mDialogJobTypeDialog.dismiss();

            }
        });
        editJobTypeDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobTypeDialog.dismiss();
            }
        });

        mDialogJobTypeDialog = editJobTypeDialog.show();

        if (mDialogJobTypeDialog.getWindow() != null) {
            mDialogJobTypeDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createJobTypeListDialog() {
        final AlertDialog.Builder dialogListJobType = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_list_job_type, null);
        dialogListJobType.setView(convertView);

        final ListView mListViewJobType = (ListView) convertView.findViewById(R.id.dialog_list_job_type_lv);

        ArrayAdapter<CharSequence> adapterJobtype = ArrayAdapter.createFromResource(getContext(), R.array.array_job_type, R.layout.list_item_job_type);

        mListViewJobType.setAdapter(adapterJobtype);

        dialogListJobType.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobTypeList.dismiss();
            }

        });

        mListViewJobType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = mListViewJobType.getItemAtPosition(position).toString();
                mEditJobTypeDialogTextView.setText(text);
                mDialogJobTypeList.dismiss();
            }
        });

        mDialogJobTypeList = dialogListJobType.show();

    }

    private void createWagesDialog(){

        final AlertDialog.Builder editWagesDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_wages, null);
        editWagesDialog.setView(editView);
        mEditWageDialogTextViewWageCurrency = (TextView) editView.findViewById(R.id.dialog_edit_wages_tv_currency);
        mEditWageDialogEditTextWageAmount = (EditText) editView.findViewById(R.id.dialog_edit_wages_et_amount);
        mEditWageDialogTextViewWageFrequency = (TextView) editView.findViewById(R.id.dialog_edit_wages_tv_frequency);

        mEditWageDialogTextViewWageCurrency.setText(wageCurrency);

        if (jobApplication.getWageAmount() == 0.00) {
            mEditWageDialogEditTextWageAmount.setHint(String.format(Locale.UK, "%.2f", jobApplication.getWageAmount()));
        } else {
            mEditWageDialogEditTextWageAmount.setText(String.format(Locale.UK, "%.2f", jobApplication.getWageAmount()));
        }

        mEditWageDialogTextViewWageFrequency.setText(jobApplication.getWageFrequency());

        mEditWageDialogEditTextWageAmount.setSelection(mEditWageDialogEditTextWageAmount.getText().length());

        mEditWageDialogTextViewWageCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWageCurrencyListDialog();
            }
        });

        mEditWageDialogTextViewWageFrequency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWageFrequencyListDialog();
            }
        });

        editWagesDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    wageAmount = Double.parseDouble(mEditWageDialogEditTextWageAmount.getText().toString().trim());
                } catch (NumberFormatException e) {
                    wageAmount = 0.00;
                }

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_WAGE_CURRENCY, mEditWageDialogTextViewWageCurrency.getText().toString().trim());
                updatedProperties.put(Constants.FIREBASE_PROPERTY_WAGE_AMOUNT, wageAmount);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_WAGE_FREQUENCY, mEditWageDialogTextViewWageFrequency.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);

                mDialogWage.dismiss();

            }
        });
        editWagesDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogWage.dismiss();
            }
        });

        mDialogWage = editWagesDialog.show();

    }

    private void createWageCurrencyListDialog() {
        final AlertDialog.Builder dialogListWageCurrency = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_list_currency, null);
        dialogListWageCurrency.setView(convertView);

        final ListView mListViewWageCurrency = (ListView) convertView.findViewById(R.id.dialog_list_currency_lv);

        ArrayAdapter<CharSequence> adapterWageCurrency = ArrayAdapter.createFromResource(getContext(), R.array.array_currency, R.layout.list_item_currency);

        mListViewWageCurrency.setAdapter(adapterWageCurrency);

        dialogListWageCurrency.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogWageCurrencyList.dismiss();
            }

        });

        mListViewWageCurrency.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = mListViewWageCurrency.getItemAtPosition(position).toString();
                mEditWageDialogTextViewWageCurrency.setText(text);
                mDialogWageCurrencyList.dismiss();
            }
        });

        mDialogWageCurrencyList = dialogListWageCurrency.show();

    }

    private void createWageFrequencyListDialog() {
        final AlertDialog.Builder dialogListWageFrequency = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_list_wage_frequency, null);
        dialogListWageFrequency.setView(convertView);

        final ListView mListViewWageFrequency = (ListView) convertView.findViewById(R.id.dialog_list_wage_frequency_lv);

        ArrayAdapter<CharSequence> adapterWageCurrency = ArrayAdapter.createFromResource(getContext(), R.array.array_wage_frequency, R.layout.list_item_wage_frequency);

        mListViewWageFrequency.setAdapter(adapterWageCurrency);

        dialogListWageFrequency.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogWageFrequencyList.dismiss();
            }

        });

        mListViewWageFrequency.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = mListViewWageFrequency.getItemAtPosition(position).toString();
                mEditWageDialogTextViewWageFrequency.setText(text);
                mDialogWageFrequencyList.dismiss();
            }
        });

        mDialogWageFrequencyList = dialogListWageFrequency.show();

    }

    private void createJobReferenceDialog(){

        final AlertDialog.Builder editJobReferenceDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_job_reference, null);
        editJobReferenceDialog.setView(editView);
        final EditText mEditTextJobReference = (EditText) editView.findViewById(R.id.dialog_edit_job_reference_et);

        mEditTextJobReference.setText(jobReference);

        mEditTextJobReference.setSelection(mEditTextJobReference.getText().length());

        editJobReferenceDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                /* Add properties changed to a HashMap */
                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_REFERENCE, mEditTextJobReference.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobReference.dismiss();

            }
        });
        editJobReferenceDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobReference.dismiss();
            }
        });

        editJobReferenceDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_REFERENCE, "");
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobReference.dismiss();
            }
        });

        mDialogJobReference = editJobReferenceDialog.show();

        if (mDialogJobReference.getWindow() != null) {
            mDialogJobReference.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createJobDescriptionDialog(){

        final AlertDialog.Builder editJobDescriptionDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_job_description, null);
        editJobDescriptionDialog.setView(editView);
        final EditText mEditTextJobDescription = (EditText) editView.findViewById(R.id.dialog_edit_job_description_et);

        mEditTextJobDescription.setText(jobDescription);

        mEditTextJobDescription.setSelection(mEditTextJobDescription.getText().length());

        editJobDescriptionDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_DESCRIPTION, mEditTextJobDescription.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobDescription.dismiss();

            }
        });
        editJobDescriptionDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobDescription.dismiss();
            }
        });

        editJobDescriptionDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_DESCRIPTION, "");
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobDescription.dismiss();
            }
        });

        mDialogJobDescription = editJobDescriptionDialog.show();

        if(mDialogJobDescription.getWindow() != null) {
            mDialogJobDescription.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createJobAdvertUrlDialog(){

        final AlertDialog.Builder editJobAdvertUrlDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_job_advert_url, null);
        editJobAdvertUrlDialog.setView(editView);
        final EditText mEditTextJobAdvertUrl = (EditText) editView.findViewById(R.id.dialog_edit_job_advert_url_et);

        mEditTextJobAdvertUrl.setText(jobApplication.getJobAdvertURL());

        mEditTextJobAdvertUrl.setSelection(mEditTextJobAdvertUrl.getText().length());

        editJobAdvertUrlDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_ADVERT_URL, mEditTextJobAdvertUrl.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobAdvertUrl.dismiss();
            }
        });
        editJobAdvertUrlDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobAdvertUrl.dismiss();
            }
        });

        editJobAdvertUrlDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JOB_ADVERT_URL, "");
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogJobAdvertUrl.dismiss();
            }
        });

        mDialogJobAdvertUrl = editJobAdvertUrlDialog.show();

        if (mDialogJobAdvertUrl.getWindow() != null) {
            mDialogJobAdvertUrl.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createJobAdvertUrlActionSelect(){

        final AlertDialog.Builder jobAdvertUrlActionDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_job_advert_action, null);
        jobAdvertUrlActionDialog.setView(dialogView);

        TextView mTextViewJobAdvertUrlGoTo = (TextView) dialogView.findViewById(R.id.dialog_job_advert_action_tv_go_to);
        TextView mTextViewJobAdvertUrlEdit = (TextView) dialogView.findViewById(R.id.dialog_job_advert_action_tv_edit_url);

        mTextViewJobAdvertUrlGoTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!jobAdvertURL.startsWith("http://") && !jobAdvertURL.startsWith("https://"))
                    jobAdvertURL = "http://" + jobAdvertURL;

                mDialogJobAdvertUrlAction.dismiss();

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(jobAdvertURL));
                startActivity(browserIntent);

            }
        });

        mTextViewJobAdvertUrlEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJobAdvertUrlDialog();
                mDialogJobAdvertUrlAction.dismiss();
            }
        });

        mDialogJobAdvertUrlAction = jobAdvertUrlActionDialog.show();
    }

    private void createNotesDialog(){

        final AlertDialog.Builder editNotesDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_notes, null);
        editNotesDialog.setView(editView);
        final EditText mEditTextNotes = (EditText) editView.findViewById(R.id.dialog_edit_notes_et);

        mEditTextNotes.setText(applicationNotes);

        mEditTextNotes.setSelection(mEditTextNotes.getText().length());

        editNotesDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_APPLICATION_NOTES, mEditTextNotes.getText().toString().trim());
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogEditNotes.dismiss();

            }
        });
        editNotesDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditNotes.dismiss();
            }
        });

        editNotesDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_APPLICATION_NOTES, "");
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogEditNotes.dismiss();
            }
        });

        mDialogEditNotes = editNotesDialog.show();

        if (mDialogEditNotes.getWindow() != null) {
            mDialogEditNotes.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createContactDetailsDialog(){

        final AlertDialog.Builder editContactNameDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_contact_details, null);
        editContactNameDialog.setView(editView);
        final EditText mEditTextContactTitle = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_title);
        final EditText mEditTextContactFirstName = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_first_name);
        final EditText mEditTextContactSurname = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_surname);
        final EditText mEditTextContactPosition = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_position);
        final EditText mEditTextContactEmail = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_email);
        final EditText mEditTextContactPhone = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_phone);

        mEditTextContactTitle.setText(jobApplication.getContactTitle());
        mEditTextContactFirstName.setText(jobApplication.getContactFirstName());
        mEditTextContactSurname.setText(jobApplication.getContactSurname());
        mEditTextContactPosition.setText(jobApplication.getContactPosition());
        mEditTextContactEmail.setText(jobApplication.getContactEmail());
        mEditTextContactPhone.setText(jobApplication.getContactPhone());

        mEditTextContactTitle.setSelection(mEditTextContactTitle.getText().length());
        mEditTextContactFirstName.setSelection(mEditTextContactFirstName.getText().length());
        mEditTextContactSurname.setSelection(mEditTextContactSurname.getText().length());
        mEditTextContactPosition.setSelection(mEditTextContactPosition.getText().length());
        mEditTextContactEmail.setSelection(mEditTextContactEmail.getText().length());
        mEditTextContactPhone.setSelection(mEditTextContactPhone.getText().length());

        editContactNameDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                contactTitle = mEditTextContactTitle.getText().toString().trim();
                contactFirstName = mEditTextContactFirstName.getText().toString().trim();
                contactSurname = mEditTextContactSurname.getText().toString().trim();
                contactPosition = mEditTextContactPosition.getText().toString().trim();
                contactEmail = mEditTextContactEmail.getText().toString().trim();
                contactPhone = mEditTextContactPhone.getText().toString().trim();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_TITLE, contactTitle);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_FIRST_NAME, contactFirstName);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_SURNAME, contactSurname);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_POSITION, contactPosition);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_EMAIL, contactEmail);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_PHONE, contactPhone);
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mDialogContactDetails.dismiss();

            }
        });

        editContactNameDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogContactDetails.dismiss();
            }
        });

        editContactNameDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                createDeleteContactDetailsDialog();
            }
        });

        mDialogContactDetails = editContactNameDialog.show();
        mDialogContactDetails.setCancelable(false);
        mDialogContactDetails.setCanceledOnTouchOutside(false);

    }

    private void createDeleteContactDetailsDialog() {
        new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog)
                .setTitle(getContext().getString(R.string.dialog_delete_contact_details_title))
                .setMessage(getContext().getString(R.string.dialog_delete_contact_details_message_p1))
                .setPositiveButton(getContext().getString(R.string.dialog_delete_contact_details_positive_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteContactDetails();
                    }
                })
                .setNegativeButton(R.string.dialog_delete_contact_details_negative_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    private void deleteContactDetails() {

        contactTitle = "";
        contactFirstName = "";
        contactSurname = "";
        contactPosition = "";
        contactEmail = "";
        contactPhone = "";

        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_TITLE, contactTitle);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_FIRST_NAME, contactFirstName);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_SURNAME, contactSurname);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_POSITION, contactPosition);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_EMAIL, contactEmail);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_CONTACT_PHONE, contactPhone);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
    }

    private void createAssociatedJSAListDialog() {
        final AlertDialog.Builder associatedJSAListDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_recycler_view, null);
        associatedJSAListDialog.setView(convertView);
        final ProgressBar mProgressBarAssociatedJobsiteAgency = (ProgressBar) convertView.findViewById(R.id.dialog_recycler_view_pb);
        final TextView mTextViewAssociatedJsa = (TextView) convertView.findViewById(R.id.dialog_recycler_view_tv);
        final RecyclerView mRecyclerViewAssociatedJobsiteAgencyID = (RecyclerView) convertView.findViewById(R.id.dialog_recycler_view_rv);

        associatedJSAListDialog.setPositiveButton(getResources().getString(R.string.dialog_associated_jsa_list_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent goToJsaNewActivity = new Intent(getContext(), JsaNewActivity.class);
                goToJsaNewActivity.putExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY, JobApplicationDetailActivity.class.getSimpleName());
                goToJsaNewActivity.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
                goToJsaNewActivity.putExtra(Constants.INTENT_EXTRA_PINNED, false);
                startActivity(goToJsaNewActivity);
                getActivity().finish();
            }
        });

        DatabaseReference associatedJSARef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);

        final Query orderedAssociatedJSARef;
        orderedAssociatedJSARef = associatedJSARef.orderByKey();

        orderedAssociatedJSARef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0) {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.VISIBLE);
                    mTextViewAssociatedJsa.setText(R.string.dialog_associated_jsa_list_empty);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.GONE);
                } else {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.GONE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getContext());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewAssociatedJobsiteAgencyID.setHasFixedSize(false);
                    mRecyclerViewAssociatedJobsiteAgencyID.setLayoutManager(managerPriority);

                    mAssociatedJsaListRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);

                    mRecyclerViewAdapterAssociatedJsaList = new FirebaseRecyclerAdapter<JobsiteAgency, AssociatedJsaItemHolder>(JobsiteAgency.class, R.layout.list_item_associated_jsa, AssociatedJsaItemHolder.class, mAssociatedJsaListRef.orderByKey()) {
                        @Override
                        public void populateViewHolder(AssociatedJsaItemHolder jobApplicationPriorityView, JobsiteAgency jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getAssociatedJsaListItemSelected(mRecyclerViewAdapterAssociatedJsaList.getItem(position));
                            jobApplicationPriorityView.getAssociatedJsaListItemKey(mRecyclerViewAdapterAssociatedJsaList.getRef(position).getKey());
                            jobApplicationPriorityView.setAssociatedJsaListItemJsaName(jobApplicationPriority.getJsaName());
                            jobApplicationPriorityView.setAssociatedJsaListItemJsaLocation(jobApplicationPriority.getJsaLocationName());

                        }
                    };

                    mRecyclerViewAssociatedJobsiteAgencyID.setAdapter(mRecyclerViewAdapterAssociatedJsaList);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDialogAssociatedJSAList = associatedJSAListDialog.show();

        mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
        mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);
    }

    private void associatedJobsiteAgencyClear() {

        jsaId = "";

        mTextViewAssociatedJSA.setText(getResources().getString(R.string.pager_job_application_details_tv_associated_jsa_link_empty));
        mTextViewAssociatedJSA.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID, "");
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);

        mImageViewAssociatedJobsiteAgencyClear.setVisibility(View.GONE);
        mImageViewAssociatedJobsiteAgencyLink.setVisibility(View.VISIBLE);
        mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.white));

    }

    private void createAdditionalInformationDialog() {

        final AlertDialog.Builder additionalInformationDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_additional_information, null);
        additionalInformationDialog.setView(convertView);
        additionalInformationDialog.setTitle(getResources().getString(R.string.dialog_additional_information_title));

        TextView mTextViewDateCreated = (TextView) convertView.findViewById(R.id.dialog_additional_information_details_tv_date_created);
        TextView mTextViewDateEdited = (TextView) convertView.findViewById(R.id.dialog_additional_information_details_tv_date_edited);

        mTextViewDateEdited.setText(sdtf.format(timestampLastChanged));
        mTextViewDateCreated.setText(sdtf.format(timestampCreated));

        additionalInformationDialog.setNegativeButton(getResources().getString(R.string.dialog_additional_information_button_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogAdditionalInformation.dismiss();
            }
        });


        mDialogAdditionalInformation = additionalInformationDialog.show();

    }

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getContext(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiStart();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter(Constants.INTENT_EXTRA_ASSOCIATED_JSA));
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
        // Cleanup when the activity is stopped.
        if (mJobApplicationRef != null) {
            mJobApplicationRef.removeEventListener(mActiveListListener);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {



    }

}
