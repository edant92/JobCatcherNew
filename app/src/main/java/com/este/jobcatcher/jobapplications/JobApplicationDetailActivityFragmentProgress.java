package com.este.jobcatcher.jobapplications;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.alarms.AlarmServiceBroadcastReceiver;
import com.este.jobcatcher.emailfollowup.FollowupEmailActivity;
import com.este.jobcatcher.jobapplications.recyclerviewholders.ApplicationStageHolder;
import com.este.jobcatcher.models.ApplicationStage;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.RecyclerViewSnapping;
import com.este.jobcatcher.utils.Utils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Displays progress of a JobApplication
 */
public class JobApplicationDetailActivityFragmentProgress extends Fragment {

    private static final String LOG_TAG = JobApplicationDetailActivityFragmentProgress.class.getSimpleName();

    private final Utils utils = new Utils();
    
    private String jobApplicationId;
    private Boolean applicationPinned;
    private JobApplication jobApplication;
    private SimpleDateFormat sdf;
    private SimpleDateFormat stf;
    private SimpleDateFormat sdtf;
    private Calendar currentDeviceTimeCalendar;
    private Calendar currentApplicationStageDateStartedCalendar;
    private Calendar currentApplicationStageDateCompletedCalendar;
    private Calendar newApplicationStageDateStartedCalendar;
    private Calendar newApplicationStageDateCompletedCalendar;
    private String userUID;

    //Firebase References
    private DatabaseReference mApplicationStageRef;
    private DatabaseReference mJobApplicationRef;
    private DatabaseReference mPreviousApplicationStagePushRef;
    private  ValueEventListener mActiveListListener;

    //Firebase Object Variables
    private String jobTitle;
    private String companyName;
    private String currentApplicationStage;
    private String currentApplicationStageStatus;
    private Long currentApplicationStageDateStarted;
    private Long currentApplicationStageDateCompleted;
    private String currentApplicationStageNotes;
    private Boolean currentApplicationStageCompleted;
    private String newApplicationStage;
    private String newApplicationStageStatus;
    private Long newApplicationStageDateStarted;
    private Long newApplicationStageDateCompleted;
    private String newApplicationStageNotes;
    private Boolean newApplicationStageCompleted;
    private Long deadlineDate;
    private Boolean reminderSet;
    private Long reminderDateTime;
    private Integer reminderId;
    private String applicationStatus;
    private Long applicationDateApplied;
    private Long applicationDateCompleted;
    private Long emailFollowupTimeSent;
    private Long emailFollowupTimeCheck;
    private Calendar emailFollowupTimeCheckCalendar;
    private Boolean emailFollowupSent;
    private Long timestampLastChanged;
    private Long timestampCreated;

    private ApplicationStage previousApplicationStage;

    //UI - Main
    private TextView mTextViewApplicationStatus;
    private CardView mCardViewEmailFollowUp;
    private TextView mTextViewEmailFollowup;
    private Button mButtonEmailFollowupYes;
    private Button mButtonEmailFollowupNo;
    private Button mButtonEmailFollowupResend;
    private LinearLayout mLinearLayoutCurrentApplicationStage;
    private TextView mTextViewCurrentApplicationStage;
    private AppCompatImageView mImageViewCurrentApplicationStageDateStarted;
    private TextView mTextViewCurrentApplicationStageDateStarted;
    private AppCompatImageView mImageViewCurrentApplicationStageDateCompleted;
    private TextView mTextViewCurrentApplicationStageDateCompleted;
    private TextView mTextViewCurrentApplicationStageNotes;
    private Button mButtonApplicationStageAdd;
    private Button mButtonApplicationStageHistory;
    private Button mButtonSendEmailFollowUpSend;
    private Button mButtonDeadline;
    private Button mButtonReminder;

    private Dialog mApplicationStageEditDialog;
    private TextView mSpinnerDialogEditApplicationStageStatus;
    private TextView mTextViewDialogEditApplicationStageDateStarted;
    private TextView mTextViewDialogEditApplicationStageDateCompleted;

    private Dialog mApplicationStageAddDialog;
    private TextView mSpinnerDialogAddApplicationStage;
    private TextView mSpinnerDialogAddApplicationStageStatus;
    private TextView mTextViewDialogAddApplicationStageDateStarted;
    private TextView mTextViewDialogAddApplicationStageDateCompleted;
    private TextView mEditTextDialogAddApplicationStageNotes;
    private Dialog mDialogApplicationStageHistory;
    private FirebaseRecyclerAdapter<ApplicationStage, ApplicationStageHolder> recyclerAdapterApplicationStageHistory;

    private Dialog mDialogApplicationStageList;
    private Dialog mDialogAddApplicationStageStatusList;
    private Dialog mDialogEditApplicationStageList;

    private AlertDialog mDialogDeadline;
    private LinearLayout mLinearLayoutDeadlineEditDate;
    private TextView mTextViewDeadlineEditDate;
    private AlertDialog mDialogReminder;
    private LinearLayout mLinearLayoutReminderEditTime;
    private TextView mTextViewReminderEditTime;
    private LinearLayout mLinearLayoutReminderEditDate;
    private TextView mTextViewReminderEditDate;

    private Dialog mDialogFollowupTimeCheck;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pager_job_application_progress, container, false);

        currentDeviceTimeCalendar = Calendar.getInstance();
        currentApplicationStageDateStartedCalendar = Calendar.getInstance();
        currentApplicationStageDateCompletedCalendar = Calendar.getInstance();
        currentApplicationStageDateStartedCalendar.setTimeInMillis(0L);
        currentApplicationStageDateCompletedCalendar.setTimeInMillis(0L);

        String dateFormat = "dd MMM yyyy";
        sdf = new SimpleDateFormat(dateFormat, Locale.US);
        String timeFormat = "HH:mm";
        stf = new SimpleDateFormat(timeFormat, Locale.US);
        String dateTimeFormat = "dd MMM, HH:mm";
        sdtf = new SimpleDateFormat(dateTimeFormat, Locale.US);

        JobApplicationDetailActivity activity = (JobApplicationDetailActivity) getActivity();
        Intent intent = activity.getIntent();
        jobApplicationId = intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID);
        applicationPinned = intent.getBooleanExtra(Constants.INTENT_EXTRA_PINNED, false);

        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        createFirebaseReferences();

        initialiseLayout(rootView);

        addFirebaseListener();

        return rootView;

    }

    private void createFirebaseReferences() {

        DatabaseReference mBaseRef = FirebaseDatabase.getInstance().getReference();

        if (applicationPinned) {
            mJobApplicationRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(jobApplicationId);
            mApplicationStageRef = mJobApplicationRef.child(Constants.FIREBASE_PROPERTY_APPLICATION_STAGE_HISTORY);
        } else {
            mJobApplicationRef = mBaseRef.child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(jobApplicationId);
            mApplicationStageRef = mJobApplicationRef.child(Constants.FIREBASE_PROPERTY_APPLICATION_STAGE_HISTORY);
        }

    }

    private void initialiseLayout(View rootView) {

        mTextViewApplicationStatus = (TextView) rootView.findViewById(R.id.pager_job_application_progress_tv_application_status);
        mCardViewEmailFollowUp = (CardView) rootView.findViewById(R.id.pager_job_application_progress_cv_email_follow_up);
        mTextViewEmailFollowup = (TextView) rootView.findViewById(R.id.pager_job_application_progress_tv_email_follow_up);
        mButtonEmailFollowupYes = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_email_follow_up_yes);
        mButtonEmailFollowupNo = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_email_follow_up_no);
        mButtonEmailFollowupResend = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_email_follow_up_resend);
        mLinearLayoutCurrentApplicationStage = (LinearLayout) rootView.findViewById(R.id.pager_job_application_progress_ll_current_application_stage);
        mTextViewCurrentApplicationStage = (TextView) rootView.findViewById(R.id.pager_job_application_progress_tv_current_application_stage);
        mTextViewCurrentApplicationStageDateStarted = (TextView) rootView.findViewById(R.id.pager_job_application_progress_tv_current_application_stage_date_start);
        mImageViewCurrentApplicationStageDateStarted = (AppCompatImageView) rootView.findViewById(R.id.pager_job_application_progress_iv_current_application_stage_date_start);
        mTextViewCurrentApplicationStageDateCompleted = (TextView) rootView.findViewById(R.id.pager_job_application_progress_tv_current_application_stage_date_complete);
        mImageViewCurrentApplicationStageDateCompleted = (AppCompatImageView) rootView.findViewById(R.id.pager_job_application_progress_iv_current_application_stage_date_complete);
        mTextViewCurrentApplicationStageNotes = (TextView) rootView.findViewById(R.id.pager_job_application_progress_tv_current_application_stage_notes);
        mButtonApplicationStageAdd = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_application_stage_add);
        mButtonApplicationStageHistory = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_application_stage_history);
        mButtonSendEmailFollowUpSend = (Button) rootView.findViewById(R.id.pager_job_application_progress_tv_followup_email);
        mButtonReminder = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_reminder);
        mButtonDeadline = (Button) rootView.findViewById(R.id.pager_job_application_progress_bt_deadline);

        mLinearLayoutCurrentApplicationStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createApplicationStageEditDialog();
            }
        });

        mButtonApplicationStageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAddApplicationStageAddDialog();
            }
        });

        mButtonApplicationStageHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createApplicationStageHistoryDialog();
            }
        });

        mButtonSendEmailFollowUpSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEmailFollowUpActivity();
            }
        });

        mButtonDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDeadlineEditDialog();
            }
        });

        mButtonReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createReminderEditDialog();
            }
        });

    }

    private void addFirebaseListener() {

        mActiveListListener = mJobApplicationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                jobApplication = dataSnapshot.getValue(JobApplication.class);

                if (jobApplication == null) {
                    getActivity().finish();
                    //If Job Application is empty, return to MainActivity
                    return;
                }

                getJobApplicationValues(jobApplication);

                variableNullChecks();

                currentApplicationStageDateStartedCalendar.setTimeInMillis(currentApplicationStageDateStarted);
                currentApplicationStageDateCompletedCalendar.setTimeInMillis(currentApplicationStageDateCompleted);

                setValuesToUI();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

    }

    private void getJobApplicationValues(JobApplication jobApplication) {
        //Get Job Application Property Values
        jobTitle = jobApplication.getJobTitle();
        companyName = jobApplication.getCompanyName();
        currentApplicationStage = jobApplication.getCurrentApplicationStage();
        currentApplicationStageStatus = jobApplication.getCurrentApplicationStageStatus();
        currentApplicationStageDateStarted = jobApplication.getCurrentApplicationStageDateStarted();
        currentApplicationStageDateCompleted = jobApplication.getCurrentApplicationStageDateCompleted();
        currentApplicationStageNotes = jobApplication.getCurrentApplicationStageNotes();
        currentApplicationStageCompleted = jobApplication.getCurrentApplicationStageCompleted();
        deadlineDate = jobApplication.getDeadlineDate();
        reminderSet = jobApplication.getReminderSet();
        reminderDateTime = jobApplication.getReminderDateTime();
        reminderId = jobApplication.getReminderId();
        emailFollowupTimeSent = jobApplication.getEmailFollowupTimeSent();
        emailFollowupTimeCheck = jobApplication.getEmailFollowupTimeCheck();
        emailFollowupSent = jobApplication.getEmailFollowupSent();
        applicationStatus = jobApplication.getApplicationStatus();
        applicationDateApplied = jobApplication.getDateApplied();
        applicationDateCompleted = jobApplication.getDateCompleted();
        timestampCreated = jobApplication.getTimestampCreatedLong();
        timestampLastChanged = jobApplication.getTimestampLastChangedLong();
    }

    private void variableNullChecks() {

        if (jobTitle == null) {
            jobTitle = "";
        }

        if (companyName == null) {
            companyName = "";
        }

        if (currentApplicationStage == null) {
            currentApplicationStage = getResources().getStringArray(R.array.array_application_stage)[0];
        }

        if (currentApplicationStageStatus == null) {
            currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
        }

        if (currentApplicationStageDateStarted == null) {
            currentApplicationStageDateStarted = 0L;
        }

        if (currentApplicationStageDateCompleted == null) {
            currentApplicationStageDateCompleted = 0L;
        }

        if (currentApplicationStageNotes == null) {
            currentApplicationStageNotes = "";
        }

        if (currentApplicationStageCompleted == null) {
            currentApplicationStageCompleted = false;
        }

        if (deadlineDate == null) {
            deadlineDate = 0L;
        }

        if (reminderSet == null) {
            reminderSet = false;
        }

        if (reminderDateTime == null) {
            reminderDateTime = 0L;
        }

        if (reminderId == null) {
            reminderId = 0;
        }

        if (applicationStatus == null) {
            applicationStatus = getResources().getStringArray(R.array.array_application_status)[0];
        }

        if (applicationDateApplied == null) {
            applicationDateApplied = 0L;
        }

        if (applicationDateCompleted == null) {
            applicationDateCompleted = 0L;
        }

        if (emailFollowupTimeSent == null) {
            emailFollowupTimeSent = 0L;
        }

        if (emailFollowupTimeCheck == null) {
            emailFollowupTimeCheck = 0L;
        }

        if (emailFollowupSent == null) {
            emailFollowupSent = false;
        }

        if (timestampLastChanged == null) {
            timestampLastChanged = 0L;
        }

        if (timestampCreated == null) {
            timestampCreated = 0L;
        }

    }

    private void setValuesToUI() {
        // Set TextViews to Property Values
        if (applicationStatus.equals(getResources().getStringArray(R.array.array_application_status)[0]) || applicationStatus.equals(getResources().getStringArray(R.array.array_application_status)[1]) || applicationStatus.equals("")) {
            mTextViewApplicationStatus.setVisibility(View.GONE);
        } else {
            //If Application status is successful/unsuccessful/withdrawn
            mTextViewApplicationStatus.setVisibility(View.VISIBLE);
            mTextViewApplicationStatus.setText(applicationStatus.toUpperCase());
            if (applicationStatus.equals(getResources().getStringArray(R.array.array_application_status)[2])) {
                mTextViewApplicationStatus.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
            } else if (applicationStatus.equals(getResources().getStringArray(R.array.array_application_status)[3])) {
                mTextViewApplicationStatus.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
            } else if (applicationStatus.equals(getResources().getStringArray(R.array.array_application_status)[4])) {
                mTextViewApplicationStatus.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.black));
            }

        }

        mTextViewCurrentApplicationStage.setText(currentApplicationStage);

        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
            mButtonApplicationStageAdd.setEnabled(false);
        }

        if (currentApplicationStageDateStarted.equals(0L)) {
            mTextViewCurrentApplicationStageDateStarted.setText(getResources().getString(R.string.activity_job_application_detail_tv_application_stage_date_started_empty));
            mTextViewCurrentApplicationStageDateStarted.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
            mImageViewCurrentApplicationStageDateStarted.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_circle_empty));
            mImageViewCurrentApplicationStageDateStarted.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused));
        } else {
            mTextViewCurrentApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted));
            mTextViewCurrentApplicationStageDateStarted.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_black));
            mImageViewCurrentApplicationStageDateStarted.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_check_circle));
            mImageViewCurrentApplicationStageDateStarted.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_black));
        }

        if (currentApplicationStageDateCompleted.equals(0L)) {
            mTextViewCurrentApplicationStageDateCompleted.setText(getResources().getString(R.string.activity_job_application_detail_tv_application_stage_date_completed_empty));
            mTextViewCurrentApplicationStageDateCompleted.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
            mImageViewCurrentApplicationStageDateCompleted.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_circle_empty));
            mImageViewCurrentApplicationStageDateCompleted.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused));
        } else {
            mTextViewCurrentApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));
            mTextViewCurrentApplicationStageDateCompleted.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_black));
            mImageViewCurrentApplicationStageDateCompleted.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_check_circle));
            mImageViewCurrentApplicationStageDateCompleted.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_black));
        }

        if (currentApplicationStageNotes.equals("")) {
            mTextViewCurrentApplicationStageNotes.setText(R.string.pager_job_application_progress_tv_current_application_stage_notes_empty);
        } else {
            mTextViewCurrentApplicationStageNotes.setText(currentApplicationStageNotes);
        }

        if (deadlineDate == 0L) {
            mButtonDeadline.setText(R.string.pager_job_application_progress_tv_deadline_empty);
        } else {
            mButtonDeadline.setText(sdf.format(deadlineDate));
        }

        if (reminderSet) {
            mButtonReminder.setText(sdtf.format(reminderDateTime));
        } else {
            mButtonReminder.setText(R.string.pager_job_application_progress_tv_reminder_empty);
        }

        //If emailFollowUp not set and application stage is IN PROGRESS then display email follow up button
        if (!emailFollowupSent) {
            if (applicationStatus.equals(getResources().getStringArray(R.array.array_application_status)[1])) {
                mButtonSendEmailFollowUpSend.setVisibility(View.VISIBLE);
            }
        } else {
            mButtonSendEmailFollowUpSend.setVisibility(View.GONE);
        }

        long currentTime = Calendar.getInstance().getTimeInMillis();

        if (emailFollowupSent && (currentTime >= emailFollowupTimeCheck)) {

            mCardViewEmailFollowUp.setVisibility(View.VISIBLE);

            long dateDifference = Calendar.getInstance().getTimeInMillis() - emailFollowupTimeSent; //result in millis
            String dayString = String.valueOf(dateDifference / (24 * 60 * 60 * 1000L));
            int days = Integer.parseInt(dayString);
            mTextViewEmailFollowup.setText(getResources().getQuantityString(R.plurals.pager_job_application_progress_ll_email_follow_up, days, days));

            mButtonEmailFollowupYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> emailFollowupProperties = new HashMap<>();
                    emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_SENT, 0L);
                    emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_CHECK, 0L);
                    emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_SENT, false);
                    mJobApplicationRef.updateChildren(emailFollowupProperties);
                    utils.updateTimestampLastChanged(mJobApplicationRef);
                    mCardViewEmailFollowUp.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Awesome!", Toast.LENGTH_SHORT).show();
                }
            });
            mButtonEmailFollowupNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createFollowupTimeCheckDialog();
                }
            });
            mButtonEmailFollowupResend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, Object> emailFollowupProperties = new HashMap<>();
                    emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_CHECK, currentDeviceTimeCalendar.getTimeInMillis());
                    mJobApplicationRef.updateChildren(emailFollowupProperties);
                    utils.updateTimestampLastChanged(mJobApplicationRef);
                    goToEmailFollowUpActivity();
                    mCardViewEmailFollowUp.setVisibility(View.GONE);
                }
            });
        } else {
            mCardViewEmailFollowUp.setVisibility(View.GONE);
        }
    }

    private void createApplicationStageEditDialog() {
        final AlertDialog.Builder editApplicationStageDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_application_stage, null);
        editApplicationStageDialog.setView(dialogView);

        TextView mSpinnerDialogEditApplicationStage = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_stage);
        mSpinnerDialogEditApplicationStageStatus = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_status);
        mTextViewDialogEditApplicationStageDateStarted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_started);
        mTextViewDialogEditApplicationStageDateCompleted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_completed);
        final EditText mEditTextDialogEditApplicationStageNotes = (EditText) dialogView.findViewById(R.id.dialog_new_application_stage_et_notes);

        mSpinnerDialogEditApplicationStage.setText(currentApplicationStage);
        mSpinnerDialogEditApplicationStage.setEnabled(false);
        mSpinnerDialogEditApplicationStage.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
        mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);

        mSpinnerDialogEditApplicationStageStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEditApplicationStageStatusListDialog();
            }
        });

        mEditTextDialogEditApplicationStageNotes.setText(currentApplicationStageNotes);

        //If currentApplicationStage = Not Yet Applied, disable dialog UI
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
            mSpinnerDialogEditApplicationStageStatus.setEnabled(false);
            mSpinnerDialogEditApplicationStageStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
            mTextViewDialogEditApplicationStageDateStarted.setText("-");
            mTextViewDialogEditApplicationStageDateStarted.setEnabled(false);
            mTextViewDialogEditApplicationStageDateStarted.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
            mTextViewDialogEditApplicationStageDateCompleted.setText("-");
            mTextViewDialogEditApplicationStageDateCompleted.setEnabled(false);
            mTextViewDialogEditApplicationStageDateCompleted.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
            mEditTextDialogEditApplicationStageNotes.setEnabled(false);
        } else {
            mTextViewDialogEditApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted));

            if (!currentApplicationStageDateCompleted.equals(0L)) {
                mTextViewDialogEditApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));
            } else {
                mTextViewDialogEditApplicationStageDateCompleted.setText("-");
            }

            //If Application stage is any other than Not Yet Applied, allow user to edit date completed.
            mTextViewDialogEditApplicationStageDateCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createEditApplicationStageDateCompletedDialog();
                }
            });

        }

        mTextViewDialogEditApplicationStageDateStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEditApplicationStageDateStartedDialog();
            }
        });

        editApplicationStageDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS, currentApplicationStageStatus);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED, currentApplicationStageDateStarted);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED, currentApplicationStageDateCompleted);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES, mEditTextDialogEditApplicationStageNotes.getText().toString());
                updatedProperties.put(Constants.FIREBASE_PROPERTY_APPLICATION_STATUS, applicationStatus);
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mApplicationStageEditDialog.dismiss();

            }
        });
        editApplicationStageDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mApplicationStageEditDialog.dismiss();
            }
        });

        mApplicationStageEditDialog = editApplicationStageDialog.show();
        mApplicationStageEditDialog.setCanceledOnTouchOutside(false);

    }

    private void createEditApplicationStageDateStartedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                currentApplicationStageDateStartedCalendar.set(Calendar.YEAR, year);
                currentApplicationStageDateStartedCalendar.set(Calendar.MONTH, monthOfYear);
                currentApplicationStageDateStartedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                currentApplicationStageDateStartedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.MINUTE, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.SECOND, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.MILLISECOND, 0);

                currentApplicationStageDateStarted = currentApplicationStageDateStartedCalendar.getTimeInMillis();
                mTextViewDialogEditApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted));
            }
        };
        DatePickerDialog datePickerDialogDateStarted = new DatePickerDialog(getContext(), R.style.CustomStyleTimeDatePickerDialog, deadline_date, currentApplicationStageDateStartedCalendar.get(Calendar.YEAR), currentApplicationStageDateStartedCalendar.get(Calendar.MONTH), currentApplicationStageDateStartedCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogDateStarted.setTitle(R.string.activity_job_application_detail_tv_application_stage_date_started);
        datePickerDialogDateStarted.show();
    }

    private void createEditApplicationStageDateCompletedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                currentApplicationStageDateCompletedCalendar.set(Calendar.YEAR, year);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MONTH, monthOfYear);
                currentApplicationStageDateCompletedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                currentApplicationStageDateCompletedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MINUTE, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.SECOND, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MILLISECOND, 0);

                currentApplicationStageDateCompleted = currentApplicationStageDateCompletedCalendar.getTimeInMillis();
                mTextViewDialogEditApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));

                currentApplicationStageCompleted = true;

                //Default set currentApplicationStageStatus to 'Successful'
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[1];
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);

                createEditApplicationStageStatusListDialog();

            }
        };

        if (currentApplicationStageDateCompleted.equals(0L)) {
            currentApplicationStageDateCompletedCalendar = Calendar.getInstance();
        }

        DatePickerDialog datePickerDialogDateCompleted = new DatePickerDialog(getContext(), R.style.CustomStyleTimeDatePickerDialog, deadline_date, currentApplicationStageDateCompletedCalendar.get(Calendar.YEAR), currentApplicationStageDateCompletedCalendar.get(Calendar.MONTH), currentApplicationStageDateCompletedCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogDateCompleted.setTitle(R.string.activity_job_application_detail_tv_application_stage_date_completed);
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentApplicationStageDateCompletedCalendar = Calendar.getInstance();
                currentApplicationStageDateCompleted = 0L;
                mTextViewDialogEditApplicationStageDateCompleted.setText("-");
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.show();
    }

    private void createAddApplicationStageAddDialog(){
        final AlertDialog.Builder addApplicationStageDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_application_stage, null);
        addApplicationStageDialog.setView(dialogView);
        addApplicationStageDialog.setTitle(getResources().getString(R.string.dialog_new_application_stage_title));

        mSpinnerDialogAddApplicationStage = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_stage);
        mSpinnerDialogAddApplicationStageStatus = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_status);
        mTextViewDialogAddApplicationStageDateStarted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_started);
        mTextViewDialogAddApplicationStageDateCompleted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_completed);
        mEditTextDialogAddApplicationStageNotes = (EditText) dialogView.findViewById(R.id.dialog_new_application_stage_et_notes);

        addPreviousApplicationStageToApplicationStageHistory();

        resetCurrentApplicationStageProperties();

        mSpinnerDialogAddApplicationStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageListDialog();
            }
        });

        mSpinnerDialogAddApplicationStageStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageStatusListDialog();
            }
        });

        mTextViewDialogAddApplicationStageDateStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageDateStartedDialog();
            }
        });

        mTextViewDialogAddApplicationStageDateCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageDateCompletedDialog();
            }
        });

        addApplicationStageDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                newApplicationStageNotes = mEditTextDialogAddApplicationStageNotes.getText().toString();

                if (!newApplicationStageDateStarted.equals(0L)) {

                    //If New Application Stage anything other than Not Yet Applied then set dateApplied to same as applicationDateStarted
                    if (!newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
                        applicationDateApplied = newApplicationStageDateStarted;
                    }

                    //If New Application Stage = Complete then set applicationDateCompleted to same as applicationDateStarted
                    if (newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
                        applicationDateCompleted = newApplicationStageDateStarted;
                    }

                }

                applicationStatus = utils.setApplicationStatus(newApplicationStage, newApplicationStageStatus, getResources());

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE, newApplicationStage);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS, newApplicationStageStatus);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED, newApplicationStageDateStarted);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED, newApplicationStageDateCompleted);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES, newApplicationStageNotes);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_COMPLETED, newApplicationStageCompleted);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_APPLICATION_STATUS, applicationStatus);
                mJobApplicationRef.updateChildren(updatedProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                Log.d(LOG_TAG, "New Application Stage:" + updatedProperties.toString());

                mApplicationStageAddDialog.dismiss();

            }
        });
        addApplicationStageDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Remove previous applicationStage from ApplicationStageHistory and add back to main Job Application Object
                mPreviousApplicationStagePushRef.removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        currentApplicationStage = previousApplicationStage.getApplicationStage();
                        currentApplicationStageStatus = previousApplicationStage.getApplicationStageStatus();
                        currentApplicationStageDateStarted = previousApplicationStage.getApplicationStageDateStarted();
                        currentApplicationStageDateCompleted = 0L;
                        currentApplicationStageNotes = previousApplicationStage.getApplicationStageNotes();
                        currentApplicationStageCompleted = previousApplicationStage.getApplicationStageCompleted();
                    }
                });
            }
        });

        mApplicationStageAddDialog = addApplicationStageDialog.show();

    }

    private void createAddApplicationStageDateStartedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newApplicationStageDateStartedCalendar.set(Calendar.YEAR, year);
                newApplicationStageDateStartedCalendar.set(Calendar.MONTH, monthOfYear);
                newApplicationStageDateStartedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                newApplicationStageDateStartedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                newApplicationStageDateStartedCalendar.set(Calendar.MINUTE, 0);
                newApplicationStageDateStartedCalendar.set(Calendar.SECOND, 0);
                newApplicationStageDateStartedCalendar.set(Calendar.MILLISECOND, 0);

                newApplicationStageDateStarted = newApplicationStageDateStartedCalendar.getTimeInMillis();
                mTextViewDialogAddApplicationStageDateStarted.setText(sdf.format(newApplicationStageDateStarted));
            }
        };
        DatePickerDialog dateStartedPickerDialog = new DatePickerDialog(getContext(), R.style.CustomStyleTimeDatePickerDialog, deadline_date, newApplicationStageDateStartedCalendar.get(Calendar.YEAR), newApplicationStageDateStartedCalendar.get(Calendar.MONTH), newApplicationStageDateStartedCalendar.get(Calendar.DAY_OF_MONTH));
        dateStartedPickerDialog.show();
    }

    private void createAddApplicationStageDateCompletedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newApplicationStageDateCompletedCalendar.set(Calendar.YEAR, year);
                newApplicationStageDateCompletedCalendar.set(Calendar.MONTH, monthOfYear);
                newApplicationStageDateCompletedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                newApplicationStageDateCompletedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                newApplicationStageDateCompletedCalendar.set(Calendar.MINUTE, 0);
                newApplicationStageDateCompletedCalendar.set(Calendar.SECOND, 0);
                newApplicationStageDateCompletedCalendar.set(Calendar.MILLISECOND, 0);

                newApplicationStageDateCompleted = newApplicationStageDateCompletedCalendar.getTimeInMillis();
                mTextViewDialogAddApplicationStageDateCompleted.setText(sdf.format(newApplicationStageDateCompleted));

                newApplicationStageCompleted = true;

                //Default set currentApplicationStageStatus to 'Successful'
                newApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[1];
                mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus);

                createAddApplicationStageStatusListDialog();

            }
        };
        DatePickerDialog datePickerDialogDateCompleted = new DatePickerDialog(getContext(), R.style.CustomStyleTimeDatePickerDialog, deadline_date, newApplicationStageDateCompletedCalendar.get(Calendar.YEAR), newApplicationStageDateCompletedCalendar.get(Calendar.MONTH), newApplicationStageDateCompletedCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogDateCompleted.setTitle(R.string.activity_job_application_detail_tv_application_stage_date_completed);
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newApplicationStageDateCompletedCalendar = Calendar.getInstance();
                newApplicationStageDateCompleted = 0L;
                currentApplicationStageCompleted = false;
                mTextViewDialogAddApplicationStageDateCompleted.setText("-");
                newApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.show();
    }

    private void createApplicationStageHistoryDialog() {
        final AlertDialog.Builder applicationStageHistoryDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_application_stage_history, null);
        applicationStageHistoryDialog.setView(convertView);
        applicationStageHistoryDialog.setTitle(getResources().getString(R.string.dialog_application_stage_history_title));
        final ProgressBar mProgressBarApplicationStageHistory = (ProgressBar) convertView.findViewById(R.id.dialog_application_stage_history_pb);
        final TextView mTextViewApplicationStageHistory = (TextView) convertView.findViewById(R.id.dialog_application_stage_history_tv);
        final RecyclerViewSnapping mRecyclerViewApplicationStageHistory = (RecyclerViewSnapping) convertView.findViewById(R.id.dialog_application_stage_history_rv);

        LinearLayoutManager applicationStageHistoryLLManager = new LinearLayoutManager(getContext());
        applicationStageHistoryLLManager.setReverseLayout(true);
        applicationStageHistoryLLManager.setStackFromEnd(true);
        applicationStageHistoryLLManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewApplicationStageHistory.setHasFixedSize(false);
        mRecyclerViewApplicationStageHistory.setLayoutManager(applicationStageHistoryLLManager);

        applicationStageHistoryDialog.setNegativeButton(getResources().getString(R.string.dialog_application_stage_history_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (recyclerAdapterApplicationStageHistory != null) {
                    recyclerAdapterApplicationStageHistory.cleanup();
                }
                mDialogApplicationStageHistory.dismiss();
            }

        });

        final Query mApplicationStageOrder = mApplicationStageRef.orderByChild(Constants.FIREBASE_PROPERTY_APPLICATION_STAGE_TIMESTAMP_STARTED
                + "/" +  Constants.FIREBASE_PROPERTY_APPLICATION_STAGE_TIMESTAMP);

        mApplicationStageOrder.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0) {
                    mProgressBarApplicationStageHistory.setVisibility(View.GONE);
                    mTextViewApplicationStageHistory.setVisibility(View.VISIBLE);
                    mRecyclerViewApplicationStageHistory.setVisibility(View.GONE);
                } else {
                    mProgressBarApplicationStageHistory.setVisibility(View.GONE);
                    mTextViewApplicationStageHistory.setVisibility(View.GONE);
                    mRecyclerViewApplicationStageHistory.setVisibility(View.VISIBLE);

                    recyclerAdapterApplicationStageHistory = new FirebaseRecyclerAdapter<ApplicationStage, ApplicationStageHolder>(ApplicationStage.class, R.layout.cardview_application_stage, ApplicationStageHolder.class, mApplicationStageOrder) {
                        @Override
                        public void populateViewHolder(ApplicationStageHolder applicationStageView, ApplicationStage applicationStage, int position) {

                            applicationStageView.getApplicationStageItemKey(recyclerAdapterApplicationStageHistory.getRef(position).getKey());
                            applicationStageView.setApplicationStage(applicationStage.getApplicationStage());
                            applicationStageView.setApplicationStageNotes(applicationStage.getApplicationStageNotes());
                            applicationStageView.setApplicationStageDateStarted(applicationStage.getApplicationStageDateStarted());
                            applicationStageView.setApplicationStageDateEnded(applicationStage.getApplicationStageDateCompleted());

                        }
                    };

                    mRecyclerViewApplicationStageHistory.setAdapter(recyclerAdapterApplicationStageHistory);
                    System.out.println("recyclerAdapterApplicationStageHistory: " + recyclerAdapterApplicationStageHistory);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDialogApplicationStageHistory = applicationStageHistoryDialog.show();

    }

    private void createDeadlineEditDialog() {

        final AlertDialog.Builder deadlineEditAlertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_deadline_edit, null);
        deadlineEditAlertDialog.setView(convertView);
        mLinearLayoutDeadlineEditDate = (LinearLayout) convertView.findViewById(R.id.dialog_deadline_edit_ll);
        mTextViewDeadlineEditDate = (TextView) convertView.findViewById(R.id.dialog_deadline_edit_tv);
        Button mButtonAddDeadlineToCalendar = (Button) convertView.findViewById(R.id.dialog_deadline_edit_bt_add_to_calendar);

        mButtonAddDeadlineToCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDeadlineToDeviceCalendar();
            }
        });

        deadlineEditTimeDatePickers();

        String deadlineNegativeButtonText;

        if (deadlineDate == 0L) {
            deadlineNegativeButtonText = getString(R.string.dialog_reminder_edit_negative_button_cancel);
            mTextViewDeadlineEditDate.setText(getResources().getString(R.string.pager_job_application_progress_tv_deadline_empty));
        } else {
            deadlineNegativeButtonText = getString(R.string.dialog_reminder_edit_negative_button_close);
            mTextViewDeadlineEditDate.setText(sdf.format(deadlineDate));
        }

        deadlineEditAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_reminder_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submitDeadlineEditData();
                mDialogDeadline.dismiss();

            }
        });
        deadlineEditAlertDialog.setNegativeButton(deadlineNegativeButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogDeadline.dismiss();

            }
        });
        deadlineEditAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_reminder_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteDeadline();
            }
        });

        mDialogDeadline = deadlineEditAlertDialog.create();
        mDialogDeadline.show();

        mDialogDeadline.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

    }

    private void deadlineEditTimeDatePickers() {

        final Calendar c = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener deadline_edit_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                mTextViewDeadlineEditDate.setText(sdf.format(c.getTime()));
                deadlineDate = c.getTimeInMillis();
                mDialogDeadline.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            }
        };

        mLinearLayoutDeadlineEditDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Integer calendarDefaultDeadlineYear;
                final Integer calendarDefaultDeadlineMonth;
                final Integer calendarDefaultDeadlineDay;
                if (deadlineDate == 0L) {
                    //if no deadline, set to today's date as default
                    calendarDefaultDeadlineYear = c.get(Calendar.YEAR);
                    calendarDefaultDeadlineMonth = c.get(Calendar.MONTH);
                    calendarDefaultDeadlineDay = c.get(Calendar.DAY_OF_MONTH);
                } else {
                    //if deadline, set to deadline's date as default
                    Calendar calendarDefaultDeadline = Calendar.getInstance();
                    calendarDefaultDeadline.setTimeInMillis(deadlineDate);

                    calendarDefaultDeadlineYear = calendarDefaultDeadline.get(Calendar.YEAR);
                    calendarDefaultDeadlineMonth = calendarDefaultDeadline.get(Calendar.MONTH);
                    calendarDefaultDeadlineDay = calendarDefaultDeadline.get(Calendar.DAY_OF_MONTH);
                }

                DatePickerDialog deadlineDatePickerDialog = new DatePickerDialog(getContext(),
                        R.style.CustomStyleTimeDatePickerDialog, deadline_edit_date, calendarDefaultDeadlineYear, calendarDefaultDeadlineMonth, calendarDefaultDeadlineDay);
                deadlineDatePickerDialog.show();
            }
        });
    }

    private void submitDeadlineEditData() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_DEADLINE_DATE, deadlineDate);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
    }

    private void deleteDeadline() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_DEADLINE_DATE, 0L);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
    }

    public void createReminderEditDialog() {

        final AlertDialog.Builder reminderEditAlertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_reminder_edit, null);
        reminderEditAlertDialog.setView(convertView);
        mLinearLayoutReminderEditTime = (LinearLayout) convertView.findViewById(R.id.dialog_reminder_edit_ll_time);
        mLinearLayoutReminderEditDate = (LinearLayout) convertView.findViewById(R.id.dialog_reminder_edit_ll_date);
        mTextViewReminderEditTime = (TextView) convertView.findViewById(R.id.dialog_reminder_edit_tv_time);
        mTextViewReminderEditDate = (TextView) convertView.findViewById(R.id.dialog_reminder_edit_tv_date);

        reminderEditTimeDatePickers();

        String reminderNegativeButtonText;

        if (reminderSet) {
            mTextViewReminderEditTime.setText(stf.format(reminderDateTime));
            mTextViewReminderEditDate.setText(sdf.format(reminderDateTime));
            reminderNegativeButtonText = getString(R.string.dialog_reminder_edit_negative_button_close);
        } else {
            reminderDateTime = 0L;
            reminderNegativeButtonText = getString(R.string.dialog_reminder_edit_negative_button_cancel);
        }

        reminderEditAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_reminder_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submitReminderEditData();
                mDialogReminder.dismiss();
            }
        });
        reminderEditAlertDialog.setNegativeButton(reminderNegativeButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogReminder.dismiss();
            }
        });
        reminderEditAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_reminder_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteReminder();
                alarmScheduleServiceDelete();
            }
        });

        mDialogReminder = reminderEditAlertDialog.create();
        mDialogReminder.show();

        mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

    }

    private void reminderEditTimeDatePickers() {

        final Calendar c = Calendar.getInstance();

        final TimePickerDialog.OnTimeSetListener reminder_edit_time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);
                mTextViewReminderEditTime.setText(stf.format(c.getTime()));
                reminderDateTime = c.getTimeInMillis();
                if (!mTextViewReminderEditDate.getText().toString().equals("")) {
                    mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        final DatePickerDialog.OnDateSetListener reminder_edit_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mTextViewReminderEditDate.setText(sdf.format(c.getTime()));
                reminderDateTime = c.getTimeInMillis();
                if (!mTextViewReminderEditTime.getText().toString().equals("")) {
                    mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        mLinearLayoutReminderEditTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog reminderTimePickerDialog = new TimePickerDialog(getContext(),
                        R.style.CustomStyleTimeDatePickerDialog, reminder_edit_time, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
                reminderTimePickerDialog.show();
            }
        });
        mLinearLayoutReminderEditDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog reminderDatePickerDialog = new DatePickerDialog(getContext(),
                        R.style.CustomStyleTimeDatePickerDialog, reminder_edit_date, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                reminderDatePickerDialog.show();
            }
        });
    }

    private void submitReminderEditData() {
        reminderSet = true;
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_SET, reminderSet);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE, reminderDateTime);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
        alarmScheduleServiceEdit();
    }

    private void deleteReminder() {
        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_SET, false);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_ID, reminderId);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE, 0L);
        mJobApplicationRef.updateChildren(updatedProperties);
        utils.updateTimestampLastChanged(mJobApplicationRef);
    }

    private void alarmScheduleServiceEdit() {
        Intent alarmServiceIntentEdit = new Intent(getContext(), AlarmServiceBroadcastReceiver.class);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, reminderDateTime);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
        alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, false);
        if (applicationPinned) {
            alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_PINNED, true);
        } else {
            alarmServiceIntentEdit.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        }
        getContext().sendBroadcast(alarmServiceIntentEdit, null);
    }

    private void alarmScheduleServiceDelete() {
        Intent alarmServiceIntentDelete = new Intent(getContext(), AlarmServiceBroadcastReceiver.class);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, reminderDateTime);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, true);
        if (applicationPinned) {
            alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_PINNED, true);
        } else {
            alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        }
        getContext().sendBroadcast(alarmServiceIntentDelete, null);
    }

    private void createFollowupTimeCheckDialog() {

        final AlertDialog.Builder emailFollowupTimeCheckDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_list_email_followup_time, null);
        emailFollowupTimeCheckDialog.setView(convertView);
        emailFollowupTimeCheckDialog.setTitle(getResources().getString(R.string.dialog_email_followup_time_title));

        emailFollowupTimeCheckCalendar = Calendar.getInstance();

        emailFollowupTimeCheckDialog.setPositiveButton(getString(R.string.dialog_email_followup_time_positive_button), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                final DatePickerDialog.OnDateSetListener email_followup_time_date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        emailFollowupTimeCheckCalendar.set(Calendar.YEAR, year);
                        emailFollowupTimeCheckCalendar.set(Calendar.MONTH, monthOfYear);
                        emailFollowupTimeCheckCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        emailFollowupTimeCheckCalendar.set(Calendar.HOUR_OF_DAY, 0);
                        emailFollowupTimeCheckCalendar.set(Calendar.MINUTE, 0);
                        emailFollowupTimeCheckCalendar.set(Calendar.SECOND, 0);
                        emailFollowupTimeCheckCalendar.set(Calendar.MILLISECOND, 0);

                        emailFollowupTimeCheck = emailFollowupTimeCheckCalendar.getTimeInMillis();

                        HashMap<String, Object> emailFollowupProperties = new HashMap<>();
                        emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_CHECK, emailFollowupTimeCheck);
                        mJobApplicationRef.updateChildren(emailFollowupProperties);
                        utils.updateTimestampLastChanged(mJobApplicationRef);
                        mCardViewEmailFollowUp.setVisibility(View.GONE);

                    }
                };
                DatePickerDialog emailFollowupTimeDatePickerDialog = new DatePickerDialog(getContext(), R.style.CustomStyleTimeDatePickerDialog, email_followup_time_date, emailFollowupTimeCheckCalendar.get(Calendar.YEAR), emailFollowupTimeCheckCalendar.get(Calendar.MONTH), emailFollowupTimeCheckCalendar.get(Calendar.DAY_OF_MONTH));
                emailFollowupTimeDatePickerDialog.show();
            }
        });

        emailFollowupTimeCheckDialog.setNeutralButton(getResources().getString(R.string.dialog_email_followup_time_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        ListView mListViewDeadline = (ListView) convertView.findViewById(R.id.dialog_email_followup_time_lv);

        ArrayAdapter<CharSequence> adapterEmailFollowupTimeCheck = ArrayAdapter.createFromResource(getContext(), R.array.array_email_followup_time_check, android.R.layout.simple_spinner_dropdown_item);
        mListViewDeadline.setAdapter(adapterEmailFollowupTimeCheck);

        mListViewDeadline.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    emailFollowupTimeCheckCalendar.add(Calendar.DAY_OF_MONTH, 1);
                }
                if (position == 1) {
                    emailFollowupTimeCheckCalendar.add(Calendar.DAY_OF_MONTH, 7);
                }
                if (position == 2) {
                    emailFollowupTimeCheckCalendar.add(Calendar.MONTH, 1);
                }

                emailFollowupTimeCheckCalendar.set(Calendar.HOUR_OF_DAY, 9);
                emailFollowupTimeCheckCalendar.set(Calendar.MINUTE, 0);
                emailFollowupTimeCheckCalendar.set(Calendar.SECOND, 0);

                emailFollowupTimeCheck = emailFollowupTimeCheckCalendar.getTimeInMillis();

                Log.d(LOG_TAG, sdtf.format(emailFollowupTimeCheck));

                HashMap<String, Object> emailFollowupProperties = new HashMap<>();
                emailFollowupProperties.put(Constants.FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_CHECK, emailFollowupTimeCheck);
                mJobApplicationRef.updateChildren(emailFollowupProperties);
                utils.updateTimestampLastChanged(mJobApplicationRef);
                mCardViewEmailFollowUp.setVisibility(View.GONE);

                mDialogFollowupTimeCheck.dismiss();
            }
        });

        mDialogFollowupTimeCheck = emailFollowupTimeCheckDialog.show();

    }

    private void goToEmailFollowUpActivity(){
        Intent goToFollowupEmail = new Intent(getContext(), FollowupEmailActivity.class);
        goToFollowupEmail.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, jobApplicationId);
        goToFollowupEmail.putExtra(Constants.INTENT_EXTRA_PINNED, applicationPinned);
        getContext().startActivity(goToFollowupEmail);
    }

    private void addDeadlineToDeviceCalendar() {
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, deadlineDate);
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,deadlineDate);
        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
        intent.putExtra(CalendarContract.Events.TITLE, "Deadline for " + currentApplicationStage + " for " + jobTitle + " at " + companyName);
        startActivity(intent);

        if (mDialogDeadline != null) {
            mDialogDeadline.dismiss();
        }

    }

    private void createAddApplicationStageListDialog() {
        final AlertDialog.Builder dialogListApplicationStage = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_list_generic, null);
        dialogListApplicationStage.setView(dialogView);

        ListView mDialogListViewApplicationStage = (ListView) dialogView.findViewById(R.id.dialog_list_lv);
        mDialogListViewApplicationStage.setAdapter(addApplicationStageAdapter());

        mDialogListViewApplicationStage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                newApplicationStage = addApplicationStageAdapter().getItem(position);
                mSpinnerDialogAddApplicationStage.setText(newApplicationStage);

                //If Complete create dialog to set status
                if (newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
                    newApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[1];
                    mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus);
                } else {
                    newApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                    mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus);
                }


                mDialogApplicationStageList.dismiss();

            }
        });

        dialogListApplicationStage.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogApplicationStageList.dismiss();
            }

        });

        mDialogApplicationStageList = dialogListApplicationStage.show();
        mDialogApplicationStageList.setCanceledOnTouchOutside(false);

    }

    private void createAddApplicationStageStatusListDialog() {
        final AlertDialog.Builder dialogListApplicationStageStatus = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_list_generic, null);
        dialogListApplicationStageStatus.setView(dialogView);

        ListView mDialogListViewApplicationStageStatus = (ListView) dialogView.findViewById(R.id.dialog_list_lv);
        mDialogListViewApplicationStageStatus.setAdapter(addApplicationStageStatusAdapter());

        mDialogListViewApplicationStageStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                newApplicationStageStatus = addApplicationStageStatusAdapter().getItem(position);
                mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus);

                //If stageStatus set to unsuccessful/withdrawn then disable applicationStageSpinner
                if (newApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[2]) ||
                        newApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[3])) {
                    mSpinnerDialogAddApplicationStage.setEnabled(false);
                    mSpinnerDialogAddApplicationStage.setTextColor(ContextCompat.getColor(getContext(), R.color.spinner_unused));
                } else {
                    mSpinnerDialogAddApplicationStage.setEnabled(true);
                    mSpinnerDialogAddApplicationStage.setTextColor(ContextCompat.getColor(getContext(), R.color.spinner_active));
                }

                //If currentApplicationStageStatus isn't in progress, set currentApplicationStageDateCompleted to current date.
                if (!newApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[0])) {
                    newApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
                    mTextViewDialogAddApplicationStageDateCompleted.setText(sdf.format(newApplicationStageDateCompleted));
                    newApplicationStageCompleted = true;
                } else {
                    newApplicationStageDateCompleted = 0L;
                    mTextViewDialogAddApplicationStageDateCompleted.setText("-");
                    newApplicationStageCompleted = false;
                }

                mDialogAddApplicationStageStatusList.dismiss();

            }
        });

        dialogListApplicationStageStatus.setNegativeButton(getResources().getString(R.string.dialog_negative_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogAddApplicationStageStatusList.dismiss();
            }

        });

        mDialogAddApplicationStageStatusList = dialogListApplicationStageStatus.show();
        mDialogAddApplicationStageStatusList.setCanceledOnTouchOutside(false);

    }

    private void createEditApplicationStageStatusListDialog() {
        final AlertDialog.Builder dialogListApplicationStageStatus = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_list_generic, null);
        dialogListApplicationStageStatus.setView(dialogView);

        final ListView mListViewEditApplicationStageStatus = (ListView) dialogView.findViewById(R.id.dialog_list_lv);

        mListViewEditApplicationStageStatus.setAdapter(editApplicationStageStatusAdapter());

        dialogListApplicationStageStatus.setNegativeButton(getResources().getString(R.string.dialog_negative_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditApplicationStageList.dismiss();
            }

        });

        mListViewEditApplicationStageStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentApplicationStageStatus = editApplicationStageStatusAdapter().getItem(position);
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);

                //If currentApplicationStageStatus isn't in progress, set currentApplicationStageDateCompleted to current date.
                if (!currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[0])) {
                    currentApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
                    mTextViewDialogEditApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));
                    currentApplicationStageCompleted = true;
                } else {
                    currentApplicationStageDateCompleted = 0L;
                    mTextViewDialogEditApplicationStageDateCompleted.setText("-");
                    currentApplicationStageCompleted = false;
                }

                mDialogEditApplicationStageList.dismiss();

            }
        });

        mDialogEditApplicationStageList = dialogListApplicationStageStatus.show();
        mDialogEditApplicationStageList.setCanceledOnTouchOutside(false);

    }

    private ArrayAdapter<String> addApplicationStageAdapter() {

        ArrayAdapter<String> adb = new ArrayAdapter<>(getContext(), R.layout.list_item_generic, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_application_stage))));

        for (int i = 0; i < adb.getCount(); i++) {
            if (newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[i])) {
                adb.remove(getResources().getStringArray(R.array.array_application_stage)[i]);
            }
        }

        return adb;
    }

    private ArrayAdapter<String> addApplicationStageStatusAdapter() {
        final ArrayAdapter<String> adapterApplicationStageStatus = new ArrayAdapter<>(getContext(), R.layout.list_item_generic, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_application_stage_status))));

        //If Application Stage is completed, can not be 'In Progress'
        if (newApplicationStageCompleted) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[0]);
        }

        //If Complete, status cannot be In Progress
        if (newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[0]);
        }

        //If Not Yet Applied, status cannot be Successful, Unsuccessful or Withdrawn
        if (newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[1]);
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[2]);
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[3]);
        }

        return adapterApplicationStageStatus;
    }

    private ArrayAdapter<String> editApplicationStageStatusAdapter() {
        final ArrayAdapter<String> adapterApplicationStageStatus = new ArrayAdapter<>(getContext(), R.layout.list_item_currency, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_application_stage_status))));

        //If Application Stage is completed, can not be 'In Progress'
        if (currentApplicationStageCompleted) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[0]);
        }

        //If Complete, status cannot be In Progress
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[0]);
        }

        //If Not Yet Applied, status cannot be Successful, Unsuccessful or Withdrawn
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[1]);
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[2]);
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[3]);
        }

        return adapterApplicationStageStatus;
    }

    private void addPreviousApplicationStageToApplicationStageHistory() {
        //Add previous Application Stage to ApplicationStageHistory HashMap<>
        if (currentApplicationStageDateCompleted.equals(0L)) {
            currentApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
        }
        previousApplicationStage = new ApplicationStage(currentApplicationStage, currentApplicationStageStatus, currentApplicationStageNotes, currentApplicationStageDateStarted, currentApplicationStageDateCompleted, currentDeviceTimeCalendar.getTimeInMillis(), currentApplicationStageCompleted);
        mPreviousApplicationStagePushRef = mJobApplicationRef.child(Constants.FIREBASE_PROPERTY_APPLICATION_STAGE_HISTORY).push();
        mPreviousApplicationStagePushRef.setValue(previousApplicationStage);

        utils.updateTimestampLastChanged(mJobApplicationRef);

    }

    private void resetCurrentApplicationStageProperties() {

        setDefaultApplicationStage();

        setDefaultApplicationStageStatusFromApplicationStage();

        setDefaultApplicationStageDateStarted();

        setDefaultApplicationStageDateCompleted();

        setDefaultApplicationStageCompleted();

        newApplicationStageNotes = ""; //No Notes

        mSpinnerDialogAddApplicationStage.setText(newApplicationStage);
        mSpinnerDialogAddApplicationStageStatus.setText(newApplicationStageStatus); //In Progress
        mTextViewDialogAddApplicationStageDateStarted.setText(sdf.format(newApplicationStageDateStarted)); //Today's Date
        if (newApplicationStageCompleted) {
            mTextViewDialogAddApplicationStageDateCompleted.setText(sdf.format(newApplicationStageDateCompleted));
        } else {
            mTextViewDialogAddApplicationStageDateCompleted.setText("-");
        }

        mEditTextDialogAddApplicationStageNotes.setText(newApplicationStageNotes); //No Notes

    }

    private void setDefaultApplicationStage() {
        //Reset Current Application Stage property defaults
        for (int i = 0; i < 4; i++) {
            if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[i])) {
                newApplicationStage = getResources().getStringArray(R.array.array_application_stage)[i+1];
                break;
            }
        }
    }

    private void setDefaultApplicationStageStatusFromApplicationStage() {

        final String NOT_YET_APPLIED = getResources().getStringArray(R.array.array_application_stage)[0];
        final String CV_APPLICATION_FORM = getResources().getStringArray(R.array.array_application_stage)[1];
        final String TESTS_ASSESSMENTS = getResources().getStringArray(R.array.array_application_stage)[2];
        final String INTERVIEW = getResources().getStringArray(R.array.array_application_stage)[3];
        final String COMPLETE = getResources().getStringArray(R.array.array_application_stage)[4];

        final String IN_PROGRESS = getResources().getStringArray(R.array.array_application_status)[1];
        final String SUCCESSFUL = getResources().getStringArray(R.array.array_application_status)[2];

        if (newApplicationStage.equals(NOT_YET_APPLIED)) {
            newApplicationStageStatus = IN_PROGRESS;
        } else if (newApplicationStage.equals(CV_APPLICATION_FORM)) {
            newApplicationStageStatus = IN_PROGRESS;
        } else if (newApplicationStage.equals(TESTS_ASSESSMENTS)) {
            newApplicationStageStatus = IN_PROGRESS;
        } else if (newApplicationStage.equals(INTERVIEW)) {
            newApplicationStageStatus = IN_PROGRESS;
        } else if (newApplicationStage.equals(COMPLETE)) {
            newApplicationStageStatus = SUCCESSFUL;
        }

    }

    private void setDefaultApplicationStageDateStarted() {
        newApplicationStageDateStartedCalendar = currentDeviceTimeCalendar;
        newApplicationStageDateStarted = currentDeviceTimeCalendar.getTimeInMillis(); //Today's Date
    }

    private void setDefaultApplicationStageDateCompleted() {

        final String COMPLETE = getResources().getStringArray(R.array.array_application_stage)[4];

        newApplicationStageDateCompletedCalendar = currentDeviceTimeCalendar;

        if (newApplicationStage.equals(COMPLETE)) {
            newApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
        } else {
            newApplicationStageDateCompleted = 0L;
        }
    }

    private void setDefaultApplicationStageCompleted() {
        newApplicationStageCompleted = newApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4]);
    }

    @Override
    public void onStop() {
        super.onStop();
        // Cleanup when the activity is stopped.
        if (mJobApplicationRef != null) {
            mJobApplicationRef.removeEventListener(mActiveListListener);
        }
    }

}
