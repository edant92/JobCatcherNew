package com.este.jobcatcher.jobapplications;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.alarms.AlarmServiceBroadcastReceiver;
import com.este.jobcatcher.general.UpgradeActivity;
import com.este.jobcatcher.jobapplications.recyclerviewholders.AssociatedJsaItemHolder;
import com.este.jobcatcher.jsas.JsaNewActivity;
import com.este.jobcatcher.models.ApplicationStage;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.este.jobcatcher.utils.Utils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Allows a user to create a new Job Application
 */

public class JobApplicationNewActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    //General Variables
    private final String LOG_TAG = JobApplicationNewActivity.class.getSimpleName();

    private SharedPreferences mSharedPrefs;

    private String userUID;
    private String mJobApplicationId;
    private DatabaseReference mNewJobApplicationRef;
    private DatabaseReference mNewJobApplicationPushRef;
    private DatabaseReference mAssociatedJsaListRef;
    private DatabaseReference mAssociatedJsaItemRef;

    private Calendar currentDeviceTimeCalendar;
    private Calendar currentApplicationStageDateStartedCalendar;
    private Calendar currentApplicationStageDateCompletedCalendar;

    private Calendar deadlineDateCalendar;
    private Calendar reminderDateCalendar;
    private SimpleDateFormat stf;
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdtf;

    private String progressText;

    private ApplicationStage previousApplicationStage;
    private String previousApplicationStageKey;

    //Firebase Object Variables
    private String jobTitle;
    private String companyName;
    private Boolean locationRemote;
    private String locationName;
    private Double locationLat;
    private Double locationLng;
    private Boolean locationSet;
    private String currentApplicationStage;
    private String currentApplicationStageStatus;
    private Long currentApplicationStageDateStarted;
    private Long currentApplicationStageDateCompleted;
    private String currentApplicationStageNotes;
    private Boolean currentApplicationStageCompleted;
    private HashMap<String, ApplicationStage> applicationStageHistory;
    private Long deadlineDate;
    private Boolean reminderSet;
    private Integer reminderId;
    private Long reminderDateTime;
    private String wageCurrency;
    private Double wageAmount;
    private String wageFrequency;
    private String jobType;
    private Double jobHours;
    private String jobReference;
    private String jobDescription;
    private String jobAdvertURL;
    private String applicationNotes;
    private String contactTitle;
    private String contactFirstName;
    private String contactSurname;
    private String contactPosition;
    private String contactEmail;
    private String contactPhone;
    private Integer applicationPriority;
    private String associatedJobsiteAgencyID;

    private String applicationStatus;
    private Long applicationDateApplied;
    private Long applicationDateCompleted;

    //UI
    private EditText mEditTextJobTitle;
    private EditText mEditTextCompanyName;
    private LinearLayout mLinearLayoutLocation;
    private TextView mTextViewLocation;

    private LinearLayout mLinearLayoutCurrentApplicationStage;
    private TextView mTextViewCurrentApplicationStage;
    private TextView mTextViewCurrentApplicationStageStatus;
    private Button mButtonApplicationStageAdd;
    private AlertDialog mApplicationStageEditDialog;
    private TextView mSpinnerDialogEditApplicationStageStatus;
    private TextView mTextViewDialogEditApplicationStageDateStarted;
    private TextView mTextViewDialogEditApplicationStageDateCompleted;
    private AlertDialog mApplicationStageAddDialog;
    private TextView mSpinnerDialogAddApplicationStage;
    private TextView mSpinnerDialogAddApplicationStageStatus;
    private TextView mTextViewDialogAddApplicationStageDateStarted;
    private TextView mTextViewDialogAddApplicationStageDateComplete;
    private EditText mEditTextDialogAddApplicationStageNotes;

    private Dialog mDialogApplicationStageList;
    private Dialog mDialogAddApplicationStageStatusList;
    private Dialog mDialogEditApplicationStageList;

    private ListView mDialogListViewApplicationStage;
    private ListView mDialogListViewApplicationStageStatus;

    private LinearLayout mLinearLayoutDeadline;
    private TextView mTextViewDeadline;
    private LinearLayout mLinearLayoutReminder;
    private TextView mTextViewReminder;
    private TextView mTextViewWageCurrency;
    private EditText mEditTextWageAmount;
    private TextView mTextViewWageFrequency;
    private TextView mTextViewJobType;
    private EditText mEditTextJobHours;
    private EditText mEditTextJobReference;
    private EditText mEditTextJobDescription;
    private EditText mEditTextJobAdvertURL;
    private EditText mEditTextApplicationNotes;

    private TextView mTextViewContactTitle;
    private TextView mTextViewContactFirstName;
    private TextView mTextViewContactSurname;
    private TextView mTextViewContactPosition;
    private TextView mTextViewContactEmail;
    private TextView mTextViewContactPhone;
    private LinearLayout mLinearLayoutContactDetails;

    private RelativeLayout mLinearLayoutAssociatedJsa;
    private FrameLayout mFrameLayoutAssociatedJobsiteAgency;
    private AppCompatImageView mImageViewAssociatedJsaLink;
    private TextView mTextViewAssociatedJsa;
    private AppCompatImageView mImageViewAssociatedJsaClear;

    private SeekBar mSeekBarPriority;
    private TextView mTextViewEditPriority;

    private Dialog mDialogAssociatedJSAList;
    private ProgressBar mProgressBarAssociatedJobsiteAgency;
    private RecyclerView mRecyclerViewAssociatedJobsiteAgencyID;
    private FirebaseRecyclerAdapter<JobsiteAgency, AssociatedJsaItemHolder> mRecyclerViewAdapterAssociatedJsaList;


    private AlertDialog mDialogDeadline;
    private AlertDialog mDialogLocation;
    private AlertDialog mDialogWageCurrencyList;
    private AlertDialog mDialogWageFrequencyList;
    private Dialog mDialogJobTypeList;

    private AlertDialog mEditContactNameDialog;

    private AppCompatImageView mImageViewLocation;
    private AutoCompleteTextView mAutocompleteTextView;
    private ListView mListViewAutoComplete;
    private ProgressBar mAutocompleteProgressBar;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLngBounds BOUNDS_USER;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;

    //Reminder Dialog UI
    private AlertDialog mDialogReminder;
    private LinearLayout mLinearLayoutReminderEditTime;
    private TextView mTextViewReminderEditTime;
    private LinearLayout mLinearLayoutReminderEditDate;
    private TextView mTextViewReminderEditDate;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_application_new);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_job_application_new_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        currentDeviceTimeCalendar = Calendar.getInstance();

        String timeFormat = "hh:mm aaa";
        stf = new SimpleDateFormat(timeFormat, Locale.US);
        String dateFormat = "dd MMM";
        sdf = new SimpleDateFormat(dateFormat, Locale.US);
        String dateTimeformat = "dd MMM HH:mm";
        sdtf = new SimpleDateFormat(dateTimeformat, Locale.US);

        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(JobApplicationNewActivity.this);

        userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");
        mNewJobApplicationRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mNewJobApplicationPushRef = mNewJobApplicationRef.push();
        mJobApplicationId = mNewJobApplicationPushRef.getKey();
        mAssociatedJsaListRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);

        //Set location bounds from sharedPrefs
        Double mUserDefaultBoundsLat = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, "0.0"));
        Double mUserDefaultBoundsLng = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, "0.0"));
        Log.d(LOG_TAG, "Current LatLng: " + mUserDefaultBoundsLat + ", " + mUserDefaultBoundsLng);
        BOUNDS_USER = new LatLngBounds(
                new LatLng(mUserDefaultBoundsLat, mUserDefaultBoundsLng), new LatLng(mUserDefaultBoundsLat, mUserDefaultBoundsLng));

        setDefaultValues();

        initialiseUI();

        setDefaultSavedInputsValues();

        Intent intent = this.getIntent();
        String fromActivity = intent.getStringExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY);
        if (fromActivity != null && fromActivity.equals(JsaNewActivity.class.getSimpleName())) {
            associatedJobsiteAgencyID = intent.getStringExtra(Constants.INTENT_EXTRA_JSA_ID);
            if (associatedJobsiteAgencyID != null && !associatedJobsiteAgencyID.equals("")) {
                mAssociatedJsaItemRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(associatedJobsiteAgencyID);
                jsaListConfirmAssociate();
            }
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present. */
        getMenuInflater().inflate(R.menu.menu_job_application_new_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save_new_job_application) {
            getValuesToCreateNewJobApplicationObject();
        }

        if (id == R.id.menu_new_application_form_action_clear_form) {
            clearFormValuesCheck();
        }

        //return true;
        return super.onOptionsItemSelected(item);
    }

    //Set Default values and create default Job Application Object in Database
    private void setDefaultValues() {
        jobTitle = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TITLE, "");
        companyName = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_COMPANY_NAME, "");
        locationRemote = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_REMOTE, false);
        locationName = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_NAME, "");
        locationLat = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LAT, "0.0"));
        locationLng = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LONG, "0.0"));
        locationSet = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_SET, false);
        currentApplicationStage = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE, getResources().getStringArray(R.array.array_application_stage)[0]); //Not Yet Applied
        currentApplicationStageStatus = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS, getResources().getStringArray(R.array.array_application_stage_status)[0]); //In Progress
        currentApplicationStageDateStartedCalendar = currentDeviceTimeCalendar;
        currentApplicationStageDateCompletedCalendar = currentDeviceTimeCalendar;
        currentApplicationStageDateStarted = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED, 0L);
        currentApplicationStageDateCompleted = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED, 0L); //Not Completed
        currentApplicationStageNotes = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES, ""); //No Notes
        currentApplicationStageCompleted = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_COMPLETED, false);
        applicationStageHistory = new HashMap<>();
        fetchSavedApplicationStageHistoryData();
        applicationStatus = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS, getResources().getStringArray(R.array.array_application_status)[0]); //Prospective
        if (mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS, "").equals("")) {
            applicationStatus = getResources().getStringArray(R.array.array_application_status)[0];
        }
        deadlineDateCalendar = currentDeviceTimeCalendar;
        reminderDateCalendar = currentDeviceTimeCalendar;
        applicationDateApplied = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_APPLIED, 0L);
        applicationDateCompleted = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_COMPLETED, 0L);
        deadlineDate = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_DEADLINE_DATE, 0L);
        reminderSet = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_SET, false);
        reminderId = mSharedPrefs.getInt(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_ID, 0);
        reminderDateTime = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_DATE_TIME, 0L);
        wageCurrency = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_CURRENCY, getResources().getStringArray(R.array.array_currency)[0]);
        try {
            wageAmount = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_AMOUNT, "0.00"));
        } catch (NumberFormatException e) {
            wageAmount = 0.00;
        }
        wageFrequency = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_FREQUENCY, getResources().getStringArray(R.array.array_wage_frequency)[0]);
        jobReference = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_REFERENCE, "");
        jobDescription = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_DESCRIPTION, "");
        jobAdvertURL = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_ADVERT_URL, "");
        jobType = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TYPE, getResources().getStringArray(R.array.array_job_type)[0]);
        try {
            jobHours = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_HOURS, "0.0"));
        } catch (NumberFormatException e) {
            jobHours = 0.0;
        }
        applicationNotes = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_NOTES, "");
        applicationPriority = mSharedPrefs.getInt(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_PRIORITY, 2);
        associatedJobsiteAgencyID = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_ASSOCIATED_JSA_ID, "");
        contactTitle = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_TITLE, "");
        contactFirstName = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_FIRST_NAME, "");
        contactSurname = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_SURNAME, "");
        contactPosition = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_POSITION, "");
        contactEmail = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_EMAIL, "");
        contactPhone = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_PHONE, "");

    }

    @SuppressLint("CommitPrefEdits")
    public void clearDefaultValues() {
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TITLE, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_COMPANY_NAME, "");
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_APPLIED, 0L);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_COMPLETED, 0L);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE, getResources().getStringArray(R.array.array_application_stage)[0]);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS, getResources().getStringArray(R.array.array_application_stage_status)[0]);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED, 0L);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED, 0L);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS, getResources().getStringArray(R.array.array_application_status)[0]);

        saveData(JobApplicationNewActivity.this, applicationStageHistory);

        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_DEADLINE_DATE, 0L);
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_SET, false);
        editor.putInt(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_ID, 0);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_DATE_TIME, 0L);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_CURRENCY, mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_WAGE_CURRENCY, getResources().getStringArray(R.array.array_currency)[0])); //User default currency
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_AMOUNT, "0.00");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_FREQUENCY, getResources().getStringArray(R.array.array_wage_frequency)[0]);
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_REMOTE, false);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_NAME, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LAT, "0.0");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LONG, "0.0");
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_SET, false);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_REFERENCE, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_DESCRIPTION, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_ADVERT_URL, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TYPE, getResources().getStringArray(R.array.array_job_type)[0]);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_HOURS, "0.0");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_NOTES, "");
        editor.putInt(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_PRIORITY, 2);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_ASSOCIATED_JSA_ID, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_TITLE, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_FIRST_NAME, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_SURNAME, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_POSITION, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_EMAIL, "");
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_PHONE, "");
        editor.commit();

        jobTitle = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TITLE, "");
        companyName = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_COMPANY_NAME, "");
        locationRemote = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_REMOTE, false);
        locationName = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_NAME, "");
        locationLat = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LAT, "0.0"));
        locationLng = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LONG, "0.0"));
        locationSet = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_SET, false);
        currentApplicationStage = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE, getResources().getStringArray(R.array.array_application_stage)[0]); //Not Yet Applied
        currentApplicationStageStatus = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS, getResources().getStringArray(R.array.array_application_stage_status)[0]); //In Progress
        currentApplicationStageDateStartedCalendar = currentDeviceTimeCalendar;
        currentApplicationStageDateCompletedCalendar = currentDeviceTimeCalendar;
        currentApplicationStageDateStarted = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED, 0L);
        currentApplicationStageDateCompleted = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED, 0L); //Not Completed
        currentApplicationStageNotes = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES, ""); //No Notes
        currentApplicationStageCompleted = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_COMPLETED, false);
        applicationStageHistory.clear();
        saveData(JobApplicationNewActivity.this, applicationStageHistory);
        applicationStatus = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS, getResources().getStringArray(R.array.array_application_status)[0]); //Prospective
        if (mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS, "").equals("")) {
            applicationStatus = getResources().getStringArray(R.array.array_application_status)[0];
        }
        deadlineDateCalendar = currentDeviceTimeCalendar;
        reminderDateCalendar = currentDeviceTimeCalendar;
        applicationDateApplied = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_APPLIED, 0L);
        applicationDateCompleted = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_COMPLETED, 0L);
        deadlineDate = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_DEADLINE_DATE, 0L);
        reminderSet = mSharedPrefs.getBoolean(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_SET, false);
        reminderId = mSharedPrefs.getInt(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_ID, 0);
        reminderDateTime = mSharedPrefs.getLong(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_DATE_TIME, 0L);
        wageCurrency = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_CURRENCY, mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_WAGE_CURRENCY, getResources().getStringArray(R.array.array_currency)[0])); //User default Currency
        try {
            wageAmount = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_AMOUNT, "0.00"));
        } catch (NumberFormatException e) {
            wageAmount = 0.00;
        }
        wageFrequency = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_FREQUENCY, getResources().getStringArray(R.array.array_wage_frequency)[0]);
        jobReference = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_REFERENCE, "");
        jobDescription = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_DESCRIPTION, "");
        jobAdvertURL = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_ADVERT_URL, "");
        jobType = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TYPE, getResources().getStringArray(R.array.array_job_type)[0]);
        try {
            jobHours = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_HOURS, "0.0"));
        } catch (NumberFormatException e) {
            jobHours = 0.0;
        }
        applicationNotes = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_NOTES, "");
        applicationPriority = mSharedPrefs.getInt(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_PRIORITY, 2);
        associatedJobsiteAgencyID = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_ASSOCIATED_JSA_ID, "");
        contactTitle = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_TITLE, "");
        contactFirstName = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_FIRST_NAME, "");
        contactSurname = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_SURNAME, "");
        contactPosition = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_POSITION, "");
        contactEmail = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_EMAIL, "");
        contactPhone = mSharedPrefs.getString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_PHONE, "");

    }

    private void setDefaultSavedInputsValues() {

        if (!jobTitle.equals("")) {
            mEditTextJobTitle.setText(jobTitle);
        }
        if (!companyName.equals("")) {
            mEditTextCompanyName.setText(companyName);
        }
        if (!locationName.equals("")) {
            mTextViewLocation.setText(locationName);
        }

        mTextViewCurrentApplicationStage.setText(currentApplicationStage);
        mTextViewCurrentApplicationStageStatus.setText(currentApplicationStageStatus);

        if (!deadlineDate.equals(0L)) {
            mTextViewDeadline.setText(sdf.format(deadlineDate));
        }
        if (!reminderDateTime.equals(0L)) {
            mTextViewReminder.setText(sdtf.format(reminderDateTime));
        }

        mTextViewWageCurrency.setText(wageCurrency);

        if (!wageAmount.equals(0.00) ) {
            mEditTextWageAmount.setText(String.format(Locale.UK, "%.2f", wageAmount));
        }

        mTextViewWageFrequency.setText(wageFrequency);

        if (!jobType.equals("")) {
            mTextViewJobType.setText(jobType);
        }

        if (!jobHours.equals(0.0)) {
            mEditTextJobHours.setText(NumberFormat.getInstance().format(jobHours));
        }

        if (!jobReference.equals("")) {
            mEditTextJobReference.setText(jobReference);

        }

        if (!jobDescription.equals("")) {
            mEditTextJobDescription.setText(jobDescription);

        }

        if (!jobAdvertURL.equals("")) {
            mEditTextJobAdvertURL.setText(jobAdvertURL);

        }

        if (!applicationNotes.equals("")) {
            mEditTextApplicationNotes.setText(applicationNotes);
        }


        if (!contactTitle.equals("")) {
            mTextViewContactTitle.setText(contactTitle);
        }
        if (!contactFirstName.equals("")) {
            mTextViewContactFirstName.setText(contactFirstName);
        }
        if (!contactSurname.equals("")) {
            mTextViewContactSurname.setText(contactSurname);
        }
        if (!contactPosition.equals("")) {
            mTextViewContactPosition.setText(contactPosition);
        }
        if (!contactEmail.equals("")) {
            mTextViewContactEmail.setText(contactEmail);
        }
        if (!contactPhone.equals("")) {
            mTextViewContactPhone.setText(contactPhone);
        }

        mSeekBarPriority.setProgress(applicationPriority);

        if (!associatedJobsiteAgencyID.equals("")) {

            mAssociatedJsaItemRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(associatedJobsiteAgencyID);
            mAssociatedJsaItemRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    JobsiteAgency associatedJsaItemData = dataSnapshot.getValue(JobsiteAgency.class);

                    String associatedJsaName = associatedJsaItemData.getJsaName();

                    mTextViewAssociatedJsa.setText(associatedJsaName);
                    mTextViewAssociatedJsa.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.white));

                    mImageViewAssociatedJsaClear.setVisibility(View.VISIBLE);
                    mImageViewAssociatedJsaLink.setVisibility(View.GONE);
                    mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.colorAccent));

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    public void clearInputsValues() {
        mEditTextJobTitle.setText(jobTitle);
        mEditTextCompanyName.setText(companyName);
        mTextViewLocation.setText(getResources().getString(R.string.activity_job_application_new_tv_location));
        mTextViewCurrentApplicationStage.setText(currentApplicationStage);
        mTextViewCurrentApplicationStageStatus.setText(currentApplicationStageStatus);
        mTextViewDeadline.setText(R.string.activity_job_application_new_tv_deadline_date_empty);
        mTextViewReminder.setText(R.string.activity_job_application_new_tv_reminder_empty);
        mTextViewWageCurrency.setText(wageCurrency);
        mEditTextWageAmount.setText("");
        mTextViewWageFrequency.setText(wageFrequency);
        mTextViewJobType.setText(jobType);
        mEditTextJobHours.setText("");
        mEditTextJobReference.setText(jobReference);
        mEditTextJobDescription.setText(jobDescription);
        mEditTextJobAdvertURL.setText(jobAdvertURL);
        mEditTextApplicationNotes.setText(applicationNotes);
        mTextViewContactTitle.setText(contactTitle);
        mTextViewContactFirstName.setText(contactFirstName);
        mTextViewContactSurname.setText(contactSurname);
        mTextViewContactPosition.setText(contactPosition);
        mTextViewContactEmail.setText(getResources().getString(R.string.activity_job_application_new_tv_contact_email_empty));
        mTextViewContactPhone.setText(getResources().getString(R.string.activity_job_application_new_tv_contact_phone_empty));
        mSeekBarPriority.setProgress(applicationPriority);
        mTextViewAssociatedJsa.setText(getResources().getString(R.string.activity_job_application_new_tv_associated_jsa_link_empty));
        mTextViewAssociatedJsa.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
        mImageViewAssociatedJsaClear.setVisibility(View.GONE);
        mImageViewAssociatedJsaLink.setVisibility(View.VISIBLE);
        mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.white));

    }

    @SuppressLint("CommitPrefEdits")
    private void saveFormPropertiesToSharedPrefs() {
        //If user has entered some details but hasn't saved the form
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TITLE, mEditTextJobTitle.getText().toString().trim());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_COMPANY_NAME, mEditTextCompanyName.getText().toString().trim());
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_APPLIED, applicationDateApplied);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_DATE_COMPLETED, applicationDateCompleted);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE, currentApplicationStage);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS, currentApplicationStageStatus);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED, currentApplicationStageDateStarted);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED, currentApplicationStageDateCompleted);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES, currentApplicationStageNotes);
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_COMPLETED, currentApplicationStageCompleted);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS, applicationStatus);
        saveData(JobApplicationNewActivity.this, applicationStageHistory);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_DEADLINE_DATE, deadlineDate);
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_SET, reminderSet);
        editor.putInt(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_ID, reminderId);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_DATE_TIME, reminderDateTime);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_CURRENCY, wageCurrency);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_AMOUNT, mEditTextWageAmount.getText().toString().trim());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_WAGE_FREQUENCY, wageFrequency);
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_REMOTE, locationRemote);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_NAME, locationName);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LAT, locationLat.toString());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_LONG, locationLng.toString());
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_LOCATION_SET, locationSet);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_REFERENCE, mEditTextJobReference.getText().toString().trim());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_DESCRIPTION, mEditTextJobDescription.getText().toString().trim());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_ADVERT_URL, mEditTextJobAdvertURL.getText().toString().trim());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_TYPE, jobType);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_JOB_HOURS, mEditTextJobHours.getText().toString().trim());
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_NOTES, mEditTextApplicationNotes.getText().toString());
        editor.putInt(Constants.SHARED_PREF_FORM_PROPERTY_APPLICATION_PRIORITY, applicationPriority);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_ASSOCIATED_JSA_ID, associatedJobsiteAgencyID);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_TITLE, contactTitle);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_FIRST_NAME, contactFirstName);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_SURNAME, contactSurname);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_POSITION, contactPosition);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_EMAIL, contactEmail);
        editor.putString(Constants.SHARED_PREF_FORM_PROPERTY_CONTACT_PHONE, contactPhone);
        editor.commit();
        Log.d(LOG_TAG, editor.toString());
    }

    private void initialiseUI() {
        mEditTextJobTitle = (EditText) findViewById(R.id.activity_ja_new_et_job_title);
        mEditTextCompanyName = (EditText) findViewById(R.id.activity_ja_new_et_company_name);
        mLinearLayoutLocation = (LinearLayout) findViewById(R.id.activity_ja_new_ll_location);
        mTextViewLocation = (TextView) findViewById(R.id.activity_ja_new_tv_location);
        mLinearLayoutCurrentApplicationStage = (LinearLayout) findViewById(R.id.activity_ja_new_ll_application_stage);
        mTextViewCurrentApplicationStage = (TextView) findViewById(R.id.activity_ja_new_tv_application_stage);
        mTextViewCurrentApplicationStageStatus = (TextView) findViewById(R.id.activity_ja_new_tv_application_stage_status);
        mButtonApplicationStageAdd = (Button) findViewById(R.id.activity_ja_new_bt_add_application_stage);
        mLinearLayoutDeadline = (LinearLayout) findViewById(R.id.activity_ja_new_ll_deadline_date);
        mTextViewDeadline = (TextView) findViewById(R.id.activity_ja_new_tv_deadline_date);
        mLinearLayoutReminder = (LinearLayout) findViewById(R.id.activity_ja_new_ll_reminder);
        mTextViewReminder = (TextView) findViewById(R.id.activity_ja_new_tv_reminder);
        mTextViewWageCurrency = (TextView) findViewById(R.id.activity_ja_new_tv_wage_currency);
        mEditTextWageAmount = (EditText) findViewById(R.id.activity_ja_new_et_wage_amount);
        mTextViewWageFrequency = (TextView) findViewById(R.id.activity_ja_new_tv_wage_frequency);
        mTextViewJobType = (TextView) findViewById(R.id.activity_ja_new_tv_job_type);
        mEditTextJobHours = (EditText) findViewById(R.id.activity_ja_new_et_job_hours);
        mEditTextJobReference = (EditText) findViewById(R.id.activity_ja_new_et_job_reference);
        mEditTextJobDescription = (EditText) findViewById(R.id.activity_ja_new_et_job_description);
        mEditTextJobAdvertURL = (EditText) findViewById(R.id.activity_ja_new_et_job_advert_url);
        mEditTextApplicationNotes = (EditText) findViewById(R.id.activity_ja_new_et_application_notes);

        mTextViewContactTitle = (TextView) findViewById(R.id.activity_ja_new_tv_contact_title);
        mTextViewContactFirstName = (TextView) findViewById(R.id.activity_ja_new_tv_contact_first_name);
        mTextViewContactSurname = (TextView) findViewById(R.id.activity_ja_new_tv_contact_surname);
        mTextViewContactPosition = (TextView) findViewById(R.id.activity_ja_new_tv_contact_position);
        mTextViewContactEmail = (TextView) findViewById(R.id.activity_ja_new_tv_contact_email);
        mTextViewContactPhone = (TextView) findViewById(R.id.activity_ja_new_tv_contact_phone);
        mLinearLayoutContactDetails = (LinearLayout) findViewById(R.id.activity_ja_new_ll_contact_details);

        mLinearLayoutAssociatedJsa = (RelativeLayout) findViewById(R.id.activity_ja_new_rl_associated_jsa);
        mFrameLayoutAssociatedJobsiteAgency = (FrameLayout) findViewById(R.id.activity_ja_new_fl_associated_jsa);
        mImageViewAssociatedJsaLink = (AppCompatImageView) findViewById(R.id.activity_ja_new_iv_associated_jsa_link);
        mTextViewAssociatedJsa = (TextView) findViewById(R.id.activity_ja_new_tv_associated_jsa);
        mImageViewAssociatedJsaClear = (AppCompatImageView) findViewById(R.id.activity_ja_new_ib_associated_jsa_clear);

        mSeekBarPriority = (SeekBar) findViewById(R.id.activity_ja_new_sb_priority);
        mTextViewEditPriority = (TextView) findViewById(R.id.activity_ja_new_tv_priority);

        mLinearLayoutDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDeadlineDialog();
            }
        });

        mLinearLayoutReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createReminderEditDialog();
            }
        });

        mLinearLayoutLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createLocationAddEditDialog();
            }
        });

        mLinearLayoutCurrentApplicationStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createApplicationStageEditDialog();
            }
        });

        mTextViewCurrentApplicationStage.setText(currentApplicationStage);
        mTextViewCurrentApplicationStageStatus.setText(currentApplicationStageStatus);

        mButtonApplicationStageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createApplicationStageAddDialog();
            }
        });

        mTextViewWageCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createWageCurrencyListDialog();
            }
        });

        mTextViewWageFrequency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createWageFrequencyListDialog();
            }
        });


        mTextViewJobType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createJobTypeListDialog();
            }
        });

/*        ArrayAdapter<CharSequence> adapterJobtype = ArrayAdapter.createFromResource(JobApplicationNewActivity.this, R.array.array_job_type, R.layout.list_item_job_type);
        mTextViewJobType.setAdapter(adapterJobtype);*/

        mEditTextJobAdvertURL.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!mEditTextJobAdvertURL.getText().toString().equals("")) {
                    if (!Patterns.WEB_URL.matcher(mEditTextJobAdvertURL.getText().toString()).matches()) {
                        mEditTextJobAdvertURL.setError("Hmm, that doesn't look like a real url...");
                    }
                }
            }
        });

        mLinearLayoutContactDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEditContactDetailsDialog();
            }
        });

        mLinearLayoutAssociatedJsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageViewAssociatedJsaClear.getVisibility() == View.GONE) {
                    createAssociatedJsaListDialog();
                } else {
                    associatedJobsiteAgencyClear();
                }
            }
        });

        mImageViewAssociatedJsaClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                associatedJobsiteAgencyClear();
            }
        });

        mSeekBarPriority.setProgress(applicationPriority);
        progressText = getResources().getStringArray(R.array.array_job_priority)[applicationPriority];
        mTextViewEditPriority.setText(progressText);
        mSeekBarPriority.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                applicationPriority = progressValue;
                progressText = getResources().getStringArray(R.array.array_job_priority)[applicationPriority];
                mTextViewEditPriority.setText(progressText);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }

    private void createLocationAddEditDialog() {

        final AlertDialog.Builder addLocationDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_location_edit, null);
        addLocationDialog.setView(dialogView);

        mImageViewLocation = (AppCompatImageView) dialogView.findViewById(R.id.dialog_location_edit_iv_location_icon);
        mAutocompleteProgressBar = (ProgressBar) dialogView.findViewById(R.id.dialog_location_edit_progress_bar);
        mAutocompleteTextView = (AutoCompleteTextView) dialogView.findViewById(R.id.dialog_location_edit_autocomplete_create_account);
        mListViewAutoComplete = (ListView) dialogView.findViewById(R.id.activity_map_edit_lv_search_result);
        final CheckBox mCheckBoxNoLocation = (CheckBox) dialogView.findViewById(R.id.dialog_location_edit_cb_no_location);
        final LinearLayout mLinearLayoutLocationUser = (LinearLayout) dialogView.findViewById(R.id.dialog_location_edit_ll_location_user);
        final AppCompatImageView mImageViewLocationUser = (AppCompatImageView) dialogView.findViewById(R.id.dialog_location_edit_iv_location_user_icon);
        final TextView mTextViewLocationUser = (TextView) dialogView.findViewById(R.id.dialog_location_edit_tv_location_user);

        //Location API Code
        mGoogleApiClient = new GoogleApiClient.Builder(JobApplicationNewActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiStart();
        if (locationName != null) {
            mAutocompleteTextView.setText(locationName);
            mAutocompleteTextView.setSelection(mAutocompleteTextView.getText().length());
        }
        setAutoCompleteOnClickListener();
        mAutocompleteTextView.setThreshold(2);
        mAutocompleteTextView.setDropDownHeight(0);
        mPlaceArrayAdapter = new PlaceArrayAdapter(JobApplicationNewActivity.this, R.layout.list_item_location_autocomplete,BOUNDS_USER, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);
        mListViewAutoComplete.setAdapter(mPlaceArrayAdapter);

        mCheckBoxNoLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    locationRemote = true;
                    locationName = "Remote";
                    locationLat = 0.0;
                    locationLng = 0.0;
                    locationSet = false;
                    mAutocompleteTextView.setEnabled(false);
                    mAutocompleteTextView.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
                    mLinearLayoutLocationUser.setEnabled(false);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
                    mTextViewLocationUser.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused), ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
                    mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                } else {
                    locationRemote = false;
                    locationName = "";
                    locationLat = 0.0;
                    locationLng = 0.0;
                    locationSet = false;
                    mAutocompleteTextView.setEnabled(true);
                    mAutocompleteTextView.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.black));
                    mLinearLayoutLocationUser.setEnabled(true);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.black));
                    mTextViewLocationUser.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.black));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.black));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(android.R.color.black, android.R.color.black);
                    mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                }
            }
        });

        if (locationRemote) {
            mCheckBoxNoLocation.setChecked(true);
        } else {
            mCheckBoxNoLocation.setChecked(false);
        }

        mLinearLayoutLocationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserLocation();
            }
        });

        mAutocompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mListViewAutoComplete.getCount() > 0) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                    if (mListViewAutoComplete.getAdapter() == null){
                        mListViewAutoComplete.setAdapter(mPlaceArrayAdapter);
                    }
                } else if (mAutocompleteTextView.getText().toString().length() > 2) {
                    mAutocompleteProgressBar.setVisibility(View.VISIBLE);
                    mImageViewLocation.setVisibility(View.GONE);
                    if (mListViewAutoComplete.getAdapter() == null){
                        mListViewAutoComplete.setAdapter(mPlaceArrayAdapter);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mListViewAutoComplete.getCount() > 0) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                }
            }
        });

        mAutocompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = mAutocompleteTextView.getRight()
                            - mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        mAutocompleteTextView.setText("");
                        mListViewAutoComplete.setAdapter(null);
                        return true;
                    }
                }
                return false;
            }
        });

        addLocationDialog.setPositiveButton(getResources().getString(R.string.dialog_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (locationName != null) {
                    mTextViewLocation.setText(locationName);
                }
                googleApiStop();
                mDialogLocation.dismiss();
            }
        });
        addLocationDialog.setNegativeButton(getResources().getString(R.string.dialog_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiStop();
                mDialogLocation.dismiss();
            }
        });

        addLocationDialog.setNeutralButton(getResources().getString(R.string.dialog_location_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                locationRemote = false;
                locationName = "";
                locationLat = 0.0;
                locationLng = 0.0;
                locationSet = false;
                mTextViewLocation.setText(getResources().getString(R.string.activity_job_application_new_tv_location));
                googleApiStop();
                mDialogLocation.dismiss();
            }
        });

        mDialogLocation = addLocationDialog.show();
        mDialogLocation.setCancelable(false);
        mDialogLocation.setCanceledOnTouchOutside(false);

    }

    private void setAutoCompleteOnClickListener() {
        AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(LOG_TAG, "Selected Position: " + position);
                final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
                if (item != null) {
                    final String placeId = String.valueOf(item.placeId);
                    Log.i(LOG_TAG, "Selected: " + item.description);
                    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
                    setPlaceDetailsCallback(placeResult);
                    Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
                }
            }
        };
        mListViewAutoComplete.setOnItemClickListener(mAutocompleteClickListener);
    }

    private void getUserLocation() {
        if (ContextCompat.checkSelfPermission(JobApplicationNewActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(JobApplicationNewActivity.this, new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(JobApplicationNewActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            System.out.println("locationLat: " + lastLocation);

            if (lastLocation != null) {

                locationLat = lastLocation.getLatitude();
                locationLng = lastLocation.getLongitude();
                locationSet = true;

                try {

                    Geocoder geo = new Geocoder(JobApplicationNewActivity.this, Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(locationLat, locationLng, 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(JobApplicationNewActivity.this, "Waiting for location", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (addresses.size() > 0) {
                            if (addresses.get(0).getLocality() != null) {
                                locationName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
                            } else {
                                locationName = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getCountryName();
                            }

                            Log.d(LOG_TAG, addresses.get(0).toString());

                            if (locationName != null) {
                                mTextViewLocation.setText(locationName);
                            }
                            googleApiStop();
                            if (mDialogLocation != null) {
                                mDialogLocation.dismiss();
                            }

                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                Toast.makeText(JobApplicationNewActivity.this, getResources().getString(R.string.dialog_location_edit_tv_user_location_error), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void createDeadlineDialog() {
        final AlertDialog.Builder deadlineDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_deadline_date, null);
        deadlineDialog.setView(convertView);
        deadlineDialog.setTitle(getResources().getString(R.string.dialog_deadline_date_title));

        String deadlineNeutralButtonText;

        deadlineDateCalendar = Calendar.getInstance();

        if (deadlineDate == 0L) {
            deadlineNeutralButtonText = getString(R.string.dialog_deadline_date_neutral_button_cancel);
        } else {
            deadlineNeutralButtonText = getString(R.string.dialog_deadline_date_neutral_button_remove);
        }

        deadlineDialog.setPositiveButton(getString(R.string.dialog_deadline_date_positive_button), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        deadlineDateCalendar.set(Calendar.YEAR, year);
                        deadlineDateCalendar.set(Calendar.MONTH, monthOfYear);
                        deadlineDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        deadlineDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
                        deadlineDateCalendar.set(Calendar.MINUTE, 0);
                        deadlineDateCalendar.set(Calendar.SECOND, 0);
                        deadlineDateCalendar.set(Calendar.MILLISECOND, 0);

                        deadlineDate = deadlineDateCalendar.getTimeInMillis();
                        mTextViewDeadline.setText(sdf.format(deadlineDate));
                    }
                };
                DatePickerDialog deadlineDatePickerDialog = new DatePickerDialog(JobApplicationNewActivity.this, R.style.CustomStyleTimeDatePickerDialog, deadline_date, deadlineDateCalendar.get(Calendar.YEAR), deadlineDateCalendar.get(Calendar.MONTH), deadlineDateCalendar.get(Calendar.DAY_OF_MONTH));
                deadlineDatePickerDialog.show();
            }
        });
        deadlineDialog.setNeutralButton(deadlineNeutralButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deadlineDate = 0L;
                mTextViewDeadline.setText(R.string.activity_job_application_new_tv_deadline_date_empty);
            }
        });

        ListView mListViewDeadline = (ListView) convertView.findViewById(R.id.dialog_deadline_date_lv);

        ArrayAdapter<CharSequence> adapterDeadline = ArrayAdapter.createFromResource(JobApplicationNewActivity.this, R.array.array_deadline_date, android.R.layout.simple_spinner_dropdown_item);
        mListViewDeadline.setAdapter(adapterDeadline);

        mListViewDeadline.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {
                    deadlineDateCalendar.add(Calendar.DAY_OF_MONTH, 1);
                }
                if (position == 2) {
                    deadlineDateCalendar.add(Calendar.DAY_OF_MONTH, 7);
                }
                if (position == 3) {
                    deadlineDateCalendar.add(Calendar.MONTH, 1);
                }

                deadlineDateCalendar.set(Calendar.HOUR_OF_DAY, 9);
                deadlineDateCalendar.set(Calendar.MINUTE, 0);
                deadlineDateCalendar.set(Calendar.SECOND, 0);

                deadlineDate = deadlineDateCalendar.getTimeInMillis();

                mTextViewDeadline.setText(sdf.format(deadlineDate));

                mDialogDeadline.dismiss();
            }
        });

        mDialogDeadline = deadlineDialog.show();

    }

    private void createReminderEditDialog() {

        final AlertDialog.Builder reminderEditAlertDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_reminder_edit, null);
        reminderEditAlertDialog.setView(convertView);
        mLinearLayoutReminderEditTime = (LinearLayout) convertView.findViewById(R.id.dialog_reminder_edit_ll_time);
        mLinearLayoutReminderEditDate = (LinearLayout) convertView.findViewById(R.id.dialog_reminder_edit_ll_date);
        mTextViewReminderEditTime = (TextView) convertView.findViewById(R.id.dialog_reminder_edit_tv_time);
        mTextViewReminderEditDate = (TextView) convertView.findViewById(R.id.dialog_reminder_edit_tv_date);

        reminderEditTimeDatePickers();

        if (!reminderDateTime.equals(0L)) {
            mTextViewReminderEditTime.setText(stf.format(reminderDateTime));
            mTextViewReminderEditDate.setText(sdf.format(reminderDateTime));
        }

        reminderEditAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_reminder_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mTextViewReminder.setText(sdtf.format(reminderDateTime));
                reminderSet = true;
                mDialogReminder.dismiss();
            }
        });
        reminderEditAlertDialog.setNegativeButton(getString(R.string.dialog_reminder_edit_negative_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogReminder.dismiss();
            }
        });
        reminderEditAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_reminder_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createDeleteReminderDialog();
            }
        });

        mDialogReminder = reminderEditAlertDialog.create();
        mDialogReminder.show();

        mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

    }

    private void createDeleteReminderDialog() {
        new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle(JobApplicationNewActivity.this.getString(R.string.dialog_reminder_delete_title))
                .setMessage(JobApplicationNewActivity.this.getString(R.string.dialog_reminder_delete_message_p1)
                        + JobApplicationNewActivity.this.getString(R.string.dialog_reminder_delete_message_p2))
                .setPositiveButton(JobApplicationNewActivity.this.getString(R.string.dialog_reminder_delete_positive_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteReminder();
                        callMathAlarmScheduleServiceDelete();
                    }
                })
                .setNegativeButton(R.string.dialog_reminder_delete_negative_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    @SuppressLint("CommitPrefEdits")
    private void deleteReminder() {
        reminderSet = false;
        reminderId = 0;
        reminderDateTime = 0L;
        reminderDateCalendar = currentDeviceTimeCalendar;

        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putBoolean(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_SET, reminderSet);
        editor.putInt(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_ID, reminderId);
        editor.putLong(Constants.SHARED_PREF_FORM_PROPERTY_REMINDER_DATE_TIME, reminderDateTime);
        editor.commit();

        mTextViewReminder.setText(getResources().getString(R.string.activity_job_application_new_tv_reminder_empty));
    }

    private void reminderEditTimeDatePickers() {

        if (!reminderDateTime.equals(0L)) {
            reminderDateCalendar.setTimeInMillis(reminderDateTime);
        }

        final TimePickerDialog.OnTimeSetListener reminder_edit_time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                reminderDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                reminderDateCalendar.set(Calendar.MINUTE, minute);
                mTextViewReminderEditTime.setText(stf.format(reminderDateCalendar.getTime()));
                reminderDateTime = reminderDateCalendar.getTimeInMillis();
                if (!mTextViewReminderEditDate.getText().toString().equals("")) {
                    mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        final DatePickerDialog.OnDateSetListener reminder_edit_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                reminderDateCalendar.set(Calendar.YEAR, year);
                reminderDateCalendar.set(Calendar.MONTH, monthOfYear);
                reminderDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mTextViewReminderEditDate.setText(sdf.format(reminderDateCalendar.getTime()));
                reminderDateTime = reminderDateCalendar.getTimeInMillis();
                if (!mTextViewReminderEditTime.getText().toString().equals("")) {
                    mDialogReminder.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        };

        mLinearLayoutReminderEditTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog reminderTimePickerDialog = new TimePickerDialog(JobApplicationNewActivity.this,
                        R.style.CustomStyleTimeDatePickerDialog, reminder_edit_time, reminderDateCalendar.get(Calendar.HOUR_OF_DAY), reminderDateCalendar.get(Calendar.MINUTE), true);
                reminderTimePickerDialog.show();
            }
        });
        mLinearLayoutReminderEditDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog reminderDatePickerDialog = new DatePickerDialog(JobApplicationNewActivity.this,
                        R.style.CustomStyleTimeDatePickerDialog, reminder_edit_date, reminderDateCalendar.get(Calendar.YEAR), reminderDateCalendar.get(Calendar.MONTH), reminderDateCalendar.get(Calendar.DAY_OF_MONTH));
                reminderDatePickerDialog.show();
            }
        });
    }

    public void getValuesToCreateNewJobApplicationObject() {
        jobTitle = mEditTextJobTitle.getText().toString().trim();
        companyName = mEditTextCompanyName.getText().toString().trim();

        try {
            wageAmount = Double.parseDouble(mEditTextWageAmount.getText().toString().trim());
        } catch (NumberFormatException e) {
            wageAmount = 0.00;
        }

        try {
            jobHours = Double.parseDouble(mEditTextJobHours.getText().toString().trim());
        } catch (NumberFormatException e) {
            jobHours = 0.0;
        }

        jobReference = mEditTextJobReference.getText().toString().trim();
        jobDescription = mEditTextJobDescription.getText().toString().trim();
        jobAdvertURL = mEditTextJobAdvertURL.getText().toString().trim();
        Boolean urlOK = true;
        if (!jobAdvertURL.equals("")) {
            if (!Patterns.WEB_URL.matcher(mEditTextJobAdvertURL.getText().toString()).matches()) {
                mEditTextJobAdvertURL.setError("Hmm, that doesn't look like a real url...");
                urlOK = false;
                /*mScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        mScrollView.smoothScrollTo(0, mEditTextJobAdvertURL.getTop());
                    }
                });*/
            }
        }
        applicationNotes = mEditTextApplicationNotes.getText().toString();

        if (urlOK) {
            createNewJobApplicationObject();
        }

    }

    //Add new Job Application
    private void createNewJobApplicationObject() {

        setApplicationStatus();

        //test and check if this is needed
        HashMap<String, Object> dateCreatedObj = new HashMap<>();
        dateCreatedObj.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

        final JobApplication newJobApplication = new JobApplication(jobTitle, companyName, applicationDateApplied, applicationDateCompleted, deadlineDate, currentApplicationStage, currentApplicationStageStatus, currentApplicationStageDateStarted, currentApplicationStageDateCompleted, currentApplicationStageNotes, currentApplicationStageCompleted, applicationStageHistory, applicationStatus, reminderSet, reminderId, reminderDateTime, wageCurrency, wageAmount, wageFrequency, locationRemote, locationName, locationLat, locationLng, locationSet, jobReference, jobDescription, jobAdvertURL, jobType, jobHours, applicationNotes, applicationPriority, associatedJobsiteAgencyID, contactTitle, contactFirstName, contactSurname, contactPosition, contactEmail, contactPhone, 0L, 0L, false, dateCreatedObj);

        //If user is not Premium
        if (!mSharedPrefs.getBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, false)) {

            mNewJobApplicationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.getChildrenCount() < 5) {
                        mNewJobApplicationPushRef.setValue(newJobApplication);
                        if (reminderSet) {
                            createSetReminder();
                        }
                        clearDefaultValues();
                        clearInputsValues();
                        goToJobApplicationDetailActivity();
                    } else {
                        createJobApplicationLimitDialog();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            mNewJobApplicationPushRef.setValue(newJobApplication);
            if (reminderSet) {
                createSetReminder();
            }
            clearDefaultValues();
            clearInputsValues();
            goToJobApplicationDetailActivity();
        }

    }

    private void goToJobApplicationDetailActivity() {
        Intent goToNewlyCreatedDetailActivity = new Intent(JobApplicationNewActivity.this, JobApplicationDetailActivity.class);
        goToNewlyCreatedDetailActivity.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mJobApplicationId);
        goToNewlyCreatedDetailActivity.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        startActivity(goToNewlyCreatedDetailActivity);
        finish();
    }

    private void createJobApplicationLimitDialog() {

        new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle(JobApplicationNewActivity.this.getString(R.string.dialog_free_limit_reached))
                .setMessage(JobApplicationNewActivity.this.getString(R.string.dialog_free_limit_reached_message))
                .setPositiveButton(JobApplicationNewActivity.this.getString(R.string.dialog_free_limit_reached_button_positive), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(JobApplicationNewActivity.this, UpgradeActivity.class));
                    }
                })
                .setNegativeButton(R.string.dialog_free_limit_reached_button_negative, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();

    }

    private void createSetReminder() {
        //Stored reminderDateTime Calendar
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(reminderDateTime);

        Log.d(LOG_TAG, "ReminderSet: " + reminderSet);
        Log.d(LOG_TAG, "ReminderSetTimeLong: " + reminderDateTime);

        String timeformat = "dd MMM HH:mm";
        final SimpleDateFormat sdft = new SimpleDateFormat(timeformat, Locale.US);
        Log.d(LOG_TAG, "ReminderSetTimeLong: " + sdft.format(reminderDateTime));

        reminderId = (int) c.getTimeInMillis();

        callMathAlarmScheduleService();
    }

    private void callMathAlarmScheduleService() {
        Intent mathAlarmServiceIntent = new Intent(JobApplicationNewActivity.this, AlarmServiceBroadcastReceiver.class);
        mathAlarmServiceIntent.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, reminderDateTime);
        mathAlarmServiceIntent.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
        mathAlarmServiceIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mJobApplicationId);
        mathAlarmServiceIntent.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        JobApplicationNewActivity.this.sendBroadcast(mathAlarmServiceIntent, null);
    }

    private void callMathAlarmScheduleServiceDelete() {
        Intent alarmServiceIntentDelete = new Intent(JobApplicationNewActivity.this, AlarmServiceBroadcastReceiver.class);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_LONG_REMINDER_DATE_TIME, reminderDateTime);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_REMINDER_ID, reminderId);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mJobApplicationId);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_REMINDER_DELETE, true);
        alarmServiceIntentDelete.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        JobApplicationNewActivity.this.sendBroadcast(alarmServiceIntentDelete, null);
    }

    private void googleApiStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    private void googleApiStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    private void setPlaceDetailsCallback(PendingResult<PlaceBuffer> placeResult) {
        ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(@NonNull PlaceBuffer places) {
                if (!places.getStatus().isSuccess()) {
                    Log.e(LOG_TAG, "Place query did not complete. Error: " +
                            places.getStatus().toString());
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                    mImageViewLocation.setVisibility(View.VISIBLE);
                    return;
                }

                // Selecting the first object buffer.
                final Place place = places.get(0);

                //Removes progress bar
                mAutocompleteProgressBar.setVisibility(View.GONE);
                mImageViewLocation.setVisibility(View.VISIBLE);

                String str = place.getLatLng().toString();
                String latlongRemoveStart = str.replace("lat/lng:", "");
                String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
                String[] parts = latlongRemoveEnd.split(",");
                locationLat = Double.parseDouble(parts[0]);
                locationLng = Double.parseDouble(parts[1]);

                locationName = place.getName().toString();

                locationRemote = false;
                locationSet = true;

                //Moves text cursor to end of EditText
                mAutocompleteTextView.setText(locationName);
                mAutocompleteTextView.setSelection(mAutocompleteTextView.getText().length());

                mListViewAutoComplete.setAdapter(null);

            }
        };

        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
    }

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(JobApplicationNewActivity.this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiStart();
        LocalBroadcastManager.getInstance(JobApplicationNewActivity.this).registerReceiver(mMessageReceiver, new IntentFilter(Constants.INTENT_EXTRA_ASSOCIATED_JSA));
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiStop();
        LocalBroadcastManager.getInstance(JobApplicationNewActivity.this).unregisterReceiver(mMessageReceiver);
        saveFormPropertiesToSharedPrefs();
    }

    private void createApplicationStageEditDialog() {
        final AlertDialog.Builder editApplicationStageDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_application_stage, null);
        editApplicationStageDialog.setView(dialogView);

        TextView mSpinnerDialogEditApplicationStage = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_stage);
        mSpinnerDialogEditApplicationStageStatus = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_status);
        mTextViewDialogEditApplicationStageDateStarted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_started);
        mTextViewDialogEditApplicationStageDateCompleted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_completed);
        final EditText mEditTextDialogEditApplicationStageNotes = (EditText) dialogView.findViewById(R.id.dialog_new_application_stage_et_notes);

        mSpinnerDialogEditApplicationStage.setText(currentApplicationStage);
        mSpinnerDialogEditApplicationStage.setEnabled(false);
        mSpinnerDialogEditApplicationStage.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
        mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);

        mSpinnerDialogEditApplicationStageStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEditApplicationStageStatusListDialog();
            }
        });

        mEditTextDialogEditApplicationStageNotes.setText(currentApplicationStageNotes);

        //If currentApplicationStage = Not Yet Applied, disable dialog UI
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
            mSpinnerDialogEditApplicationStageStatus.setEnabled(false);
            mSpinnerDialogEditApplicationStageStatus.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
            mTextViewDialogEditApplicationStageDateStarted.setText("-");
            mTextViewDialogEditApplicationStageDateStarted.setEnabled(false);
            mTextViewDialogEditApplicationStageDateStarted.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
            mTextViewDialogEditApplicationStageDateCompleted.setText("-");
            mTextViewDialogEditApplicationStageDateCompleted.setEnabled(false);
            mTextViewDialogEditApplicationStageDateCompleted.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
            mEditTextDialogEditApplicationStageNotes.setEnabled(false);
        } else {
            mTextViewDialogEditApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted));

            if (!currentApplicationStageDateCompleted.equals(0L)) {
                mTextViewDialogEditApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));
            } else {
                mTextViewDialogEditApplicationStageDateCompleted.setText("-");
            }

            //If Application stage is any other than Not Yet Applied, allow user to edit date completed.
            mTextViewDialogEditApplicationStageDateCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createEditApplicationStageDateCompletedDialog();
                }
            });

        }

        mTextViewDialogEditApplicationStageDateStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEditApplicationStageDateStartedDialog();
            }
        });

        editApplicationStageDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                currentApplicationStageNotes = mEditTextDialogEditApplicationStageNotes.getText().toString();
                mTextViewCurrentApplicationStage.setText(currentApplicationStage);
                mTextViewCurrentApplicationStageStatus.setText(currentApplicationStageStatus);

                mApplicationStageEditDialog.dismiss();

            }
        });
        editApplicationStageDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mApplicationStageEditDialog.dismiss();
            }
        });

        mApplicationStageEditDialog = editApplicationStageDialog.show();
        mApplicationStageEditDialog.setCancelable(false);
        mApplicationStageEditDialog.setCanceledOnTouchOutside(false);

    }

    private void createEditApplicationStageDateStartedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                currentApplicationStageDateStartedCalendar.set(Calendar.YEAR, year);
                currentApplicationStageDateStartedCalendar.set(Calendar.MONTH, monthOfYear);
                currentApplicationStageDateStartedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                currentApplicationStageDateStartedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.MINUTE, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.SECOND, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.MILLISECOND, 0);

                currentApplicationStageDateStarted = currentApplicationStageDateStartedCalendar.getTimeInMillis();
                mTextViewDialogEditApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted));
            }
        };
        DatePickerDialog datePickerDialogDateStarted = new DatePickerDialog(JobApplicationNewActivity.this, R.style.CustomStyleTimeDatePickerDialog, deadline_date, currentApplicationStageDateStartedCalendar.get(Calendar.YEAR), currentApplicationStageDateStartedCalendar.get(Calendar.MONTH), currentApplicationStageDateStartedCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogDateStarted.setTitle(R.string.activity_job_application_detail_tv_application_stage_date_started);
        datePickerDialogDateStarted.show();
    }

    private void createEditApplicationStageDateCompletedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                currentApplicationStageDateCompletedCalendar.set(Calendar.YEAR, year);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MONTH, monthOfYear);
                currentApplicationStageDateCompletedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                currentApplicationStageDateCompletedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MINUTE, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.SECOND, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MILLISECOND, 0);

                currentApplicationStageDateCompleted = currentApplicationStageDateCompletedCalendar.getTimeInMillis();
                mTextViewDialogEditApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));

                currentApplicationStageCompleted = true;

                //Default set currentApplicationStageStatus to 'Successful'
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[1];
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);

                createEditApplicationStageStatusListDialog();

            }
        };

        if (currentApplicationStageDateCompleted.equals(0L)) {
            currentApplicationStageDateCompletedCalendar = Calendar.getInstance();
        }

        DatePickerDialog datePickerDialogDateCompleted = new DatePickerDialog(JobApplicationNewActivity.this, R.style.CustomStyleTimeDatePickerDialog, deadline_date, currentApplicationStageDateCompletedCalendar.get(Calendar.YEAR), currentApplicationStageDateCompletedCalendar.get(Calendar.MONTH), currentApplicationStageDateCompletedCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogDateCompleted.setTitle(R.string.activity_job_application_detail_tv_application_stage_date_completed);
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentApplicationStageDateCompletedCalendar = Calendar.getInstance();
                currentApplicationStageDateCompleted = 0L;
                mTextViewDialogEditApplicationStageDateCompleted.setText("-");
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0]; //Set current applicationStageStatus to 'In Progress'
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.show();
    }

    private void createApplicationStageAddDialog(){
        final AlertDialog.Builder addApplicationStageDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_application_stage, null);
        addApplicationStageDialog.setView(dialogView);
        addApplicationStageDialog.setTitle(getResources().getString(R.string.dialog_new_application_stage_title));

        mSpinnerDialogAddApplicationStage = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_stage);
        mSpinnerDialogAddApplicationStageStatus = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_sp_status);
        mTextViewDialogAddApplicationStageDateStarted = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_started);
        mTextViewDialogAddApplicationStageDateComplete = (TextView) dialogView.findViewById(R.id.dialog_new_application_stage_tv_date_completed);
        mEditTextDialogAddApplicationStageNotes = (EditText) dialogView.findViewById(R.id.dialog_new_application_stage_et_notes);

        addPreviousApplicationStageToApplicationStageHistory();

        resetCurrentApplicationStageProperties();

        mSpinnerDialogAddApplicationStage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createApplicationStageListDialog();
            }
        });

        mSpinnerDialogAddApplicationStageStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageStatusListDialog();
            }
        });

        mTextViewDialogAddApplicationStageDateStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageDateStartedDialog();
            }
        });

        mTextViewDialogAddApplicationStageDateComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAddApplicationStageDateCompletedDialog();
            }
        });

        addApplicationStageDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                currentApplicationStageNotes = mEditTextDialogAddApplicationStageNotes.getText().toString();

                mTextViewCurrentApplicationStage.setText(currentApplicationStage);
                mTextViewCurrentApplicationStageStatus.setText(currentApplicationStageStatus);

                if (!currentApplicationStageDateStarted.equals(0L)) {

                    //If New Application Stage anything other than Not Yet Applied then set dateApplied to same as applicationDateStarted
                    if (!currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
                        applicationDateApplied = currentApplicationStageDateStarted;
                    }

                    //If New Application Stage = Complete then set applicationDateCompleted to same as applicationDateStarted
                    if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
                        applicationDateCompleted = currentApplicationStageDateStarted;
                    }

                }

                mApplicationStageAddDialog.dismiss();

            }
        });
        addApplicationStageDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Remove previous applicationStage from ApplicationStageHistory and add back to main Job Application Object
                applicationStageHistory.remove(previousApplicationStageKey);
                currentApplicationStage = previousApplicationStage.getApplicationStage();
                currentApplicationStageStatus = previousApplicationStage.getApplicationStageStatus();
                currentApplicationStageDateStarted = previousApplicationStage.getApplicationStageDateStarted();
                currentApplicationStageDateCompleted = 0L;
                currentApplicationStageNotes = previousApplicationStage.getApplicationStageNotes();
            }
        });

        mApplicationStageAddDialog = addApplicationStageDialog.show();

    }

    private void addPreviousApplicationStageToApplicationStageHistory() {
        //Add previous Application Stage to ApplicationStageHistory HashMap<>
        if (currentApplicationStageDateCompleted.equals(0L)) {
            currentApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
        }
        previousApplicationStage = new ApplicationStage(currentApplicationStage, currentApplicationStageStatus, currentApplicationStageNotes, currentApplicationStageDateStarted, currentApplicationStageDateCompleted, currentDeviceTimeCalendar.getTimeInMillis(), currentApplicationStageCompleted);
        previousApplicationStageKey = mNewJobApplicationPushRef.child(Constants.FIREBASE_PROPERTY_APPLICATION_STAGE_HISTORY).push().getKey();
        applicationStageHistory.put(previousApplicationStageKey, previousApplicationStage);
    }

    private void resetCurrentApplicationStageProperties() {

        setDefaultApplicationStage();

        setDefaultApplicationStageStatusFromApplicationStage();

        setDefaultApplicationStageDateStarted();

        setDefaultApplicationStageDateCompleted();

        setDefaultApplicationStageCompleted();

        currentApplicationStageNotes = ""; //No Notes

        mSpinnerDialogAddApplicationStage.setText(currentApplicationStage);
        mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus); //In Progress
        mTextViewDialogAddApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted)); //Today's Date
        if (currentApplicationStageCompleted) {
            mTextViewDialogAddApplicationStageDateComplete.setText(sdf.format(currentApplicationStageDateCompleted));
        } else {
            mTextViewDialogAddApplicationStageDateComplete.setText("-");
        }

        mEditTextDialogAddApplicationStageNotes.setText(currentApplicationStageNotes); //No Notes

    }

    private void createAddApplicationStageDateStartedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                currentApplicationStageDateStartedCalendar.set(Calendar.YEAR, year);
                currentApplicationStageDateStartedCalendar.set(Calendar.MONTH, monthOfYear);
                currentApplicationStageDateStartedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                currentApplicationStageDateStartedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.MINUTE, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.SECOND, 0);
                currentApplicationStageDateStartedCalendar.set(Calendar.MILLISECOND, 0);

                currentApplicationStageDateStarted = currentApplicationStageDateStartedCalendar.getTimeInMillis();
                mTextViewDialogAddApplicationStageDateStarted.setText(sdf.format(currentApplicationStageDateStarted));
            }
        };
        DatePickerDialog dateStartedPickerDialog = new DatePickerDialog(JobApplicationNewActivity.this, R.style.CustomStyleTimeDatePickerDialog, deadline_date, currentApplicationStageDateStartedCalendar.get(Calendar.YEAR), currentApplicationStageDateStartedCalendar.get(Calendar.MONTH), currentApplicationStageDateStartedCalendar.get(Calendar.DAY_OF_MONTH));
        dateStartedPickerDialog.show();
    }

    private void createAddApplicationStageDateCompletedDialog() {
        final DatePickerDialog.OnDateSetListener deadline_date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                currentApplicationStageDateCompletedCalendar.set(Calendar.YEAR, year);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MONTH, monthOfYear);
                currentApplicationStageDateCompletedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                currentApplicationStageDateCompletedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MINUTE, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.SECOND, 0);
                currentApplicationStageDateCompletedCalendar.set(Calendar.MILLISECOND, 0);

                currentApplicationStageDateCompleted = currentApplicationStageDateCompletedCalendar.getTimeInMillis();
                mTextViewDialogAddApplicationStageDateComplete.setText(sdf.format(currentApplicationStageDateCompleted));

                currentApplicationStageCompleted = true;

                //Default set currentApplicationStageStatus to 'Successful'
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[1];
                mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus);

                createAddApplicationStageStatusListDialog();

            }
        };
        DatePickerDialog datePickerDialogDateCompleted = new DatePickerDialog(JobApplicationNewActivity.this, R.style.CustomStyleTimeDatePickerDialog, deadline_date, currentApplicationStageDateCompletedCalendar.get(Calendar.YEAR), currentApplicationStageDateCompletedCalendar.get(Calendar.MONTH), currentApplicationStageDateCompletedCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogDateCompleted.setTitle(R.string.activity_job_application_detail_tv_application_stage_date_completed);
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentApplicationStageDateCompletedCalendar = Calendar.getInstance();
                currentApplicationStageDateCompleted = 0L;
                currentApplicationStageCompleted = false;
                mTextViewDialogAddApplicationStageDateComplete.setText("-");
                currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus);
            }
        });
        datePickerDialogDateCompleted.show();
    }

    private void createEditContactDetailsDialog(){

        final AlertDialog.Builder editContactNameDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_edit_contact_details, null);
        editContactNameDialog.setView(convertView);
        final EditText mEditTextContactTitle = (EditText) convertView.findViewById(R.id.dialog_edit_contact_name_et_title);
        final EditText mEditTextContactFirstName = (EditText) convertView.findViewById(R.id.dialog_edit_contact_name_et_first_name);
        final EditText mEditTextContactSurname = (EditText) convertView.findViewById(R.id.dialog_edit_contact_name_et_surname);
        final EditText mEditTextContactPosition = (EditText) convertView.findViewById(R.id.dialog_edit_contact_name_et_position);
        final EditText mEditTextContactEmail = (EditText) convertView.findViewById(R.id.dialog_edit_contact_name_et_email);
        final EditText mEditTextContactPhone = (EditText) convertView.findViewById(R.id.dialog_edit_contact_name_et_phone);

        mEditTextContactTitle.setText(contactTitle);
        mEditTextContactFirstName.setText(contactFirstName);
        mEditTextContactSurname.setText(contactSurname);
        mEditTextContactPosition.setText(contactPosition);
        mEditTextContactEmail.setText(contactEmail);
        mEditTextContactPhone.setText(contactPhone);

        mEditTextContactTitle.setSelection(mEditTextContactTitle.getText().length());
        mEditTextContactFirstName.setSelection(mEditTextContactFirstName.getText().length());
        mEditTextContactSurname.setSelection(mEditTextContactSurname.getText().length());
        mEditTextContactPosition.setSelection(mEditTextContactPosition.getText().length());
        mEditTextContactEmail.setSelection(mEditTextContactEmail.getText().length());
        mEditTextContactPhone.setSelection(mEditTextContactPhone.getText().length());

        editContactNameDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                contactTitle = mEditTextContactTitle.getText().toString().trim();
                contactFirstName = mEditTextContactFirstName.getText().toString().trim();
                contactSurname = mEditTextContactSurname.getText().toString().trim();
                contactPosition = mEditTextContactPosition.getText().toString().trim();
                contactEmail = mEditTextContactEmail.getText().toString().trim();
                contactPhone = mEditTextContactPhone.getText().toString().trim();

                if (!contactTitle.equals("")) {
                    mTextViewContactTitle.setText(contactTitle);
                }
                if (!contactFirstName.equals("")) {
                    mTextViewContactFirstName.setText(contactFirstName);
                }
                if (!contactSurname.equals("")) {
                    mTextViewContactSurname.setText(contactSurname);
                }
                if (!contactPosition.equals("")) {
                    mTextViewContactPosition.setText(contactPosition);
                }
                if (!contactEmail.equals("")) {
                    mTextViewContactEmail.setText(contactEmail);
                }
                if (!contactPhone.equals("")) {
                    mTextViewContactPhone.setText(contactPhone);
                }

                mEditContactNameDialog.dismiss();

            }
        });
        editContactNameDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditContactNameDialog.dismiss();
            }
        });

        editContactNameDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createDeleteContactDetailsDialog();
            }
        });

        mEditContactNameDialog = editContactNameDialog.show();

    }

    private void deleteContactDetails() {
        contactTitle = "";
        contactFirstName = "";
        contactSurname = "";
        contactPosition = "";
        contactEmail = "";
        contactPhone = "";

        mTextViewContactTitle.setText(contactTitle);
        mTextViewContactFirstName.setText(contactFirstName);
        mTextViewContactSurname.setText(contactSurname);
        mTextViewContactPosition.setText(contactPosition);
        mTextViewContactEmail.setText(getResources().getString(R.string.activity_job_application_new_tv_contact_email_empty));
        mTextViewContactPhone.setText(getResources().getString(R.string.activity_job_application_new_tv_contact_phone_empty));

    }

    private void createDeleteContactDetailsDialog() {
        new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle(JobApplicationNewActivity.this.getString(R.string.dialog_delete_contact_details_title))
                .setMessage(JobApplicationNewActivity.this.getString(R.string.dialog_delete_contact_details_message_p1))
                .setPositiveButton(JobApplicationNewActivity.this.getString(R.string.dialog_delete_contact_details_positive_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteContactDetails();
                        if (mEditContactNameDialog != null) {
                            mEditContactNameDialog.dismiss();
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_delete_contact_details_negative_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    private void createAssociatedJsaListDialog() {
        final AlertDialog.Builder associatedJSAListDialog = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_recycler_view, null);
        associatedJSAListDialog.setView(convertView);
        mProgressBarAssociatedJobsiteAgency = (ProgressBar) convertView.findViewById(R.id.dialog_recycler_view_pb);
        final TextView mTextViewAssociatedJsa = (TextView) convertView.findViewById(R.id.dialog_recycler_view_tv);
        mRecyclerViewAssociatedJobsiteAgencyID = (RecyclerView) convertView.findViewById(R.id.dialog_recycler_view_rv);

        associatedJSAListDialog.setPositiveButton(getResources().getString(R.string.dialog_associated_jsa_list_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent goToJsaNewActivity = new Intent(JobApplicationNewActivity.this, JsaNewActivity.class);
                goToJsaNewActivity.putExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY, JobApplicationNewActivity.class.getSimpleName());
                startActivity(goToJsaNewActivity);
                finish();
            }
        });

        final Query orderedAssociatedJSARef;
        orderedAssociatedJSARef = mAssociatedJsaListRef.orderByChild(Constants.FIREBASE_PROPERTY_JSA_NAME);

        orderedAssociatedJSARef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0) {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.VISIBLE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.GONE);
                } else {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.GONE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(JobApplicationNewActivity.this);
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewAssociatedJobsiteAgencyID.setHasFixedSize(false);
                    mRecyclerViewAssociatedJobsiteAgencyID.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterAssociatedJsaList = new FirebaseRecyclerAdapter<JobsiteAgency, AssociatedJsaItemHolder>(JobsiteAgency.class, R.layout.list_item_associated_jsa, AssociatedJsaItemHolder.class, orderedAssociatedJSARef) {
                        @Override
                        public void populateViewHolder(AssociatedJsaItemHolder jobApplicationPriorityView, JobsiteAgency jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getAssociatedJsaListItemSelected(mRecyclerViewAdapterAssociatedJsaList.getItem(position));
                            jobApplicationPriorityView.getAssociatedJsaListItemKey(mRecyclerViewAdapterAssociatedJsaList.getRef(position).getKey());
                            jobApplicationPriorityView.setAssociatedJsaListItemJsaName(jobApplicationPriority.getJsaName());
                            jobApplicationPriorityView.setAssociatedJsaListItemJsaLocation(jobApplicationPriority.getJsaLocationName());

                        }
                    };

                    mRecyclerViewAssociatedJobsiteAgencyID.setAdapter(mRecyclerViewAdapterAssociatedJsaList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDialogAssociatedJSAList = associatedJSAListDialog.show();

        mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
        mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);
    }

    private void associatedJobsiteAgencyClear() {
        mTextViewAssociatedJsa.setText(getResources().getString(R.string.activity_job_application_new_tv_associated_jsa_link_empty));
        mTextViewAssociatedJsa.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));

        associatedJobsiteAgencyID = "";

        mImageViewAssociatedJsaClear.setVisibility(View.GONE);
        mImageViewAssociatedJsaLink.setVisibility(View.VISIBLE);
        mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.white));

    }

    private void jsaListConfirmAssociate() {

        mAssociatedJsaItemRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                associatedJobsiteAgencyID = dataSnapshot.getKey();

                JobsiteAgency associatedJsaItemData = dataSnapshot.getValue(JobsiteAgency.class);

                String associatedJsaName = associatedJsaItemData.getJsaName();

                if (!associatedJsaName.equals("")) {
                    mTextViewAssociatedJsa.setText(associatedJsaName);
                    mTextViewAssociatedJsa.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, android.R.color.white));
                } else {
                    mTextViewAssociatedJsa.setText(getResources().getString(R.string.activity_job_application_new_tv_associated_jsa_link_empty));
                    mTextViewAssociatedJsa.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.text_view_unused));
                }

                mImageViewAssociatedJsaClear.setVisibility(View.VISIBLE);
                mImageViewAssociatedJsaLink.setVisibility(View.GONE);
                mFrameLayoutAssociatedJobsiteAgency.setBackgroundColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.colorAccent));

                if (mDialogAssociatedJSAList != null) {
                    mDialogAssociatedJSAList.dismiss();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //Called when Associated Jsa RecyclerView Item is clicked
            associatedJobsiteAgencyID = intent.getStringExtra(Constants.INTENT_EXTRA_JSA_ID);
            mAssociatedJsaItemRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(associatedJobsiteAgencyID);
            jsaListConfirmAssociate();
            mDialogAssociatedJSAList.dismiss();
            Log.d(LOG_TAG, "AJSAClicked");
        }
    };

    @SuppressLint("CommitPrefEdits")
    private void saveData(Context context, HashMap<String, ApplicationStage> mlist) {
        SharedPreferences shared;
        SharedPreferences.Editor editor;

        shared = context.getSharedPreferences("applicationStageHistory", Context.MODE_PRIVATE);
        editor = shared.edit();

        Gson gson = new Gson();
        String json = gson.toJson(mlist);
        editor.putString("applicationStageHistory", json).commit();
        fetchSavedApplicationStageHistoryData();

    }

    public void fetchSavedApplicationStageHistoryData() {
        SharedPreferences shared;
        Gson gson = new Gson();
        shared = JobApplicationNewActivity.this.getSharedPreferences("applicationStageHistory", Context.MODE_PRIVATE);
        applicationStageHistory = gson.fromJson(
                shared.getString("applicationStageHistory", null),
                new TypeToken<HashMap<String, ApplicationStage>>() {
                }.getType());
        if (applicationStageHistory != null) {
            Log.d(LOG_TAG, applicationStageHistory.toString());
        } else {
            applicationStageHistory = new HashMap<>();
        }

    }

    private void setDefaultApplicationStage() {
        //Reset Current Application Stage property defaults
        for (int i = 0; i < 4; i++) {
            if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[i])) {
                currentApplicationStage = getResources().getStringArray(R.array.array_application_stage)[i+1];
                break;
            }
        }
    }

    private void setDefaultApplicationStageStatusFromApplicationStage() {

        final String NOT_YET_APPLIED = getResources().getStringArray(R.array.array_application_stage)[0];
        final String CV_APPLICATION_FORM = getResources().getStringArray(R.array.array_application_stage)[1];
        final String TESTS_ASSESSMENTS = getResources().getStringArray(R.array.array_application_stage)[2];
        final String INTERVIEW = getResources().getStringArray(R.array.array_application_stage)[3];
        final String COMPLETE = getResources().getStringArray(R.array.array_application_stage)[4];

        final String IN_PROGRESS = getResources().getStringArray(R.array.array_application_status)[1];
        final String SUCCESSFUL = getResources().getStringArray(R.array.array_application_status)[2];

        if (currentApplicationStage.equals(NOT_YET_APPLIED)) {
            currentApplicationStageStatus = IN_PROGRESS;
        } else if (currentApplicationStage.equals(CV_APPLICATION_FORM)) {
            currentApplicationStageStatus = IN_PROGRESS;
        } else if (currentApplicationStage.equals(TESTS_ASSESSMENTS)) {
            currentApplicationStageStatus = IN_PROGRESS;
        } else if (currentApplicationStage.equals(INTERVIEW)) {
            currentApplicationStageStatus = IN_PROGRESS;
        } else if (currentApplicationStage.equals(COMPLETE)) {
            currentApplicationStageStatus = SUCCESSFUL;
        }

    }

    private void setDefaultApplicationStageDateStarted() {
        currentApplicationStageDateStartedCalendar = currentDeviceTimeCalendar;
        currentApplicationStageDateStarted = currentDeviceTimeCalendar.getTimeInMillis(); //Today's Date
    }

    private void setDefaultApplicationStageDateCompleted() {

        final String COMPLETE = getResources().getStringArray(R.array.array_application_stage)[4];

        currentApplicationStageDateCompletedCalendar = currentDeviceTimeCalendar;

        if (currentApplicationStage.equals(COMPLETE)) {
            currentApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
        } else {
            currentApplicationStageDateCompleted = 0L;
        }
    }

    private void setDefaultApplicationStageCompleted() {
        currentApplicationStageCompleted = currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4]);
    }

    private void setApplicationStatus() {
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
            //If Not Yet Applied set applicationStatus prospective
            applicationStatus = getResources().getStringArray(R.array.array_application_status)[0];
        } else if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[1])
                || currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[2])
                || currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[3])) {
            //If currentApplicationStage is CV/Application Form Submitted, Tests/Assessments or Interview
            // check If currentApplicationStageStatus is Unsuccessful or Withdrawn then applicationStatus to same otherwise set to In Progress
            if (currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[2])) {
                applicationStatus = getResources().getStringArray(R.array.array_application_status)[3];
            } else if (currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[3])) {
                applicationStatus = getResources().getStringArray(R.array.array_application_status)[4];
            } else {
                applicationStatus = getResources().getStringArray(R.array.array_application_status)[1];
            }
        } else if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
            //If currentApplicationStage is Complete set applicationStatus to Successful / Unsuccessful / Withdrawn
            if (currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[1])) {
                //if CurrentApplicationStatus is Successful
                applicationStatus = getResources().getStringArray(R.array.array_application_status)[2];
            } else if (currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[2])) {
                //if CurrentApplicationStatus is Unsuccessful
                applicationStatus = getResources().getStringArray(R.array.array_application_status)[3];
            } else if (currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[3])) {
                //if CurrentApplicationStatus is Withdrawn
                applicationStatus = getResources().getStringArray(R.array.array_application_status)[4];
            }
        }
    }

    private void createJobTypeListDialog() {
        final AlertDialog.Builder dialogListJobType = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_list_job_type, null);
        dialogListJobType.setView(convertView);

        final ListView mListViewJobType = (ListView) convertView.findViewById(R.id.dialog_list_job_type_lv);

        ArrayAdapter<CharSequence> adapterJobtype = ArrayAdapter.createFromResource(JobApplicationNewActivity.this, R.array.array_job_type, R.layout.list_item_job_type);

        mListViewJobType.setAdapter(adapterJobtype);

        dialogListJobType.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogJobTypeList.dismiss();
            }

        });

        mListViewJobType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                jobType = mListViewJobType.getItemAtPosition(position).toString();
                mTextViewJobType.setText(jobType);
                mDialogJobTypeList.dismiss();
            }
        });

        mDialogJobTypeList = dialogListJobType.show();

    }

    private void createWageCurrencyListDialog() {
        final AlertDialog.Builder dialogListWageCurrency = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_list_currency, null);
        dialogListWageCurrency.setView(convertView);

        final ListView mListViewWageCurrency = (ListView) convertView.findViewById(R.id.dialog_list_currency_lv);

        ArrayAdapter<CharSequence> adapterWageCurrency = ArrayAdapter.createFromResource(JobApplicationNewActivity.this, R.array.array_currency, R.layout.list_item_currency);

        mListViewWageCurrency.setAdapter(adapterWageCurrency);

        dialogListWageCurrency.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogWageCurrencyList.dismiss();
            }

        });

        mListViewWageCurrency.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                wageCurrency = mListViewWageCurrency.getItemAtPosition(position).toString();
                mTextViewWageCurrency.setText(wageCurrency);
                mDialogWageCurrencyList.dismiss();
            }
        });

        mDialogWageCurrencyList = dialogListWageCurrency.show();

    }

    private void createWageFrequencyListDialog() {
        final AlertDialog.Builder dialogListWageFrequency = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_list_wage_frequency, null);
        dialogListWageFrequency.setView(convertView);

        final ListView mListViewWageFrequency = (ListView) convertView.findViewById(R.id.dialog_list_wage_frequency_lv);

        ArrayAdapter<CharSequence> adapterWageCurrency = ArrayAdapter.createFromResource(JobApplicationNewActivity.this, R.array.array_wage_frequency, R.layout.list_item_wage_frequency);

        mListViewWageFrequency.setAdapter(adapterWageCurrency);

        dialogListWageFrequency.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogWageFrequencyList.dismiss();
            }

        });

        mListViewWageFrequency.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                wageFrequency = mListViewWageFrequency.getItemAtPosition(position).toString();
                mTextViewWageFrequency.setText(wageFrequency);
                mDialogWageFrequencyList.dismiss();
            }
        });

        mDialogWageFrequencyList = dialogListWageFrequency.show();

    }

    private void createApplicationStageListDialog() {
        final AlertDialog.Builder dialogListApplicationStage = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_list_generic, null);
        dialogListApplicationStage.setView(dialogView);

        mDialogListViewApplicationStage = (ListView) dialogView.findViewById(R.id.dialog_list_lv);

        mDialogListViewApplicationStage.setAdapter(addApplicationStageAdapter());

        mDialogListViewApplicationStage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentApplicationStage = addApplicationStageAdapter().getItem(position);
                mSpinnerDialogAddApplicationStage.setText(currentApplicationStage);

                //If Complete create dialog to set status
                if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
                    currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[1];
                    mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus);
                } else {
                    currentApplicationStageStatus = getResources().getStringArray(R.array.array_application_stage_status)[0];
                    mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus);
                }


                mDialogApplicationStageList.dismiss();

            }
        });

        dialogListApplicationStage.setNegativeButton(getResources().getString(R.string.dialog_associated_jsa_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogApplicationStageList.dismiss();
            }

        });

        mDialogApplicationStageList = dialogListApplicationStage.show();
        mDialogApplicationStageList.setCanceledOnTouchOutside(false);

    }

    private void createAddApplicationStageStatusListDialog() {
        final AlertDialog.Builder dialogListApplicationStageStatus = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_list_generic, null);
        dialogListApplicationStageStatus.setView(dialogView);

        mDialogListViewApplicationStageStatus = (ListView) dialogView.findViewById(R.id.dialog_list_lv);

        mDialogListViewApplicationStageStatus.setAdapter(applicationStageStatusAdapter());

        mDialogListViewApplicationStageStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentApplicationStageStatus = applicationStageStatusAdapter().getItem(position);
                mSpinnerDialogAddApplicationStageStatus.setText(currentApplicationStageStatus);

                //If stageStatus set to unsuccessful/withdrawn then disable applicationStageSpinner
                if (currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[2]) ||
                        currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[3])) {
                    mSpinnerDialogAddApplicationStage.setEnabled(false);
                    mSpinnerDialogAddApplicationStage.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.spinner_unused));
                } else {
                    mSpinnerDialogAddApplicationStage.setEnabled(true);
                    mSpinnerDialogAddApplicationStage.setTextColor(ContextCompat.getColor(JobApplicationNewActivity.this, R.color.spinner_active));
                }

                //If currentApplicationStageStatus isn't in progress, set currentApplicationStageDateCompleted to current date.
                if (!currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[0])) {
                    currentApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
                    mTextViewDialogAddApplicationStageDateComplete.setText(sdf.format(currentApplicationStageDateCompleted));
                    currentApplicationStageCompleted = true;
                    Toast.makeText(JobApplicationNewActivity.this, "New DateCompleted: " + sdf.format(currentApplicationStageDateCompleted), Toast.LENGTH_SHORT).show();
                } else {
                    currentApplicationStageDateCompleted = 0L;
                    mTextViewDialogAddApplicationStageDateComplete.setText("-");
                    currentApplicationStageCompleted = false;
                    Toast.makeText(JobApplicationNewActivity.this, "New DateCompleted: " + sdf.format(currentApplicationStageDateCompleted), Toast.LENGTH_SHORT).show();
                }

                mDialogAddApplicationStageStatusList.dismiss();

            }
        });

        dialogListApplicationStageStatus.setNegativeButton(getResources().getString(R.string.dialog_negative_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogAddApplicationStageStatusList.dismiss();
            }

        });

        mDialogAddApplicationStageStatusList = dialogListApplicationStageStatus.show();
        mDialogAddApplicationStageStatusList.setCanceledOnTouchOutside(false);

    }

    private void createEditApplicationStageStatusListDialog() {
        final AlertDialog.Builder dialogListApplicationStageStatus = new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(JobApplicationNewActivity.this, R.layout.dialog_list_generic, null);
        dialogListApplicationStageStatus.setView(dialogView);

        final ListView mListViewEditApplicationStageStatus = (ListView) dialogView.findViewById(R.id.dialog_list_lv);

        mListViewEditApplicationStageStatus.setAdapter(applicationStageStatusAdapter());

        dialogListApplicationStageStatus.setNegativeButton(getResources().getString(R.string.dialog_negative_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDialogEditApplicationStageList.dismiss();
            }

        });

        mListViewEditApplicationStageStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentApplicationStageStatus = applicationStageStatusAdapter().getItem(position);
                mSpinnerDialogEditApplicationStageStatus.setText(currentApplicationStageStatus);

                //If currentApplicationStageStatus isn't in progress, set currentApplicationStageDateCompleted to current date.
                if (!currentApplicationStageStatus.equals(getResources().getStringArray(R.array.array_application_stage_status)[0])) {
                    currentApplicationStageDateCompleted = currentDeviceTimeCalendar.getTimeInMillis();
                    mTextViewDialogEditApplicationStageDateCompleted.setText(sdf.format(currentApplicationStageDateCompleted));
                    currentApplicationStageCompleted = true;
                    Toast.makeText(JobApplicationNewActivity.this, "New DateCompleted: " + sdf.format(currentApplicationStageDateCompleted), Toast.LENGTH_SHORT).show();
                } else {
                    currentApplicationStageDateCompleted = 0L;
                    mTextViewDialogEditApplicationStageDateCompleted.setText("-");
                    currentApplicationStageCompleted = false;
                    Toast.makeText(JobApplicationNewActivity.this, "New DateCompleted: " + sdf.format(currentApplicationStageDateCompleted), Toast.LENGTH_SHORT).show();
                }

                mDialogEditApplicationStageList.dismiss();

            }
        });

        mDialogEditApplicationStageList = dialogListApplicationStageStatus.show();
        mDialogEditApplicationStageList.setCanceledOnTouchOutside(false);

    }

    private ArrayAdapter<String> addApplicationStageAdapter() {

        ArrayAdapter<String> adb = new ArrayAdapter<>(JobApplicationNewActivity.this, R.layout.list_item_generic, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_application_stage))));

        for (int i = 0; i < adb.getCount(); i++) {
            if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[i])) {
                adb.remove(getResources().getStringArray(R.array.array_application_stage)[i]);
            }
        }

        return adb;
    }

    private ArrayAdapter<String> applicationStageStatusAdapter() {
        final ArrayAdapter<String> adapterApplicationStageStatus = new ArrayAdapter<>(JobApplicationNewActivity.this, R.layout.list_item_currency, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_application_stage_status))));

        //If Application Stage is completed, can not be 'In Progress'
        if (currentApplicationStageCompleted) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[0]);
        }

        //If Complete, status cannot be In Progress
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[4])) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[0]);
        }

        //If Not Yet Applied, status cannot be Successful, Unsuccessful or Withdrawn
        if (currentApplicationStage.equals(getResources().getStringArray(R.array.array_application_stage)[0])) {
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[1]);
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[2]);
            adapterApplicationStageStatus.remove(getResources().getStringArray(R.array.array_application_stage_status)[3]);
        }

        return adapterApplicationStageStatus;
    }

    private void clearFormValuesCheck() {
        new AlertDialog.Builder(JobApplicationNewActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle("Clear form")
                .setMessage("Are you sure you want to clear the form?")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        clearDefaultValues();
                        clearInputsValues();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

}
