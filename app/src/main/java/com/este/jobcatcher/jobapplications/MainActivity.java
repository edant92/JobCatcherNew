package com.este.jobcatcher.jobapplications;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.este.jobcatcher.BuildConfig;
import com.este.jobcatcher.R;
import com.este.jobcatcher.compare.CompareFragment;
import com.este.jobcatcher.general.SettingsActivity;
import com.este.jobcatcher.jobparser.JobParserUtil;
import com.este.jobcatcher.overview.OverviewFragment;
import com.este.jobcatcher.general.UpgradeActivity;
import com.este.jobcatcher.jsas.JsaListActivity;
import com.este.jobcatcher.maps.JobMapFragment;
import com.este.jobcatcher.utils.Constants;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

/**
 * MainActivity contains Main Feature Fragments
 */

public class MainActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {

    private final String LOG_TAG = MainActivity.class.getSimpleName();

    private SharedPreferences sharedPrefs;

    private String currentFragmentTag;

    //In-app Purchase/Billing
    private static final String SKU_PREMIUM = "sku_premium";
    BillingProcessor bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.title_activity_overview));

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        //If User is not premium then show banner advert
        if (!sharedPrefs.getBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, false)) {

            MobileAds.initialize(getApplicationContext(), "ca-app-pub-4246614092674613~7414893086");

            LinearLayout mLinearLayoutAdView = (LinearLayout) findViewById(R.id.activity_main_activity_ll_adview);
            mLinearLayoutAdView.setVisibility(View.VISIBLE);

            AdView mAdView = (AdView) findViewById(R.id.activity_main_activity_adview);
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mAdView.loadAd(adRequest);
        }

        Log.d(LOG_TAG, "First Fragment: " + currentFragmentTag);

        BottomBar bottomBar = (BottomBar) findViewById(R.id.activity_main_bottom_bar);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_overview) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_OVERVIEW;
                    commitFragment(new OverviewFragment());
                    setTitle(getResources().getString(R.string.title_activity_overview));
                } else if (tabId == R.id.tab_main) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_MAIN;
                    commitFragment(new MainFragment());
                    setTitle(getResources().getString(R.string.app_name));
                } else if (tabId == R.id.tab_jobmap) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_JOBMAP;
                    commitFragment(new JobMapFragment());
                    setTitle(getResources().getString(R.string.activity_jobmap));
                } else if (tabId == R.id.tab_compare) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_COMPARE;
                    commitFragment(new CompareFragment());
                    setTitle(getResources().getString(R.string.activity_compare));
                } else {
                    //Default to MainFragment
                    currentFragmentTag = Constants.FRAGMENT_TAG_MAIN;
                    commitFragment(new MainFragment());
                    setTitle(getResources().getString(R.string.app_name));
                }
                Log.d(LOG_TAG, "FragTag Select: " + currentFragmentTag);
                invalidateOptionsMenu();
                Log.d(LOG_TAG, "Current Fragment Select: " + currentFragmentTag);
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_overview) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_OVERVIEW;
                    commitFragment(new OverviewFragment());
                    setTitle(getResources().getString(R.string.title_activity_overview));
                } else if (tabId == R.id.tab_main) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_MAIN;
                    commitFragment(new MainFragment());
                    setTitle(getResources().getString(R.string.app_name));
                }else if (tabId == R.id.tab_jobmap) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_JOBMAP;
                    commitFragment(new JobMapFragment());
                    setTitle(getResources().getString(R.string.activity_jobmap));
                } else if (tabId == R.id.tab_compare) {
                    currentFragmentTag = Constants.FRAGMENT_TAG_COMPARE;
                    commitFragment(new CompareFragment());
                    setTitle(getResources().getString(R.string.activity_compare));
                } else {
                    //Default to MainFragment
                    currentFragmentTag = Constants.FRAGMENT_TAG_MAIN;
                    commitFragment(new MainFragment());
                    setTitle(getResources().getString(R.string.app_name));
                }
                Log.d(LOG_TAG, "FragTag Reselect: " + currentFragmentTag);
                invalidateOptionsMenu();
                Log.d(LOG_TAG, "Current Fragment Reselect: " + currentFragmentTag);
            }
        });

        bottomBar.setDefaultTab(R.id.tab_main);
        currentFragmentTag = Constants.FRAGMENT_TAG_MAIN;
        commitFragment(new MainFragment());
        setTitle(getResources().getString(R.string.app_name));

        Intent intent = this.getIntent();

        int jobParserReceiverError = intent.getIntExtra(Constants.INTENT_EXTRA_JOB_PARSER_RECEIVER_ERROR, 0);

        switch (jobParserReceiverError) {
            case 0:
                break;
            case 1:
                Toast.makeText(this, "Hmm, there's something wrong with this link.", Toast.LENGTH_SHORT).show();
                Log.d(LOG_TAG, "JobParser Error 1");
                break;
            case 2:
                JobParserUtil jobParserUtil = new JobParserUtil();
                jobParserUtil.createJobParserJobApplicationLimitDialog(MainActivity.this);
                Log.d(LOG_TAG, "JobParser Error 2");
                break;
            case 3:
                Toast.makeText(this, "Sorry, there's a problem with the Job Parser please try again later.", Toast.LENGTH_SHORT).show();
                Log.d(LOG_TAG, "JobParser Error 3");
                break;
        }

        Boolean fromApplicationArchived = intent.getBooleanExtra(Constants.INTENT_EXTRA_ARCHIVED, false);
        CoordinatorLayout coordinatorlayout = (CoordinatorLayout) findViewById(R.id.activity_main_cl);
        System.out.println("archived: " + fromApplicationArchived);

        if (fromApplicationArchived) {
            Snackbar snackbar = Snackbar.make(coordinatorlayout, "You can still access this Job Application from the archive.", Snackbar.LENGTH_LONG)
                    .setAction("View Archives", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(MainActivity.this, ArchiveListActivity.class));
                        }
                    });
            snackbar.show();
        }

        int updateVersionCodeLastSeen = sharedPrefs.getInt(Constants.SHARED_PREF_APP_UPDATE_VERSION_CODE_LAST_SEEN, 0);
        Log.d(LOG_TAG, "updateVersionCodeLastSeen: " + String.valueOf(updateVersionCodeLastSeen));
        if (updateVersionCodeLastSeen < BuildConfig.VERSION_CODE) {
            displayUpdateDialog();
        }

    }

    private void commitFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.myScrollingContent, fragment, currentFragmentTag);
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        Log.d(LOG_TAG, "IT WORKS");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    //Inflate the menu; this adds items to the action bar if it is present.

        if (currentFragmentTag.equals(Constants.FRAGMENT_TAG_MAIN)) {
            getMenuInflater().inflate(R.menu.menu_main_list, menu);
            //If user is premium remove 'upgrade' option
            bp = new BillingProcessor(this, getResources().getString(R.string.google_play_iap_base_64) ,this);

            if (bp.loadOwnedPurchasesFromGoogle() && bp.isPurchased(SKU_PREMIUM)) {
                menu.findItem(R.id.action_view_upgrade).setVisible(false);
                sharedPrefs.edit().putBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, true).apply();
                Log.d(LOG_TAG, " Purchased");
            } else {
                menu.findItem(R.id.action_view_upgrade).setVisible(true);
                Log.d(LOG_TAG, " Not Purchased");
            }
        }

        if (currentFragmentTag.equals(Constants.FRAGMENT_TAG_OVERVIEW)) {
            getMenuInflater().inflate(R.menu.menu_overview, menu);
        }

        if (currentFragmentTag.equals(Constants.FRAGMENT_TAG_NEW_APPLICATION)) {
            getMenuInflater().inflate(R.menu.menu_job_application_new_activity, menu);
        }

        if (currentFragmentTag.equals(Constants.FRAGMENT_TAG_JOBMAP)) {
            getMenuInflater().inflate(R.menu.menu_jobmap, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sort) {
            MainFragment mainApplicationFragment = (MainFragment)getSupportFragmentManager().findFragmentByTag(Constants.FRAGMENT_TAG_MAIN);
            if (mainApplicationFragment != null) {
                mainApplicationFragment.createSortDialog();
            }
        }

        //MainFragment Menu Items
        if (id == R.id.action_view_jsa) {
            startActivity(new Intent(MainActivity.this, JsaListActivity.class));
        }

        if (id == R.id.action_view_archives) {
            startActivity(new Intent(MainActivity.this, ArchiveListActivity.class));
        }

        if (id == R.id.action_view_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        }

        if (id == R.id.action_view_upgrade) {
            startActivity(new Intent(MainActivity.this, UpgradeActivity.class));
        }

        //JobMapFragment Menu Items
        if (id == R.id.menu_jobmap_action_zoom_to_bounds) {
            JobMapFragment JobMapFragment = (JobMapFragment)getSupportFragmentManager().findFragmentByTag(Constants.FRAGMENT_TAG_JOBMAP);
            JobMapFragment.zoomToBounds();
        }

        if (id == R.id.menu_jobmap_action_hide_user_location) {
            JobMapFragment JobMapFragment = (JobMapFragment)getSupportFragmentManager().findFragmentByTag(Constants.FRAGMENT_TAG_JOBMAP);
            JobMapFragment.userLocationMenuItemSetChecked(item);
            JobMapFragment.toggleUserLocationVisible();
        }

        if (id == R.id.menu_jobmap_action_change_location) {
            JobMapFragment JobMapFragment = (JobMapFragment)getSupportFragmentManager().findFragmentByTag(Constants.FRAGMENT_TAG_JOBMAP);
            JobMapFragment.createLocationEditDialog();
        }

        //return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(MainActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle("Quit JobCatcher")
                .setMessage("Are you sure you want to quit JobCatcher? (This will not log you out)")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "On Restart called");
        if (currentFragmentTag.equals(Constants.FRAGMENT_TAG_NEW_APPLICATION)) {
            currentFragmentTag = Constants.FRAGMENT_TAG_MAIN;
            commitFragment(new MainFragment());
            setTitle(getResources().getString(R.string.app_name));
            invalidateOptionsMenu();
        }

    }

    private void displayUpdateDialog() {

        String title = getResources().getString(R.string.activity_main_app_update_dialog_title);
        String description = getResources().getString(R.string.activity_main_app_update_dialog_description);

        new AlertDialog.Builder(MainActivity.this, R.style.CustomStyleAlertDialog)
                .setTitle(title)
                .setMessage(description)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sharedPrefs.edit().putInt(Constants.SHARED_PREF_APP_UPDATE_VERSION_CODE_LAST_SEEN, BuildConfig.VERSION_CODE).apply();
                        Log.d(LOG_TAG, "updateVersionCodeLastSeenNew: " + String.valueOf(BuildConfig.VERSION_CODE));
                    }
                })
                .setIcon(R.drawable.ic_update)
                .show();
    }

    @Override
    public void onDestroy() {

        if (bp != null) {
            bp.release();
        }

        super.onDestroy();
    }

    @Override
    public void onBillingInitialized() {
        //Method needed for BillingProcessor but not actually used in this activity.
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        //Method needed for BillingProcessor but not actually used in this activity.
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        //Method needed for BillingProcessor but not actually used in this activity.
    }

    @Override
    public void onPurchaseHistoryRestored() {
        //Method needed for BillingProcessor but not actually used in this activity.
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

}