package com.este.jobcatcher.jobapplications;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobparser.JobParserActivity;
import com.este.jobcatcher.jobapplications.recyclerviewholders.MainListItemHolder;
import com.este.jobcatcher.jobapplications.recyclerviewholders.MainListPinnedItemHolder;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.Utils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

/**
 * Main List of Job Applications
 */
public class MainFragment extends Fragment {

    private static final String LOG_TAG = MainFragment.class.getSimpleName();

    private DatabaseReference mMainListRef;
    private DatabaseReference mMainListPinnedRef;
    private ValueEventListener mMainListRefListener;
    private ValueEventListener mMainListPinnedRefListener;

    private Utils mUtils;

    private LinearLayout mLinearLayoutLists;
    private CardView mCardViewPinnedJobApplications;
    private RecyclerView mRecyclerViewPinnedJobApplications;
    private RecyclerView mRecyclerViewJobApplications;


    private FirebaseRecyclerAdapter<JobApplication, MainListItemHolder> mRecycleViewAdapterJobApplications;
    private FirebaseRecyclerAdapter<JobApplication, MainListPinnedItemHolder> mRecyclerViewAdapterJobApplicationsPinned;
    private Query orderedListQuery;

    private TextView mTextViewMainEmptyState;

    private SharedPreferences mSharedPrefs;

    private Long currentDateCalendarMillis;

    private AlertDialog sortDialog;

    private long jobApplicationsCount;

    private FloatingActionsMenu mFAMMenuNewJobApplication;
    private FloatingActionButton mFABNewJobApplicationManual;
    private FloatingActionButton mFABNewJobApplicationUrl;

    private ProgressDialog mAuthProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.fragment_main, container, false);

        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        String userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mUtils = new Utils();

        mTextViewMainEmptyState = (TextView) convertView.findViewById(R.id.activity_main_list_ll_empty_state);
        mLinearLayoutLists = (LinearLayout) convertView.findViewById(R.id.fragment_main_ll_lists);
        mCardViewPinnedJobApplications = (CardView) convertView.findViewById(R.id.activity_main_list_cv_main_list_priority);
        mRecyclerViewPinnedJobApplications = (RecyclerView) convertView.findViewById(R.id.activity_main_list_rv_main_list_priority);
        mRecyclerViewJobApplications = (RecyclerView) convertView.findViewById(R.id.activity_main_list_rv_main_list);
        mFAMMenuNewJobApplication = (FloatingActionsMenu) convertView.findViewById(R.id.fragment_main_list_fam);
        mFABNewJobApplicationManual = (FloatingActionButton) convertView.findViewById(R.id.fragment_main_list_fab_new_job_application_manual);
        mFABNewJobApplicationUrl = (FloatingActionButton) convertView.findViewById(R.id.fragment_main_list_fab_new_job_application_url);

        mFABNewJobApplicationManual.setIconDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add));
        mFABNewJobApplicationUrl.setIconDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_link_white));

        mFABNewJobApplicationManual.setColorNormal(ContextCompat.getColor(getContext(), R.color.colorAccent));
        mFABNewJobApplicationUrl.setColorNormal(ContextCompat.getColor(getContext(), R.color.colorAccent));

        mFABNewJobApplicationManual.setColorPressed(ContextCompat.getColor(getContext(), R.color.colorAccent));
        mFABNewJobApplicationUrl.setColorPressed(ContextCompat.getColor(getContext(), R.color.colorAccent));

        //Create Firebase references
        mMainListRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mMainListRef.keepSynced(true);
        mMainListPinnedRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);
        mMainListPinnedRef.keepSynced(true);

        mMainListRefListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                jobApplicationsCount += dataSnapshot.getChildrenCount();

                if (jobApplicationsCount == 0) {
                    mTextViewMainEmptyState.setVisibility(View.VISIBLE);
                    mLinearLayoutLists.setVisibility(View.GONE);
                } else {
                    mTextViewMainEmptyState.setVisibility(View.GONE);
                    mLinearLayoutLists.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.getChildrenCount() == 0) {
                    mRecyclerViewJobApplications.setVisibility(View.GONE);
                } else {
                    mRecyclerViewJobApplications.setVisibility(View.VISIBLE);
                }

                Log.d(LOG_TAG, "MainList Children: " + dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mMainListPinnedRefListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                jobApplicationsCount += dataSnapshot.getChildrenCount();

                if (jobApplicationsCount == 0) {
                    mTextViewMainEmptyState.setVisibility(View.VISIBLE);
                    mLinearLayoutLists.setVisibility(View.GONE);
                } else {
                    mTextViewMainEmptyState.setVisibility(View.GONE);
                    mLinearLayoutLists.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.getChildrenCount() == 0) {
                    mCardViewPinnedJobApplications.setVisibility(View.GONE);
                } else {
                    mCardViewPinnedJobApplications.setVisibility(View.VISIBLE);
                }

                Log.d(LOG_TAG, "MainListPinned Children: " + dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mMainListRef.addValueEventListener(mMainListRefListener);
        mMainListPinnedRef.addListenerForSingleValueEvent(mMainListPinnedRefListener);

        currentDateCalendarMillis = getCurrentDateCalendarMillis();

        setFilter();
        setUpJobApplicationPinnedRecyclerView();
        setUpJobApplicationRecyclerView();

        mFABNewJobApplicationManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getContext().startActivity(new Intent(getActivity(), JobApplicationNewActivity.class));
                mFAMMenuNewJobApplication.collapse();
            }
        });

        mFABNewJobApplicationUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFAMMenuNewJobApplication.collapse();
                getContext().startActivity(new Intent(getActivity(), JobParserActivity.class));
            }
        });

        return convertView;

    }

    private void setUpJobApplicationRecyclerView() {
        Boolean sortAscOrder = mSharedPrefs.getBoolean(Constants.KEY_SORT_ORDER_ASC_DESC, true);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        if (sortAscOrder) {
            manager.setReverseLayout(false);
            manager.setStackFromEnd(false);
        } else {
            manager.setReverseLayout(true);
            manager.setStackFromEnd(true);
        }
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewJobApplications.setHasFixedSize(false);
        mRecyclerViewJobApplications.setLayoutManager(manager);

        mRecycleViewAdapterJobApplications = new FirebaseRecyclerAdapter<JobApplication, MainListItemHolder>(JobApplication.class, R.layout.list_item_job_application, MainListItemHolder.class, orderedListQuery) {
            @Override
            public void populateViewHolder(MainListItemHolder jobApplicationView, JobApplication jobApplication, int position) {

                jobApplicationView.getMainListItemKey(mRecycleViewAdapterJobApplications.getRef(position).getKey());
                jobApplicationView.setMainListItemJobTitle(jobApplication.getJobTitle());
                jobApplicationView.setMainListItemCompanyName(jobApplication.getCompanyName());
                jobApplicationView.setMainListItemCurrentApplicationStageAndStatus(jobApplication.getCurrentApplicationStage(), jobApplication.getApplicationStatus());
                jobApplicationView.setMainListItemReminderDateTime(jobApplication.getDeadlineDate(), currentDateCalendarMillis);

            }
        };

        mRecyclerViewJobApplications.setAdapter(mRecycleViewAdapterJobApplications);

    }

    private void setUpJobApplicationPinnedRecyclerView() {
        LinearLayoutManager managerPinned = new LinearLayoutManager(getActivity());
        managerPinned.setReverseLayout(false);
        managerPinned.setStackFromEnd(false);
        managerPinned.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewPinnedJobApplications.setHasFixedSize(false);
        mRecyclerViewPinnedJobApplications.setLayoutManager(managerPinned);

        mRecyclerViewAdapterJobApplicationsPinned = new FirebaseRecyclerAdapter<JobApplication, MainListPinnedItemHolder>(JobApplication.class, R.layout.list_item_job_application, MainListPinnedItemHolder.class, mMainListPinnedRef.orderByKey()) {
            @Override
            public void populateViewHolder(MainListPinnedItemHolder jobApplicationPinnedView, JobApplication jobApplicationPinned, int position) {

                jobApplicationPinnedView.getMainListPinnedItemKey(mRecyclerViewAdapterJobApplicationsPinned.getRef(position).getKey());
                jobApplicationPinnedView.setMainListPinnedItemJobTitle(jobApplicationPinned.getJobTitle());
                jobApplicationPinnedView.setMainListPinnedItemCompanyName(jobApplicationPinned.getCompanyName());
                jobApplicationPinnedView.setMainListPinnedItemCurrentApplicationStageAndStatus(jobApplicationPinned.getCurrentApplicationStage(), jobApplicationPinned.getApplicationStatus());
                jobApplicationPinnedView.setMainListPinnedItemReminderDateTime(jobApplicationPinned.getDeadlineDate(), currentDateCalendarMillis);

            }
        };

        mRecyclerViewPinnedJobApplications.setAdapter(mRecyclerViewAdapterJobApplicationsPinned);

    }

    public void createSortDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_filter_sort_list, null);
        alertDialog.setView(convertView);

        System.out.println("-");
        Log.d(LOG_TAG, "LinearLayoutPinned Height: " + mRecyclerViewPinnedJobApplications.getHeight());
        Log.d(LOG_TAG, "LinearLayoutPinned Visible: " + mRecyclerViewPinnedJobApplications.getVisibility());
        Log.d(LOG_TAG, "LinearLayoutNormal Height: " + mRecyclerViewJobApplications.getHeight());
        Log.d(LOG_TAG, "LinearLayoutNormal Visible: " + mRecyclerViewJobApplications.getVisibility());
        final Spinner mSpinnerSortBy = (Spinner) convertView.findViewById(R.id.dialog_filter_sort_list_sp_sort_by);
        final Spinner mSpinnerAscDesc = (Spinner) convertView.findViewById(R.id.dialog_filter_sort_list_sp_asc_desc);
        final Spinner mSpinnerFilterStage = (Spinner) convertView.findViewById(R.id.dialog_filter_sort_list_sp_filter_stage);

        final ArrayAdapter<CharSequence> adapterSortBy = ArrayAdapter.createFromResource(getContext(), R.array.array_sort_by, android.R.layout.simple_spinner_dropdown_item);
        final ArrayAdapter<CharSequence> adapterAscDesc = ArrayAdapter.createFromResource(getContext(), R.array.array_sort_asc_desc, android.R.layout.simple_spinner_dropdown_item);
        final ArrayAdapter<CharSequence> adapterFilterStage = ArrayAdapter.createFromResource(getContext(), R.array.array_filter_stage, android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSortBy.setAdapter(adapterSortBy);
        mSpinnerAscDesc.setAdapter(adapterAscDesc);
        mSpinnerFilterStage.setAdapter(adapterFilterStage);

        Boolean sortAscDesc = mSharedPrefs.getBoolean(Constants.KEY_SORT_ORDER_ASC_DESC, true);

        if (sortAscDesc) {
            //Sort by Ascending
            mUtils.selectSpinnerValue(mSpinnerAscDesc, getResources().getStringArray(R.array.array_sort_asc_desc)[0]);
        } else {
            //Sort by Descending
            mUtils.selectSpinnerValue(mSpinnerAscDesc, getResources().getStringArray(R.array.array_sort_asc_desc)[1]);
        }

        String filterStageSelection = mSharedPrefs.getString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_ALL);

        if (filterStageSelection.equals(Constants.KEY_FILTER_APPLICATION_STAGE_ALL)) {
            mUtils.selectSpinnerValue(mSpinnerFilterStage, getResources().getStringArray(R.array.array_filter_stage)[0]);
        }
        if (filterStageSelection.equals(Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED)) {
            mUtils.selectSpinnerValue(mSpinnerFilterStage, getResources().getStringArray(R.array.array_filter_stage)[1]);
        }
        if (filterStageSelection.equals(Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM)) {
            mUtils.selectSpinnerValue(mSpinnerFilterStage, getResources().getStringArray(R.array.array_filter_stage)[2]);
        }
        if (filterStageSelection.equals(Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS)) {
            mUtils.selectSpinnerValue(mSpinnerFilterStage, getResources().getStringArray(R.array.array_filter_stage)[3]);
        }
        if (filterStageSelection.equals(Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW)) {
            mUtils.selectSpinnerValue(mSpinnerFilterStage, getResources().getStringArray(R.array.array_filter_stage)[4]);
        }
        if (filterStageSelection.equals(Constants.KEY_FILTER_APPLICATION_STAGE_COMPLETE)) {
            mUtils.selectSpinnerValue(mSpinnerFilterStage, getResources().getStringArray(R.array.array_filter_stage)[5]);
        }

        final String sortOrder = mSharedPrefs.getString(Constants.KEY_SORT_ORDER_LISTS, Constants.FIREBASE_SORT_ORDER_BY_KEY);

        switch (sortOrder) {
            case Constants.FIREBASE_SORT_ORDER_BY_KEY:
                mUtils.selectSpinnerValue(mSpinnerSortBy, getResources().getStringArray(R.array.array_sort_by)[0]);
                break;
            case Constants.FIREBASE_PROPERTY_DEADLINE_DATE:
                mUtils.selectSpinnerValue(mSpinnerSortBy, getResources().getStringArray(R.array.array_sort_by)[1]);
                break;
            case Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE:
                mUtils.selectSpinnerValue(mSpinnerSortBy, getResources().getStringArray(R.array.array_sort_by)[2]);
                break;
            case Constants.FIREBASE_PROPERTY_JOB_TITLE:
                mUtils.selectSpinnerValue(mSpinnerSortBy, getResources().getStringArray(R.array.array_sort_by)[3]);
                break;
            case Constants.FIREBASE_PROPERTY_APPLICATION_PRIORITY:
                mUtils.selectSpinnerValue(mSpinnerSortBy, getResources().getStringArray(R.array.array_sort_by)[4]);
                break;
            case Constants.FIREBASE_SORT_TIMESTAMP_LAST_CHANGED:
                mUtils.selectSpinnerValue(mSpinnerSortBy, getResources().getStringArray(R.array.array_sort_by)[5]);
                break;
        }

        alertDialog.setPositiveButton(getString(R.string.dialog_sort_list_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String checkSortAscValue = mSpinnerAscDesc.getSelectedItem().toString();
                if (checkSortAscValue.equals(getResources().getStringArray(R.array.array_sort_asc_desc)[0])) {
                    mSharedPrefs.edit().putBoolean(Constants.KEY_SORT_ORDER_ASC_DESC, true).apply();
                } else {
                    mSharedPrefs.edit().putBoolean(Constants.KEY_SORT_ORDER_ASC_DESC, false).apply();
                }

                String newSortValue = "";
                Integer spinnerPosition = mSpinnerSortBy.getSelectedItemPosition();

                switch (spinnerPosition) {
                    case 0:
                        newSortValue = Constants.FIREBASE_SORT_ORDER_BY_KEY;
                        break;
                    case 1:
                        newSortValue = Constants.FIREBASE_PROPERTY_DEADLINE_DATE;
                        break;
                    case 2:
                        newSortValue = Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE;
                        break;
                    case 3:
                        newSortValue = Constants.FIREBASE_PROPERTY_JOB_TITLE;
                        break;
                    case 4:
                        newSortValue = Constants.FIREBASE_PROPERTY_APPLICATION_PRIORITY;
                        break;
                    case 5:
                        newSortValue = Constants.FIREBASE_SORT_TIMESTAMP_LAST_CHANGED;
                        break;
                }

                mSharedPrefs.edit().putString(Constants.KEY_SORT_ORDER_LISTS, newSortValue).apply();

                String mSpinnerFilterStageValue = mSpinnerFilterStage.getSelectedItem().toString();

                if (mSpinnerFilterStageValue.equals(getResources().getStringArray(R.array.array_filter_stage)[0])) {
                    mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_ALL).apply();
                }
                if (mSpinnerFilterStageValue.equals(getResources().getStringArray(R.array.array_filter_stage)[1])) {
                    mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED).apply();
                }
                if (mSpinnerFilterStageValue.equals(getResources().getStringArray(R.array.array_filter_stage)[2])) {
                    mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM).apply();
                }
                if (mSpinnerFilterStageValue.equals(getResources().getStringArray(R.array.array_filter_stage)[3])) {
                    mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS).apply();
                }
                if (mSpinnerFilterStageValue.equals(getResources().getStringArray(R.array.array_filter_stage)[4])) {
                    mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW).apply();
                }
                if (mSpinnerFilterStageValue.equals(getResources().getStringArray(R.array.array_filter_stage)[5])) {
                    mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_COMPLETE).apply();
                }

                setFilter();
                setUpJobApplicationRecyclerView();

                sortDialog.dismiss();

            }
        });

        alertDialog.setNegativeButton(getString(R.string.dialog_sort_list_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sortDialog.dismiss();
            }
        });

        alertDialog.setNeutralButton("Clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                orderedListQuery = mMainListRef.orderByChild(Constants.FIREBASE_SORT_ORDER_BY_KEY);
                mSharedPrefs.edit().putBoolean(Constants.KEY_SORT_ORDER_ASC_DESC, false).apply();
                mSharedPrefs.edit().putString(Constants.KEY_SORT_ORDER_LISTS, Constants.FIREBASE_SORT_ORDER_BY_KEY).apply();
                mSharedPrefs.edit().putString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_ALL).apply();
                setUpJobApplicationRecyclerView();
                sortDialog.dismiss();
            }
        });

        sortDialog = alertDialog.show();
    }

    private void setFilter() {

        String filterApplicationStage = mSharedPrefs.getString(Constants.KEY_FILTER_STAGE, Constants.KEY_FILTER_APPLICATION_STAGE_ALL);
        String sortByOrder = mSharedPrefs.getString(Constants.KEY_SORT_ORDER_LISTS, Constants.FIREBASE_SORT_ORDER_BY_KEY);

        if (filterApplicationStage.equals(Constants.KEY_FILTER_APPLICATION_STAGE_ALL)) {
            orderedListQuery = mMainListRef.orderByChild(sortByOrder);
        }
        if (filterApplicationStage.equals(Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED)) {
            orderedListQuery = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED);
        }
        if (filterApplicationStage.equals(Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM)) {
            orderedListQuery = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM);
        }
        if (filterApplicationStage.equals(Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS)) {
            orderedListQuery = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS);
        }
        if (filterApplicationStage.equals(Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW)) {
            orderedListQuery = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW);
        }
        if (filterApplicationStage.equals(Constants.KEY_FILTER_APPLICATION_STAGE_COMPLETE)) {
            orderedListQuery = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_COMPLETE);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRecycleViewAdapterJobApplications != null) {
            mRecycleViewAdapterJobApplications.cleanup();
        }
        if (mRecyclerViewAdapterJobApplicationsPinned != null) {
            mRecyclerViewAdapterJobApplicationsPinned.cleanup();
        }

        if (mMainListRef != null && mMainListRefListener != null) {
            mMainListRef.removeEventListener(mMainListRefListener);
        }

        if (mMainListPinnedRef != null && mMainListPinnedRefListener != null) {
            mMainListPinnedRef.removeEventListener(mMainListPinnedRefListener);
        }

    }

    private Long getCurrentDateCalendarMillis() {
        Calendar currentDateCalendar = Calendar.getInstance();
        currentDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        currentDateCalendar.set(Calendar.MINUTE, 0);
        currentDateCalendar.set(Calendar.SECOND, 0);
        currentDateCalendar.set(Calendar.MILLISECOND, 0);

        return currentDateCalendar.getTimeInMillis();
    }

    @Override
    public void onPause() {
        super.onPause();
        mFAMMenuNewJobApplication.collapse();
    }

}
