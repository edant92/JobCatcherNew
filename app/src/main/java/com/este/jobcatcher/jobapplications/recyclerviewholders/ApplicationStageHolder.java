package com.este.jobcatcher.jobapplications.recyclerviewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.este.jobcatcher.R;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ApplicationStageHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private final View mView;

    private String mApplicationStageItemKey;
    private final SimpleDateFormat sdf;

    public ApplicationStageHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mView = itemView;

        String dateFormat = "dd MMM yy";
        sdf = new SimpleDateFormat(dateFormat, Locale.US);


    }

    public void getApplicationStageItemKey(String key) {
        mApplicationStageItemKey = key;
    }

    @Override
    public void onClick(View v) {
        System.out.println("mMainListItemKey: " + mApplicationStageItemKey);
    }

    public void setApplicationStage(String applicationStageName) {
        TextView mTextViewStageName = (TextView) mView.findViewById(R.id.cardview_application_stage_tv_stage);
        mTextViewStageName.setText(applicationStageName);
    }

    public void setApplicationStageNotes(String applicationStageNotes) {
        TextView mTextViewNotes = (TextView) mView.findViewById(R.id.cardview_application_stage_tv_notes);

        if (applicationStageNotes.equals("")) {
            mTextViewNotes.setText(R.string.cardview_application_stage_tv_notes_empty);
        } else {
            mTextViewNotes.setText(applicationStageNotes);
        }

    }

    public void setApplicationStageDateStarted(Long dateStarted) {
        TextView mTextViewDateStarted = (TextView) mView.findViewById(R.id.cardview_application_stage_tv_date_started);

        if (dateStarted.equals(0L)) {
            mTextViewDateStarted.setText("-");
        } else {
            mTextViewDateStarted.setText(sdf.format(dateStarted));
        }
    }

    public void setApplicationStageDateEnded(Long dateFinished) {
        TextView mTextViewDateFinished = (TextView) mView.findViewById(R.id.cardview_application_stage_tv_date_finished);

        if (dateFinished.equals(0L)) {
            mTextViewDateFinished.setText("-");
        } else {
            mTextViewDateFinished.setText(sdf.format(dateFinished));
        }
    }

}
