package com.este.jobcatcher.jobapplications.recyclerviewholders;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.LetterTileProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ArchiveListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private static final String LOG_TAG = ArchiveListItemHolder.class.getSimpleName();

    private String mMainListItemKey;
    private final String UserUID;
    private final DatabaseReference mFirebaseRef;

    private DatabaseReference mArchiveListRef;
    private DatabaseReference mMoveToRef;

    private final SharedPreferences sharedPrefs;
    private final Resources resources;
    private final Context context;
    private final Intent intent;

    private final TextView textViewJobTitle;
    private final TextView textViewCompanyName;
    private final TextView textViewCurrentApplicationStage;
    private final TextView textViewReminderIndicator;

    private final int tileSize;
    final private LetterTileProvider tileProvider;
    private Bitmap letterTile;
    private final AppCompatImageView priorityDivider;

    private final String dateFormat;
    private final SimpleDateFormat sdf;

    public ArchiveListItemHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);

        context = itemView.getContext();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        resources = itemView.getResources();

        intent = new Intent(context, JobApplicationDetailActivity.class);

        UserUID = sharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(UserUID);

        textViewJobTitle = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_job_title);
        textViewCompanyName = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_company_name);
        textViewCurrentApplicationStage = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_current_application_stage);
        textViewReminderIndicator = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_reminder_indicator);

        tileSize = resources.getDimensionPixelSize(R.dimen.letter_tile_size);
        tileProvider = new LetterTileProvider(context);
        priorityDivider = (AppCompatImageView) itemView.findViewById(R.id.view_priority_highlight_divider);

        dateFormat = "dd MMM";
        sdf = new SimpleDateFormat(dateFormat, Locale.US);

    }

    public void getArchiveListItemKey(String key) {
        mMainListItemKey = key;
    }

    public void setArchiveListItemJobTitle(String jobTitle) {

        if (jobTitle.equals("")) {
            textViewJobTitle.setText(R.string.list_item_job_application_tv_job_title);
            textViewJobTitle.setTypeface(null, Typeface.ITALIC);
            textViewJobTitle.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
        } else {
            textViewJobTitle.setText(jobTitle);
        }

        letterTile = tileProvider.getLetterTile(jobTitle, jobTitle, tileSize, tileSize);

        priorityDivider.setBackground(new BitmapDrawable(resources, letterTile));

    }

    public void setArchiveListItemCompanyName(String companyName) {
        if (companyName.equals("")) {
            textViewCompanyName.setText(R.string.list_item_job_application_tv_company_name);
            textViewCompanyName.setTypeface(null, Typeface.ITALIC);
            textViewCompanyName.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
        } else {
            textViewCompanyName.setText(companyName);
        }
    }

    public void setArchiveListItemCurrentApplicationStage(String currentApplicationStage) {
        textViewCurrentApplicationStage.setText(currentApplicationStage);
    }

    public void setArchiveListItemReminderDateTime(Long deadlineDate, Long currentDate) {

        long dateDifference = deadlineDate - currentDate; //result in millis
        long days = (dateDifference / (24 * 60 * 60 * 1000L));

        if (deadlineDate == 0L) {
            textViewReminderIndicator.setText("");
        } else {
            textViewReminderIndicator.setText(sdf.format(deadlineDate));

            //If deadline passed
            if (days <= 0) {
                textViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
            }
            //If deadline same day or within next day
            if (days == 1 || days == 0) {
                textViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.reminder_red));
            }
            //if deadline 2 or 3 days away
            if (days == 2 || days == 3) {
                textViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.reminder_orange));
            }
            //if deadline 4 or more days away
            if (days >= 4) {
                textViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.reminder_green));
            }

        }
    }

    @Override
    public void onClick(View v) {
        if (mMainListItemKey != null) {
            intent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mMainListItemKey);
            intent.putExtra(Constants.INTENT_EXTRA_PINNED, false);
            context.startActivity(intent);
        }
    }

    @Override
    public boolean onLongClick(View v) {

        if (mMainListItemKey != null) {

            mArchiveListRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE).child(mMainListItemKey);

            mArchiveListRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    mMoveToRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(mMainListItemKey);

                    moveFirebaseRecord();

                    deleteJobApplication();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            Snackbar.make(itemView, "Job Application unarchived", Snackbar.LENGTH_LONG).show();

        }
        return false;
    }

    private void moveFirebaseRecord() {

        mArchiveListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mMoveToRef.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            System.out.println("moveFirebaseRecordToArchive() failed. firebaseError = %s");
                        } else {
                            System.out.println("moveFirebaseRecordToArchive() Great success!. firebaseError = %s");
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("moveFirebaseRecordToArchive() failed. firebaseError = %s");
            }
        });

    }

    private void deleteJobApplication() {

        mArchiveListRef.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(LOG_TAG, context.getString(R.string.log_error_updating_data) + databaseError.getMessage());
                }
            }
        });

    }

}
