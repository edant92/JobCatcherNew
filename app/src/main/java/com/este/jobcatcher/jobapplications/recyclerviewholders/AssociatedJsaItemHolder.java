package com.este.jobcatcher.jobapplications.recyclerviewholders;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;

public class AssociatedJsaItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private JobsiteAgency mAssociatedJsaListItemSelected;
    private String mAssociatedJsaListItemKey;

    public AssociatedJsaItemHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

    }

    public void getAssociatedJsaListItemSelected(JobsiteAgency selectedItem) {
        mAssociatedJsaListItemSelected = selectedItem;
    }

    public void getAssociatedJsaListItemKey(String key) {
        mAssociatedJsaListItemKey = key;
    }

    @Override
    public void onClick(View v) {
        System.out.println("mAssociatedJsaListItemSelected: " + mAssociatedJsaListItemSelected);
        System.out.println("mAssociatedJsaListItemKey: " + mAssociatedJsaListItemKey);
        if (mAssociatedJsaListItemSelected != null) {
            Intent newIntent = new Intent(Constants.INTENT_EXTRA_ASSOCIATED_JSA);
            newIntent.putExtra(Constants.INTENT_EXTRA_JSA_ID, mAssociatedJsaListItemKey);
            LocalBroadcastManager.getInstance(itemView.getContext()).sendBroadcast(newIntent);
        }
    }

    public void setAssociatedJsaListItemJsaName(String jsaName) {
        TextView textViewJsaName = (TextView) itemView.findViewById(R.id.list_item_associated_jsa_tv_jsa_name);

        if (jsaName.equals("")) {
            textViewJsaName.setText(R.string.list_item_associated_jsa_tv_jsa_name);
            textViewJsaName.setTypeface(null, Typeface.ITALIC);
            textViewJsaName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
        } else {
            textViewJsaName.setText(jsaName);
        }

    }

    public void setAssociatedJsaListItemJsaLocation(String jsaLocation) {
        TextView textViewJsaLocation = (TextView) itemView.findViewById(R.id.list_item_associated_jsa_tv_jsa_location);

        if (jsaLocation.equals("")) {
            textViewJsaLocation.setText(R.string.list_item_associated_jsa_tv_jsa_location);
            textViewJsaLocation.setTypeface(null, Typeface.ITALIC);
            textViewJsaLocation.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
        } else {
            textViewJsaLocation.setText(jsaLocation);
        }
    }

}
