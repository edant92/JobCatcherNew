package com.este.jobcatcher.jobapplications.recyclerviewholders;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.LetterTileProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Holder Class for items in MainList that are 'pinned'
 */
public class MainListPinnedItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private final String LOG_TAG = MainListItemHolder.class.getSimpleName();

    private String mainListPriorityItemKey;
    private final String userUID;
    private final DatabaseReference mFirebaseRef;

    private final Resources resources;
    private final Context context;
    private final SharedPreferences sharedPrefs;
    private final Intent intent;

    private final TextView mTextViewJobTitle;
    private final TextView mTextViewCompanyName;
    private final TextView mTextViewCurrentApplicationStage;
    private final TextView mTextViewReminderIndicator;

    private final int tileSize;
    private final LetterTileProvider tileProvider;
    private final AppCompatImageView priorityDivider;

    private final SimpleDateFormat sdf;

    public MainListPinnedItemHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        userUID = sharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);

        intent = new Intent(context, JobApplicationDetailActivity.class);

        mTextViewJobTitle = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_job_title);
        mTextViewCompanyName = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_company_name);
        mTextViewCurrentApplicationStage = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_current_application_stage);
        mTextViewReminderIndicator = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_reminder_indicator);

        resources = itemView.getResources();
        tileSize = resources.getDimensionPixelSize(R.dimen.letter_tile_size);
        tileProvider = new LetterTileProvider(context);
        priorityDivider = (AppCompatImageView) itemView.findViewById(R.id.view_priority_highlight_divider);

        String dateFormat = "dd MMM";
        sdf = new SimpleDateFormat(dateFormat, Locale.US);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);

    }

    public void getMainListPinnedItemKey(String key) {
        mainListPriorityItemKey = key;
    }

    public void setMainListPinnedItemJobTitle(String jobTitle) {

        if (jobTitle != null) {
            if (jobTitle.equals("")) {
                mTextViewJobTitle.setText(R.string.list_item_job_application_tv_job_title);
                mTextViewJobTitle.setTypeface(null, Typeface.ITALIC);
                mTextViewJobTitle.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
            } else {
                mTextViewJobTitle.setText(jobTitle);
                mTextViewJobTitle.setTypeface(null, Typeface.NORMAL);
                mTextViewJobTitle.setTextColor(ContextCompat.getColor(context, R.color.text_view_black));
            }

            final Bitmap letterTile = tileProvider.getLetterTile(jobTitle, jobTitle, tileSize, tileSize);

            priorityDivider.setBackground(new BitmapDrawable(resources, letterTile));
        }

    }

    public void setMainListPinnedItemCompanyName(String companyName) {

        if (companyName != null) {
            if (companyName.equals("")) {
                mTextViewCompanyName.setText(R.string.list_item_job_application_tv_company_name);
                mTextViewCompanyName.setTypeface(null, Typeface.ITALIC);
                mTextViewCompanyName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
            } else {
                mTextViewCompanyName.setText(companyName);
                mTextViewCompanyName.setTypeface(null, Typeface.NORMAL);
                mTextViewCompanyName.setTextColor(ContextCompat.getColor(context, R.color.text_view_black));
            }
        }

    }

    public void setMainListPinnedItemCurrentApplicationStageAndStatus(String currentApplicationStage, String applicationStatus) {
        if (currentApplicationStage != null && applicationStatus != null) {
            if (applicationStatus.equals(resources.getStringArray(R.array.array_application_status)[2]) || applicationStatus.equals(resources.getStringArray(R.array.array_application_status)[3]) || applicationStatus.equals(resources.getStringArray(R.array.array_application_status)[4])) {
                mTextViewCurrentApplicationStage.setText(applicationStatus);
            } else {
                mTextViewCurrentApplicationStage.setText(currentApplicationStage);
            }
        }
    }

    public void setMainListPinnedItemReminderDateTime(Long deadlineDate, Long currentDate) {

        if (deadlineDate != null && currentDate != null) {
            long dateDifference = deadlineDate - currentDate; //result in millis
            long days = (dateDifference / (24 * 60 * 60 * 1000L));

            if (deadlineDate == 0L) {
                mTextViewReminderIndicator.setText("");
            } else {
                mTextViewReminderIndicator.setText(sdf.format(deadlineDate));

                //If deadline passed
                if (days < 0) {
                    mTextViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
                }
                //If deadline same day or within next day
                if (days == 1 || days == 0) {
                    mTextViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.reminder_red));
                }
                //if deadline 2 or 3 days away
                if (days == 2 || days == 3) {
                    mTextViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.reminder_orange));
                }
                //if deadline 4 or more days away
                if (days >= 4) {
                    mTextViewReminderIndicator.setTextColor(ContextCompat.getColor(context, R.color.reminder_green));
                }

            }
        }

    }

    @Override
    public void onClick(View v) {

        if (mainListPriorityItemKey != null) {
            intent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mainListPriorityItemKey);
            intent.putExtra(Constants.INTENT_EXTRA_PINNED, true);
            context.startActivity(intent);
        }
    }

    @Override
    public boolean onLongClick(View v) {

        if (mainListPriorityItemKey != null) {
            new AlertDialog.Builder(context, R.style.CustomStyleAlertDialog)
                    .setMessage(resources.getString(R.string.main_list_activity_dialog_message_unpin))
                    .setPositiveButton(resources.getString(R.string.main_list_activity_dialog_positive_button), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            //Move Job Application to Priority List
                            moveFirebaseRecordFromPriority();

                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(resources.getString(R.string.main_list_activity_dialog_negative_button), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
        return false;
    }

    private void moveFirebaseRecordFromPriority() {

        mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(mainListPriorityItemKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(mainListPriorityItemKey).setValue(dataSnapshot.getValue())
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(LOG_TAG, "moveFirebaseRecordFromPriority() Great success!. firebaseError = %s");
                            //Delete Job Application from Main List
                            deleteJobApplicationFromPriorityList();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(LOG_TAG, "moveFirebaseRecordFromPriority() failed. firebaseError = %s");
                            Log.d(LOG_TAG, e.toString());
                        }
                    });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void deleteJobApplicationFromPriorityList() {

        mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST).child(mainListPriorityItemKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY).child(mainListPriorityItemKey).removeValue();
                } else {
                    Toast.makeText(context, "Error moving Job Application from Priority", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
