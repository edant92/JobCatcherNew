package com.este.jobcatcher.jobparser;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;

import com.este.jobcatcher.R;
import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * JobParser Activity - Allows users to create Job Applications from a URL
 */

public class JobParserActivity extends AppCompatActivity {

    private TextInputEditText mEditTextJobAdvertUrl;
    private JobParserUtil jobParserUtil = new JobParserUtil();

    private ProgressDialog mAuthProgressDialog;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_parser);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_job_parser_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        mEditTextJobAdvertUrl = (TextInputEditText) findViewById(R.id.activity_job_parser_et_job_advert_url);

        Button  mButtonJobAdvertCreate = (Button) findViewById(R.id.activity_job_parser_bt_create);
        mButtonJobAdvertCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = mEditTextJobAdvertUrl.getText().toString().toLowerCase().trim();
                startJobParser(url);
            }
        });

    }

    private void startJobParser(final String url) {

        if (!url.equals("") && Patterns.WEB_URL.matcher(url).matches()) {

            SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(JobParserActivity.this);

            if (!mSharedPrefs.getBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, false)) {

                String userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");
                DatabaseReference mNewJobApplicationRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);

                mNewJobApplicationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getChildrenCount() < 5) {

                            mAuthProgressDialog = new ProgressDialog(JobParserActivity.this);
                            mAuthProgressDialog.setMessage(getResources().getString(R.string.activity_job_parser_tv_loading_getting_data));
                            mAuthProgressDialog.setCancelable(false);
                            mAuthProgressDialog.show();
                            jobParserUtil.parsingActions(JobParserActivity.this, url);
                            mAuthProgressDialog.hide();
                            finish();

                        } else {
                            // Free limit reached
                            jobParserUtil.createJobParserJobApplicationLimitDialog(JobParserActivity.this);
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                mAuthProgressDialog = new ProgressDialog(JobParserActivity.this);
                mAuthProgressDialog.setMessage(getResources().getString(R.string.activity_job_parser_tv_loading_getting_data));
                mAuthProgressDialog.setCancelable(false);
                mAuthProgressDialog.show();
                jobParserUtil.parsingActions(JobParserActivity.this, url);
                mAuthProgressDialog.hide();
                finish();
            }

        } else {
            mEditTextJobAdvertUrl.setError("Hmm, that doesn't look like a real url...");
        }
    }

}
