package com.este.jobcatcher.jobparser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.MainActivity;
import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Used when link to Job Appliction is shared from an external Android application.
 */

public class JobParserReceiver extends AppCompatActivity {

    ProgressDialog mAuthProgressDialog;
    Intent goToMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = this.getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        goToMainActivity = new Intent(this, MainActivity.class);

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {

                SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

                final JobParserUtil jobParserUtil = new JobParserUtil();
                final String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

                if (!mSharedPrefs.getBoolean(Constants.SHARED_PREF_USER_IS_PREMIUM, false)) {

                    String userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");
                    DatabaseReference mNewJobApplicationRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);

                    mNewJobApplicationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.getChildrenCount() < 5) {

                                mAuthProgressDialog = new ProgressDialog(JobParserReceiver.this);
                                mAuthProgressDialog.setMessage(getResources().getString(R.string.activity_job_parser_tv_loading_getting_data));
                                mAuthProgressDialog.setCancelable(false);
                                mAuthProgressDialog.show();
                                jobParserUtil.handleSendText(JobParserReceiver.this, sharedText); // Handle text being sent
                                mAuthProgressDialog.hide();
                                startActivity(goToMainActivity);
                                finish();
                            } else {
                                // Create Job Application Limit Dialog
                                goToMainActivity.putExtra(Constants.INTENT_EXTRA_JOB_PARSER_RECEIVER_ERROR, 2);
                                startActivity(goToMainActivity);
                                finish();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            goToMainActivity.putExtra(Constants.INTENT_EXTRA_JOB_PARSER_RECEIVER_ERROR, 3);
                            startActivity(goToMainActivity);
                            finish();
                        }
                    });
                } else {
                    mAuthProgressDialog = new ProgressDialog(JobParserReceiver.this);
                    mAuthProgressDialog.setMessage(getResources().getString(R.string.activity_job_parser_tv_loading_getting_data));
                    mAuthProgressDialog.setCancelable(false);
                    mAuthProgressDialog.show();
                    jobParserUtil.handleSendText(JobParserReceiver.this, sharedText); // Handle text being sent
                    mAuthProgressDialog.hide();
                    startActivity(goToMainActivity);
                    finish();
                }

            } else {
                //Create error with URL toast
                goToMainActivity.putExtra(Constants.INTENT_EXTRA_JOB_PARSER_RECEIVER_ERROR, 1);
                startActivity(goToMainActivity);
                finish();
            }

        }

    }

}
