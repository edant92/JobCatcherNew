package com.este.jobcatcher.jobparser;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.general.UpgradeActivity;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.models.JobLocation;
import com.este.jobcatcher.models.JobParserRecord;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.Jobsites;
import com.este.jobcatcher.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * JobParserUtil Activity - Allows users to Import Job Advert data from a URL (relevant API is called and data is returned as JSON)
 */

public class JobParserUtil {

    private final String LOG_TAG = JobParserUtil.class.getSimpleName();

    private Context ctx;
    private SharedPreferences mSharedPrefs;
    private String userUID;
    private String mJobApplicationId;
    private DatabaseReference mNewJobApplicationRef;
    private DatabaseReference mNewJobApplicationPushRef;
    private DatabaseReference mAssociatedJsaListRef;

    private JobApplication newJobApplication;

    public void parsingActions(Context context, String url) {

        ctx = context;

        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);

        userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");
        mNewJobApplicationRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID).child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mNewJobApplicationPushRef = mNewJobApplicationRef.push();
        mJobApplicationId = mNewJobApplicationPushRef.getKey();
        mAssociatedJsaListRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);

        Log.d(LOG_TAG, "JobSite Url: " + url);

        final String jobSiteName = getJobSiteNameFromUrl(url);
        Log.d(LOG_TAG, "JobSite Name: " + jobSiteName);

        if (!jobSiteName.equals("")) {
            String jobSiteJobAdvertId = getJobSiteJobAdvertIdFromUrl(url, jobSiteName);
            Log.d(LOG_TAG, "JobSite AdvertId: " + jobSiteJobAdvertId);

            String jobSiteAPIQueryUrl = createJobSiteAPIQueryUrl(jobSiteName, jobSiteJobAdvertId);
            Log.d(LOG_TAG, "JobSite API QueryUrl: " + jobSiteAPIQueryUrl);

            getDataFromAPI(jobSiteAPIQueryUrl, jobSiteName);

        } else {
            showJobParseFailUI();
        }

        saveToJobParserRecord(url);

    }

    void handleSendText(Context context, String sharedText) {
        ctx = context;
        if (sharedText != null) {
            Log.d(LOG_TAG, "sharedText Original:" + sharedText);
            String sharedTextUrl = getUrlFromSharedText(sharedText);
            if (sharedTextUrl != null) {
                parsingActions(context, sharedTextUrl);
            } else {
                Toast.makeText(context, "Sorry, we either couldn't find a Url or don't support that site yet.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getUrlFromSharedText(String string) {

        String urlFromSharedText = string;

        try {
            urlFromSharedText = string.substring(string.indexOf("http")).trim();
        } catch (StringIndexOutOfBoundsException e) {
            Toast.makeText(ctx, "Sorry, we either couldn't find a Url or don't support that site yet.", Toast.LENGTH_SHORT).show();
        }

        return urlFromSharedText;
    }

    private String getJobSiteNameFromUrl(String url) {

        String jobsiteName = "";
        String urlPathHost;

        try {
            urlPathHost = HttpUrl.parse(url).host();
        } catch (NullPointerException e) {
            Toast.makeText(ctx, "Sorry, this isn't a valid Url.", Toast.LENGTH_SHORT).show();
            return "";
        }

        Log.d(LOG_TAG, "Jobsite Url Path Host: " + urlPathHost);

        List<String> jobsiteList = Jobsites.JOBSITES_LIST;
        Log.d(LOG_TAG, jobsiteList.toString());

        for (String jname : jobsiteList) {
            if (urlPathHost.contains(jname)) {
                jobsiteName = jname;
            }
        }

        return jobsiteName;
    }

    private String getJobSiteJobAdvertIdFromUrl(String url, String jobsiteName) {

        String jobSiteJobId = "";

        switch (jobsiteName) {
            case Jobsites.ADZUNA:
                jobSiteJobId = HttpUrl.parse(url).queryParameter("topdoc_id");
                if (jobSiteJobId == null) {
                    try {
                        jobSiteJobId = HttpUrl.parse(url).pathSegments().get(3);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        Log.d(LOG_TAG, "Adzuna: Can't get from URL 2nd method");
                    }

                }
                break;
            case Jobsites.AUTHENTIC_JOBS:
                try {
                    jobSiteJobId = HttpUrl.parse(url).pathSegments().get(1);
                    break;
                } catch (IndexOutOfBoundsException e) {
                    jobSiteJobId = "";
                    break;
                }
            case Jobsites.INDEED:
                jobSiteJobId = HttpUrl.parse(url).queryParameter("jk");
                if (jobSiteJobId == null  || jobSiteJobId.equals("") ) {
                    jobSiteJobId = HttpUrl.parse(url).encodedFragment();
                }
                break;
            case Jobsites.REED:
                try {
                    jobSiteJobId = HttpUrl.parse(url).pathSegments().get(2);
                    break;
                } catch (IndexOutOfBoundsException e) {
                    jobSiteJobId = "";
                    break;
                }
        }

        return jobSiteJobId;
    }

    private String createJobSiteAPIQueryUrl(String jobSiteName, String jobAdvertId) {

        Log.d(LOG_TAG, "jobSiteAPIQueryUrl jobsitename: " + jobSiteName);

        String jobSiteAPIQueryUrl = "";

        switch (jobSiteName) {
            case Jobsites.ADZUNA:
                jobSiteAPIQueryUrl = "http://api.adzuna.com/v1/api/jobs/gb/search/1?app_id=" + ctx.getResources().getString(R.string.api_app_id_adzuna) + "&app_key=" + ctx.getResources().getString(R.string.api_app_key_adzuna) + "&what=" + jobAdvertId;
                break;
            case Jobsites.AUTHENTIC_JOBS:
                jobSiteAPIQueryUrl = "https://authenticjobs.com/api/?api_key=" + ctx.getResources().getString(R.string.api_key_authentic_jobs) + "&method=aj.jobs.get&id=" + jobAdvertId + "&format=json";
                break;
            case Jobsites.INDEED:
                String publisherId = ctx.getResources().getString(R.string.api_publisher_id_indeed);
                jobSiteAPIQueryUrl = "http://api.indeed.com/ads/apigetjobs?publisher=" + publisherId + "&jobkeys=" + jobAdvertId + "&v=2&format=json";
                break;
            case Jobsites.REED:
                jobSiteAPIQueryUrl = "https://www.reed.co.uk/api/1.0/jobs/" + jobAdvertId;
                break;
        }

        return jobSiteAPIQueryUrl;
    }

    private void getDataFromAPI(String url, String jobsiteName) {

            JSONObject jsonObject = useOkHttpClientForJson(url, jobsiteName);
            Log.d(LOG_TAG, "Data Received");
            addNewJobApplicationObject(parseJSONObjectToHashMap(jsonObject, jobsiteName));

    }

    private JSONObject useOkHttpClientForJson(String url, String jobsiteName) {
        OkHttpClient client = new OkHttpClient();
        Request request = null;

        switch (jobsiteName) {
            case Jobsites.ADZUNA:
            case Jobsites.AUTHENTIC_JOBS:
            case Jobsites.INDEED:
                request = new Request.Builder()
                        .url(url)
                        .build();
                break;
            case Jobsites.REED:
                request = new Request.Builder()
                        .url(url)
                        .addHeader("Authorization", okhttp3.Credentials.basic(ctx.getResources().getString(R.string.api_key_reed), ""))
                        .build();
                break;
        }

        //TODO Look at how to catch OkHttpClientResponseconnect timed out (if user has poor signal) - any other exceptions thrown?

        if (request != null) {

            Log.d(LOG_TAG, "OkHttpClient Request: " + request.toString());

            JSONObject jsonObject = new JSONObject();

            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                Response response = client.newCall(request).execute();

                String responseString = response.body().string();
                Log.d(LOG_TAG, "OkHttpClientResponse: " + responseString);

                switch (jobsiteName) {
                    case Jobsites.ADZUNA:
                        jsonObject = new JSONObject(responseString).getJSONArray("results").getJSONObject(0);
                        break;
                    case Jobsites.AUTHENTIC_JOBS:
                        jsonObject = new JSONObject(responseString);
                        break;
                    case Jobsites.INDEED:
                        jsonObject = new JSONObject(responseString).getJSONArray("results").getJSONObject(0);
                        break;
                    case Jobsites.REED:
                        jsonObject = new JSONObject(responseString);
                        break;
                }

                Log.d(LOG_TAG, "OkHttpClientResponseJsonObject: " + jsonObject.toString());

                return jsonObject;

            } catch (IOException | JSONException e) {
                Log.d(LOG_TAG, "Error with OkHttpClientResponse" + e.getLocalizedMessage());
                Toast.makeText(ctx, ctx.getResources().getString(R.string.activity_job_parser_toast_error), Toast.LENGTH_LONG).show();
            }
        }

        return null;
    }


    private HashMap<String, Object> parseJSONObjectToHashMap(JSONObject jsonObject, String jobsiteName) {

        // Create hashmap so new values can be added to Firebase
        HashMap<String, Object> jsonResultsHash = new HashMap<>();

        switch (jobsiteName) {
            case Jobsites.ADZUNA:
                jsonResultsHash = jsonResultsHashFromAdzuna(jsonObject);
                break;
            case Jobsites.AUTHENTIC_JOBS:
                jsonResultsHash = jsonResultsHashFromAuthenticJobs(jsonObject);
                break;
            case Jobsites.INDEED:
                jsonResultsHash = jsonResultsHashFromIndeed(jsonObject);
                break;
            case Jobsites.REED:
                jsonResultsHash = jsonResultsHashFromReed(jsonObject);
                break;
        }

        return jsonResultsHash;

    }

    private HashMap<String, Object> jsonResultsHashFromAdzuna(JSONObject jsonObject) {

        // Create hashmap so new values can be added to Firebase
        HashMap<String, Object> jsonResultsHash = new HashMap<>();

        try {

            try {
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, jsonObject.getDouble("latitude"));
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, jsonObject.getDouble("longitude"));
                JSONArray blah = jsonObject.getJSONObject("location").getJSONArray("area");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, blah.get(blah.length()-1).toString());
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, true);
            } catch (JSONException e) {
                Log.d(LOG_TAG, e.getLocalizedMessage());
            }

            int salaryMax;
            int salaryMin;

            try {
                salaryMax = jsonObject.getInt("salary_max");
                salaryMin = jsonObject.getInt("salary_min");
            } catch (JSONException e) {
                salaryMax = 0;
                salaryMin = 0;
            }

            jsonResultsHash.put(Constants.FIREBASE_PROPERTY_WAGE_AMOUNT, salaryMax / salaryMin);

            String jobTitle = jsonObject.getString("title");
            jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TITLE, jobTitle);

            String jobUrl = jsonObject.getString("redirect_url");
            jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_ADVERT_URL, jobUrl);

            String jobDescription = jsonObject.getString("description");
            jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_DESCRIPTION, jobDescription);

            checkIfJsaExistsAndSetKey(Jobsites.ADZUNA);

            Log.d(LOG_TAG, "JobSite Json Results HashMap: " + jsonResultsHash.toString());
            return jsonResultsHash;

        } catch (JSONException e) {
            Log.d(LOG_TAG, e.getLocalizedMessage());
        }
        return null;
    }

    private HashMap<String, Object> jsonResultsHashFromAuthenticJobs(JSONObject jsonObject) {

        // Create hashmap so new values can be added to Firebase
        HashMap<String, Object> jsonResultsHash = new HashMap<>();

        try {

            if (jsonObject != null && !jsonObject.getString("stat").equals("fail")) {

                JSONObject listingJSON = jsonObject.getJSONObject("listing");
                JSONObject companyJSON = jsonObject.getJSONObject("company");

                String companyName = listingJSON.getJSONObject("company").getString("name");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_COMPANY_NAME, companyName);

                String jobTitle = listingJSON.getString("title");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TITLE, jobTitle);

                JSONObject location = companyJSON.getJSONObject("location");

                if (listingJSON.getInt("telecommuting") == 0) {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, location.getString("city"));
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, location.getDouble("lat"));
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, location.getDouble("lng"));
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, true);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, false);
                } else {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, true);
                }

                String jobUrl = jsonObject.getString("url");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_ADVERT_URL, jobUrl);

                Integer jobTypeId = listingJSON.getJSONObject("type").getInt("id");

                switch (jobTypeId) {
                    case 1:
                        //Full-Time
                        jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[0]);
                        break;
                    case 2:
                        //Freelance
                        jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[4]);
                        break;
                    case 3:
                        //Contract
                        jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[3]);
                        break;
                    case 4:
                        //Internship
                        jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[2]);
                        break;
                    case 5:
                        //Part-Time
                        jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[1]);
                        break;
                }

                String jobDescription = listingJSON.getString("description");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_DESCRIPTION, jobDescription);

                checkIfJsaExistsAndSetKey(Jobsites.AUTHENTIC_JOBS);

                Log.d(LOG_TAG, "JobSite Json Results HashMap: " + jsonResultsHash.toString());
                return jsonResultsHash;

            } else {
                showJobParseFailUI();
            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, e.getLocalizedMessage());
        }
        return null;
    }

    private HashMap<String, Object> jsonResultsHashFromReed(JSONObject jsonObject) {

        // Create hashmap so new values can be added to Firebase
        HashMap<String, Object> jsonResultsHash = new HashMap<>();

        try {
            if (jsonObject != null) {

                String companyName = jsonObject.getString("employerName");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_COMPANY_NAME, companyName);

                String jobTitle = jsonObject.getString("jobTitle");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TITLE, jobTitle);

                String location = jsonObject.getString("locationName");

                if (location != null) {
                    JobLocation jobLocation = getJobLocation(location);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, jobLocation.getName());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, jobLocation.getLatitude());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, jobLocation.getLongitude());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, jobLocation.getRemote());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, true);
                }

                //Work out average of salaries
                double minimumSalary;
                double maximumSalary;
                try {
                    minimumSalary = jsonObject.getDouble("minimumSalary");
                } catch (JSONException e) {
                    minimumSalary = 0.0;
                }

                try {
                    maximumSalary = jsonObject.getDouble("maximumSalary");
                } catch (JSONException e) {
                    maximumSalary = 0.0;
                }

                double wageAmount = (minimumSalary + maximumSalary) / 2;

                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_WAGE_AMOUNT, wageAmount);

                String currency = jsonObject.getString("currency");

                switch (currency) {
                    case "GBP":
                        currency = ctx.getResources().getStringArray(R.array.array_currency)[0];
                        break;
                    case "USD":
                        currency = ctx.getResources().getStringArray(R.array.array_currency)[1];
                        break;
                    case "EUR":
                        currency = ctx.getResources().getStringArray(R.array.array_currency)[2];
                        break;
                    default: //GBP
                        currency = ctx.getResources().getStringArray(R.array.array_currency)[0];
                        break;
                }

                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_WAGE_CURRENCY, currency);

                String salaryType = jsonObject.getString("salaryType");

                switch(salaryType) {
                    case "per hour":
                        salaryType = ctx.getResources().getStringArray(R.array.array_wage_frequency)[0]; //Hourly
                        break;
                    case "per day":
                        salaryType = ctx.getResources().getStringArray(R.array.array_wage_frequency)[1]; //Daily
                        break;
                    case "per week":
                        salaryType = ctx.getResources().getStringArray(R.array.array_wage_frequency)[2]; //Weekly
                        break;
                    case "per month":
                        salaryType = ctx.getResources().getStringArray(R.array.array_wage_frequency)[4]; //Monthly
                        break;
                    case "per annum":
                        salaryType = ctx.getResources().getStringArray(R.array.array_wage_frequency)[5]; //Yearly
                        break;
                    default:
                        salaryType = ctx.getResources().getStringArray(R.array.array_wage_frequency)[0]; //Hourly
                        break;
                }

                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_WAGE_FREQUENCY, salaryType);

                String jobUrl = jsonObject.getString("jobUrl");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_ADVERT_URL, jobUrl);

                boolean partTime = jsonObject.getBoolean("partTime");
                boolean fullTime = jsonObject.getBoolean("fullTime");

                if (partTime) {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[1]);
                } else if (fullTime) {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[0]);
                }

                String contractType = jsonObject.getString("contractType");

                if (contractType.equals("contract") || contractType.equals("temporary")) {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TYPE, ctx.getResources().getStringArray(R.array.array_job_type)[3]);
                }

                String jobDescription = jsonObject.getString("jobDescription");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_DESCRIPTION, jobDescription);

                checkIfJsaExistsAndSetKey(Jobsites.REED);

                Log.d(LOG_TAG, "JobSite Json Results HashMap: " + jsonResultsHash.toString());

                return jsonResultsHash;

            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, e.getLocalizedMessage());
        }
        return null;
    }

    private HashMap<String, Object> jsonResultsHashFromIndeed(JSONObject jsonObject) {

        // Create hashmap so new values can be added to Firebase
        HashMap<String, Object> jsonResultsHash = new HashMap<>();

        try {
            if (jsonObject != null) {

                String companyName = jsonObject.getString("company");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_COMPANY_NAME, companyName);

                String jobTitle = jsonObject.getString("jobtitle");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_TITLE, jobTitle);

                String locationName = jsonObject.getString("city");
                double locationLatitude;
                double locationLongitude;
                try {
                    locationLatitude = jsonObject.getDouble("latitude");
                } catch (JSONException e) {
                    locationLatitude = 0.0;
                }

                try {
                    locationLongitude = jsonObject.getDouble("longitude");
                } catch (JSONException e) {
                    locationLongitude = 0.0;
                }

                if (locationLatitude != 0.0 && locationLongitude != 0.0) {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, locationName);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, locationLatitude);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, locationLongitude);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, false);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, true);
                } else if (locationName != null) {
                    JobLocation jobLocation = getJobLocation(locationName);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, jobLocation.getName());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, jobLocation.getLatitude());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, jobLocation.getLongitude());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, jobLocation.getRemote());
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, true);
                } else {
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_NAME, "");
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LAT, "");
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_LONG, "");
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_REMOTE, false);
                    jsonResultsHash.put(Constants.FIREBASE_PROPERTY_LOCATION_SET, false);
                }

                String jobUrl = jsonObject.getString("url");
                String publisherId = ctx.getResources().getString(R.string.api_publisher_id_indeed);
                jobUrl = jobUrl.concat("&indpubnum=" + publisherId);
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_ADVERT_URL, jobUrl);

                String jobDescription = jsonObject.getString("snippet");
                jsonResultsHash.put(Constants.FIREBASE_PROPERTY_JOB_DESCRIPTION, jobDescription);

                checkIfJsaExistsAndSetKey(Jobsites.INDEED);

                Log.d(LOG_TAG, "JobSite Json Results HashMap: " + jsonResultsHash.toString());
                return jsonResultsHash;

            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, e.getLocalizedMessage());
        }
        return null;
    }

    private void addNewJobApplicationObject(HashMap<String, Object> updatedProperties) {

        Utils utils = new Utils();

        newJobApplication = utils.createJobApplicationWithDefaultValues(ctx);

        if (updatedProperties != null) {

            mNewJobApplicationPushRef.setValue(newJobApplication);
            mNewJobApplicationPushRef.updateChildren(updatedProperties);
            updateTimestampLastChanged();
            //mAuthProgressDialog.hide();
            goToJobApplicationDetailActivity();

        }

    }

    private void goToJobApplicationDetailActivity() {
        Intent goToNewlyCreatedDetailActivity = new Intent(ctx, JobApplicationDetailActivity.class);
        goToNewlyCreatedDetailActivity.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mJobApplicationId);
        goToNewlyCreatedDetailActivity.putExtra(Constants.INTENT_EXTRA_PINNED, false);
        ctx.startActivity(goToNewlyCreatedDetailActivity);
    }

    private JobLocation getJobLocation(String locationName) {

        Log.d(LOG_TAG, "Passed Location Name: " + locationName);

        Double locationLat;
        Double locationLong;

        try {
            Geocoder geo = new Geocoder(ctx, Locale.getDefault());
            Log.d(LOG_TAG, "Trying GeoCoder");
            List<Address> addresses = geo.getFromLocationName(locationName, 1);
            Log.d(LOG_TAG, "Trying Addresses");

            while (addresses.isEmpty()) {
                int counter = 0;
                while (counter < 5) {
                    addresses = geo.getFromLocationName(locationName, 1);
                    Log.d(LOG_TAG, "Geocoder attempt #" + counter);
                    counter++;
                }

            }

            //If Addresses is still empty
            if (addresses.isEmpty()) {
                Log.d(LOG_TAG, "Addresses Empty");
                return new JobLocation("", 0.0, 0.0, false);
            } else {
                    locationLat = addresses.get(0).getLatitude();
                    locationLong = addresses.get(0).getLongitude();
                    Log.d(LOG_TAG, "Passed Location Latitude: " + locationLat.toString());
                    Log.d(LOG_TAG, "Passed Location Longitude: " + locationLong.toString());
                    return new JobLocation(locationName, locationLat, locationLong, false);
            }

        } catch (Exception e) {
            Log.d(LOG_TAG, "Unable to find location" + e.getLocalizedMessage());
            return new JobLocation("", 0.0, 0.0, false);
        }

    }

    private void updateTimestampLastChanged() {
        HashMap<String, Object> dateLastChangedObj = new HashMap<>();
        dateLastChangedObj.put("date", ServerValue.TIMESTAMP);
        mNewJobApplicationPushRef.child(Constants.FIREBASE_PROPERTY_TIMESTAMP_LAST_CHANGED).setValue(dateLastChangedObj);
    }

    private void showJobParseFailUI() {
        //mAuthProgressDialog.hide();
        Toast.makeText(ctx, ctx.getResources().getString(R.string.activity_job_parser_toast_error_getting_data), Toast.LENGTH_SHORT).show();
    }

    private void checkIfJsaExistsAndSetKey(String jobsiteName) {

        final String jobsiteNameCapital = WordUtils.capitalizeFully(jobsiteName);

        Query searchJsaByName = mAssociatedJsaListRef.orderByChild(Constants.FIREBASE_PROPERTY_JSA_NAME).equalTo(jobsiteNameCapital).limitToFirst(1);

        Log.d(LOG_TAG, "Query: " + searchJsaByName.toString());

        searchJsaByName.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "Datasnapshot: " + dataSnapshot.toString());

                if (dataSnapshot.getValue() == null) {
                    Log.d(LOG_TAG, "JSA: " + jobsiteNameCapital + " doesn't exist");
                    JobsiteAgency newJobsiteAgency = new JobsiteAgency(jobsiteNameCapital, "", "", "", "", "", "", "", "", false, "", 0.0, 0.0);

                    /* Add the jsa to database */
                    DatabaseReference mNewJsaPushRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).push();
                    mNewJsaPushRef.setValue(newJobsiteAgency);

                    mNewJobApplicationPushRef.child(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).setValue(mNewJsaPushRef.getKey());

                    Log.d(LOG_TAG, "JSAID " + dataSnapshot.getKey() + " added");
                } else {

                    for (DataSnapshot child : dataSnapshot.getChildren()) {

                        Log.d(LOG_TAG, "Child Datasnapshot: " + child.toString());

                        Log.d(LOG_TAG, "JSA: " + jobsiteNameCapital + " exists");

                        mNewJobApplicationPushRef.child(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).setValue(child.getKey());

                        Log.d(LOG_TAG, "JSAID " + child.getKey() + " added");

                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void createJobParserJobApplicationLimitDialog(final Context ctx) {

        Log.d(LOG_TAG, "Limit Dialog is showing");

        new AlertDialog.Builder(ctx, R.style.CustomStyleAlertDialog)
                .setTitle(ctx.getResources().getString(R.string.dialog_free_limit_reached))
                .setMessage(ctx.getResources().getString(R.string.dialog_free_limit_reached_message_job_parser))
                .setPositiveButton(ctx.getResources().getString(R.string.dialog_free_limit_reached_button_positive), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ctx.startActivity(new Intent(ctx, UpgradeActivity.class));
                    }
                })
                .setNegativeButton(R.string.dialog_free_limit_reached_button_negative, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(R.drawable.ic_action_warning)
                .show();

    }

    private void saveToJobParserRecord(String url) {

        Calendar cal = Calendar.getInstance();
        int year  = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date  = cal.get(Calendar.DATE);
        cal.clear();
        cal.set(year, month, date);
        Long dateTimestamp = cal.getTimeInMillis();

        DatabaseReference jobParserRecordRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_PARSER_URLS_RECORD).child(dateTimestamp.toString());
        DatabaseReference jobParserRecordPushRef = jobParserRecordRef.push();

        String userLocation = mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, "");
        if (userLocation.equals("")) {
            userLocation = "No Location set";
        }

        JobParserRecord jobParserRecord = new JobParserRecord(url, userLocation, Calendar.getInstance().getTimeInMillis());

        jobParserRecordPushRef.setValue(jobParserRecord);

    }

}
