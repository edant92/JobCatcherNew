package com.este.jobcatcher.jsas;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.este.jobcatcher.R;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JsaDetailActivity extends AppCompatActivity {

    private static final String LOG_TAG = JsaDetailActivity.class.getSimpleName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String mJSAId;
    private DatabaseReference mFirebaseRef;
    private DatabaseReference mJobApplicationListRef;
    private DatabaseReference mJobApplicationListPriorityRef;
    private DatabaseReference mJobApplicationListArchiveRef;
    private DatabaseReference mJsaListRef;
    private Long jsaAssociatedApplicationNumber;

    private SharedPreferences mSharedPref;
    private String userUID;
    private String mJsaName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsa_details);

        Intent intent = this.getIntent();

        mJSAId = intent.getStringExtra(Constants.INTENT_EXTRA_JSA_ID);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_jsa_details_toolbar);
        /* Common toolbar setup */
        setSupportActionBar(toolbar);
        /* Add back button to the action bar */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mJsaListRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(mJSAId);

        mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);
        mJobApplicationListRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mJobApplicationListPriorityRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);
        mJobApplicationListArchiveRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE);

        jsaAssociatedApplicationNumber = 0L;

        mJsaListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                JobsiteAgency jobsiteAgency = dataSnapshot.getValue(JobsiteAgency.class);

                if (jobsiteAgency == null) {
            /* No point in continuing without a valid ID. */
                    finish();
                } else {

                    mJsaName = jobsiteAgency.getJsaName();

                    Query queryRef = mJobApplicationListRef.orderByChild(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).equalTo(mJSAId);
                    Query queryRefPriority = mJobApplicationListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).equalTo(mJSAId);
                    Query queryRefArchive = mJobApplicationListArchiveRef.orderByChild(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).equalTo(mJSAId);

                    queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            jsaAssociatedApplicationNumber += dataSnapshot.getChildrenCount();

                            if (tabLayout.getTabAt(1) != null) {
                                tabLayout.getTabAt(1).setText("Applications" + " (" + jsaAssociatedApplicationNumber + ")");
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    queryRefPriority.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            jsaAssociatedApplicationNumber += dataSnapshot.getChildrenCount();
                            tabLayout.getTabAt(1).setText("Applications" + " (" + jsaAssociatedApplicationNumber + ")");
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    queryRefArchive.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            jsaAssociatedApplicationNumber += dataSnapshot.getChildrenCount();
                            tabLayout.getTabAt(1).setText("Applications" + " (" + jsaAssociatedApplicationNumber + ")");
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    viewPager = (ViewPager) findViewById(R.id.viewpager);
                    setupViewPager(viewPager);

                    tabLayout = (TabLayout) findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(viewPager);

                    Intent a = new Intent (JsaDetailActivity.this, JsaDetailActivityFragmentDetails.class);
                    a.putExtra(Constants.INTENT_EXTRA_JSA_ID, mJSAId);
                    setIntent(a);

                    Intent b = new Intent (JsaDetailActivity.this, JsaDetailActivityFragmentAssociatedApplications.class);
                    b.putExtra(Constants.INTENT_EXTRA_JSA_ID, mJSAId);
                    setIntent(b);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new JsaDetailActivityFragmentDetails(), "Details");
        adapter.addFragment(new JsaDetailActivityFragmentAssociatedApplications(), "");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present. */
        getMenuInflater().inflate(R.menu.menu_jsa_detail_activity, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_delete_jsa) {

            String jsaDeleteName;

            if (mJsaName.equals("")) {
                jsaDeleteName = getResources().getString(R.string.jsa_detail_jsa_name_empty);
            } else {
                jsaDeleteName = mJsaName;
            }

            new AlertDialog.Builder(JsaDetailActivity.this, R.style.CustomStyleAlertDialog)
                    .setTitle(this.getString(R.string.dialog_activity_jsa_details_delete_application_title))
                    .setMessage(this.getString(R.string.dialog_activity_jsa_details_delete_application_message_p1)
                            + jsaDeleteName
                            + this.getString(R.string.dialog_activity_jsa_details_delete_application_message_p2))
                    .setPositiveButton(this.getString(R.string.dialog_activity_jsa_details_delete_application_positive_button), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteJsa();
                            Intent goToJsaListActivity = new Intent(JsaDetailActivity.this, JsaListActivity.class);
                            startActivity(goToJsaListActivity);
                        }
                    })
                    .setNegativeButton(R.string.dialog_activity_jsa_details_delete_application_negative_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(R.drawable.ic_action_warning)
                    .show();

            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    private void deleteJsa() {

        HashMap<String, Object> deleteJsaData = new HashMap<>();

        deleteJsaData.put("/" + mJSAId, null);

        DatabaseReference firebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);

        /* Do a deep-path update */
        firebaseRef.updateChildren(deleteJsaData, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(LOG_TAG, getString(R.string.log_error_updating_data) + databaseError.getMessage());
                }

            }
        });

    }

}