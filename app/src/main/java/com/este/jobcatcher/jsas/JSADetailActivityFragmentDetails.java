package com.este.jobcatcher.jsas;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Displays details of a Jobsite/Agency
 */
public class JsaDetailActivityFragmentDetails extends Fragment implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback {

    private static final String LOG_TAG = JsaDetailActivity.class.getSimpleName();
    private DatabaseReference mJsaRef;
    private String mJSAId;

    private LinearLayout mLinearLayoutJsaName;
    private TextView mTextViewJSAName;
    private LinearLayout mLinearLayoutJsaContactName;
    private TextView mTextViewJSAContactTitle;
    private TextView mTextViewJSAContactFirstName;
    private TextView mTextViewJSAContactSurname;
    private TextView mTextViewJSAContactPosition;
    private LinearLayout mLinearLayoutJsaLocation;
    private TextView mTextViewJsaLocation;
    private LinearLayout mLinearLayoutJsaContactEmail;
    private TextView mTextViewJSAContactEmail;
    private LinearLayout mLinearLayoutJsaContactPhone;
    private TextView mTextViewJSAContactPhone;
    private LinearLayout mLinearLayoutJsaContactWebsite;
    private TextView mTextViewJSAContactWebsite;
    private LinearLayout mLinearLayoutJsaNotes;
    private TextView mTextViewJSANotes;

    private Dialog locationMapDialog;
    private MapFragment mMapDialogFragmentLocation;
    private GoogleMap mMap;

    private UiSettings mapSettings;

    private Dialog mEditJsaNameDialog;
    private Dialog mEditJsaContactNameDialog;
    private Dialog mEditJsaContactEmailDialog;
    private Dialog mEditJsaContactPhoneDialog;
    private Dialog mEditJsaContactWebsiteDialog;
    private Dialog mDialogJsaContactWebsiteAction;
    private Dialog mEditJsaNotesDialog;

    private String mJsaName;
    private String mJsaContactTitle;
    private String mJsaContactFirstName;
    private String mJsaContactSurname;
    private String mJsaContactPosition;
    private String mJsaContactEmail;
    private String mJsaContactPhone;
    private String jsaContactWebsite;
    private String mJsaNotes;

    private Boolean mJsaLocationRemote;
    private String mJsaLocationName;
    private Double mJsaLocationLat;
    private Double mJsaLocationLong;

    private SharedPreferences mSharedPref;
    private String userUID;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;
    private AlertDialog locationAddEditDialog;
    private AutoCompleteTextView mAutocompleteTextViewLocation;
    private ProgressBar mAutocompleteProgressBar;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLngBounds BOUNDS_USER;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pager_jsa_details_details, container, false);

        JsaDetailActivity activity = (JsaDetailActivity) getActivity();
        Intent b = activity.getIntent();
        mJSAId = b.getStringExtra(Constants.INTENT_EXTRA_JSA_ID);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mJsaRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID).child(mJSAId);

        mLinearLayoutJsaName = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_jsa_name);
        mTextViewJSAName = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_jsa_name);
        mLinearLayoutJsaContactName = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_contact_name);
        mTextViewJSAContactTitle = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_title);
        mTextViewJSAContactFirstName = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_first_name);
        mTextViewJSAContactSurname = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_surname_);
        mTextViewJSAContactPosition = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_position);
        mLinearLayoutJsaLocation = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_location);
        mTextViewJsaLocation = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_location);
        mLinearLayoutJsaContactEmail = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_contact_email);
        mTextViewJSAContactEmail = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_email);
        mLinearLayoutJsaContactPhone = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_contact_phone);
        mTextViewJSAContactPhone = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_phone);
        mLinearLayoutJsaContactWebsite = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_contact_website);
        mTextViewJSAContactWebsite = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_contact_website);
        mLinearLayoutJsaNotes = (LinearLayout) rootView.findViewById(R.id.pager_jsa_details_details_ll_notes);
        mTextViewJSANotes = (TextView) rootView.findViewById(R.id.pager_jsa_details_details_tv_notes);

        mLinearLayoutJsaName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditJsaNameDialog();
            }
        });

        mLinearLayoutJsaContactName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditJsaContactNameDialog();
            }
        });

        mLinearLayoutJsaContactEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditJsaContactEmailDialog();
            }
        });

        mLinearLayoutJsaContactPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditJsaContactPhoneDialog();
            }
        });

        mLinearLayoutJsaContactWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (jsaContactWebsite.equals("")) {
                    createEditJsaContactWebsiteDialog();
                } else {
                    createJsaContactWebsiteActionSelect();
                }
            }
        });

        mLinearLayoutJsaNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditJsaNotesDialog();
            }
        });

        mJsaRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                JobsiteAgency mJobsiteAgency = dataSnapshot.getValue(JobsiteAgency.class);

                if (mJobsiteAgency == null) {
                    /* No point in continuing without a valid ID. */
                    getActivity().finish();
                    return;
                }

                mJsaName = mJobsiteAgency.getJsaName();
                mJsaContactTitle = mJobsiteAgency.getJsaContactTitle();
                mJsaContactFirstName = mJobsiteAgency.getJsaContactFirstName();
                mJsaContactSurname = mJobsiteAgency.getJsaContactSurname();
                mJsaContactPosition = mJobsiteAgency.getJsaContactPosition();
                mJsaContactEmail = mJobsiteAgency.getJsaContactEmail();
                mJsaContactPhone = mJobsiteAgency.getJsaContactPhone();
                jsaContactWebsite = mJobsiteAgency.getJsaContactWebsite();
                mJsaNotes = mJobsiteAgency.getJsaNotes();

                mJsaLocationRemote = mJobsiteAgency.getJsaLocationRemote();
                mJsaLocationName = mJobsiteAgency.getJsaLocationName();
                mJsaLocationLat = mJobsiteAgency.getJsaLocationLat();
                mJsaLocationLong = mJobsiteAgency.getJsaLocationLong();

                if (mJsaName.equals("")) {
                    mTextViewJSAName.setText(getResources().getString(R.string.pager_jsa_details_details_tv_jsa_name_empty));
                    mTextViewJSAName.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                } else {
                    mTextViewJSAName.setText(mJsaName);
                }

                mTextViewJSAContactTitle.setText(mJsaContactTitle);
                mTextViewJSAContactFirstName.setText(mJsaContactFirstName);
                mTextViewJSAContactSurname.setText(mJsaContactSurname);

                if (mJsaContactPosition.equals("")) {
                    mTextViewJSAContactPosition.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_position_empty));
                    mTextViewJSAContactPosition.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                } else {
                    mTextViewJSAContactPosition.setText(mJsaContactPosition);
                }

                if (mJsaContactEmail.equals("")) {
                    mTextViewJSAContactEmail.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_email_empty));
                    mTextViewJSAContactEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                } else {
                    mTextViewJSAContactEmail.setText(mJsaContactEmail);
                }

                if (mJsaContactPhone.equals("")) {
                    mTextViewJSAContactPhone.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_phone_empty));
                    mTextViewJSAContactPhone.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                } else {
                    mTextViewJSAContactPhone.setText(mJsaContactPhone);
                }

                if (jsaContactWebsite.equals("")) {
                    mTextViewJSAContactWebsite.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_website_empty));
                    mTextViewJSAContactWebsite.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                } else {
                    mTextViewJSAContactWebsite.setText(jsaContactWebsite);
                }

                if (mJsaLocationName.equals("")) {
                    //If Location is empty
                    mTextViewJsaLocation.setText(R.string.jsa_details_tv_location_name_empty);
                    mTextViewJsaLocation.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));

                    mLinearLayoutJsaLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            createLocationAddEditDialog();
                        }
                    });

                } else if(mJsaLocationRemote) {
                    //If Location is remote
                    mTextViewJsaLocation.setText(mJsaLocationName);
                    mTextViewJsaLocation.setTextColor(ContextCompat.getColor(getActivity(), R.color.list_item_secondary_text));

                    mLinearLayoutJsaLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            createLocationAddEditDialog();
                        }
                    });
                } else {
                    //If Location isn't empty and isn't remote
                    mTextViewJsaLocation.setText(mJsaLocationName);
                    mTextViewJsaLocation.setTextColor(ContextCompat.getColor(getActivity(), R.color.list_item_secondary_text));

                    mLinearLayoutJsaLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            createLocationMapDialog();
                        }
                    });

                }

                if (mJsaNotes.equals("")) {
                    mTextViewJSANotes.setText(R.string.jsa_detail_jsa_notes_empty);
                    mTextViewJSANotes.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                } else {
                    mTextViewJSANotes.setText(mJsaNotes);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

        return rootView;

    }

    private void createLocationMapDialog() {

        final AlertDialog.Builder locationMapAlertDialog = new AlertDialog.Builder(getActivity(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_location_map, null);
        locationMapAlertDialog.setView(convertView);

        locationMapAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_map_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().getFragmentManager().beginTransaction().remove(mMapDialogFragmentLocation).commit();
                createLocationAddEditDialog();
                locationMapDialog.dismiss();
            }
        });
        locationMapAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_map_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().getFragmentManager().beginTransaction().remove(mMapDialogFragmentLocation).commit();
                locationMapDialog.dismiss();
            }
        });

        locationMapDialog = locationMapAlertDialog.show();
        locationMapDialog.setCancelable(false);
        locationMapDialog.setCanceledOnTouchOutside(false);

        mMapDialogFragmentLocation = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map_detail_activity));

        mMapDialogFragmentLocation.getMapAsync(this);

    }

    private void createLocationAddEditDialog() {

        final AlertDialog.Builder locationAddAlertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_location_edit, null);
        locationAddAlertDialog.setView(convertView);

        final CheckBox mCheckBoxNoLocation = (CheckBox) convertView.findViewById(R.id.dialog_location_edit_cb_no_location);
        final LinearLayout mLinearLayoutLocationUser = (LinearLayout) convertView.findViewById(R.id.dialog_location_edit_ll_location_user);
        final AppCompatImageView mImageViewLocationUser = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_edit_iv_location_user_icon);
        final AppCompatImageView mImageViewLocation = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_edit_iv_location_icon);

        mAutocompleteTextViewLocation = (AutoCompleteTextView) convertView.findViewById(R.id.dialog_location_edit_autocomplete_create_account);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_edit_progress_bar);

        locationAddAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (mCheckBoxNoLocation.isChecked()) {
                    mJsaLocationRemote = true;
                    mJsaLocationName = "Remote";
                    mJsaLocationLat = 0.0;
                    mJsaLocationLong = 0.0;
                    mTextViewJsaLocation.setText(mJsaLocationName);
                    mTextViewJsaLocation.setTextColor(ContextCompat.getColor(getActivity(), R.color.list_item_secondary_text));
                    setLocationEditData();
                } else {
                    if (!mJsaLocationName.equals("")) {
                        mTextViewJsaLocation.setText(mJsaLocationName);
                        mTextViewJsaLocation.setTextColor(ContextCompat.getColor(getActivity(), R.color.list_item_secondary_text));
                        setLocationEditData();
                    }
                }

                googleApiStop();
                locationAddEditDialog.dismiss();
            }
        });
        locationAddAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                googleApiStop();
                locationAddEditDialog.dismiss();
            }
        });

        locationAddAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_location_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mJsaLocationRemote = false;
                mJsaLocationName = "";
                mJsaLocationLat = 0.0;
                mJsaLocationLong = 0.0;
                setLocationEditData();
                googleApiStop();
                mTextViewJsaLocation.setText(getResources().getString(R.string.activity_job_application_new_tv_location));
                mTextViewJsaLocation.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_view_unused));
                locationAddEditDialog.dismiss();
            }
        });

        locationAddEditDialog = locationAddAlertDialog.show();
        locationAddEditDialog.setCancelable(false);
        locationAddEditDialog.setCanceledOnTouchOutside(false);

        //LOCATION API CODE
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiStart();
        mAutocompleteTextViewLocation = (AutoCompleteTextView) convertView.findViewById(R.id
                .dialog_location_edit_autocomplete_create_account);
        mAutocompleteTextViewLocation.setThreshold(3);
        mAutocompleteTextViewLocation.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), R.layout.list_item_location_autocomplete,
                BOUNDS_USER, null);
        mAutocompleteTextViewLocation.setAdapter(mPlaceArrayAdapter);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_edit_progress_bar);

        mCheckBoxNoLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mAutocompleteTextViewLocation.setEnabled(false);
                    mAutocompleteTextViewLocation.setHintTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mAutocompleteTextViewLocation.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mLinearLayoutLocationUser.setEnabled(false);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(ContextCompat.getColor(getContext(), R.color.text_view_unused), ContextCompat.getColor(getContext(), R.color.text_view_unused));
                    mAutocompleteTextViewLocation.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                } else {
                    mAutocompleteTextViewLocation.setEnabled(true);

                    mLinearLayoutLocationUser.setEnabled(true);
                    mAutocompleteTextViewLocation.setHintTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
                    mAutocompleteTextViewLocation.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(android.R.color.black, android.R.color.black);
                    mAutocompleteTextViewLocation.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                }
            }
        });

        if (mJsaLocationRemote) {
            mCheckBoxNoLocation.setChecked(true);
        } else {
            mCheckBoxNoLocation.setChecked(false);
        }

        mLinearLayoutLocationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION },
                            PERMISSION_ACCESS_COARSE_LOCATION);
                }

                if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    System.out.println("locationLat: " + lastLocation);

                    if (lastLocation != null) {
                        mJsaLocationLat = lastLocation.getLatitude();
                        mJsaLocationLong = lastLocation.getLongitude();

                        try {

                            Geocoder geo = new Geocoder(getContext(), Locale.getDefault());
                            List<Address> addresses = geo.getFromLocation(mJsaLocationLat, mJsaLocationLong, 1);
                            if (addresses.isEmpty()) {
                                Toast.makeText(getContext(), "Waiting for location", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                if (addresses.size() > 0) {

                                    if (addresses.get(0).getLocality() != null) {
                                        mJsaLocationName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
                                    } else {
                                        mJsaLocationName = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getCountryName();
                                    }

                                    Log.d(LOG_TAG, addresses.get(0).toString());

                                    mAutocompleteTextViewLocation.setText(mJsaLocationName);

                                }

                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace(); // getFromLocation() may sometimes fail
                        }
                    } else {
                        Toast.makeText(getContext(), "Unable to get current location", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        mAutocompleteTextViewLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAutocompleteProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mAutocompleteTextViewLocation.getText().toString().equals("")) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                }
            }
        });

        mAutocompleteTextViewLocation.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = mAutocompleteTextViewLocation.getRight()
                            - mAutocompleteTextViewLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        mAutocompleteTextViewLocation.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

    }

    private final AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            if (item != null) {
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Selected: " + item.description);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            }
        }
    };

    private final ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                mAutocompleteProgressBar.setVisibility(View.GONE);
                return;
            }

            // Selecting the first object buffer.
            final Place place = places.get(0);

            //Removes progress bar
            mAutocompleteProgressBar.setVisibility(View.GONE);

            String str = place.getLatLng().toString();
            String latlongRemoveStart = str.replace("lat/lng:", "");
            String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
            String[] parts = latlongRemoveEnd.split(",");
            mJsaLocationLat = Double.parseDouble(parts[0]);
            mJsaLocationLong = Double.parseDouble(parts[1]);

            mJsaLocationName = place.getName().toString();

            mJsaLocationRemote = false;

        }
    };

    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getContext(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    private void setLocationEditData() {

        HashMap<String, Object> updatedProperties = new HashMap<>();
        updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_LOCATION_REMOTE, mJsaLocationRemote);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_LOCATION_NAME, mJsaLocationName);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_LOCATION_LAT, mJsaLocationLat);
        updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_LOCATION_LONG, mJsaLocationLong);
        mJsaRef.updateChildren(updatedProperties);

    }

    @Override
    public void onStart() {
        googleApiStart();
        super.onStart();
    }

    @Override
    public void onStop() {
        googleApiStop();
        super.onStop();
    }

    private void googleApiStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    private void googleApiStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {

        mMap = map;

        if (mMapDialogFragmentLocation == null) {
            Toast.makeText(getActivity(), "Sorry! unable to create Map.", Toast.LENGTH_SHORT).show();
        } else {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mapSettings = mMap.getUiSettings();
            mapSettings.setZoomControlsEnabled(true);
        }

        LatLng jobApplicationLatLong = new LatLng(mJsaLocationLat, mJsaLocationLong);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(jobApplicationLatLong, 10));
        mMap.addMarker(new MarkerOptions()
                .position(jobApplicationLatLong)
                .title("Job Location")
                .snippet(mJsaName + " at " + mJsaLocationName));


    }

    private void createEditJsaNameDialog(){

        final AlertDialog.Builder editJsaNameDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_jsa_name, null);
        editJsaNameDialog.setView(editView);
        final EditText mEditTextJsaName = (EditText) editView.findViewById(R.id.dialog_edit_jsa_name_et_title);

        mEditTextJsaName.setText(mJsaName);

        mEditTextJsaName.setSelection(mEditTextJsaName.getText().length());

        editJsaNameDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mJsaName = mEditTextJsaName.getText().toString().trim();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_NAME, mJsaName);
                mJsaRef.updateChildren(updatedProperties);

                if (mJsaName.equals("")) {
                    mTextViewJSAName.setText(getResources().getString(R.string.pager_jsa_details_details_tv_jsa_name_empty));
                    mTextViewJSAName.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                } else {
                    mTextViewJSAName.setText(mJsaName);
                    mTextViewJSAName.setTextColor(ContextCompat.getColor(getContext(), R.color.list_item_secondary_text));
                }

                mEditJsaNameDialog.dismiss();

            }
        });
        editJsaNameDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditJsaNameDialog.dismiss();
            }
        });

        editJsaNameDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_NAME, "");
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSAName.setText(getResources().getString(R.string.pager_jsa_details_details_tv_jsa_name_empty));
                mTextViewJSAName.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                mEditJsaNameDialog.dismiss();
            }
        });

        mEditJsaNameDialog = editJsaNameDialog.show();

        if (mEditJsaNameDialog.getWindow() != null) {
            mEditJsaNameDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createEditJsaContactNameDialog(){

        final AlertDialog.Builder editJsaContactNameDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_contact_name_position, null);
        editJsaContactNameDialog.setView(editView);
        final EditText mEditTextContactTitle = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_title);
        final EditText mEditTextContactFirstName = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_first_name);
        final EditText mEditTextContactSurname = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_surname);
        final EditText mEditTextContactPosition = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_position);

        mEditTextContactTitle.setText(mJsaContactTitle);
        mEditTextContactFirstName.setText(mJsaContactFirstName);
        mEditTextContactSurname.setText(mJsaContactSurname);
        mEditTextContactPosition.setText(mJsaContactPosition);

        mEditTextContactTitle.setSelection(mEditTextContactTitle.getText().length());
        mEditTextContactFirstName.setSelection(mEditTextContactFirstName.getText().length());
        mEditTextContactSurname.setSelection(mEditTextContactSurname.getText().length());
        mEditTextContactPosition.setSelection(mEditTextContactPosition.getText().length());

        editJsaContactNameDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mJsaContactTitle = mEditTextContactTitle.getText().toString().trim();
                mJsaContactFirstName = mEditTextContactFirstName.getText().toString().trim();
                mJsaContactSurname = mEditTextContactSurname.getText().toString().trim();
                mJsaContactPosition = mEditTextContactPosition.getText().toString().trim();

                /* Add properties changed to a HashMap */
                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_TITLE, mJsaContactTitle);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_FIRST_NAME, mJsaContactFirstName);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_SURNAME, mJsaContactSurname);
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_POSITION, mJsaContactPosition);
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSAContactTitle.setText(mJsaContactTitle);
                mTextViewJSAContactFirstName.setText(mJsaContactFirstName);
                mTextViewJSAContactSurname.setText(mJsaContactSurname);

                if (mJsaContactPosition.equals("")) {
                    mTextViewJSAContactPosition.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_position_empty));
                    mTextViewJSAContactPosition.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));
                } else {
                    mTextViewJSAContactPosition.setText(mJsaContactPosition);
                    mTextViewJSAContactPosition.setTextColor(ContextCompat.getColor(getContext(), R.color.list_item_secondary_text));
                }

                mEditJsaContactNameDialog.dismiss();

            }
        });
        editJsaContactNameDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditJsaContactNameDialog.dismiss();
            }
        });

        editJsaContactNameDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_TITLE, "");
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_FIRST_NAME, "");
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_SURNAME, "");
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_POSITION, "");
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSAContactTitle.setText(mJsaContactTitle);
                mTextViewJSAContactFirstName.setText(mJsaContactFirstName);
                mTextViewJSAContactSurname.setText(mJsaContactSurname);
                mTextViewJSAContactPosition.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_position_empty));
                mTextViewJSAContactPosition.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                mEditJsaContactNameDialog.dismiss();
            }
        });

        mEditJsaContactNameDialog = editJsaContactNameDialog.show();

        if (mEditJsaContactNameDialog.getWindow() != null) {
            mEditJsaContactNameDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createEditJsaContactEmailDialog(){

        final AlertDialog.Builder editJsaContactEmailDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_contact_email, null);
        editJsaContactEmailDialog.setView(editView);
        final EditText mEditTextContactEmail = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_email);

        mEditTextContactEmail.setText(mJsaContactEmail);

        mEditTextContactEmail.setSelection(mEditTextContactEmail.getText().length());

        editJsaContactEmailDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mJsaContactEmail = mEditTextContactEmail.getText().toString().trim();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_EMAIL, mJsaContactEmail);
                mJsaRef.updateChildren(updatedProperties);

                if (mJsaContactEmail.equals("")) {
                    mTextViewJSAContactEmail.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_email_empty));
                    mTextViewJSAContactEmail.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                } else {
                    mTextViewJSAContactEmail.setText(mJsaContactEmail);
                    mTextViewJSAContactEmail.setTextColor(ContextCompat.getColor(getContext(), R.color.list_item_secondary_text));
                }

                mEditJsaContactEmailDialog.dismiss();

            }
        });
        editJsaContactEmailDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditJsaContactEmailDialog.dismiss();
            }
        });

        editJsaContactEmailDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_EMAIL, "");
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSAContactEmail.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_email_empty));
                mTextViewJSAContactEmail.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                mEditJsaContactEmailDialog.dismiss();
            }
        });

        mEditJsaContactEmailDialog = editJsaContactEmailDialog.show();

        if (mEditJsaContactEmailDialog.getWindow() != null) {
            mEditJsaContactEmailDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createEditJsaContactPhoneDialog(){

        final AlertDialog.Builder editJsaContactPhoneDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_contact_phone, null);
        editJsaContactPhoneDialog.setView(editView);
        final EditText mEditTextContactPhone = (EditText) editView.findViewById(R.id.dialog_edit_contact_name_et_phone);

        mEditTextContactPhone.setText(mJsaContactPhone);

        mEditTextContactPhone.setSelection(mEditTextContactPhone.getText().length());

        editJsaContactPhoneDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mJsaContactPhone = mEditTextContactPhone.getText().toString().trim();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_PHONE, mJsaContactPhone);
                mJsaRef.updateChildren(updatedProperties);

                if (mJsaContactPhone.equals("")) {
                    mTextViewJSAContactPhone.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_phone_empty));
                    mTextViewJSAContactPhone.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                } else {
                    mTextViewJSAContactPhone.setText(mJsaContactPhone);
                    mTextViewJSAContactPhone.setTextColor(ContextCompat.getColor(getContext(), R.color.list_item_secondary_text));
                }

                mEditJsaContactPhoneDialog.dismiss();

            }
        });
        editJsaContactPhoneDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditJsaContactPhoneDialog.dismiss();
            }
        });

        editJsaContactPhoneDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_PHONE, "");
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSAContactPhone.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_phone_empty));
                mTextViewJSAContactPhone.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                mEditJsaContactPhoneDialog.dismiss();
            }
        });

        mEditJsaContactPhoneDialog = editJsaContactPhoneDialog.show();

        if (mEditJsaContactPhoneDialog.getWindow() != null) {
            mEditJsaContactPhoneDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createEditJsaContactWebsiteDialog(){

        final AlertDialog.Builder editJsaContactWebsiteDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_contact_website, null);
        editJsaContactWebsiteDialog.setView(editView);
        final EditText mEditTextContactWebsite = (EditText) editView.findViewById(R.id.dialog_edit_contact_website_et);

        mEditTextContactWebsite.setText(jsaContactWebsite);

        mEditTextContactWebsite.setSelection(mEditTextContactWebsite.getText().length());

        editJsaContactWebsiteDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                jsaContactWebsite = mEditTextContactWebsite.getText().toString().trim();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_WEBSITE, jsaContactWebsite);
                mJsaRef.updateChildren(updatedProperties);

                if (jsaContactWebsite.equals("")) {
                    mTextViewJSAContactWebsite.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_website_empty));
                    mTextViewJSAContactWebsite.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                } else {
                    mTextViewJSAContactWebsite.setText(jsaContactWebsite);
                    mTextViewJSAContactWebsite.setTextColor(ContextCompat.getColor(getContext(), R.color.list_item_secondary_text));
                }

                mEditJsaContactWebsiteDialog.dismiss();

            }
        });
        editJsaContactWebsiteDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditJsaContactWebsiteDialog.dismiss();
            }
        });

        editJsaContactWebsiteDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_CONTACT_WEBSITE, "");
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSAContactWebsite.setText(getResources().getString(R.string.pager_jsa_details_details_tv_contact_website_empty));
                mTextViewJSAContactWebsite.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                mEditJsaContactWebsiteDialog.dismiss();
            }
        });

        mEditJsaContactWebsiteDialog = editJsaContactWebsiteDialog.show();

        if (mEditJsaContactWebsiteDialog.getWindow() != null) {
            mEditJsaContactWebsiteDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void createJsaContactWebsiteActionSelect(){

        final AlertDialog.Builder jobAdvertUrlActionDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_job_advert_action, null);
        jobAdvertUrlActionDialog.setView(dialogView);

        TextView mTextViewJobAdvertUrlGoTo = (TextView) dialogView.findViewById(R.id.dialog_job_advert_action_tv_go_to);
        TextView mTextViewJobAdvertUrlEdit = (TextView) dialogView.findViewById(R.id.dialog_job_advert_action_tv_edit_url);

        mTextViewJobAdvertUrlGoTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!jsaContactWebsite.startsWith("http://") && !jsaContactWebsite.startsWith("https://"))
                    jsaContactWebsite = "http://" + jsaContactWebsite;

                mDialogJsaContactWebsiteAction.dismiss();

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(jsaContactWebsite));
                startActivity(browserIntent);

            }
        });

        mTextViewJobAdvertUrlEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEditJsaContactWebsiteDialog();
                mDialogJsaContactWebsiteAction.dismiss();
            }
        });

        mDialogJsaContactWebsiteAction = jobAdvertUrlActionDialog.show();
    }

    private void createEditJsaNotesDialog(){

        final AlertDialog.Builder editJsaNotesDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View editView = View.inflate(getContext(), R.layout.dialog_edit_notes, null);
        editJsaNotesDialog.setView(editView);
        final EditText mEditTextNotes = (EditText) editView.findViewById(R.id.dialog_edit_notes_et);

        mEditTextNotes.setText(mJsaNotes);

        mEditTextNotes.setSelection(mEditTextNotes.getText().length());

        editJsaNotesDialog.setPositiveButton(getResources().getString(R.string.dialog_application_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mJsaNotes = mEditTextNotes.getText().toString().trim();

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_NOTES, mJsaNotes);
                mJsaRef.updateChildren(updatedProperties);

                if (mJsaNotes.equals("")) {
                    mTextViewJSANotes.setText(getResources().getString(R.string.pager_jsa_details_details_tv_notes_empty));
                    mTextViewJSANotes.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                } else {
                    mTextViewJSANotes.setText(mJsaNotes);
                    mTextViewJSANotes.setTextColor(ContextCompat.getColor(getContext(), R.color.list_item_secondary_text));
                }

                mEditJsaNotesDialog.dismiss();

            }
        });
        editJsaNotesDialog.setNegativeButton(getResources().getString(R.string.dialog_application_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEditJsaNotesDialog.dismiss();
            }
        });

        editJsaNotesDialog.setNeutralButton(getResources().getString(R.string.dialog_application_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HashMap<String, Object> updatedProperties = new HashMap<>();
                updatedProperties.put(Constants.FIREBASE_PROPERTY_JSA_NOTES, "");
                mJsaRef.updateChildren(updatedProperties);

                mTextViewJSANotes.setText(getResources().getString(R.string.pager_jsa_details_details_tv_notes_empty));
                mTextViewJSANotes.setTextColor(ContextCompat.getColor(getContext(), R.color.text_view_unused));

                mEditJsaNotesDialog.dismiss();
            }
        });

        mEditJsaNotesDialog = editJsaNotesDialog.show();

        if (mEditJsaNotesDialog.getWindow() != null) {
            mEditJsaNotesDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

}
