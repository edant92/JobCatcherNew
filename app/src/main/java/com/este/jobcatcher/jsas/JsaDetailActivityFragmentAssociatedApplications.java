package com.este.jobcatcher.jsas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jsas.recyclerviewholders.BasicJobApplicationListItemHolder;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Displays a list of Job Applications associated to a Jobsite/Agency
 */
public class JsaDetailActivityFragmentAssociatedApplications extends Fragment {

    private DatabaseReference mFirebaseRef;
    private DatabaseReference mJobApplicationsListRef;
    private DatabaseReference mJobApplicationsListPriorityRef;
    private DatabaseReference mJobApplicationsListArchiveRef;

    private String mJSAId;

    private SharedPreferences mSharedPref;
    private String userUID;

    private RecyclerView mRecyclerViewJobApplicationsList;
    private RecyclerView mRecyclerViewJobApplicationsListPriority;
    private RecyclerView mRecyclerViewJobApplicationsListArchive;
    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecycleViewAdapter;
    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecycleViewPriorityAdapter;
    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecycleViewArchiveAdapter;

    private Query queryRef;
    private Query queryRefPriority;
    private Query queryRefArchive;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        JsaDetailActivity activity = (JsaDetailActivity) getActivity();
        Intent b = activity.getIntent();
        mJSAId = b.getStringExtra(Constants.INTENT_EXTRA_JSA_ID);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        //Create Firebase references
        mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);
        mJobApplicationsListRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mJobApplicationsListPriorityRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);
        mJobApplicationsListArchiveRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE);

        View rootView = inflater.inflate(R.layout.pager_jsa_details_associated_applications, container, false);

        mRecyclerViewJobApplicationsList = (RecyclerView) rootView.findViewById(R.id.pager_jsa_details_associated_applications_rv_list);
        mRecyclerViewJobApplicationsListPriority = (RecyclerView) rootView.findViewById(R.id.pager_jsa_details_associated_applications_rv_list_priority);
        mRecyclerViewJobApplicationsListArchive = (RecyclerView) rootView.findViewById(R.id.pager_jsa_details_associated_applications_rv_list_archive);

        queryRef = mJobApplicationsListRef.orderByChild(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).equalTo(mJSAId);
        queryRefPriority = mJobApplicationsListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).equalTo(mJSAId);
        queryRefArchive = mJobApplicationsListArchiveRef.orderByChild(Constants.FIREBASE_PROPERTY_ASSOCIATED_JSA_ID).equalTo(mJSAId);

        setUpJobApplicationListRecyclerView();
        setUpJobApplicationListPriorityRecyclerView();
        setUpJobApplicationListArchiveRecyclerView();

        return rootView;

    }

    private void setUpJobApplicationListRecyclerView() {

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setReverseLayout(false);
        manager.setStackFromEnd(false);

        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewJobApplicationsList.setHasFixedSize(false);
        mRecyclerViewJobApplicationsList.setLayoutManager(manager);

        mRecycleViewAdapter = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryRef) {
            @Override
            public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationView, JobApplication jobApplication, int position) {

                jobApplicationView.getPinned(false);
                jobApplicationView.getMainListItemKey(mRecycleViewAdapter.getRef(position).getKey());
                jobApplicationView.setJobTitle(jobApplication.getJobTitle());
                jobApplicationView.setCompanyName(jobApplication.getCompanyName());

            }
        };

        mRecyclerViewJobApplicationsList.setAdapter(mRecycleViewAdapter);

    }

    private void setUpJobApplicationListPriorityRecyclerView() {

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setReverseLayout(false);
        manager.setStackFromEnd(false);

        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewJobApplicationsListPriority.setHasFixedSize(false);
        mRecyclerViewJobApplicationsListPriority.setLayoutManager(manager);

        mRecycleViewPriorityAdapter = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryRefPriority) {
            @Override
            public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationView, JobApplication jobApplication, int position) {

                jobApplicationView.getPinned(true);
                jobApplicationView.getMainListItemKey(mRecycleViewPriorityAdapter.getRef(position).getKey());
                jobApplicationView.setJobTitle(jobApplication.getJobTitle());
                jobApplicationView.setCompanyName(jobApplication.getCompanyName());

            }
        };

        mRecyclerViewJobApplicationsListPriority.setAdapter(mRecycleViewPriorityAdapter);

    }

    private void setUpJobApplicationListArchiveRecyclerView() {

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setReverseLayout(false);
        manager.setStackFromEnd(false);

        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewJobApplicationsListArchive.setHasFixedSize(false);
        mRecyclerViewJobApplicationsListArchive.setLayoutManager(manager);

        mRecycleViewArchiveAdapter = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryRefArchive) {
            @Override
            public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationView, JobApplication jobApplication, int position) {
                jobApplicationView.getPinned(false);
                jobApplicationView.getMainListItemKey(mRecycleViewArchiveAdapter.getRef(position).getKey());
                jobApplicationView.setJobTitle(jobApplication.getJobTitle());
                jobApplicationView.setCompanyName(jobApplication.getCompanyName());

            }
        };

        mRecyclerViewJobApplicationsListArchive.setAdapter(mRecycleViewArchiveAdapter);

    }

}
