package com.este.jobcatcher.jsas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jsas.recyclerviewholders.JsaListItemHolder;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

//import com.firebase.client.Firebase;

public class JsaListActivity extends AppCompatActivity {

    private DatabaseReference mJsaListRef;
    private Query mJsaOrderedListQuery;

    private RecyclerView mRecyclerViewJsas;
    private FirebaseRecyclerAdapter<JobsiteAgency, JsaListItemHolder> mRecycleViewAdapter;

    private LinearLayout mLinearLayoutJsaEmptyState;
    private LinearLayout mLinearLayoutJsa;
    private FloatingActionButton mFabNewJsa;

    private SharedPreferences mSharedPref;
    private String userUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsa_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_jsa_list_toolbar);
        setSupportActionBar(toolbar);

        /* Add back button to the action bar */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mLinearLayoutJsaEmptyState = (LinearLayout) findViewById(R.id.activity_jsa_list_ll_empty_state);
        mLinearLayoutJsa = (LinearLayout) findViewById(R.id.activity_jsa_list_ll);

        //Create Firebase references
        mJsaListRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);
        mJsaListRef.keepSynced(true);

        mJsaOrderedListQuery = mJsaListRef.orderByChild(Constants.FIREBASE_PROPERTY_JSA_NAME);

        mFabNewJsa = (FloatingActionButton) findViewById(R.id.activity_jsa_list_fab);
        mFabNewJsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JsaListActivity.this, JsaNewActivity.class);
                startActivity(intent);
            }
        });

        mJsaListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    mLinearLayoutJsaEmptyState.setVisibility(View.VISIBLE);
                    mLinearLayoutJsa.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mRecyclerViewJsas = (RecyclerView) findViewById(R.id.activity_jsa_list_rv);

        setUpJsaRecyclerView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpJsaRecyclerView() {

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(false);
        manager.setStackFromEnd(false);
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerViewJsas.setHasFixedSize(false);
        mRecyclerViewJsas.setLayoutManager(manager);

        mRecycleViewAdapter = new FirebaseRecyclerAdapter<JobsiteAgency, JsaListItemHolder>(JobsiteAgency.class, R.layout.list_item_jsa, JsaListItemHolder.class, mJsaOrderedListQuery) {
            @Override
            public void populateViewHolder(JsaListItemHolder jsaView, JobsiteAgency jsa, int position) {

                jsaView.getJsaListItemSelected(mRecycleViewAdapter.getItem(position));
                jsaView.getJsaListItemKey(mRecycleViewAdapter.getRef(position).getKey());
                jsaView.setJsaListItemJsaName(jsa.getJsaName());
                jsaView.setJsaListItemJsaLocation(jsa.getJsaLocationName());

            }
        };

        mRecyclerViewJsas.setAdapter(mRecycleViewAdapter);

    }

}
