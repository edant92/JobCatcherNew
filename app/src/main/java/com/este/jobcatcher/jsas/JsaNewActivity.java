package com.este.jobcatcher.jsas;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.jobapplications.JobApplicationNewActivity;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.models.User;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

/**
 * Allows user to create a new JSA
 */
public class JsaNewActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private SharedPreferences mSharedPref;
    private String userUID;

    private DatabaseReference mUserSettingsRef;
    private User mUserSettings;
    private DatabaseReference mNewJsaRef;
    private DatabaseReference mNewJsaPushRef;

    private EditText mEditTextJSAName;
    private EditText mEditTextJSAContactTitle;
    private EditText mEditTextJSAContactFirstName;
    private EditText mEditTextJSAContactSurname;
    private EditText mEditTextJSAContactPosition;
    private LinearLayout mLinearLayoutJsaLocation;
    private TextView mTextViewJsaLocation;
    private EditText mEditTextJSAContactEmail;
    private EditText mEditTextJSAContactPhone;
    private EditText mEditTextJSAContactWebsite;
    private EditText mEditTextJSANotes;

    private String jsaName;
    private String jsaContactTitle;
    private String jsaContactFirstName;
    private String jsaContactSurname;
    private String jsaContactPosition;
    private String jsaContactEmail;
    private String jsaContactPhone;
    private String jsaContactWebsite;
    private String jsaNotes;

    private Boolean jsaLocationRemote;
    private String jsaLocationName;
    private Double jsaLocationLat;
    private Double jsaLocationLong;

    private Double defaultUserLat;
    private Double defaultUserLong;

    private AlertDialog locationAddEditDialog;
    private AutoCompleteTextView mAutocompleteTextView;
    private ProgressBar mAutocompleteProgressBar;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLngBounds BOUNDS_USER;
    private static final String LOG_TAG = "JsaNewActivity";
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsa_new);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_jsa_new_app_bar);
        /* Common toolbar setup */
        setSupportActionBar(toolbar);
        /* Add back button to the action bar */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mUserSettingsRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_USERS).child(userUID);
        mNewJsaRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES).child(userUID);
        mNewJsaPushRef = mNewJsaRef.push();

        //Defaults
        defaultUserLat = 0.0;
        defaultUserLong = 0.0;

        jsaName = "";
        jsaContactTitle = "";
        jsaContactFirstName = "";
        jsaContactSurname = "";
        jsaContactPosition = "";
        jsaContactEmail = "";
        jsaContactPhone = "";
        jsaContactWebsite = "";
        jsaNotes = "";
        jsaLocationRemote = false;
        jsaLocationName = "";
        jsaLocationLat = 0.0;
        jsaLocationLong = 0.0;

        mEditTextJSAName = (EditText) findViewById(R.id.activity_jsa_new_et_jsa_name);
        mEditTextJSAContactTitle = (EditText) findViewById(R.id.activity_jsa_new_et_contact_title);
        mEditTextJSAContactFirstName = (EditText) findViewById(R.id.activity_jsa_new_et_contact_first_name);
        mEditTextJSAContactSurname = (EditText) findViewById(R.id.activity_jsa_new_et_contact_surname);
        mEditTextJSAContactPosition = (EditText) findViewById(R.id.activity_jsa_new_et_contact_position);
        mLinearLayoutJsaLocation = (LinearLayout) findViewById(R.id.activity_jsa_new_ll_location);
        mTextViewJsaLocation = (TextView) findViewById(R.id.activity_jsa_new_tv_location);
        mEditTextJSAContactEmail = (EditText) findViewById(R.id.activity_jsa_new_et_contact_email);
        mEditTextJSAContactPhone = (EditText) findViewById(R.id.activity_jsa_new_et_contact_phone);
        mEditTextJSAContactWebsite = (EditText) findViewById(R.id.activity_jsa_new_et_contact_website);
        mEditTextJSANotes = (EditText) findViewById(R.id.activity_jsa_new_et_notes);

        mLinearLayoutJsaLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createLocationAddEditDialog();
            }
        });


        mUserSettingsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUserSettings = dataSnapshot.getValue(User.class);

                if (mUserSettings.getUserLocationLat() != null || mUserSettings.getUserLocationLat() != 0.0) {
                    defaultUserLat = mUserSettings.getUserLocationLat();
                } else {
                    defaultUserLat = 0.0;
                }

                if (mUserSettings.getUserLocationLong() != null || mUserSettings.getUserLocationLong() != 0.0) {
                    defaultUserLong = mUserSettings.getUserLocationLong();
                } else {
                    defaultUserLong = 0.0;
                }

                BOUNDS_USER = new LatLngBounds(
                        new LatLng(defaultUserLat, defaultUserLong), new LatLng(defaultUserLat, defaultUserLong));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                databaseError.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present. */
        getMenuInflater().inflate(R.menu.menu_jsa_new_activity, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_submit_new_jsa) {

            addJSA();

            Intent intent = this.getIntent();
            String fromActivity = intent.getStringExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY);

            if (fromActivity.equals(JobApplicationDetailActivity.class.getSimpleName())) {
                Intent returnToJobApplicationDetailActivity = new Intent(JsaNewActivity.this, JobApplicationDetailActivity.class);
                returnToJobApplicationDetailActivity.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, intent.getStringExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID));
                returnToJobApplicationDetailActivity.putExtra(Constants.INTENT_EXTRA_PINNED, intent.getStringExtra(Constants.INTENT_EXTRA_PINNED));
                returnToJobApplicationDetailActivity.putExtra(Constants.INTENT_EXTRA_JSA_ID, mNewJsaPushRef.getKey());
                returnToJobApplicationDetailActivity.putExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY, JsaNewActivity.class.getSimpleName());
                JsaNewActivity.this.startActivity(returnToJobApplicationDetailActivity);
            } else if (fromActivity.equals(JobApplicationNewActivity.class.getSimpleName())) {
                Intent returnToJobApplicationNewActivity = new Intent(JsaNewActivity.this, JobApplicationNewActivity.class);
                returnToJobApplicationNewActivity.putExtra(Constants.INTENT_EXTRA_JSA_ID, mNewJsaPushRef.getKey());
                returnToJobApplicationNewActivity.putExtra(Constants.INTENT_EXTRA_FROM_ACTIVITY, JsaNewActivity.class.getSimpleName());

                JsaNewActivity.this.startActivity(returnToJobApplicationNewActivity);
            } else {
                Intent goToListActivity = new Intent(JsaNewActivity.this, JsaListActivity.class);
                JsaNewActivity.this.startActivity(goToListActivity);
            }

            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Add new job application
     */
    private void addJSA() {

        jsaName = mEditTextJSAName.getText().toString().trim();
        jsaContactTitle = mEditTextJSAContactTitle.getText().toString().trim();
        jsaContactFirstName = mEditTextJSAContactFirstName.getText().toString().trim();
        jsaContactSurname = mEditTextJSAContactSurname.getText().toString().trim();
        jsaContactPosition = mEditTextJSAContactPosition.getText().toString().trim();
        jsaContactEmail = mEditTextJSAContactEmail.getText().toString().trim();
        jsaContactPhone = mEditTextJSAContactPhone.getText().toString().trim();
        jsaContactWebsite = mEditTextJSAContactWebsite.getText().toString().trim();
        jsaNotes = mEditTextJSANotes.getText().toString().trim();

        JobsiteAgency newJobsiteAgency = new JobsiteAgency(jsaName, jsaContactTitle, jsaContactFirstName, jsaContactSurname, jsaContactPosition, jsaContactEmail, jsaContactPhone, jsaContactWebsite, jsaNotes, jsaLocationRemote, jsaLocationName, jsaLocationLat, jsaLocationLong);

        /* Add the jsa to database */
        mNewJsaPushRef.setValue(newJobsiteAgency);



    }

    private final AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            if (item != null) {
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Selected: " + item.description);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            }
        }
    };

    private final ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                mAutocompleteProgressBar.setVisibility(View.GONE);
                return;
            }

            // Selecting the first object buffer.
            final Place place = places.get(0);

            //Removes progress bar
            mAutocompleteProgressBar.setVisibility(View.GONE);

            //Moves text cursor to start of EditText
            mAutocompleteTextView.setSelection(0);

            String str = place.getLatLng().toString();
            String latlongRemoveStart = str.replace("lat/lng:", "");
            String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
            String[] parts = latlongRemoveEnd.split(",");
            jsaLocationLat = Double.parseDouble(parts[0]);
            jsaLocationLong = Double.parseDouble(parts[1]);

            jsaLocationName = place.getName().toString();

            jsaLocationRemote = false;
        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private void createLocationAddEditDialog() {

        final AlertDialog.Builder locationAddAlertDialog = new AlertDialog.Builder(JsaNewActivity.this, R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(this, R.layout.dialog_location_edit, null);
        locationAddAlertDialog.setView(convertView);

        final CheckBox mCheckBoxNoLocation = (CheckBox) convertView.findViewById(R.id.dialog_location_edit_cb_no_location);
        final LinearLayout mLinearLayoutLocationUser = (LinearLayout) convertView.findViewById(R.id.dialog_location_edit_ll_location_user);
        final AppCompatImageView mImageViewLocationUser = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_edit_iv_location_user_icon);
        final AppCompatImageView mImageViewLocation = (AppCompatImageView) convertView.findViewById(R.id.dialog_location_edit_iv_location_icon);

        locationAddAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mCheckBoxNoLocation.isChecked()) {
                    jsaLocationRemote = true;
                    jsaLocationName = "Remote";
                    jsaLocationLat = 0.0;
                    jsaLocationLong = 0.0;
                    mTextViewJsaLocation.setText(jsaLocationName);
                    mTextViewJsaLocation.setTypeface(null, Typeface.NORMAL);
                } else {
                    if (jsaLocationName != null) {
                        mTextViewJsaLocation.setText(jsaLocationName);
                        mTextViewJsaLocation.setTypeface(null, Typeface.NORMAL);
                    }
                }
                onStop();
                locationAddEditDialog.dismiss();
            }
        });
        locationAddAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onStop();
                locationAddEditDialog.dismiss();
            }
        });

        locationAddAlertDialog.setNeutralButton(getResources().getString(R.string.dialog_location_edit_neutral_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onStop();
                jsaLocationRemote = false;
                jsaLocationName = "";
                jsaLocationLat = 0.0;
                jsaLocationLong = 0.0;
                mTextViewJsaLocation.setText(getResources().getString(R.string.activity_job_application_new_tv_location));
                mTextViewJsaLocation.setTypeface(null, Typeface.ITALIC);
                locationAddEditDialog.dismiss();
            }
        });

        locationAddEditDialog = locationAddAlertDialog.show();
        locationAddEditDialog.setCancelable(false);
        locationAddEditDialog.setCanceledOnTouchOutside(false);

        //LOCATION API CODE
        mGoogleApiClient = new GoogleApiClient.Builder(JsaNewActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        onStart();
        mAutocompleteTextView = (AutoCompleteTextView) convertView.findViewById(R.id
                .dialog_location_edit_autocomplete_create_account);
        mAutocompleteTextView.setThreshold(3);
        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(this, R.layout.list_item_location_autocomplete,
                BOUNDS_USER, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);
        mAutocompleteProgressBar = (ProgressBar) convertView.findViewById(R.id.dialog_location_edit_progress_bar);

        mCheckBoxNoLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mAutocompleteTextView.setEnabled(false);
                    mAutocompleteTextView.setHintTextColor(ContextCompat.getColor(JsaNewActivity.this, R.color.text_view_unused));
                    mAutocompleteTextView.setTextColor(ContextCompat.getColor(JsaNewActivity.this, R.color.text_view_unused));
                    mLinearLayoutLocationUser.setEnabled(false);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(JsaNewActivity.this, R.color.text_view_unused));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(JsaNewActivity.this, R.color.text_view_unused));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(ContextCompat.getColor(JsaNewActivity.this, R.color.text_view_unused), ContextCompat.getColor(JsaNewActivity.this, R.color.text_view_unused));
                    mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                } else {
                    mAutocompleteTextView.setEnabled(true);
                    mAutocompleteTextView.setHintTextColor(ContextCompat.getColor(JsaNewActivity.this, R.color.colorAccent));
                    mLinearLayoutLocationUser.setEnabled(true);
                    mImageViewLocationUser.setColorFilter(ContextCompat.getColor(JsaNewActivity.this, android.R.color.black));
                    mImageViewLocation.setColorFilter(ContextCompat.getColor(JsaNewActivity.this, android.R.color.black));
                    final int DRAWABLE_RIGHT = 2;
                    ColorFilter filter = new LightingColorFilter(android.R.color.black, android.R.color.black);
                    mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].setColorFilter(filter);
                }
            }
        });

        if (jsaLocationRemote) {
            mCheckBoxNoLocation.setChecked(true);
        } else {
            mCheckBoxNoLocation.setChecked(false);
        }

        mLinearLayoutLocationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(JsaNewActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(JsaNewActivity.this, new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION },
                            PERMISSION_ACCESS_COARSE_LOCATION);
                }

                if (ContextCompat.checkSelfPermission(JsaNewActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    System.out.println("locationLat: " + lastLocation);

                    jsaLocationLat = lastLocation.getLatitude();
                    jsaLocationLong = lastLocation.getLongitude();

                    try {

                        Geocoder geo = new Geocoder(JsaNewActivity.this, Locale.getDefault());
                        List<Address> addresses = geo.getFromLocation(jsaLocationLat, jsaLocationLong, 1);
                        if (addresses.isEmpty()) {
                            Toast.makeText(JsaNewActivity.this, "Waiting for location", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            if (addresses.size() > 0) {
                                if (addresses.get(0).getLocality() != null) {
                                    jsaLocationName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
                                } else {
                                    jsaLocationName = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getCountryName();
                                }

                                Log.d(LOG_TAG, addresses.get(0).toString());

                                mAutocompleteTextView.setText(jsaLocationName);

                            }

                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace(); // getFromLocation() may sometimes fail
                    }

                }
            }
        });

        mAutocompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAutocompleteProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mAutocompleteTextView.getText().toString().equals("")) {
                    mAutocompleteProgressBar.setVisibility(View.GONE);
                }
            }
        });

        mAutocompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = mAutocompleteTextView.getRight()
                            - mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        mAutocompleteTextView.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

    }

}
