package com.este.jobcatcher.jsas.recyclerviewholders;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.utils.Constants;

public class CompareListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private String mMainListItemKey;
    private Boolean isPinned;

    public CompareListItemHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

    }

    public void getMainListItemKey(String key) {
        mMainListItemKey = key;
    }

    public void getPinned(Boolean pinned) {
        isPinned = pinned;
    }

    @Override
    public void onClick(View v) {
        if (mMainListItemKey != null) {
                    /* Get the list ID using the adapter's get ref method to get the Firebase
                     * ref and then grab the key.
                     */

            Intent newIntent = new Intent(Constants.INTENT_EXTRA_ASSOCIATED_JSA);
            newIntent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mMainListItemKey);
            newIntent.putExtra(Constants.INTENT_EXTRA_PINNED, isPinned);
            LocalBroadcastManager.getInstance(itemView.getContext()).sendBroadcast(newIntent);

        }
    }

    public void setJobTitle(String jobTitle) {
        TextView textViewJobTitle = (TextView) itemView.findViewById(R.id.list_item_basic_job_applicationtv_job_title);

        if (jobTitle.equals("")) {
            textViewJobTitle.setText(R.string.list_item_job_application_tv_job_title);
            textViewJobTitle.setTypeface(null, Typeface.ITALIC);
            textViewJobTitle.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
        } else {
            textViewJobTitle.setText(jobTitle);
        }

    }

    public void setCompanyName(String companyName) {
        TextView textViewCompanyName = (TextView) itemView.findViewById(R.id.list_item_basic_job_application_tv_company_name);

        if (companyName.equals("")) {
            textViewCompanyName.setText(R.string.list_item_job_application_tv_company_name);
            textViewCompanyName.setTypeface(null, Typeface.ITALIC);
            textViewCompanyName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
        } else {
            textViewCompanyName.setText(companyName);
        }
    }

}
