package com.este.jobcatcher.jsas.recyclerviewholders;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jsas.JsaDetailActivity;
import com.este.jobcatcher.models.JobsiteAgency;
import com.este.jobcatcher.utils.Constants;

public class JsaListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private JobsiteAgency mJsaListItemSelected;
    private String mJsaListItemKey;

    public JsaListItemHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

    }

    public void getJsaListItemSelected(JobsiteAgency selectedItem) {
        mJsaListItemSelected = selectedItem;
    }

    public void getJsaListItemKey(String key) {
        mJsaListItemKey = key;
    }

    @Override
    public void onClick(View v) {
        System.out.println("mJsaListItemSelected: " + mJsaListItemSelected);
        System.out.println("mJsaListItemKey: " + mJsaListItemKey);
        if (mJsaListItemSelected != null) {
            Intent intent = new Intent(itemView.getContext(), JsaDetailActivity.class);
                    /* Get the list ID using the adapter's get ref method to get the Firebase
                     * ref and then grab the key.
                     */
            intent.putExtra(Constants.INTENT_EXTRA_JSA_ID, mJsaListItemKey);
            intent.putExtra(Constants.INTENT_EXTRA_PINNED, false);
                    /* Starts an active showing the details for the selected list */
            itemView.getContext().startActivity(intent);
        }
    }

    public void setJsaListItemJsaName(String jsaName) {
        TextView textViewJsaName = (TextView) itemView.findViewById(R.id.list_item_jsa_tv_jsa_name);

        if (jsaName.equals("")) {
            textViewJsaName.setText(R.string.list_item_jsa_tv_jsa_name);
            textViewJsaName.setTypeface(null, Typeface.ITALIC);
            textViewJsaName.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
        } else {
            textViewJsaName.setText(jsaName);
        }

    }

    public void setJsaListItemJsaLocation(String jsaLocation) {
        TextView textViewJsaLocation = (TextView) itemView.findViewById(R.id.list_item_jsa_tv_jsa_location);

        if (jsaLocation.equals("")) {
            textViewJsaLocation.setText(R.string.list_item_jsa_tv_jsa_location);
            textViewJsaLocation.setTypeface(null, Typeface.ITALIC);
            textViewJsaLocation.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.text_view_unused));
        } else {
            textViewJsaLocation.setText(jsaLocation);
        }
    }

}
