package com.este.jobcatcher.maps;

import com.google.android.gms.maps.model.LatLng;

/**
 * Map Cluster Item Model
 */
class JobMapClusterItem implements com.google.maps.android.clustering.ClusterItem {
    private LatLng position;
    private String title;
    private String snippet;
    private Boolean pinned;

    JobMapClusterItem(double lat, double lng, String title, String string, Boolean pinned) {
        this.position = new LatLng(lat, lng);
        this.title = title;
        this.snippet = string;
        this.pinned = pinned;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    public String getTitle(){
        return title;
    }

    public String getSnippet(){
        return snippet;
    }

    Boolean getPinned(){
        return pinned;
    }
}
