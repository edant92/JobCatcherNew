package com.este.jobcatcher.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.este.jobcatcher.utils.PlaceArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class JobMapFragment extends Fragment implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        ClusterManager.OnClusterItemInfoWindowClickListener<JobMapClusterItem>,
        OnMapReadyCallback {

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 0;
    private static final String LOG_TAG = JobMapFragment.class.getSimpleName();
    private DatabaseReference mFirebaseReference;
    private DatabaseReference jobApplicationLocationsRef;
    private DatabaseReference jobApplicationLocationsPinnedRef;

    private SharedPreferences mSharedPrefs;
    private String userUID;

    private LatLngBounds BOUNDS_USER;

    private AutoCompleteTextView mAutocompleteTextView;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private LatLng userLocationLatLong;
    private String userLocationDistanceUnit;

    private Dialog locationEditDialog;
    private String userLocationName;
    private Double userLocationLat;
    private Double userLocationLng;
    private Marker userLocationMarker;

    private ClusterManager<JobMapClusterItem> mClusterManager;
    private JobMapClusterItem jobMapClusterItem;

    private GoogleMap mMap;
    private SupportMapFragment mMapDialogFragmentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LatLngBounds.Builder latLongBoundsBuilder;
    private LatLngBounds latLongBounds;
    private int latLongBoundsPadding = 48; // offset from edges of the map in pixels
    private Map<String, JobMapClusterItem> markerHashMap = new HashMap<>();

    private LatLng clusterLatLng;
    private Integer clusterSizeCheck;

    private Boolean hideUserLocation;

    private View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (fragmentView != null) {
            ViewGroup parent = (ViewGroup) fragmentView.getParent();
            if (parent != null)
                parent.removeView(fragmentView);
        }
        try {
            fragmentView = inflater.inflate(R.layout.fragment_jobmap, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }

        mMapDialogFragmentLocation = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.maps_activity_map);

        if (mMapDialogFragmentLocation != null) {
            mMapDialogFragmentLocation.getMapAsync(this);
        }

        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        userUID = mSharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");
        Log.d(LOG_TAG, userUID);

        userLocationLat = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LAT, "0.0"));
        userLocationLng = Double.parseDouble(mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_LNG, "0.0"));
        userLocationName = mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_LOCATION_NAME, "No default location set");
        userLocationLatLong = new LatLng(userLocationLat, userLocationLng);
        Log.d(LOG_TAG, "Current LatLng: " + userLocationLat + ", " + userLocationLng);
        BOUNDS_USER = new LatLngBounds(userLocationLatLong, userLocationLatLong);
        userLocationDistanceUnit = mSharedPrefs.getString(Constants.SHARED_PREF_USER_DEFAULT_DISTANCE_UNIT, getResources().getStringArray(R.array.array_distance_unit)[0]);

        hideUserLocation = mSharedPrefs.getBoolean(Constants.SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION, false);

        mFirebaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);

        jobApplicationLocationsRef = mFirebaseReference.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        jobApplicationLocationsPinnedRef = mFirebaseReference.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);

        return fragmentView;

    }

    public void userLocationMenuItemSetChecked(MenuItem item) {
        item.setChecked(mSharedPrefs.getBoolean(Constants.SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION, false));
        Log.d(LOG_TAG, "User location hidden? " + mSharedPrefs.getBoolean(Constants.SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION, false));
    }

    public void toggleUserLocationVisible() {

        if (mSharedPrefs.getBoolean(Constants.SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION, false)) {
            createUserLocationMarker();
            Toast.makeText(getContext(), getResources().getString(R.string.activity_maps_user_location_marker_show), Toast.LENGTH_SHORT).show();
            hideUserLocation = false;
            mSharedPrefs.edit().putBoolean(Constants.SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION, false).apply();
            setMapZoomBounds();
        } else {
            if (userLocationMarker != null) {
                userLocationMarker.remove();
            }
            Toast.makeText(getContext(), getResources().getString(R.string.activity_maps_user_location_marker_hide), Toast.LENGTH_SHORT).show();
            hideUserLocation = true;
            mSharedPrefs.edit().putBoolean(Constants.SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION, true).apply();
            setMapZoomBounds();
        }
    }

    public void zoomToBounds() {
        Log.d(LOG_TAG, "MarkerHashMap Size: " + String.valueOf(markerHashMap.size()));
        Log.d(LOG_TAG, "User Location Lat: " + userLocationLat);
        Log.d(LOG_TAG, "User Location Lng: " + userLocationLng);

        if (latLongBounds != null) {
            Log.d(LOG_TAG, "LatLngBounds: " + latLongBounds.toString());
            if (markerHashMap.size() == 1) {
                if(hideUserLocation) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLongBounds.getCenter(), 15F));
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLongBounds, latLongBoundsPadding));
                }
            } else if (markerHashMap.size() == 0) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLongBounds.getCenter(), 0F));
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLongBounds, latLongBoundsPadding));
            }

        }
    }

    public void createLocationEditDialog() {

        final AlertDialog.Builder locationAddAlertDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View dialogView = View.inflate(getContext(), R.layout.dialog_location_temporary_edit, null);
        locationAddAlertDialog.setView(dialogView);

        LinearLayout mLinearLayoutUserLocation = (LinearLayout) dialogView.findViewById(R.id.dialog_location_temporary_edit_ll_location_user);

        mLinearLayoutUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.ACCESS_COARSE_LOCATION },
                                    PERMISSION_ACCESS_COARSE_LOCATION);
                        }

                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                            System.out.println("locationLat: " + lastLocation);

                            userLocationLat = lastLocation.getLatitude();
                            userLocationLng = lastLocation.getLongitude();

                            try {

                                Geocoder geo = new Geocoder(getContext(), Locale.getDefault());
                                List<Address> addresses = geo.getFromLocation(userLocationLat, userLocationLng, 1);
                                if (addresses.isEmpty()) {
                                    Toast.makeText(getContext(), "Waiting for location", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    if (addresses.size() > 0) {
                                        if (addresses.get(0).getLocality() != null) {
                                            userLocationName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
                                        } else {
                                            userLocationName = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getCountryName();
                                        }

                                        Log.d(LOG_TAG, addresses.get(0).toString());

                                        mAutocompleteTextView.setText(userLocationName);

                                        if (userLocationName != null) {
                                            setLocationChangeData();
                                        }
                                        AutoCompleteStop();

                                        if (locationEditDialog != null) {
                                            locationEditDialog.dismiss();
                                        }


                                    }
                                }
                            }
                            catch (Exception e) {
                                e.printStackTrace(); // getFromLocation() may sometimes fail
                            }

                        }
            }
        });


        locationAddAlertDialog.setPositiveButton(getResources().getString(R.string.dialog_maps_activity_location_edit_positive_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (userLocationName != null) {
                    setLocationChangeData();
                }
                AutoCompleteStop();
                locationEditDialog.dismiss();
            }
        });
        locationAddAlertDialog.setNegativeButton(getResources().getString(R.string.dialog_maps_activity_location_edit_negative_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AutoCompleteStop();
                locationEditDialog.dismiss();
            }
        });

        locationEditDialog = locationAddAlertDialog.show();
        locationEditDialog.setCancelable(false);
        locationEditDialog.setCanceledOnTouchOutside(false);

        //LOCATION API CODE
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        AutoCompleteStart();
        mAutocompleteTextView = (AutoCompleteTextView) dialogView.findViewById(R.id.dialog_location_temporary_edit_autocomplete_create_account);
        mAutocompleteTextView.setThreshold(3);
        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getContext(), R.layout.list_item_location_autocomplete, BOUNDS_USER, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);

        mAutocompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_RIGHT = 2;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = mAutocompleteTextView.getRight()
                            - mAutocompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        mAutocompleteTextView.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        if (locationEditDialog.getWindow() != null) {
            locationEditDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private final AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            if (item != null) {
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Selected: " + item.description);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            }

        }
    };

    private final ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);

            String str = place.getLatLng().toString();
            String latlongRemoveStart = str.replace("lat/lng:", "");
            String latlongRemoveEnd = latlongRemoveStart.replaceAll("[()]", "");
            String[] parts = latlongRemoveEnd.split(",");
            userLocationLat = Double.parseDouble(parts[0]);
            userLocationLng = Double.parseDouble(parts[1]);

            userLocationName = place.getName().toString();

        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getContext(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onStart() {
        AutoCompleteStart();
        super.onStart();
    }

    @Override
    public void onStop() {
        AutoCompleteStop();
        super.onStop();
    }

    private void AutoCompleteStart() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    private void AutoCompleteStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void setLocationChangeData() {

        BOUNDS_USER = new LatLngBounds(
                new LatLng(userLocationLat, userLocationLng), new LatLng(userLocationLat, userLocationLng));

        //Remove original marker and replace with new temporary one.

        userLocationLatLong = new LatLng(userLocationLat, userLocationLng);

        if (!hideUserLocation) {
            if (userLocationMarker != null) {
                userLocationMarker.remove();
                createUserLocationMarker();
            }
        }

        setMapZoomBounds();

    }

    private void setUpMap() {

        mClusterManager = new ClusterManager<>(getContext(), mMap);

        mClusterManager.setRenderer(new JobMapCustomClusterRenderer<>(getContext(), mMap, mClusterManager));

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);

        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());

        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<JobMapClusterItem>() {
            @Override
            public boolean onClusterItemClick(JobMapClusterItem item) {
                jobMapClusterItem = item;
                return false;
            }
        });

        mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<JobMapClusterItem>() {
            @Override
            public boolean onClusterClick(Cluster<JobMapClusterItem> cluster) {
                clusterLatLng = cluster.getPosition();

                Collection<JobMapClusterItem> items = cluster.getItems();
                System.out.println("MapsActivityItemsCluster: " + items);
                System.out.println("MapsActivityItemsSize: " + cluster.getSize());
                clusterSizeCheck = cluster.getSize();

                for(JobMapClusterItem item : items) {
                    System.out.println("MapsActivityItemTitle: " + item.getTitle());
                    System.out.println("MapsActivityItemsSize: " + clusterSizeCheck);
                }

                String distanceBetweenUs = getDistance(userLocationLatLong, clusterLatLng);
                Toast.makeText(getContext(), String.format(getResources().getString(R.string.activity_maps_distance_cluster), distanceBetweenUs), Toast.LENGTH_LONG).show();

                return true;
            }
        });

        //Sets custom Info Window adapter to Cluster Items.
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new MyCustomAdapterForItems());

    }

    private String getKeyByValue() {
        for (Map.Entry<String, JobMapClusterItem> entry : markerHashMap.entrySet()) {
            if (entry.getValue().equals(jobMapClusterItem)) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public void onClusterItemInfoWindowClick(JobMapClusterItem jobMapClusterItem) {

        Intent goToJobApplicationDetailActivity = new Intent(getContext(), JobApplicationDetailActivity.class);
        goToJobApplicationDetailActivity.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, getKeyByValue());
        goToJobApplicationDetailActivity.putExtra(Constants.INTENT_EXTRA_PINNED, jobMapClusterItem.getPinned());
        startActivity(goToJobApplicationDetailActivity);

    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        @SuppressLint("InflateParams")
        MyCustomAdapterForItems() {
            myContentsView = View.inflate(getContext(), R.layout.map_cluster_item_info_window, null);
        }
        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            TextView mTextViewJobTitle = ((TextView) myContentsView.findViewById(R.id.cluster_item_info_window_tv_job_title));
            TextView mTextViewCompany = ((TextView) myContentsView.findViewById(R.id.cluster_item_info_window_tv_company));
            TextView mTextViewDistance = ((TextView) myContentsView.findViewById(R.id.cluster_item_info_window_tv_distance));


            mTextViewJobTitle.setText(jobMapClusterItem.getTitle());
            mTextViewCompany.setText(jobMapClusterItem.getSnippet());

            String distanceBetweenUs = getDistance(userLocationLatLong, jobMapClusterItem.getPosition());
            mTextViewDistance.setText(String.format(getResources().getString(R.string.activity_maps_distance), distanceBetweenUs));

            return myContentsView;
        }
    }

    private String getDistance(LatLng start_latlong, LatLng end_latlong){
        Location l1=new Location("One");
        l1.setLatitude(start_latlong.latitude);
        l1.setLongitude(start_latlong.longitude);

        Location l2=new Location("Two");
        l2.setLatitude(end_latlong.latitude);
        l2.setLongitude(end_latlong.longitude);

        float distance=l1.distanceTo(l2);
        String dist = String.format(Locale.ENGLISH, "%.0f", distance)+ getResources().getString(R.string.activity_maps_distance_unit_metres);

        if (userLocationDistanceUnit.equals(getResources().getStringArray(R.array.array_distance_unit)[0])) {
            distance = (distance / 1000.0f) * 0.621371f;
            if (distance > 1.609f) {
                dist = String.format(Locale.ENGLISH, "%.0f", distance) + getResources().getString(R.string.activity_maps_distance_unit_miles);
            }
        } else if (userLocationDistanceUnit.equals(getResources().getStringArray(R.array.array_distance_unit)[1])) {
            distance = distance / 1000.0f;
            if(distance>1.0f) {
                dist = String.format(Locale.ENGLISH, "%.0f", distance) + getResources().getString(R.string.activity_maps_distance_unit_kilometres);
            }
        }

        return dist;
    }

    private void createUserLocationMarker() {

        if (!userLocationLat.equals(0.0) && !userLocationLng.equals(0.0)) {
            userLocationMarker = mMap.addMarker(new MarkerOptions()
                    .position(userLocationLatLong)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .title(userLocationName));
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {

        mMap = map;


        Log.d(LOG_TAG, "GoogleMapIsReady");
        Log.d(LOG_TAG, map.toString());

        if (mMapDialogFragmentLocation != null) {

            if (mMapDialogFragmentLocation.getView() != null) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    latLongBoundsPadding = (mMapDialogFragmentLocation.getView().getMeasuredHeight() / 10);

                }else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    latLongBoundsPadding = (mMapDialogFragmentLocation.getView().getMeasuredWidth() / 10);
                }
            }

        }

        mMap.getUiSettings().setMapToolbarEnabled(true);
        Log.d(LOG_TAG, "Map ToolbarEnabled");
        mMap.getUiSettings().setZoomControlsEnabled(true);
        Log.d(LOG_TAG, "Map ZoomEnabled");
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        Log.d(LOG_TAG, "Map Type Normal");

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
            Log.d(LOG_TAG, "Permission given");
        } else {
            mMap.setMyLocationEnabled(true);
            Log.d(LOG_TAG, "MyLocationEnabled");
        }

        Log.d(LOG_TAG, jobApplicationLocationsRef.orderByChild(Constants.FIREBASE_PROPERTY_LOCATION_SET).equalTo(true).toString());

        Log.d(LOG_TAG, jobApplicationLocationsRef.toString());

        jobApplicationLocationsRef.orderByChild(Constants.FIREBASE_PROPERTY_LOCATION_SET).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "Locations: " + dataSnapshot.toString());

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    JobApplication jobApplication = child.getValue(JobApplication.class);

                    Double jobApplicationLocationLat = jobApplication.getLocationLat();
                    Double jobApplicationLocationLong = jobApplication.getLocationLong();
                    String jobApplicationLocationName = jobApplication.getLocationName();
                    String jobApplicationJobTitle = jobApplication.getJobTitle();
                    String jobApplicationCompanyName = jobApplication.getCompanyName();

                    String locationKey = child.getKey();

                    if (!jobApplicationLocationName.equals("")) {

                        JobMapClusterItem jobMapClusterItem = new JobMapClusterItem(jobApplicationLocationLat, jobApplicationLocationLong, jobApplicationJobTitle, jobApplicationCompanyName, false);
                        mClusterManager.addItem(jobMapClusterItem);

                        //If marker doesn't exist in HashMap add it
                        if (markerHashMap.get(locationKey) == null) {
                            markerHashMap.put(locationKey, jobMapClusterItem);
                        }

                        setMapZoomBounds();

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Log.d(LOG_TAG, jobApplicationLocationsPinnedRef.orderByChild(Constants.FIREBASE_PROPERTY_LOCATION_SET).equalTo(true).toString());

        jobApplicationLocationsPinnedRef.orderByChild(Constants.FIREBASE_PROPERTY_LOCATION_SET).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "LocationsPinned: " + dataSnapshot.toString());

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    JobApplication jobApplication = child.getValue(JobApplication.class);

                    Double jobApplicationLocationLat = jobApplication.getLocationLat();
                    Double jobApplicationLocationLong = jobApplication.getLocationLong();
                    String jobApplicationLocationName = jobApplication.getLocationName();
                    String jobApplicationJobTitle = jobApplication.getJobTitle();
                    String jobApplicationCompanyName = jobApplication.getCompanyName();

                    String locationKey = child.getKey();

                    if (!jobApplicationLocationName.equals("")) {

                        JobMapClusterItem jobMapClusterItem = new JobMapClusterItem(jobApplicationLocationLat, jobApplicationLocationLong, jobApplicationJobTitle, jobApplicationCompanyName, true);
                        mClusterManager.addItem(jobMapClusterItem);

                        //If marker exists in HashMap, set Alpha to 1f else add the marker
                        if (markerHashMap.get(locationKey) == null) {
                            markerHashMap.put(locationKey, jobMapClusterItem);
                        }

                        setMapZoomBounds();

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        setUpMap();

        if (!hideUserLocation) {
            if (!userLocationLat.equals(0.0) && !userLocationLng.equals(0.0)) {
                createUserLocationMarker();
                setMapZoomBounds();
            }
        }

    }

    private void setMapZoomBounds() {

        latLongBoundsBuilder = new LatLngBounds.Builder();

        if (markerHashMap != null) {

            for (Map.Entry<String, JobMapClusterItem> entry : markerHashMap.entrySet()) {
                latLongBoundsBuilder.include(entry.getValue().getPosition());
                System.out.println("Location: " + entry.getValue().getPosition());
            }

            if (!hideUserLocation) {
                if (!userLocationLat.equals(0.0) && !userLocationLng.equals(0.0)) {
                    latLongBoundsBuilder.include(userLocationLatLong);
                }
            }

            try {
                latLongBounds = latLongBoundsBuilder.build();
            } catch (IllegalStateException e) {
                //Default to zooming to user location (if no points on map and even if user location marker 'hidden'
                latLongBoundsBuilder.include(userLocationLatLong);
                latLongBounds = latLongBoundsBuilder.build();
            }

            zoomToBounds();

        }

    }

}
