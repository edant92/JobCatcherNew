package com.este.jobcatcher.models;

/**
 * Defines the data structure for both Active and Archived JobApplication objects.
 */

@SuppressWarnings("JavaDoc")
public class ApplicationStage {
    private String applicationStage;
    private String applicationStageStatus;
    private String applicationStageNotes;
    private Long applicationStageDateStarted;
    private Long applicationStageDateCompleted;
    private Long applicationStageDateEdited;
    private Boolean applicationStageCompleted;

    /**
     * Required public constructor
     */
    public ApplicationStage() {
    }

    /**
     * @param  applicationStage
     * @param  applicationStageStatus
     * @param  applicationStageNotes
     * @param  applicationStageDateStarted
     * @param  applicationStageDateCompleted;
     * @param  applicationStageDateEdited
     * @param  applicationStageCompleted
     */
    public ApplicationStage(String applicationStage, String applicationStageStatus, String applicationStageNotes, Long applicationStageDateStarted, Long applicationStageDateCompleted, Long applicationStageDateEdited, Boolean applicationStageCompleted) {

        this.applicationStage = applicationStage;
        this.applicationStageStatus = applicationStageStatus;
        this.applicationStageNotes = applicationStageNotes;
        this.applicationStageDateStarted = applicationStageDateStarted;
        this.applicationStageDateCompleted = applicationStageDateCompleted;
        this.applicationStageDateEdited = applicationStageDateEdited;
        this.applicationStageCompleted = applicationStageCompleted;

    }

    public String getApplicationStage() {
        return applicationStage;
    }

    public String getApplicationStageStatus() {
        return applicationStageStatus;
    }

    public String getApplicationStageNotes() {
        return applicationStageNotes;
    }

    public Long getApplicationStageDateStarted() {
        return applicationStageDateStarted;
    }

    public Long getApplicationStageDateCompleted() {
        return applicationStageDateCompleted;
    }

    public Long getApplicationStageDateEdited() {
        return applicationStageDateEdited;
    }

    public Boolean getApplicationStageCompleted() {return applicationStageCompleted; }

}

