package com.este.jobcatcher.models;

import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Defines the data structure for both Active and Archived ShoppingList objects.
 */

@SuppressWarnings("JavaDoc")
public class EmailTemplates {

    private String emailTemplateName;
    private String emailTemplateSubject;
    private String emailTemplateSalutation;
    private String emailTemplateContent;
    private String emailTemplateClosing;
    private String emailTemplateFrom;
    private HashMap<String, Object> emailTemplateDateEdited;
    private HashMap<String, Object> emailTemplateDateCreated;

    /**
     * Required public constructor
     */
    public EmailTemplates() {
    }

    /**
     * Use this constructor to create new Job Applications.
     * Takes all Job Application details and sets the last
     * changed time to what is stored in ServerValue.TIMESTAMP
     *
     * @param emailTemplateName
     * @param emailTemplateSubject
     * @param emailTemplateSalutation
     * @param emailTemplateContent
     * @param emailTemplateClosing
     * @param emailTemplateFrom

     */
    public EmailTemplates(String emailTemplateName, String emailTemplateSubject, String emailTemplateSalutation, String emailTemplateContent, String emailTemplateClosing, String emailTemplateFrom) {
        this.emailTemplateName = emailTemplateName;
        this.emailTemplateSubject = emailTemplateSubject;
        this.emailTemplateSalutation = emailTemplateSalutation;
        this.emailTemplateContent = emailTemplateContent;
        this.emailTemplateClosing = emailTemplateClosing;
        this.emailTemplateFrom = emailTemplateFrom;

        HashMap<String, Object> emailTemplateDateCreatedObject = new HashMap<>();
        emailTemplateDateCreatedObject.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);
        this.emailTemplateDateCreated = emailTemplateDateCreatedObject;

        HashMap<String, Object> emailTemplateDateEditedObject = new HashMap<>();
        emailTemplateDateEditedObject.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);
        this.emailTemplateDateEdited = emailTemplateDateEditedObject;


    }

    public String getEmailTemplateName() {
        return emailTemplateName;
    }

    public String getEmailTemplateSubject() {
        return emailTemplateSubject;
    }

    public String getEmailTemplateSalutation() {
        return emailTemplateSalutation;
    }

    public String getEmailTemplateContent() {
        return emailTemplateContent;
    }

    public String getEmailTemplateClosing() {
        return emailTemplateClosing;
    }

    public String getEmailTemplateFrom() {
        return emailTemplateFrom;
    }


    @Exclude
    public long getEmailTemplateDateEditedLong() {

        return (long) emailTemplateDateEdited.get(Constants.FIREBASE_PROPERTY_TIMESTAMP);
    }

    @Exclude
    public long getEmailTemplateDateCreatedLong() {
        return (long) emailTemplateDateCreated.get(Constants.FIREBASE_PROPERTY_TIMESTAMP);
    }

}