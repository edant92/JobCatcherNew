package com.este.jobcatcher.models;

/**
 * Job Application Model Class
 */
public class JobLocation {
    private String locationName;
    private Double locationLat;
    private Double locationLong;
    private Boolean locationRemote;

    /**
     * Use this constructor to create new JobLocations
     * Takes all Job Application details and sets the last
     * changed time to what is stored in ServerValue.TIMESTAMP
     *
     * @param   locationName - Location Name
     * @param   locationLat - Location Latitude
     * @param   locationLong - Location Longitude
     * @param   locationRemote - Whether location is remote or not
     */
    public JobLocation(String locationName, Double locationLat, Double locationLong, Boolean locationRemote) {

        this.locationName = locationName;
        this.locationLat = locationLat;
        this.locationLong = locationLong;
        this.locationRemote = locationRemote;

    }

    public String getName() {
        return locationName;
    }

    public Double getLatitude() {
        return locationLat;
    }

    public Double getLongitude() {
        return locationLong;
    }

    public Boolean getRemote() {
        return locationRemote;
    }

}
