package com.este.jobcatcher.models;

/**
 * Defines the data structure for both Active and Archived JobApplication objects.
 */

@SuppressWarnings("JavaDoc")
public class JobParserRecord {

    private String url;
    private String userLocation;
    private Long timestamp;

    /**
     * Required public constructor
     */
    public JobParserRecord() {
    }

    public String getUrl() {
        return url;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * @param  url
     * @param  userLocation
     * @param  timestamp
     */
    public JobParserRecord(String url, String userLocation, Long timestamp) {
        this.url = url;
        this.userLocation = userLocation;
        this.timestamp = timestamp;
    }

}

