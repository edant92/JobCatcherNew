package com.este.jobcatcher.models;

import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Defines the data structure for both Active and Archived ShoppingList objects.
 */

@SuppressWarnings("JavaDoc")
public class JobsiteAgency {
    private String jsaName;
    private String jsaContactTitle;
    private String jsaContactFirstName;
    private String jsaContactSurname;
    private String jsaContactPosition;
    private String jsaContactEmail;
    private String jsaContactPhone;
    private String jsaContactWebsite;
    private String jsaNotes;
    private Boolean jsaLocationRemote;
    private String jsaLocationName;
    private Double jsaLocationLat;
    private Double jsaLocationLong;
    private HashMap<String, Object> timestampLastChanged;
    private HashMap<String, Object> timestampDateCreated;

    /**
     * Required public constructor
     */

    public JobsiteAgency() {
    }

    /**
     * Use this constructor to create new Job Applications.
     * Takes all Job Application details and sets the last
     * changed time to what is stored in ServerValue.TIMESTAMP
     *
     * @param   jsaName
     * @param   jsaContactTitle
     * @param   jsaContactFirstName
     * @param   jsaContactSurname
     * @param   jsaContactPosition
     * @param   jsaContactEmail
     * @param   jsaContactPhone
     * @param   jsaContactWebsite
     * @param   jsaNotes
     * @param   jsaLocationRemote;
     * @param   jsaLocationName;
     * @param   jsaLocationLat;
     * @param   jsaLocationLong;
     */
    public JobsiteAgency(String jsaName, String jsaContactTitle, String jsaContactFirstName, String jsaContactSurname, String jsaContactPosition, String jsaContactEmail, String jsaContactPhone, String jsaContactWebsite, String jsaNotes, Boolean jsaLocationRemote, String jsaLocationName, Double jsaLocationLat, Double jsaLocationLong) {

        this.jsaName = jsaName;
        this.jsaContactTitle = jsaContactTitle;
        this.jsaContactFirstName = jsaContactFirstName;
        this.jsaContactSurname = jsaContactSurname;
        this.jsaContactPosition = jsaContactPosition;
        this.jsaContactEmail = jsaContactEmail;
        this.jsaContactPhone = jsaContactPhone;
        this.jsaContactWebsite = jsaContactWebsite;
        this.jsaNotes = jsaNotes;
        this.jsaLocationRemote = jsaLocationRemote;
        this.jsaLocationName = jsaLocationName;
        this.jsaLocationLat = jsaLocationLat;
        this.jsaLocationLong = jsaLocationLong;

        HashMap<String, Object> timestampNowObject = new HashMap<>();
        timestampNowObject.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);
        this.timestampLastChanged = timestampNowObject;

        HashMap<String, Object> timestampCreatedObject = new HashMap<>();
        timestampCreatedObject.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);
        this.timestampDateCreated = timestampCreatedObject;

    }

    public String getJsaName() {
        return jsaName;
    }

    public String getJsaContactTitle() {
        return jsaContactTitle;
    }

    public String getJsaContactFirstName() {
        return jsaContactFirstName;
    }

    public String getJsaContactSurname() {
        return jsaContactSurname;
    }

    public String getJsaContactPosition() {
        return jsaContactPosition;
    }

    public String getJsaContactEmail() {
        return jsaContactEmail;
    }

    public String getJsaContactPhone() {
        return jsaContactPhone;
    }

    public String getJsaContactWebsite() {
        return jsaContactWebsite;
    }

    public String getJsaNotes() {
        return jsaNotes;
    }

    public Boolean getJsaLocationRemote() {
        return jsaLocationRemote;
    }

    public String getJsaLocationName() {
        return jsaLocationName;
    }

    public Double getJsaLocationLat() {
        return jsaLocationLat;
    }

    public Double getJsaLocationLong() {
        return jsaLocationLong;
    }

    public HashMap<String, Object> getTimestampLastChanged() {
        return timestampLastChanged;
    }

    public HashMap<String, Object> getTimestampDateCreated() {
        return timestampDateCreated;
    }

    @Exclude
    public long getTimestampLastChangedLong() {

        return (long) timestampLastChanged.get(Constants.FIREBASE_PROPERTY_TIMESTAMP);
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) timestampDateCreated.get(Constants.FIREBASE_PROPERTY_TIMESTAMP);
    }

}