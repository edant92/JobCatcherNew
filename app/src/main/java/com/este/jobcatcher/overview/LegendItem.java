package com.este.jobcatcher.overview;

/**
 * Map Cluster Item Model
 */
public class LegendItem {
    private Integer legendColourCode;
    private String legendLabel;

    LegendItem(Integer legendColourCode, String legendLabel) {
        this.legendColourCode = legendColourCode;
        this.legendLabel = legendLabel;
    }

    Integer getLegendColourCode(){
        return legendColourCode;
    }

    String getLegendLabel(){
        return legendLabel;
    }

}
