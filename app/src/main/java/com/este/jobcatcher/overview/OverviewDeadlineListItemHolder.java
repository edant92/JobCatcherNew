package com.este.jobcatcher.overview;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.JobApplicationDetailActivity;
import com.este.jobcatcher.utils.Constants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class OverviewDeadlineListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private String mainListItemKey;
    private final String userUID;
    private final DatabaseReference mFirebaseRef;
    private Boolean isPinned;

    private final SharedPreferences sharedPrefs;
    private final Resources resources;
    private final Context context;

    private final TextView mTextViewJobTitle;
    private final TextView mTextViewCompanyName;
    private final TextView mTextViewReminderIndicator;

    private final SimpleDateFormat sdfd;
    private final SimpleDateFormat sdfm;

    public OverviewDeadlineListItemHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        userUID = sharedPrefs.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);

        mTextViewJobTitle = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_job_title);
        mTextViewCompanyName = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_company_name);
        mTextViewReminderIndicator = (TextView) itemView.findViewById(R.id.list_item_job_application_tv_reminder_indicator);

        resources = itemView.getResources();

        String dateFormatDate = "dd";
        sdfd = new SimpleDateFormat(dateFormatDate, Locale.US);
        String dateFormatMonth = "MMM";
        sdfm = new SimpleDateFormat(dateFormatMonth, Locale.US);


        itemView.setOnClickListener(this);

    }

    void getMainListItemKey(String key) {
        mainListItemKey = key;
    }

    void getPinned(Boolean pinned) {
        isPinned = pinned;
    }

    public void setJobTitle(String jobTitle) {

        if (jobTitle.equals("")) {
            mTextViewJobTitle.setText(R.string.list_item_job_application_tv_job_title);
            mTextViewJobTitle.setTypeface(null, Typeface.ITALIC);
            mTextViewJobTitle.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
        } else {
            mTextViewJobTitle.setText(jobTitle);
        }

    }

    public void setCompanyName(String companyName) {
        if (companyName.equals("")) {
            mTextViewCompanyName.setText(R.string.list_item_job_application_tv_company_name);
            mTextViewCompanyName.setTypeface(null, Typeface.ITALIC);
            mTextViewCompanyName.setTextColor(ContextCompat.getColor(context, R.color.text_view_unused));
        } else {
            mTextViewCompanyName.setText(companyName);
        }
    }

    void setDeadlineDateTime(Long deadlineDate) {
        mTextViewReminderIndicator.setText(sdfd.format(deadlineDate) + "\n" + sdfm.format(deadlineDate));
    }

    @Override
    public void onClick(View v) {
        if (mainListItemKey != null) {
            Intent intent = new Intent(context, JobApplicationDetailActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_JOB_APPLICATION_ID, mainListItemKey);
            intent.putExtra(Constants.INTENT_EXTRA_PINNED, isPinned);
            context.startActivity(intent);
        }
    }

}
