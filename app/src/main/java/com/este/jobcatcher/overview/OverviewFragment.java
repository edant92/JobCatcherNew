package com.este.jobcatcher.overview;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.este.jobcatcher.R;
import com.este.jobcatcher.jobapplications.MainActivity;
import com.este.jobcatcher.jsas.recyclerviewholders.BasicJobApplicationListItemHolder;
import com.este.jobcatcher.models.JobApplication;
import com.este.jobcatcher.utils.Constants;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Fragment to display deadlines and 'Follow Up's due today
 */
public class OverviewFragment extends Fragment implements OnChartValueSelectedListener {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private int deadlinesCount;
    private int deadlinesMissedCount;
    private int remindersCount;

    private DatabaseReference mMainListRef;
    private DatabaseReference mMainListPriorityRef;

    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecyclerViewAdapterOverviewDeadline;
    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecyclerViewAdapterOverviewDeadlinePinned;
    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecyclerViewAdapterOverviewReminder;
    private FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder> mRecyclerViewAdapterOverviewReminderPinned;

    //Overview
    private TextView mTextViewTotalActiveJobApplications;
    private TextView mTextViewTotalCompletedJobApplications;
    private TextView mTextViewTotalInactiveJobApplications;
    private TextView mTextViewMissedDeadline;

    private LinearLayout mListViewLegend;

    private Long mTotalActiveJobApplications;
    private Long completedApplicationsCount;
    private Long missedDeadlineCount;

    private PieChart mChart;
    private final ArrayList<PieEntry> entries = new ArrayList<>();

    private long missedDeadlinesDay;

    private Dialog mDialogDeadlinesMissed;
    private FirebaseRecyclerAdapter<JobApplication, OverviewDeadlineListItemHolder> mRecyclerViewAdapterDeadlinesMissed;
    private FirebaseRecyclerAdapter<JobApplication, OverviewDeadlineListItemHolder> mRecyclerViewAdapterDeadlinesMissedPinned;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.fragment_overview, container, false);

        SharedPreferences mSharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String userUID = mSharedPref.getString(Constants.SHARED_PREF_KEY_USER_UID, "");

        //Create Firebase references
        DatabaseReference mFirebaseRef = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS).child(userUID);
        mMainListRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST);
        mMainListPriorityRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY);
        final DatabaseReference mMainListArchiveRef = mFirebaseRef.child(Constants.FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE);

        final TextView mTextViewDeadlinesEmpty = (TextView) convertView.findViewById(R.id.fragment_today_tv_deadlines_empty);
        final ProgressBar mProgressBarDeadlines = (ProgressBar) convertView.findViewById(R.id.fragment_today_pb_deadlines);
        final RecyclerView mRecyclerViewDeadlines = (RecyclerView) convertView.findViewById(R.id.fragment_today_rv_deadlines);
        final RecyclerView mRecyclerViewDeadlinesPinned = (RecyclerView) convertView.findViewById(R.id.fragment_today_rv_deadlines_pinned);
        final TextView mTextViewRemindersEmpty = (TextView) convertView.findViewById(R.id.fragment_today_tv_reminders_empty);
        final ProgressBar mProgressBarReminders = (ProgressBar) convertView.findViewById(R.id.fragment_today_pb_reminders);
        final RecyclerView mRecyclerViewReminders = (RecyclerView) convertView.findViewById(R.id.fragment_today_rv_reminders);
        final RecyclerView mRecyclerViewRemindersPinned = (RecyclerView) convertView.findViewById(R.id.fragment_today_rv_reminders_pinned);

        final long startOfDay = startOfDay();
        final long endOfDay = endOfDay();
        missedDeadlinesDay = endOfDay - 86400000; //endOfDay - 1 day in milliseconds so only does deadlines from previous day.

        final Query queryDeadlines = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_DEADLINE_DATE).startAt(startOfDay).endAt(endOfDay);

        queryDeadlines.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "deadlines: " + dataSnapshot.toString());
                deadlinesCount += dataSnapshot.getChildrenCount();

                if (deadlinesCount == 0) {
                    mTextViewDeadlinesEmpty.setVisibility(View.VISIBLE);
                } else {
                    mTextViewDeadlinesEmpty.setVisibility(View.GONE);
                }

                if(dataSnapshot.getChildrenCount() == 0) {
                    mTextViewDeadlinesEmpty.setVisibility(View.VISIBLE);
                    mProgressBarDeadlines.setVisibility(View.GONE);
                } else {
                    mTextViewDeadlinesEmpty.setVisibility(View.GONE);
                    mProgressBarDeadlines.setVisibility(View.GONE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getActivity());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewDeadlines.setHasFixedSize(false);
                    mRecyclerViewDeadlines.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterOverviewDeadline = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryDeadlines) {
                        @Override
                        public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterOverviewDeadline.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(false);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());

                        }
                    };

                    mRecyclerViewDeadlines.setAdapter(mRecyclerViewAdapterOverviewDeadline);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final Query queryDeadlineTwo = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_DEADLINE_DATE).startAt(startOfDay).endAt(endOfDay);

        queryDeadlineTwo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "queryDeadlineTwo: " + dataSnapshot.toString());
                deadlinesCount += dataSnapshot.getChildrenCount();

                if (deadlinesCount == 0) {
                    mTextViewDeadlinesEmpty.setVisibility(View.VISIBLE);
                } else {
                    mTextViewDeadlinesEmpty.setVisibility(View.GONE);
                }

                if(dataSnapshot.getChildrenCount() == 0) {
                    mProgressBarDeadlines.setVisibility(View.GONE);
                } else {
                    mTextViewDeadlinesEmpty.setVisibility(View.GONE);
                    mProgressBarDeadlines.setVisibility(View.GONE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getActivity());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewDeadlinesPinned.setHasFixedSize(false);
                    mRecyclerViewDeadlinesPinned.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterOverviewDeadlinePinned = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryDeadlineTwo) {
                        @Override
                        public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterOverviewDeadlinePinned.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(true);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());

                        }
                    };

                    mRecyclerViewDeadlinesPinned.setAdapter(mRecyclerViewAdapterOverviewDeadlinePinned);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final Query queryReminder = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE).startAt(startOfDay).endAt(endOfDay);

        queryReminder.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "queryReminder: " + dataSnapshot.toString());
                remindersCount += dataSnapshot.getChildrenCount();

                if (remindersCount == 0) {
                    mTextViewRemindersEmpty.setVisibility(View.VISIBLE);
                } else {
                    mTextViewRemindersEmpty.setVisibility(View.GONE);
                }

                if(dataSnapshot.getChildrenCount() == 0) {
                    mProgressBarReminders.setVisibility(View.GONE);
                } else {
                    mTextViewRemindersEmpty.setVisibility(View.GONE);
                    mProgressBarReminders.setVisibility(View.GONE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getActivity());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewReminders.setHasFixedSize(false);
                    mRecyclerViewReminders.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterOverviewReminder = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryReminder) {
                        @Override
                        public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterOverviewReminder.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(false);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());

                        }
                    };

                    mRecyclerViewReminders.setAdapter(mRecyclerViewAdapterOverviewReminder);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final Query queryRemindersPinned = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_REMINDER_TIME_DATE).startAt(startOfDay).endAt(endOfDay);

        queryRemindersPinned.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d(LOG_TAG, "queryRemindersPinned: " + dataSnapshot.toString());
                remindersCount += dataSnapshot.getChildrenCount();

                if (remindersCount == 0) {
                    mTextViewRemindersEmpty.setVisibility(View.VISIBLE);
                } else {
                    mTextViewRemindersEmpty.setVisibility(View.GONE);
                }

                if(dataSnapshot.getChildrenCount() == 0) {
                    mProgressBarReminders.setVisibility(View.GONE);
                } else {
                    mTextViewRemindersEmpty.setVisibility(View.GONE);
                    mProgressBarReminders.setVisibility(View.GONE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getActivity());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewRemindersPinned.setHasFixedSize(false);
                    mRecyclerViewRemindersPinned.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterOverviewReminderPinned = new FirebaseRecyclerAdapter<JobApplication, BasicJobApplicationListItemHolder>(JobApplication.class, R.layout.list_item_basic_job_application, BasicJobApplicationListItemHolder.class, queryRemindersPinned) {
                        @Override
                        public void populateViewHolder(BasicJobApplicationListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterOverviewReminderPinned.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(true);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());

                        }
                    };

                    mRecyclerViewRemindersPinned.setAdapter(mRecyclerViewAdapterOverviewReminderPinned);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mTextViewTotalActiveJobApplications = (TextView) convertView.findViewById(R.id.activity_overview_tv_total_job_applications_label);
        mTextViewTotalCompletedJobApplications = (TextView) convertView.findViewById(R.id.activity_overview_tv_total_completed_job_applications);
        mTextViewTotalInactiveJobApplications = (TextView) convertView.findViewById(R.id.activity_overview_tv_total_inactive_job_applications);
        mTextViewMissedDeadline = (TextView) convertView.findViewById(R.id.activity_overview_tv_missed_deadline);
        mTotalActiveJobApplications = 0L;
        completedApplicationsCount = 0L;
        missedDeadlineCount = 0L;

        mListViewLegend = (LinearLayout) convertView.findViewById(R.id.activity_overview_lv_legend);

        mTextViewMissedDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createMissedDeadlinesListDialog();
            }
        });

        mMainListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mTotalActiveJobApplications = mTotalActiveJobApplications + dataSnapshot.getChildrenCount();

                if (isAdded()) {
                    mTextViewTotalActiveJobApplications.setText(getResources().getQuantityString(R.plurals.activity_overview_tv_missed_deadline, Integer.parseInt(mTotalActiveJobApplications.toString()), Integer.parseInt(mTotalActiveJobApplications.toString())));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mMainListPriorityRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mTotalActiveJobApplications = mTotalActiveJobApplications + dataSnapshot.getChildrenCount();
                if (isAdded()) {
                    mTextViewTotalActiveJobApplications.setText(getResources().getQuantityString(R.plurals.activity_overview_tv_total_job_applications_label, Integer.parseInt(mTotalActiveJobApplications.toString()), Integer.parseInt(mTotalActiveJobApplications.toString())));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Query queryComplete = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_COMPLETE);
        queryComplete.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                completedApplicationsCount = dataSnapshot.getChildrenCount();

                Query queryComplete = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_COMPLETE);
                queryComplete.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (isAdded()) {
                            completedApplicationsCount += dataSnapshot.getChildrenCount();
                            //Update Total Active Job Applications
                            mTotalActiveJobApplications = mTotalActiveJobApplications - completedApplicationsCount;
                            mTextViewTotalActiveJobApplications.setText(String.format(getResources().getString(R.string.activity_overview_tv_total_job_applications_label), mTotalActiveJobApplications.toString()));
                            ///Update completed Job Applications
                            mTextViewTotalCompletedJobApplications.setText(String.format(getResources().getString(R.string.activity_overview_tv_total_job_applications_completed), String.valueOf(completedApplicationsCount)));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mMainListArchiveRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (isAdded()) {
                    mTextViewTotalInactiveJobApplications.setText(String.format(getResources().getString(R.string.activity_overview_tv_total_job_applications_archived), String.valueOf(dataSnapshot.getChildrenCount())));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mChart = (PieChart) convertView.findViewById(R.id.test_pie_chart);
        getApplicationStages();
        mChart.setUsePercentValues(false);
        Description desc = new Description();
        desc.setText("");
        mChart.setDescription(desc);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setNoDataText("");
        mChart.setCenterText("");
        mChart.setCenterTextRadiusPercent(100f);
        mChart.setCenterTextSize(16f);

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);
        mChart.setHoleRadius(70f);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);
        mChart.setTransparentCircleRadius(0f);

        mChart.setDrawCenterText(true);

        // disable rotation of the chart by touch
        mChart.setRotationEnabled(false);

        mChart.setHighlightPerTapEnabled(true);

        mChart.setOnChartValueSelectedListener(this);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        deadlineDates();

        return convertView;

    }

    private long startOfDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0); //set hours to zero
        cal.set(Calendar.MINUTE, 0); // set minutes to zero
        cal.set(Calendar.SECOND, 0); //set seconds to zero
        cal.set(Calendar.MILLISECOND, 0); //set millisecond to zero
        Log.i("Time", cal.getTime().toString());
        return cal.getTimeInMillis();
    }

    private long endOfDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23); //set hours to 23
        cal.set(Calendar.MINUTE, 59); // set minutes to 59
        cal.set(Calendar.SECOND, 59); //set seconds to 59
        cal.set(Calendar.MILLISECOND, 999); //set milliseconds to 59
        Log.i("Time", cal.getTime().toString());
        return cal.getTimeInMillis();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mRecyclerViewAdapterOverviewDeadline != null) {
            mRecyclerViewAdapterOverviewDeadline.cleanup();
        }
        if (mRecyclerViewAdapterOverviewDeadlinePinned != null) {
            mRecyclerViewAdapterOverviewDeadlinePinned.cleanup();
        }
        if (mRecyclerViewAdapterOverviewReminder != null) {
            mRecyclerViewAdapterOverviewReminder.cleanup();
        }
        if (mRecyclerViewAdapterOverviewReminderPinned != null) {
            mRecyclerViewAdapterOverviewReminderPinned.cleanup();
        }

        if (mRecyclerViewAdapterDeadlinesMissed != null) {
            mRecyclerViewAdapterDeadlinesMissed.cleanup();
        }

        if (mDialogDeadlinesMissed != null) {
            mDialogDeadlinesMissed.dismiss();
        }

    }

    private void setData() {

        if (isAdded()) {
            PieDataSet dataSet = new PieDataSet(entries, "Application Stages");
            //Changes space between pie chart 'slices'
            dataSet.setSliceSpace(3f);
            //Changes amount pie chart 'slices' expand by when touched.
            dataSet.setSelectionShift(5f);

            // add a lot of colors
            ArrayList<Integer> colors = new ArrayList<>();

            for (int c : ColorTemplate.VORDIPLOM_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.JOYFUL_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.COLORFUL_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.LIBERTY_COLORS)
                colors.add(c);

            for (int c : ColorTemplate.PASTEL_COLORS)
                colors.add(c);

            colors.add(ColorTemplate.getHoloBlue());

            dataSet.setColors(colors);

            PieData data = new PieData(dataSet);
            data.setDrawValues(false);
            mChart.setData(data);

            // undo all highlights
            mChart.highlightValues(null);

            mChart.invalidate();

            Legend legend = mChart.getLegend();
            legend.setEnabled(false);

            if (getColors(legend) != null) {
                int colorCodes[] = getColors(legend);
                String labels[] = getLabels(legend);

                ArrayList<LegendItem> legendItems = new ArrayList<>();

                for (int i = 0; i < getColors(legend).length-1; i++) {
                    legendItems.add(new LegendItem(colorCodes[i], labels[i]));
                }

                showLegend(legendItems);

                // entry label styling
                mChart.setDrawEntryLabels(false);

            }
        }

    }

    private int [] getColors(Legend legend) {
        LegendEntry[] legendEntries = legend.getEntries();
        int [] colors = new int[legendEntries.length];
        for (int i = 0; i < legendEntries.length; i++) {
            colors[i] = legendEntries[i].formColor;
        }
        return colors;
    }

    private String [] getLabels(Legend legend) {
        LegendEntry [] legendEntries = legend.getEntries();
        String [] labels = new String[legendEntries.length];
        for (int i = 0; i < legendEntries.length; i++) {
            labels[i] = legendEntries[i].label;
        }
        return labels;
    }

    private void showLegend(ArrayList<LegendItem> colourLabelHash) {

        mListViewLegend.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(getContext());

        for (int i = 0; i < colourLabelHash.size(); i++) {

            LegendItem legendItem = colourLabelHash.get(i);

            View view  = inflater.inflate(R.layout.map_legend_adapter_item, mListViewLegend, false);
            // set item content in view
            ((TextView) view.findViewById(R.id.map_legend_adapter_tv_legend)).setText(legendItem.getLegendLabel());
            (view.findViewById(R.id.map_legend_adapter_ll_legend)).setBackgroundColor(legendItem.getLegendColourCode());
            ((TextView) view.findViewById(R.id.map_legend_adapter_tv_legend_value)).setText(String.format(Locale.ENGLISH, "%.0f", entries.get(i).getValue()));

            view.setId(i+1);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), String.format("%s", view.getId()), Toast.LENGTH_SHORT).show();
                }
            });
            mListViewLegend.addView(view);

        }

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
        System.out.println("pieSelected" + true);
    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
        System.out.println("pieSelected" + false);
    }

    private void getApplicationStages() {
        Query queryNotYetApplied = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED);
        queryNotYetApplied.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Long mainListChildCount = dataSnapshot.getChildrenCount();

                Query queryNotYetApplied = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED);
                queryNotYetApplied.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if mainListChildCount + dataSnapshot.getChildrenCount() == 0 then stage will not be added to pie chart
                        if (mainListChildCount + dataSnapshot.getChildrenCount() != 0) {
                            entries.add(new PieEntry((float) mainListChildCount + dataSnapshot.getChildrenCount(), Constants.KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED));
                            System.out.println("getChildrenCount: " + dataSnapshot.getChildrenCount());
                            setData();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Query queryCVApplicationForm = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM);
        queryCVApplicationForm.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final Long mainListChildCount = dataSnapshot.getChildrenCount();

                Query queryCVApplicationForm = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM);
                queryCVApplicationForm.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if mainListChildCount + dataSnapshot.getChildrenCount() == 0 then stage will not be added to pie chart
                        if (mainListChildCount + dataSnapshot.getChildrenCount() != 0) {
                            entries.add(new PieEntry((float) mainListChildCount + dataSnapshot.getChildrenCount(), Constants.KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM));
                            System.out.println("getChildrenCount1: " + dataSnapshot.getChildrenCount());
                            setData();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        Query queryPreInterviewTests = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS);
        queryPreInterviewTests.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final Long mainListChildCount = dataSnapshot.getChildrenCount();

                Query queryPreInterviewTests = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS);
                queryPreInterviewTests.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if mainListChildCount + dataSnapshot.getChildrenCount() == 0 then stage will not be added to pie chart
                        if (mainListChildCount + dataSnapshot.getChildrenCount() != 0) {
                            entries.add(new PieEntry((float) mainListChildCount + dataSnapshot.getChildrenCount(), Constants.KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS));
                            System.out.println("getChildrenCount2: " + dataSnapshot.getChildrenCount());
                            setData();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Query queryInterview = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW);
        queryInterview.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Long mainListChildCount = dataSnapshot.getChildrenCount();

                Query queryInterview = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE).equalTo(Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW);
                queryInterview.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if mainListChildCount + dataSnapshot.getChildrenCount() == 0 then stage will not be added to pie chart
                        if (mainListChildCount + dataSnapshot.getChildrenCount() != 0) {
                            entries.add(new PieEntry((float) mainListChildCount + dataSnapshot.getChildrenCount(), Constants.KEY_FILTER_APPLICATION_STAGE_INTERVIEW));
                            System.out.println("getChildrenCount3: " + dataSnapshot.getChildrenCount());
                            setData();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void deadlineDates() {
        Query queryDeadline = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_DEADLINE_DATE).startAt(1).endAt(missedDeadlinesDay);
        queryDeadline.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                missedDeadlineCount = dataSnapshot.getChildrenCount();

                Query queryInterview = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_DEADLINE_DATE).startAt(1).endAt(missedDeadlinesDay);
                queryInterview.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        missedDeadlineCount += dataSnapshot.getChildrenCount();

                        if (isAdded()) {
                            if (missedDeadlineCount == 0) {
                                mTextViewMissedDeadline.setVisibility(View.GONE);
                                mTextViewMissedDeadline.setText(String.format(getResources().getString(R.string.activity_overview_tv_missed_deadline), String.valueOf(missedDeadlineCount)));
                            } else {
                                mTextViewMissedDeadline.setVisibility(View.VISIBLE);
                                mTextViewMissedDeadline.setText(getResources().getQuantityString(R.plurals.activity_overview_tv_missed_deadline, Integer.parseInt(missedDeadlineCount.toString()), Integer.parseInt(missedDeadlineCount.toString())));
                            }
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void createMissedDeadlinesListDialog() {
        final AlertDialog.Builder listDialog = new AlertDialog.Builder(getContext(), R.style.CustomStyleAlertDialog);
        View convertView = View.inflate(getContext(), R.layout.dialog_recycler_view, null);
        listDialog.setView(convertView);
        listDialog.setTitle(R.string.fragment_overview_dialog_missed_deadlines);
        final ProgressBar mProgressBarAssociatedJobsiteAgency = (ProgressBar) convertView.findViewById(R.id.dialog_recycler_view_pb);
        final TextView mTextViewAssociatedJsa = (TextView) convertView.findViewById(R.id.dialog_recycler_view_tv);
        final RecyclerView mRecyclerViewAssociatedJobsiteAgencyID = (RecyclerView) convertView.findViewById(R.id.dialog_recycler_view_rv);
        final RecyclerView mRecyclerViewAssociatedJobsiteAgencyIDTwo = (RecyclerView) convertView.findViewById(R.id.dialog_recycler_view_rv_two);

        final Query queryDeadline = mMainListRef.orderByChild(Constants.FIREBASE_PROPERTY_DEADLINE_DATE).startAt(1).endAt(missedDeadlinesDay);

        queryDeadline.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                deadlinesMissedCount += dataSnapshot.getChildrenCount();

                if(deadlinesMissedCount == 0) {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.VISIBLE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.GONE);
                } else {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.GONE);
                    mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getContext());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewAssociatedJobsiteAgencyID.setHasFixedSize(false);
                    mRecyclerViewAssociatedJobsiteAgencyID.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterDeadlinesMissed = new FirebaseRecyclerAdapter<JobApplication, OverviewDeadlineListItemHolder>(JobApplication.class, R.layout.list_item_overview_deadline, OverviewDeadlineListItemHolder.class, queryDeadline) {
                        @Override
                        public void populateViewHolder(OverviewDeadlineListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterDeadlinesMissed.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(false);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());
                            jobApplicationPriorityView.setDeadlineDateTime(jobApplicationPriority.getDeadlineDate());

                        }
                    };

                    mRecyclerViewAssociatedJobsiteAgencyID.setAdapter(mRecyclerViewAdapterDeadlinesMissed);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final Query queryDeadlineTwo = mMainListPriorityRef.orderByChild(Constants.FIREBASE_PROPERTY_DEADLINE_DATE).startAt(1).endAt(missedDeadlinesDay);

        queryDeadlineTwo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                deadlinesMissedCount += dataSnapshot.getChildrenCount();
                Log.d(LOG_TAG, "queryDeadlineTwo: " + dataSnapshot.toString());

                if(deadlinesMissedCount == 0) {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.VISIBLE);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setVisibility(View.GONE);
                } else {
                    mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
                    mTextViewAssociatedJsa.setVisibility(View.GONE);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setVisibility(View.VISIBLE);

                    LinearLayoutManager managerPriority = new LinearLayoutManager(getContext());
                    managerPriority.setReverseLayout(false);
                    managerPriority.setStackFromEnd(false);
                    managerPriority.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setHasFixedSize(false);
                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setLayoutManager(managerPriority);

                    mRecyclerViewAdapterDeadlinesMissedPinned = new FirebaseRecyclerAdapter<JobApplication, OverviewDeadlineListItemHolder>(JobApplication.class, R.layout.list_item_overview_deadline, OverviewDeadlineListItemHolder.class, queryDeadlineTwo) {
                        @Override
                        public void populateViewHolder(OverviewDeadlineListItemHolder jobApplicationPriorityView, JobApplication jobApplicationPriority, int position) {

                            jobApplicationPriorityView.getMainListItemKey(mRecyclerViewAdapterDeadlinesMissedPinned.getRef(position).getKey());
                            jobApplicationPriorityView.getPinned(true);
                            jobApplicationPriorityView.setJobTitle(jobApplicationPriority.getJobTitle());
                            jobApplicationPriorityView.setCompanyName(jobApplicationPriority.getCompanyName());
                            jobApplicationPriorityView.setDeadlineDateTime(jobApplicationPriority.getDeadlineDate());

                        }
                    };

                    mRecyclerViewAssociatedJobsiteAgencyIDTwo.setAdapter(mRecyclerViewAdapterDeadlinesMissedPinned);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDialogDeadlinesMissed = listDialog.show();

        //mProgressBarAssociatedJobsiteAgency.setVisibility(View.GONE);
        //mRecyclerViewAssociatedJobsiteAgencyID.setVisibility(View.VISIBLE);
    }

}
