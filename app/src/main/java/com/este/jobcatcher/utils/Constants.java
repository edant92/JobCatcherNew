package com.este.jobcatcher.utils;

/**
 * Constants class store most important strings and paths of the app
 */
public final class Constants {

    //FIREBASE LOCATIONS
    public static final String FIREBASE_LOCATION_ROOT_USERS = "userProfiles";
    public static final String FIREBASE_LOCATION_ROOT_JOB_APPLICATIONS = "userJobApplications";
    public static final String FIREBASE_LOCATION_ROOT_JOBSITE_AGENCIES = "userJobsiteAgencies";
    public static final String FIREBASE_LOCATION_JOB_APPLICATION_LIST = "jobApplicationList";
    public static final String FIREBASE_LOCATION_JOB_APPLICATION_LIST_PRIORITY = "jobApplicationListPriority";
    public static final String FIREBASE_LOCATION_JOB_APPLICATION_LIST_ARCHIVE = "jobApplicationListArchive";
    public static final String FIREBASE_LOCATION_ROOT_EMAIL_TEMPLATES = "emailTemplates";
    public static final String FIREBASE_LOCATION_ROOT_JOB_PARSER_URLS_RECORD = "jobParserUrlsRecord";

    //Shared Preference Keys
    public static final String SHARED_PREF_KEY_USER_UID = "userUID";
    public static final String SHARED_PREF_USER_DEFAULT_WAGE_CURRENCY = "defaultWageCurrency";
    public static final String SHARED_PREF_USER_DEFAULT_WAGE_FREQUENCY = "defaultWageFrequency";
    public static final String SHARED_PREF_USER_DEFAULT_JOB_TYPE = "defaultJobType";
    public static final String SHARED_PREF_USER_DEFAULT_LOCATION_LAT = "defaultLocationLat";
    public static final String SHARED_PREF_USER_DEFAULT_LOCATION_LNG = "defaultLocationLong";
    public static final String SHARED_PREF_USER_DEFAULT_LOCATION_NAME = "defaultLocationName";
    public static final String SHARED_PREF_USER_DEFAULT_DISTANCE_UNIT = "defaultDistanceUnit";
    public static final String SHARED_PREF_ACTIVITY_MAPS_HIDE_USER_LOCATION = "toggleUserLocationVisible";
    public static final String SHARED_PREF_CURRENT_FRAGMENT_TAG = "currentFragmentTagger";
    public static final String SHARED_PREF_PINNED_JOB_APPLICATIONS_COUNT = "pinnedJobApplicationsCount";
    public static final String SHARED_PREF_USER_IS_PREMIUM = "userIsPremium";
    public static final String SHARED_PREF_COMPARE_FILTER_WAGE_FREQUENCY = "compareFilterWageFrequency";
    public static final String SHARED_PREF_APP_UPDATE_VERSION_CODE_LAST_SEEN = "appUpdateVersionCodeLastSeen";


    //Shared Preference Form (Saved items from form)
    public static final String SHARED_PREF_FORM_PROPERTY_JOB_TITLE = "formJobTitle";
    public static final String SHARED_PREF_FORM_PROPERTY_COMPANY_NAME = "formCompanyName";
    public static final String SHARED_PREF_FORM_PROPERTY_DATE_APPLIED = "formDateApplied";
    public static final String SHARED_PREF_FORM_PROPERTY_DATE_COMPLETED = "formDateCompleted";
    public static final String SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE = "formCurrentApplicationStage";
    public static final String SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS = "formCurrentApplicationStageStatus";
    public static final String SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED = "formCurrentApplicationStageDateStarted";
    public static final String SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED = "formCurrentApplicationStageDateCompleted";
    public static final String SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES = "formCurrentApplicationStageNotes";
    public static final String SHARED_PREF_FORM_PROPERTY_CURRENT_APPLICATION_STAGE_COMPLETED = "formCurrentApplicationStageCompleted";
    public static final String SHARED_PREF_FORM_PROPERTY_APPLICATION_STATUS = "formApplicationStatus";
    public static final String SHARED_PREF_FORM_PROPERTY_APPLICATION_STAGE_HISTORY = "formApplicationStageHistory";
    public static final String SHARED_PREF_FORM_PROPERTY_DEADLINE_DATE = "formDeadlineDate";
    public static final String SHARED_PREF_FORM_PROPERTY_REMINDER_SET = "formReminderSet";
    public static final String SHARED_PREF_FORM_PROPERTY_REMINDER_ID = "formReminderId";
    public static final String SHARED_PREF_FORM_PROPERTY_REMINDER_DATE_TIME = "formReminderDateTime";
    public static final String SHARED_PREF_FORM_PROPERTY_WAGE_CURRENCY = "formWageCurrency";
    public static final String SHARED_PREF_FORM_PROPERTY_WAGE_AMOUNT = "formWageAmount";
    public static final String SHARED_PREF_FORM_PROPERTY_WAGE_FREQUENCY = "formWageFrequency";
    public static final String SHARED_PREF_FORM_PROPERTY_LOCATION_REMOTE = "formLocationRemote";
    public static final String SHARED_PREF_FORM_PROPERTY_LOCATION_NAME = "formLocationName";
    public static final String SHARED_PREF_FORM_PROPERTY_LOCATION_LAT = "formLocationLat";
    public static final String SHARED_PREF_FORM_PROPERTY_LOCATION_LONG = "formLocationLong";
    public static final String SHARED_PREF_FORM_PROPERTY_LOCATION_SET = "formLocationSet";
    public static final String SHARED_PREF_FORM_PROPERTY_JOB_REFERENCE = "formJobReference";
    public static final String SHARED_PREF_FORM_PROPERTY_JOB_DESCRIPTION = "formJobDescription";
    public static final String SHARED_PREF_FORM_PROPERTY_JOB_ADVERT_URL = "formJobAdvertURL";
    public static final String SHARED_PREF_FORM_PROPERTY_JOB_TYPE = "formJobType";
    public static final String SHARED_PREF_FORM_PROPERTY_JOB_HOURS = "formJobHours";
    public static final String SHARED_PREF_FORM_PROPERTY_APPLICATION_NOTES = "formApplicationNotes";
    public static final String SHARED_PREF_FORM_PROPERTY_APPLICATION_PRIORITY = "formApplicationPriority";
    public static final String SHARED_PREF_FORM_PROPERTY_ASSOCIATED_JSA_ID = "formAssociatedJobsiteAgencyID";
    public static final String SHARED_PREF_FORM_PROPERTY_CONTACT_TITLE = "formContactTitle";
    public static final String SHARED_PREF_FORM_PROPERTY_CONTACT_FIRST_NAME = "formContactFirstName";
    public static final String SHARED_PREF_FORM_PROPERTY_CONTACT_SURNAME = "formContactSurname";
    public static final String SHARED_PREF_FORM_PROPERTY_CONTACT_POSITION = "formContactPosition";
    public static final String SHARED_PREF_FORM_PROPERTY_CONTACT_EMAIL = "formContactEmail";
    public static final String SHARED_PREF_FORM_PROPERTY_CONTACT_PHONE = "formContactPhone";

    //Constants for Intent Extras
    public static final String INTENT_EXTRA_JOB_APPLICATION_ID = "mJobApplicationId";
    public static final String INTENT_EXTRA_JSA_ID = "mJsaId";
    public static final String INTENT_EXTRA_EMAIL_TEMPLATE_ID = "emailTemplateId";
    public static final String INTENT_EXTRA_PINNED = "jobApplicationPriority";
    public static final String INTENT_EXTRA_EMAIL_TEMPLATE_CHOICE = "emailTemplateChoice";
    public static final String INTENT_EXTRA_FROM_REMINDER = "fromReminder";
    public static final String INTENT_EXTRA_LONG_REMINDER_DATE_TIME = "longReminderDateTime";
    public static final String INTENT_EXTRA_REMINDER_ID = "reminderId";
    public static final String INTENT_EXTRA_REMINDER_DELETE = "deleteReminder";
    public static final String INTENT_EXTRA_FROM_ACTIVITY_JOB_APPLICATION_DETAIL ="fromActivityJobApplicationDetail";
    public static final String INTENT_EXTRA_ARCHIVED = "jobApplicationArchived";
    public static final String INTENT_EXTRA_ASSOCIATED_JSA = "intentAssociatedJsa";
    public static final String INTENT_EXTRA_EMAIL_TEMPLATE = "intentEmailTemplate";
    public static final String INTENT_EXTRA_FROM_FOLLOWUP_EMAIL_ACTIVITY = "fromFollowupEmailActivity";
    public static final String INTENT_EXTRA_FROM_ACTIVITY ="fromActivity";
    public static final String INTENT_EXTRA_JOB_PARSER_RECEIVER_ERROR = "jobParserLimitReached";


    //Job Application Model Keys
    public static final String FIREBASE_PROPERTY_JOB_TITLE = "jobTitle";
    public static final String FIREBASE_PROPERTY_COMPANY_NAME = "companyName";
    public static final String FIREBASE_PROPERTY_DATE_APPLIED = "dateApplied";
    public static final String FIREBASE_PROPERTY_DATE_COMPLETED = "dateCompleted";
    public static final String FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE = "currentApplicationStage";
    public static final String FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_STATUS = "currentApplicationStageStatus";
    public static final String FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_STARTED = "currentApplicationStageDateStarted";
    public static final String FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_DATE_COMPLETED = "currentApplicationStageDateCompleted";
    public static final String FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_NOTES = "currentApplicationStageNotes";
    public static final String FIREBASE_PROPERTY_CURRENT_APPLICATION_STAGE_COMPLETED = "currentApplicationStageCompleted";
    public static final String FIREBASE_PROPERTY_APPLICATION_STATUS = "applicationStatus";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_HISTORY = "applicationStageHistory";
    public static final String FIREBASE_PROPERTY_DEADLINE_DATE = "deadlineDate";
    public static final String FIREBASE_PROPERTY_REMINDER_SET = "reminderSet";
    public static final String FIREBASE_PROPERTY_REMINDER_ID = "reminderId";
    public static final String FIREBASE_PROPERTY_REMINDER_TIME_DATE = "reminderDateTime";
    public static final String FIREBASE_PROPERTY_WAGE_CURRENCY = "wageCurrency";
    public static final String FIREBASE_PROPERTY_WAGE_AMOUNT = "wageAmount";
    public static final String FIREBASE_PROPERTY_WAGE_FREQUENCY = "wageFrequency";
    public static final String FIREBASE_PROPERTY_LOCATION_REMOTE = "locationRemote";
    public static final String FIREBASE_PROPERTY_LOCATION_NAME = "locationName";
    public static final String FIREBASE_PROPERTY_LOCATION_LAT = "locationLat";
    public static final String FIREBASE_PROPERTY_LOCATION_LONG = "locationLong";
    public static final String FIREBASE_PROPERTY_LOCATION_SET = "locationSet";
    public static final String FIREBASE_PROPERTY_JOB_REFERENCE = "jobReference";
    public static final String FIREBASE_PROPERTY_JOB_DESCRIPTION = "jobDescription";
    public static final String FIREBASE_PROPERTY_JOB_ADVERT_URL = "jobAdvertURL";
    public static final String FIREBASE_PROPERTY_JOB_TYPE = "jobType";
    public static final String FIREBASE_PROPERTY_JOB_HOURS = "jobHours";
    public static final String FIREBASE_PROPERTY_APPLICATION_NOTES = "applicationNotes";
    public static final String FIREBASE_PROPERTY_APPLICATION_PRIORITY = "applicationPriority";
    public static final String FIREBASE_PROPERTY_ASSOCIATED_JSA_ID = "associatedJobsiteAgencyID";
    public static final String FIREBASE_PROPERTY_CONTACT_TITLE = "contactTitle";
    public static final String FIREBASE_PROPERTY_CONTACT_FIRST_NAME = "contactFirstName";
    public static final String FIREBASE_PROPERTY_CONTACT_SURNAME = "contactSurname";
    public static final String FIREBASE_PROPERTY_CONTACT_POSITION = "contactPosition";
    public static final String FIREBASE_PROPERTY_CONTACT_EMAIL = "contactEmail";
    public static final String FIREBASE_PROPERTY_CONTACT_PHONE = "contactPhone";
    public static final String FIREBASE_PROPERTY_TIMESTAMP_LAST_CHANGED = "timestampLastChanged";
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "date";

    public static final String FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_SENT = "emailFollowupTimeSent";
    public static final String FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_TIME_CHECK = "emailFollowupTimeCheck";
    public static final String FIREBASE_PROPERTY_EMAIL_FOLLOW_UP_SENT = "emailFollowupSent";



    //Constants for Firebase Application Stage objects properties
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_STAGE_NAME = "stageName";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_CURRENT_STAGE = "currentStage";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_STATUS = "statusComplete";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_NOTES = "notes";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_TIMESTAMP_STARTED = "timestampStarted";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_TIMESTAMP_EDITED = "timestampEdited";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_TIMESTAMP_ENDED = "timestampEnded";
    public static final String FIREBASE_PROPERTY_APPLICATION_STAGE_TIMESTAMP = "timestamp";

    //Constants for Firebase JSA object properties
    public static final String FIREBASE_PROPERTY_JSA_NAME = "jsaName";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_TITLE = "jsaContactTitle";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_FIRST_NAME = "jsaContactFirstName";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_SURNAME = "jsaContactSurname";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_POSITION = "jsaContactPosition";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_EMAIL = "jsaContactEmail";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_PHONE = "jsaContactPhone";
    public static final String FIREBASE_PROPERTY_JSA_CONTACT_WEBSITE = "jsaContactWebsite";
    public static final String FIREBASE_PROPERTY_JSA_LOCATION_REMOTE = "jsaLocationRemote";
    public static final String FIREBASE_PROPERTY_JSA_LOCATION_NAME = "jsaLocationName";
    public static final String FIREBASE_PROPERTY_JSA_LOCATION_LAT = "jsaLocationLat";
    public static final String FIREBASE_PROPERTY_JSA_LOCATION_LONG = "jsaLocationLong";
    public static final String FIREBASE_PROPERTY_JSA_NOTES = "jsaNotes";

    /**
     * Constants for Firebase Email Template object properties
     */
    public static final String FIREBASE_PROPERTY_EMAIL_TEMPLATE_NAME = "emailTemplateName";
    public static final String FIREBASE_PROPERTY_EMAIL_TEMPLATE_SUBJECT = "emailTemplateSubject";
    public static final String FIREBASE_PROPERTY_EMAIL_TEMPLATE_TITLE = "emailTemplateSalutation";
    public static final String FIREBASE_PROPERTY_EMAIL_TEMPLATE_CONTENT = "emailTemplateContent";
    public static final String FIREBASE_PROPERTY_EMAIL_TEMPLATE_CLOSING = "emailTemplateClosing";
    public static final String FIREBASE_PROPERTY_EMAIL_TEMPLATE_USER_NAME = "emailTemplateUserName";

    //Constants for sorting
    public static final String KEY_SORT_ORDER_LISTS = "sortOrderLists";
    public static final String KEY_SORT_ORDER_ASC_DESC = "sortOrderAscDesc";

    public static final String FIREBASE_SORT_ORDER_BY_KEY = "orderByPushKey";
    public static final String FIREBASE_SORT_REMINDER_SET = "reminderSet";
    public static final String FIREBASE_SORT_TIMESTAMP_LAST_CHANGED = "timestampLastChanged/timestamp";

    ///Constants for Filtering
    public static final String KEY_FILTER_BOOLEAN = "filteringByStage";

    public static final String KEY_FILTER_STAGE = "filterStage";
    public static final String KEY_FILTER_APPLICATION_STAGE_ALL = "All";
    public static final String KEY_FILTER_APPLICATION_STAGE_NOT_YET_APPLIED = "Not Yet Applied";
    public static final String KEY_FILTER_APPLICATION_STAGE_CV_APPLICATION_FORM = "CV/Application Form";
    public static final String KEY_FILTER_APPLICATION_STAGE_TESTS_ASSESSMENTS = "Tests/Assessments";
    public static final String KEY_FILTER_APPLICATION_STAGE_INTERVIEW = "Interview";
    public static final String KEY_FILTER_APPLICATION_STAGE_COMPLETE = "Complete";


    public static final String KEY_FILTER_STATUS = "filterStageStatus";
    public static final String KEY_FILTER_APPLICATION_STATUS_ALL = "All";
    public static final String KEY_FILTER_APPLICATION_STATUS_IN_PROGRESS = "In Progress";
    public static final String KEY_FILTER_APPLICATION_STATUS_SUCCESSFUL = "Successful";
    public static final String KEY_FILTER_APPLICATION_STATUS_UNSUCCESSFUL = "Unsuccessful";
    public static final String KEY_FILTER_APPLICATION_STATUS_WITHDRAWN = "Withdrawn";

    /**
     * Constants for Firebase User object properties
     */
    public static final String FIREBASE_PROPERTY_EMAIL = "email";
    public static final String FIREBASE_PROPERTY_USER_HAS_LOGGED_IN_WITH_PASSWORD = "hasLoggedInWithPassword";

    public static final String FIREBASE_PROVIDER_PASSWORD = "password";
    public static final String FIREBASE_PROVIDER_GOOGLE = "google.com";

    public static final String FIREBASE_PROPERTY_USER_FIRST_NAME = "firstName";
    public static final String FIREBASE_PROPERTY_USER_SURNAME = "surname";
    public static final String FIREBASE_PROPERTY_USER_EMAIL = "email";
    public static final String FIREBASE_PROPERTY_USER_LOCATION_LAT = "userLocationLat";
    public static final String FIREBASE_PROPERTY_USER_LOCATION_LONG = "userLocationLong";
    public static final String FIREBASE_PROPERTY_USER_LOCATION_NAME = "userLocationName";
    public static final String FIREBASE_PROPERTY_USER_DISTANCE_UNIT = "userDistanceUnit";
    public static final String FIREBASE_PROPERTY_USER_CURRENCY = "userCurrency";
    public static final String CONNECTION_CHECK_TIMESTAMP_LAST_ONLINE = "timestampLastOnline";
    public static final String CONNECTION_CHECK_TIMESTAMP_FIRST_ONLINE = "timestampFirstOnline";
    public static final String CONNECTION_CHECK_TIMESTAMP_TOTAL_SESSION_TIME = "totalSessionTime";
    public static final String CONNECTION_CHECK_TIMESTAMP_SESSION_TIME_OB = "sessionTimeOb";

    //Fragment Tags
    public static final String FRAGMENT_TAG_OVERVIEW = "fragmentOverview";
    public static final String FRAGMENT_TAG_MAIN = "fragmentMain";
    public static final String FRAGMENT_TAG_NEW_APPLICATION = "fragmentNewApplication";
    public static final String FRAGMENT_TAG_JOBMAP = "fragmentJobMap";
    public static final String FRAGMENT_TAG_COMPARE = "fragmentCompare";


    //Settings Auth Options
    public static final String SETTINGS_AUTH_OPTION_CHANGE_EMAIL = "settingsAuthOptionChangeEmail";
    public static final String SETTINGS_AUTH_OPTION_DELETE_JOB_APPLICATIONS = "settingsAuthOptionDeleteJobApplication";
    public static final String SETTINGS_AUTH_OPTION_DELETE_JOBSITE_AGENCIES = "settingsAuthOptionDeleteJobsiteAgencies";
    public static final String SETTINGS_AUTH_OPTION_DELETE_ACCOUNT = "settingsAuthOptionDeleteAccount";

    //Wage Frequencies
    public static final String HOURLY = "Hourly";
    public static final String DAILY = "Daily";
    public static final String WEEKLY = "Weekly";
    public static final String BIWEEKLY = "Bi-Weekly";
    public static final String MONTHLY = "Monthly";
    public static final String ANNUALLY = "Annually";
    public static final String LUMPSUM = "Lump Sum";
    public static final String OTHER = "Other";

    //Wage Frequencies for filter only
    public static final String ALL = "All";
    public static final String LUMPSUMOTHER = "Lump Sum/Other";

    //Distance Units
    public static final String MILES = "Miles";
    public static final String KILOMETRES = "Kilometres";

}
