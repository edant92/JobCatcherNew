package com.este.jobcatcher.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Jobsites Class - Contains String name values (and List<String>) of all Jobsites that can be used in JobParser.
 */

public final class Jobsites {
    public static final String ADZUNA = "adzuna";
    public static final String AUTHENTIC_JOBS = "authenticjobs";
    public static final String INDEED = "indeed";
    public static final String LINKEDIN = "linkedin";
    public static final String REED = "reed";

    public static final List<String> JOBSITES_LIST = new ArrayList<>(Arrays.asList(
            ADZUNA,
            AUTHENTIC_JOBS,
            INDEED,
            LINKEDIN,
            REED
            ));

}
