package com.este.jobcatcher.utils;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Allows RecyclerView items to 'snap' horizontally.
 */
public class RecyclerViewSnapping extends RecyclerView {

    private final int screenWidth;

    public RecyclerViewSnapping(Context context) {
        super(context);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
    }

    public RecyclerViewSnapping(Context context, AttributeSet attrs) {
        super(context, attrs);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
    }

    public RecyclerViewSnapping(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
    }

    @Override
    public boolean fling(int velocityX, int velocityY) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) getLayoutManager();

        //These four variables identify the views you see on screen.
        int lastVisibleView = linearLayoutManager.findLastVisibleItemPosition();
        int firstVisibleView = linearLayoutManager.findFirstVisibleItemPosition();
        View firstView = linearLayoutManager.findViewByPosition(firstVisibleView);
        View lastView = linearLayoutManager.findViewByPosition(lastVisibleView);

        int newViewSize = firstView.getWidth();
        int newRightMargin = (screenWidth - firstView.getWidth()) / 2 + firstView.getWidth() + 4;
        int newLeftEdge = lastView.getLeft() + 10;
        int newRightEdge = firstView.getRight();
        int newScrollDistanceLeft = newViewSize + newLeftEdge;
        int newScrollDistanceRight = newViewSize + (newRightMargin - newRightEdge);

        if (velocityX > 0) {
            smoothScrollBy(newScrollDistanceLeft, 0);
        } else {
            smoothScrollBy(-newScrollDistanceRight, 0);
        }

        System.out.println("RecyclerViewAdapterSnapping: " + linearLayoutManager.findFirstVisibleItemPosition());

        return true;
    }
}
