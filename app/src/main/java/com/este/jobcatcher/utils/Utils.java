package com.este.jobcatcher.utils;

import android.content.Context;
import android.content.res.Resources;
import android.location.Location;
import android.widget.Spinner;

import com.este.jobcatcher.R;
import com.este.jobcatcher.models.ApplicationStage;
import com.este.jobcatcher.models.JobApplication;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * This Utility class includes any useful methods that can be re-used throughout the application.
 */
public class Utils {

    public void selectSpinnerValue(Spinner spinner, String myString)
    {
        for(int i = 0; i < spinner.getCount(); i++){
            if(spinner.getItemAtPosition(i).toString().equals(myString)){
                spinner.setSelection(i);
                break;
            }
        }
    }

    public float getDistanceBetweenTwoPoints(LatLng latLngStart, LatLng latLngEnd, String distanceUnit){
        Location locationStart = new Location("locationStart");
        locationStart.setLatitude(latLngStart.latitude);
        locationStart.setLongitude(latLngStart.longitude);

        Location locationEnd = new Location("locationEnd");
        locationEnd.setLatitude(latLngEnd.latitude);
        locationEnd.setLongitude(latLngEnd.longitude);

        float distance = locationStart.distanceTo(locationEnd);

        if (distanceUnit.equals(Constants.MILES)) {
            //Distance Unit is Miles
            distance = (distance / 1000.0f) * 0.621371f;
        } else if (distanceUnit.equals(Constants.KILOMETRES)) {
            //Distance Unit is Kilometers
            distance = distance / 1000.0f;
        }

        return distance;

    }

    public String setApplicationStatus(String currentApplicationStage, String currentApplicationStageStatus, Resources resources) {
        if (currentApplicationStage.equals(resources.getStringArray(R.array.array_application_stage)[0])) {
            //If Not Yet Applied set applicationStatus prospective
            return resources.getStringArray(R.array.array_application_status)[0];
        } else if (currentApplicationStage.equals(resources.getStringArray(R.array.array_application_stage)[1])
                || currentApplicationStage.equals(resources.getStringArray(R.array.array_application_stage)[2])
                || currentApplicationStage.equals(resources.getStringArray(R.array.array_application_stage)[3])) {
            //If currentApplicationStage is CV/Application Form Submitted, Tests/Assessments or Interview
            // check If currentApplicationStageStatus is Unsuccessful or Withdrawn then applicationStatus to same otherwise set to In Progress
            if (currentApplicationStageStatus.equals(resources.getStringArray(R.array.array_application_stage_status)[2])) {
                return resources.getStringArray(R.array.array_application_status)[3];
            } else if (currentApplicationStageStatus.equals(resources.getStringArray(R.array.array_application_stage_status)[3])) {
                return resources.getStringArray(R.array.array_application_status)[4];
            } else {
                return resources.getStringArray(R.array.array_application_status)[1];
            }
        } else if (currentApplicationStage.equals(resources.getStringArray(R.array.array_application_stage)[4])) {
            //If currentApplicationStage is Complete set applicationStatus to Successful / Unsuccessful / Withdrawn
            if (currentApplicationStageStatus.equals(resources.getStringArray(R.array.array_application_stage_status)[1])) {
                //if CurrentApplicationStatus is Successful
                return resources.getStringArray(R.array.array_application_status)[2];
            } else if (currentApplicationStageStatus.equals(resources.getStringArray(R.array.array_application_stage_status)[2])) {
                //if CurrentApplicationStatus is Unsuccessful
                return resources.getStringArray(R.array.array_application_status)[3];
            } else if (currentApplicationStageStatus.equals(resources.getStringArray(R.array.array_application_stage_status)[3])) {
                //if CurrentApplicationStatus is Withdrawn
                return resources.getStringArray(R.array.array_application_status)[4];
            }
        }
        return resources.getStringArray(R.array.array_application_status)[0];
    }

    public JobApplication createJobApplicationWithDefaultValues(Context ctx) {

        //Firebase Object Variables
        String jobTitle = "";
        String companyName = "";
        Boolean locationRemote = false;
        String locationName = "";
        Double locationLat = 0.0;
        Double locationLng = 0.0;
        Boolean locationSet = false;
        String currentApplicationStage = ctx.getResources().getStringArray(R.array.array_application_stage)[0]; //Not Yet Applied
        String currentApplicationStageStatus = ctx.getResources().getStringArray(R.array.array_application_stage_status)[0]; //In Progress
        Long currentApplicationStageDateStarted = 0L;
        Long currentApplicationStageDateCompleted = 0L;
        String currentApplicationStageNotes = "";
        HashMap<String, ApplicationStage> applicationStageHistory = new HashMap<>();
        String applicationStatus = ctx.getResources().getStringArray(R.array.array_application_status)[0]; //Prospective
        Long deadlineDate = 0L;
        Boolean reminderSet = false;
        Integer reminderId = 0;
        Long reminderDateTime = 0L;
        String wageCurrency = ctx.getResources().getStringArray(R.array.array_currency)[0]; //GBP
        Double wageAmount = 0.0;
        String wageFrequency = ctx.getResources().getStringArray(R.array.array_wage_frequency)[0]; //Hourly
        String jobType = ctx.getResources().getStringArray(R.array.array_job_type)[0];
        Double jobHours = 0.0;
        String jobReference = "";
        String jobDescription = "";
        String jobAdvertURL = "";
        String applicationNotes = "";
        String contactTitle = "";
        String contactFirstName = "";
        String contactSurname = "";
        String contactPosition = "";
        String contactEmail = "";
        String contactPhone = "";
        Integer applicationPriority = 2;
        String associatedJobsiteAgencyID = "";

        Long applicationDateApplied = 0L;
        Long applicationDateCompleted = 0L;

        HashMap<String, Object> dateCreatedObj = new HashMap<>();
        dateCreatedObj.put(Constants.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

        return new JobApplication(jobTitle, companyName, applicationDateApplied, applicationDateCompleted, deadlineDate, currentApplicationStage, currentApplicationStageStatus, currentApplicationStageDateStarted, currentApplicationStageDateCompleted, currentApplicationStageNotes, false, applicationStageHistory, applicationStatus, reminderSet, reminderId, reminderDateTime, wageCurrency, wageAmount, wageFrequency, locationRemote, locationName, locationLat, locationLng, locationSet, jobReference, jobDescription, jobAdvertURL, jobType, jobHours, applicationNotes, applicationPriority, associatedJobsiteAgencyID, contactTitle, contactFirstName, contactSurname, contactPosition, contactEmail, contactPhone, 0L, 0L, false, dateCreatedObj);
    }

    public void updateTimestampLastChanged(DatabaseReference mJobApplicationRef) {
        HashMap<String, Object> dateLastChangedObj = new HashMap<>();
        dateLastChangedObj.put("date", ServerValue.TIMESTAMP);
        mJobApplicationRef.child(Constants.FIREBASE_PROPERTY_TIMESTAMP_LAST_CHANGED).setValue(dateLastChangedObj);
    }

}
